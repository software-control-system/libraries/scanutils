#ifndef _SCANSERVER_AFTERRUNACTION_H
#define _SCANSERVER_AFTERRUNACTION_H

#include <map>
#include <scansl/ScanConfig.h>
#include <scansl/ScanData.h>
#include <scansl/actors/DataCollector.h>
#include <scansl/actors/ActuatorManager.h>
#include <boost/noncopyable.hpp>
#include <boost/any.hpp>

namespace ScanUtils
{

class SCANSL_DECL AfterRunAction : private boost::noncopyable
{
public:
    AfterRunAction( const ScanConfig& config );

    void configure( DataCollectorPtr data_collector,
                    ActuatorManagerPtr act_manager,
                    ContinuousTimebaseManagerPtr ctb_manager );

    void execute( AfterRunActionDesc action );

private:

    DataCollector::DataInfo& get_sensor_dataset( long sensor_idx );
    DataCollector::DataInfo& get_actuator_dataset( long actuator_idx );

    void first_pos( void );
    void prior_pos( void );
    void maximum( void );
    void minimum( void );
    void max_derivative( void );
    void min_derivative( void );
    void center_of_mass( void );
    //void fit ( std::string fit_type );
    void goto_pos( void );

    void move_actuators_to_scan_pos( int x, int y );
    void move_actuators_to_absolute_pos( int actuator_idx, double actuator_value );

    const ScanConfig*   config_;
    AfterRunActionDesc  action_;

    DataCollectorPtr data_collector_;
    ActuatorManagerPtr  actuator_manager_;
    ContinuousTimebaseManagerPtr  ctb_manager_;
};

typedef boost::shared_ptr<AfterRunAction> AfterRunActionPtr;
}

#endif
