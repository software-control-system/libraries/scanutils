/*!
 * \file
 * \brief    Definition of GalilActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

// JIRA SCAN-51 : Avoid duplication of GalilActuatorPlugin
#include "PowerPMACAxisActuator.h"

#include <yat/plugin/PlugInSymbols.h>
#include <scansl/interfaces/IScanPlugInInfo.h>

namespace ScanUtils
{
class PowerPMACAxisActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PowerPMACAxisActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
		list.push_back("PowerPMACAxis");
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PowerPMACAxisActuator, ScanUtils::PowerPMACAxisActuatorInfo);
