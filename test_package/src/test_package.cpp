#include <iostream>
#include <string>

#include <scansl/Singleton.h>

struct Dummy : public ScanUtils::Singleton<Dummy>
{
    std::string str() const { return "Linked"; }
};

int main()
{
    std::cout << Dummy::instance().str() << '\n';

    return 0;
}
