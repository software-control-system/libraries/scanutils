/*!
* \file
* \brief    Definition of UHRAxisActuator class
* \author   Julien Malik - Synchrotron SOLEIL
*/

#include <scansl/Util.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class UHRAxisActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "UHRAxisActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("UHRAxis");
    }
};

class UHRAxisActuator : public WaitingStateChangeActuator
{
public:
    UHRAxisActuator()
    {
        config_.move_state        = Tango::MOVING;
        // MANTIS 22663
        //config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";
    }


    // ============================================================================
    // UHRAxisActuator::get_speed
    // ============================================================================
    double get_speed( std::string attr_name )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        if (device_class == "UHRAxis" && attr_name == "position")
        {
            Tango::DeviceAttribute velocity_attr = proxy_->read_attribute( "velocity" );
            double velocity;
            velocity_attr >> velocity;
            return velocity;
        }
        else
        {
            return 0;
        }
    }

    // ============================================================================
    // UHRAxisActuator::set_speed
    // ============================================================================
    void set_speed( std::string attr_name, double speed )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        if (device_class == "UHRAxis" && attr_name == "position")
        {
            SCAN_DEBUG << "Setting speed of " << config_.dev_name << " to " << speed << ENDLOG;
            Tango::DeviceAttribute velocity_attr("velocity", speed);
            proxy_->write_attribute( velocity_attr );
        }
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::UHRAxisActuator, ScanUtils::UHRAxisActuatorInfo);
