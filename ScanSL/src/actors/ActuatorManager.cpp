#include <scansl/ScanSL.h>
#include <scansl/Util.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/actors/ActuatorManager.h>
#include <scansl/actors/PolledActuatorAdapter.h>
#include <scansl/ThreadExiter.h>
#include <boost/scoped_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

// Relative precision for actuators positions
const double actuator_comp_precision = 1.0e-9;

IActuator::~IActuator()
{
}

// ============================================================================
// ActuatorManager::ActuatorManager
// ============================================================================
ActuatorManager::ActuatorManager( const ScanConfig& config )
    : config_(config), moving_group_id_(-1),
      factory_( IPolledActuatorInterfaceName )
{
    SCAN_DEBUG << "Initializing actuator factory..." << ENDLOG;
    boost::filesystem::path plugin_path( config.properties.plugin_root_path );
    plugin_path /= "actuator";
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
    std::string path_str = plugin_path.string();
#else
    std::string path_str = plugin_path.file_string();
#endif
    factory_.init( path_str );
    SCAN_DEBUG << "Actuator factory initialized" << ENDLOG;

    //- register a group with the actuators of the 1st dimension
    if ( !config.actuators.empty() )
    {
        SCAN_DEBUG << "!config.actuators.empty()" << ENDLOG;
        this->register_group( ActuatorDim_1, config.actuators );

        if ( config_.enable_scan_speed )
        {
            if( config_.scan_speed.size() == 0 )
            {
                THROW_YAT_ERROR("CONFIG_ERROR",
                                "The scan_speed array is empty!",
                                "ActuatorManager::ActuatorManager");
            }
            GroupPtr g = groups_[ActuatorDim_1];

            for( std::size_t k = 0; k  < (*g).size(); ++k )
            {
                SubGroup& s = (*g)[k];
                s.normal_speed.resize( s.attr_names.size() );
                s.scan_speed.resize( s.attr_names.size() );
                for ( size_t i = 0; i < s.attr_names.size(); i++ )
                {
                    s.normal_speed[i] = s.actuator->get_speed( s.attr_names[i] );
                    s.scan_speed[i]   = config_.scan_speed[ s.index[i] ];
                }
            }
        }
    }

    //- register a group with all actuators (dim 1 + dim 2) and a group
    //  with dim2 actuators only
    if ( !config.actuators2.empty() )
    {
        std::vector<std::string> all_actuators;

        std::copy(config.actuators.begin(),
                  config.actuators.end(),
                  std::back_inserter(all_actuators) );

        std::copy(config.actuators2.begin(),
                  config.actuators2.end(),
                  std::back_inserter(all_actuators) );

        if( config_.properties.move_dim1_then_dim2_actuators )
        {
            //- register a group with dim2 actuators only
            std::vector<std::string> actuators2;
            std::copy(config.actuators2.begin(),
                      config.actuators2.end(),
                      std::back_inserter(actuators2) );

            this->register_group( ActuatorDim_2,
                                  actuators2 );
        }

        this->register_group( ActuatorDim_1 + ActuatorDim_2,
                              all_actuators );
    }

}

// ============================================================================
// ActuatorManager::~ActuatorManager
// ============================================================================
ActuatorManager::~ActuatorManager()
{
}

// ============================================================================
// ActuatorManager::validate_trajectories
// ============================================================================
void ActuatorManager::validate_trajectories(const std::vector<std::string>& actuator_names, const ScanArray<double>::Img& trajectories)
{
    std::list<std::string> invalid_actuators;
    for(size_t i=0 ; i < actuator_names.size() ; i++) {
        const std::string& actuator_name = actuator_names[i];
        // resolve device and attribute from actuator's name
        std::pair<std::string, std::string> components = Util::instance().resolve_attr_name(actuator_name);
        // get actuator's plugin
        std::map<std::string, IActuatorPtr>::const_iterator it = actuators_.find(components.first);
        if (it == actuators_.end()) { // critical error ?
            SCAN_WARN << "no entry found for actuator \"" << actuator_name << "\". Can't validate trajectory" << ENDLOG;
            continue;
        }
        // get actuator's limits
        LimitsType limits;
        try {
            limits = it->second->get_limits(components.second);
        } catch (...) {
            SCAN_WARN << "can't extract limits from actuator \"" << actuator_name << "\". Can't validate trajectory" << ENDLOG;
            continue;
        }
        // get actuator's trajectory
        ScanArray<double>::Img::const_array_view<1>::type actuator_trajectory = trajectories[boost::indices[i][boost::multi_array_types::index_range()]];
        if (actuator_trajectory.empty()) { // is it possible ?
            SCAN_WARN << "empty trajectory \"" << actuator_name << "\"." << ENDLOG;
            continue;
        }
        // get extrema values of the actuator's trajectory (std::minmax with c++11 should be preferred)
        double min_value = *std::min_element(actuator_trajectory.begin(), actuator_trajectory.end());
        double max_value = *std::max_element(actuator_trajectory.begin(), actuator_trajectory.end());
        // check that trajectory minimum is greater than lower limit & maximum is less than upper limit
        // don't need to perform other comparisons
        bool min_available = true;
        bool max_available = true;

        try
        {
        	double limit_min = boost::lexical_cast<double>(limits.first);
            min_available = min_value >= limit_min;
            if( !min_available )
                // test equality up to a relative precision of 1e-9
                min_available = yat::fp_is_equal(min_value, limit_min, actuator_comp_precision);
        }
        catch (const boost::bad_lexical_cast&)
        {
            // some crap like "Not Specified", we just silently accept
        }
        try
        {
        	double limit_max = boost::lexical_cast<double>(limits.second);
            max_available = max_value <= limit_max;
            if( !max_available )
                // test equality up to a relative precision of 1e-9
                max_available = yat::fp_is_equal(max_value, limit_max, actuator_comp_precision);
        }
        catch (const boost::bad_lexical_cast&)
        {
            //
        }

        if( !(min_available && max_available) )
        {
            std::stringstream oss;
            oss << "Wrong trajectory bounds (" <<  min_value << ", " << max_value << ") "
                << "for actuator \"" << actuator_name << "\", "
                << "limits are (" << limits.first << ", " << limits.second << ")";
            invalid_actuators.push_back(oss.str());
        }
    }
    // throw an exception if any actuator's trajectory is invalid
    if (!invalid_actuators.empty()) {
        yat::Exception error;
        for(std::list<std::string>::const_iterator it = invalid_actuators.begin(), end = invalid_actuators.end() ; it != end ; ++it)
            error.push_error("INVALID_TRAJECTORY", *it, "ActuatorManager::validate_trajectories");
        throw error;
    }
}

// ============================================================================
// ActuatorManager::validate_trajectories
// ============================================================================
void ActuatorManager::validate_trajectories()
{
    SCAN_DEBUG << "ActuatorManager::validate_trajectories" << ENDLOG;
    validate_trajectories(config_.actuators, config_.trajectories);
    validate_trajectories(config_.actuators2, config_.trajectories2);
}

// ============================================================================
// ActuatorManager::register_group
// ============================================================================
void ActuatorManager::register_group( int id, const std::vector<std::string>& attr_names )
{
    SCAN_DEBUG << "ActuatorManager::register_group: " << id << ENDLOG;
    //- a multimap containing attr names (w/o device name), indexed by the device names
    typedef std::multimap< std::string, std::string > StrMMap;
    typedef StrMMap::const_iterator StrMMapCIt;
    StrMMap organized_attr;
    std::vector<std::pair<std::string, std::string> > resolved_attr_names;
    std::vector<std::string> full_attr_names;

    {
        //- what is done here:
        //- 1. resolve the attribute names (get the device name and and the attr names)
        //-    this phase is necessary to resolve devices and attributes aliases
        //-
        //- 2. build 'resolved_attr_names' and 'full_attr_names' so that they contain
        //-    attr names sorted in the same order than the 'attr_names' input argument
        //-
        //- 3. if the actuator object corresponding to a device name is not instantiated,
        //-    instantiate it and put it in the map 'actuators_'
        //-
        //- 4. for each attribute, call the corresponding IActuator::ensure_supported method


        size_t actuator_number=0;

        for( std::size_t i = 0; i < attr_names.size(); ++i )
        {
            std::pair<std::string, std::string> attr_resolution = Util::instance().resolve_attr_name( attr_names[i] );
            resolved_attr_names.push_back( attr_resolution );
            full_attr_names.push_back( Util::instance().make_attr_name(attr_resolution.first,attr_resolution.second) );

            organized_attr.insert( attr_resolution );
            if ( actuators_.find( attr_resolution.first ) == actuators_.end() )
            {
                IPolledActuatorPtr polled_actuator_ptr( factory_.create( attr_resolution.first ) );
                PolledActuatorAdapter* polled_actuator_p = new PolledActuatorAdapter(polled_actuator_ptr);
                IActuatorPtr actuator_ptr( polled_actuator_p, ThreadExiter() );

                if( config_.hw_continuous )
                {
                    {
                        std::ostringstream oss;
                        oss << config_.hw_continuous_nbpt;
                        // Insert parameter : Has it is used in init , it must be set before
                        polled_actuator_p->set_parameter("hw_continuous_nbpt", oss.str());
                    }
                    {
                        std::ostringstream oss;
                        oss << config_.integration_times[ 0 ];
                        // Insert parameter : Has it is used in init , it must be set before
                        polled_actuator_p->set_parameter("integration_time", oss.str());
                    }

                    {
                        double from= config_.trajectories[actuator_number][0] ;
                        std::ostringstream oss;
                        oss << from ;
                        // Insert parameter : Has it is used in init , it must be set before
                        polled_actuator_p->set_parameter("from", oss.str());
                    }

                    {
                        double to = config_.trajectories[actuator_number][ 1]  ;
                        std::ostringstream oss;
                        oss << to ;
                        // Insert parameter : Has it is used in init , it must be set before
                        polled_actuator_p->set_parameter("to", oss.str());
                    }

                    actuator_number++; // increment for next actuator


                }  //end if config_.hw_continuous

                try
                {
                    actuator_ptr->init( attr_resolution.first );
                    actuator_ptr->check_initial_condition( attr_resolution.second );
                }
                catch( yat::Exception &ex)
                {
                    LOG_EXCEPTION_ERROR( ex );
                    throw;
                }
                actuators_.insert( std::make_pair(attr_resolution.first, actuator_ptr) );
            }

            actuators_[attr_resolution.first]->ensure_supported( attr_resolution.second );
        }
    }

    GroupPtr group( new Group );

    {
        //- iterate on all element of the multimap to build the group.
        //- they are sorted by device name
        StrMMapCIt it  = organized_attr.begin();
        StrMMapCIt end = organized_attr.end();

        while ( it != end )
        {
            std::string device_name = (*it).first;
            //- find the element corresponding to the next device
            //- -> all element between 'it' and 'next_device' correspond to the same device
            //- they will be put in the same subgroup
            StrMMapCIt next_device = organized_attr.upper_bound( device_name );

            SubGroup sub_group;
            sub_group.device_name = device_name;
            sub_group.actuator = actuators_[device_name];

            for (; it != next_device; ++it)
            {
                //- find the indice of the current attribute in 'attr_names'
                const std::pair<std::string, std::string>& p = (*it);
                ptrdiff_t ind = std::find(resolved_attr_names.begin(), resolved_attr_names.end(), p)
                        - resolved_attr_names.begin();

                //- add the attribute in the subgroup ( attr name + indice )
                sub_group.attr_names.push_back( (*it).second );
                sub_group.index.push_back( ind );

                boost::shared_ptr<Tango::AttributeInfoEx> info_ptr( new Tango::AttributeInfoEx() );
                *info_ptr = DevProxies::instance().proxy( sub_group.device_name )->attribute_query((*it).second);
                sub_group.attr_info.push_back( info_ptr );
            }
            group->push_back( sub_group );
        }
    }

    //- finally, store the group by indexing it with its id
    groups_.insert( std::make_pair( id, group ) );
}

// ============================================================================
// ActuatorManager::go_to
// ============================================================================
void ActuatorManager::go_to( int id, const std::vector<double>& position )
{
    SCAN_DEBUG << "ActuatorManager::go_to: " << id << ENDLOG;
    if( groups_.find(id) != groups_.end() )
    {
        moving_group_id_ = id;
        GroupPtr group = groups_[id];

        //- iterate on all the subgroups
        for( std::size_t k = 0; k  < (*group).size(); ++k )
        {
            const SubGroup& sub_group = (*group)[k];
            //- build the vector of position for the subgroup
            std::vector<double> subgroup_pos;
            std::vector<PositionRequest> subgroup_position_requests;

            for (size_t i = 0; i < sub_group.attr_names.size(); i++)
            {
                PositionRequest pos_req;
                pos_req.attribute_name = sub_group.attr_names[i];
                pos_req.position = position[ sub_group.index[i] ];
                pos_req.attribute_info = sub_group.attr_info[i];
                subgroup_position_requests.push_back( pos_req );
            }

            for ( size_t i = 0; i < sub_group.attr_names.size(); ++i )
            {
                SCAN_INFO << sub_group.device_name << "/" << sub_group.attr_names[i]
                             << " to position " << subgroup_position_requests[i].position << ENDLOG;
            }

            //- launch the movement request
            sub_group.actuator->go_to( subgroup_position_requests );
        }
    }
}

// ============================================================================
// ActuatorManager::set_scan_speed
// ============================================================================
void ActuatorManager::set_scan_speed( void )
{
    if ( config_.enable_scan_speed && !groups_.empty()
         && groups_.find(ActuatorDim_1) != groups_.end() )
    {
        GroupPtr g = groups_[ActuatorDim_1];
        for( std::size_t k = 0; k  < (*g).size(); ++k )
        {
            SubGroup& s = (*g)[k];
            for ( size_t i = 0; i < s.attr_names.size(); i++ )
            {
                s.actuator->set_speed( s.attr_names[i], s.scan_speed[i] );
            }
        }
    }
}

// ============================================================================
// ActuatorManager::restore_speed
// ============================================================================
void ActuatorManager::restore_speed( void )
{
    SCAN_DEBUG << "ActuatorManager::restore_speed" << ENDLOG;
    if ( config_.enable_scan_speed && !groups_.empty()
         && groups_.find(ActuatorDim_1) != groups_.end() )
    {
        GroupPtr g = groups_[ActuatorDim_1];
        for( std::size_t i = 0; i  < (*g).size(); ++i )
        {
            SubGroup& s = (*g)[i];
            for ( size_t j = 0; j < s.attr_names.size(); ++j )
            {
                s.actuator->set_speed( s.attr_names[j], s.normal_speed[j] );
            }
        }
    }
}

// ============================================================================
// ActuatorManager::abort
// ============================================================================
void ActuatorManager::abort( void )
{
    SCAN_DEBUG << "ActuatorManager::abort" << ENDLOG;
    for( std::map<std::string, IActuatorPtr>::iterator it = actuators_.begin(); it != actuators_.end(); ++it )
    {
        SCAN_INFO << "Aborting movement on Actuator " << it->first << "..." << ENDLOG;
        try
        {
            it->second->abort();
        }
        catch( Tango::DevFailed& df )
        {
            _TANGO_TO_YAT_EXCEPTION(df, ex);
            LOG_EXCEPTION_ERROR( ex );
            SCAN_ERROR << "An error occured while aborting acquisition on actuator " << it->first<< "..." << ENDLOG;
        }
        catch( yat::Exception& ex )
        {
            LOG_EXCEPTION_ERROR( ex );
            SCAN_ERROR << "An error occured while aborting acquisition on actuator " << it->first << "..." << ENDLOG;
        }
        catch( ... )
        {
            SCAN_ERROR << "An unknown error occured while aborting acquisition on actuator " << it->first << "..." << ENDLOG;
        }
    }
    this->restore_speed();
}

// ============================================================================
// ActuatorManager::read_all
// ============================================================================
AttrValues ActuatorManager::read( int group_id )
{
    SCAN_DEBUG << "ActuatorManager::read: " << group_id << ENDLOG;
    AttrValues result;
    if( groups_.find(group_id) != groups_.end() )
    {
        GroupPtr group = groups_[ group_id ];
        for( std::size_t k = 0; k  < (*group).size(); ++k )
        {
            const SubGroup& subgroup = (*group)[k];
            ProxyP proxy = DevProxies::instance().proxy(subgroup.device_name);
            boost::scoped_ptr< vector<Tango::DeviceAttribute> > dev_attrs_p;
            dev_attrs_p.reset( proxy->read_attributes( const_cast<std::vector<std::string>&>(subgroup.attr_names) ) );
            vector<Tango::DeviceAttribute>& dev_attrs = *dev_attrs_p;

            for (size_t i = 0; i < dev_attrs.size(); i++)
            {
                std::string full_attr_name = Util::instance().make_attr_name(subgroup.device_name, dev_attrs[i].get_name());
                boost::any data = Util::instance().extract_data( dev_attrs[i], *(subgroup.attr_info[i]) );
                result.push_back( AttrValue(full_attr_name, data) );
            }
        }
    }

    return result;
}

// ============================================================================
// ActuatorManager::move_request_completed
// ============================================================================
bool ActuatorManager::move_request_completed( void )
{
    // move request is completed unless one of the actuator is still moving
    bool still_moving = false;
    if( groups_.find(moving_group_id_) != groups_.end() )
    {
        GroupPtr group = groups_[ moving_group_id_ ];
        for( std::size_t i = 0; i  < (*group).size(); ++i )
        {
            still_moving = ( still_moving || (*group)[i].actuator->is_moving() );
        }
    }
    return !still_moving;
}


// ============================================================================
// ActuatorManager::error_check
// ============================================================================
void ActuatorManager::error_check( void )
{
    //    SCAN_DEBUG << "ActuatorManager::error_check: " << moving_group_id_ << ENDLOG;
    if( groups_.find(moving_group_id_) != groups_.end() )
    {
        GroupPtr group = groups_[ moving_group_id_ ];
        for( std::size_t i = 0; i  < (*group).size(); ++i )
        {
            (*group)[i].actuator->error_check();
        }
    }
}

}

