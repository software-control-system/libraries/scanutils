/*!
 * \file
 * \brief    Definition of Sensor class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <limits>
#include <boost/scoped_ptr.hpp>
#include <scansl/plugin/Sensor.h>
#include <scansl/ThreadExiter.h>
#include <scansl/Util.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

// ============================================================================
// Sensor::Sensor
// ============================================================================
Sensor::Sensor()
{

}

// ============================================================================
// Sensor::~Sensor
// ============================================================================
Sensor::~Sensor()
{
}

// ============================================================================
// Sensor::init
// ============================================================================
void Sensor::init( std::string device_name, bool sync)
{
    proxy_ = DevProxies::instance().proxy(device_name);
    device_name_ = device_name;
}

// ============================================================================
// Sensor::check_initial_condition
// ============================================================================
void Sensor::check_initial_condition( ) {
    // no-op
}

// ============================================================================
// Sensor::check_initial_condition
// ============================================================================
void Sensor::check_file_generation_condition( std::string attr_name ) {

    	bool isFileGeneration = false;
	Tango::DeviceAttribute fileGeneration_attr;
	    try
  	    {
    		fileGeneration_attr = proxy_->read_attribute(attr_name);
    		fileGeneration_attr >> isFileGeneration;
            }
    	    catch( Tango::DevFailed& df )
            {
	    	// if attribute doesn't exist nothing to do 
            }
	    
	    if(isFileGeneration)
	    {
	    	std::ostringstream oss;
        	oss 	<< "The " << attr_name 
			<< " attribute must be False for the device "
                	<< device_name_;


            	THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "check_file_generation_condition");
	    }
}

// ============================================================================
// Sensor::ensure_supported
// ============================================================================
void Sensor::ensure_supported( std::string )
{
    //- do nothing : accept all attributes by default
}

// ============================================================================
// Sensor::abort
// ============================================================================
void Sensor::abort( void )
{
    //- no generic implementation of this functionnality
}

// ============================================================================
// Sensor::before_integration
// ============================================================================
void Sensor::before_integration( void )
{
    //- no generic implementation of this functionnality
}

// ============================================================================
// Sensor::after_integration
// ============================================================================
void Sensor::after_integration( void )
{
    //- no generic implementation of this functionnality
}

// ============================================================================
// Sensor::before_run
// ============================================================================
void Sensor::before_run( void )
{
    //- no generic implementation of this functionnality
}

// ============================================================================
// Sensor::after_run
// ============================================================================
void Sensor::after_run( void )
{
    //- no generic implementation of this functionnality
}
}
