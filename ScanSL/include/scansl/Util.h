#ifndef _SCANSERVER_UTIL_H
#define _SCANSERVER_UTIL_H

#include <string>
#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <boost/any.hpp>
#include <boost/algorithm/string.hpp>
#include <yat/threading/Task.h>
#include <yat/utils/Singleton.h>
#include <yat4tango/CommonHeader.h>

namespace ScanUtils
{
struct SCANSL_DECL Util : public yat::Singleton<Util>
{
    std::string resolve_device_name( std::string device_name );

    std::pair<std::string, std::string> resolve_attr_name( std::string attr_name );

    std::string complete_attr_name( std::string attr_name );

    std::string make_attr_name( std::string device_name, std::string attr_name );

    std::string get_class( std::string device_name );

    boost::any extract_data( const Tango::DeviceAttribute& dev_attr, const Tango::AttributeInfoEx& info );
    
    double extract_scalar_as_double( const Tango::DeviceAttribute& dev_attr_in, const Tango::AttributeInfoEx& info  );

    Tango::DeviceAttribute create_device_attribute(const Tango::AttributeInfo& attr_info, const double& value);
    
    Tango::DeviceAttribute create_device_attribute(std::string name, int data_type, const double& value);

};


void post_msg( yat::Task& t, size_t msg_id, size_t timeout_ms, bool wait );

template <typename T>
void post_msg( yat::Task& t, size_t msg_id, size_t timeout_ms, bool wait, const T& data )
{
    yat::Message* msg = 0;
    try
    {
        msg = yat::Message::allocate(msg_id, DEFAULT_MSG_PRIORITY, wait);
    }
    catch( yat::Exception& ex )
    {
        RETHROW_YAT_ERROR(ex,
                          "OUT_OF_MEMORY",
                          "Out of memory",
                          "post_msg");
    }

    try
    {
        msg->attach_data( data );
    }
    catch( yat::Exception& ex )
    {
        RETHROW_YAT_ERROR(ex,
                          "SOFTWARE_FAILURE",
                          "Unable to attach data",
                          "post_msg");
    }

    try
    {
        if (wait)
            t.wait_msg_handled( msg, timeout_ms );
        else
            t.post( msg, timeout_ms );
    }
    catch( yat::Exception& ex )
    {
        RETHROW_YAT_ERROR(ex,
                          "SOFTWARE_FAILURE",
                          "Unable to post a msg",
                          "ScanTask::post_msg");
    }
}


}

#endif
