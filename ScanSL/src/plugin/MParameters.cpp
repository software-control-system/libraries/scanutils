/*!
 * \file
 * \brief    Definition of Sensor class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <limits>
#include <boost/scoped_ptr.hpp>
#include <scansl/plugin/MParameters.h>
#include <scansl/ThreadExiter.h>
#include <scansl/Util.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

// ============================================================================
// MParameters::name_marshalling
// ============================================================================
std::string MParameters::name_marshalling( const std::string &_name)
{
    // to offer more flexibility in parameter names, we consider only lower case
    // alphabetic characters
    // 'Integration Time' is therefore the same as 'IntegrationTime' or 'integration_time'
    std::string name;

    // Consider only alphabetic characters
    for(std::size_t i=0; i < _name.size(); ++i)
    {
        if( isalpha(_name.c_str()[i]) )
            name.push_back(_name.c_str()[i]);
    }

    // switch to lower case
    for(std::size_t i=0; i < name.size(); ++i)
        name.replace(i, 1, 1, tolower(name.c_str()[i]));

    return name;
}

// ============================================================================
// MParameters::set_parameter
// ============================================================================
void MParameters::set_parameter( const std::string &name, const string &value)
{
    yat::AutoMutex<> lock(mutex_);
    parameter_map_[name_marshalling(name)] = value;
}

// ============================================================================
// MParameters::has_parameter
// ============================================================================
bool MParameters::has_parameter( const std::string &name)
{
    yat::AutoMutex<> lock(mutex_);
    if( parameter_map_.find(name_marshalling(name)) != parameter_map_.end() )
        return true;

    return false;
}

// ============================================================================
// MParameters::parameter_value
// ============================================================================
std::string MParameters::parameter_value(const std::string &name)
{
    yat::AutoMutex<> lock(mutex_);
    std::map<std::string, std::string>::const_iterator cit = parameter_map_.find(name_marshalling(name));
    if( cit != parameter_map_.end() )
        return cit->second;

    std::ostringstream oss;
    oss << "Parameter '" << name << "' doesn't exists" << std::endl;
    throw yat::Exception("NO_DATA", oss.str(), "Sensor::param_value");
}
}
