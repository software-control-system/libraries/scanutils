#ifndef _SCANSERVER_ISCANPLUGININFO_H
#define _SCANSERVER_ISCANPLUGININFO_H

#include <scansl/ScanSL.h>
#include <boost/shared_ptr.hpp>
#include <yat/plugin/IPlugInInfo.h>

namespace ScanUtils
{
const std::string IScanPlugInInfoInterfaceName( "IScanPlugInInfo" );

struct IScanPlugInInfo : public yat::IPlugInInfo
{
    virtual void supported_classes ( std::vector<std::string>& list ) const = 0;
};

typedef boost::shared_ptr<IScanPlugInInfo> IScanPlugInInfoPtr;

}

#endif
