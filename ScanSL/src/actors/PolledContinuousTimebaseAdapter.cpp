#include <scansl/actors/PolledContinuousTimebaseAdapter.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

const size_t START_REQUEST_MSG = yat::FIRST_USER_MSG;
const size_t ABORT_REQUEST_MSG = yat::FIRST_USER_MSG + 1;

PolledContinuousTimebaseAdapter::PolledContinuousTimebaseAdapter( IPolledContinuousTimebasePtr polled_timebase_obj )
    : polled_timebase_obj_(polled_timebase_obj),
      is_counting_(false),
      has_error_(false)
{
    this->go();
}

void   PolledContinuousTimebaseAdapter::init( std::string device_name )
{
    device_name_ = device_name;
    polled_timebase_obj_->init( device_name );
}

void   PolledContinuousTimebaseAdapter::check_initial_condition( )
{
    polled_timebase_obj_->check_initial_condition( );
}

std::vector<std::string> PolledContinuousTimebaseAdapter::get_sensors( void )
{
    return polled_timebase_obj_->get_sensors();
}

std::vector<std::string> PolledContinuousTimebaseAdapter::get_actuators( void )
{
    return polled_timebase_obj_->get_actuators();
}

std::vector<std::string> PolledContinuousTimebaseAdapter::get_actuators_proxies( void )
{
    return polled_timebase_obj_->get_actuators_proxies();
}

void   PolledContinuousTimebaseAdapter::start( double integration_time, long nb_points )
{
    // ensures that if is_counting() is called just after this method, it will return true
    {
        yat::MutexLock lock(mutex_);
        is_counting_ = true;
        has_error_ = false;
    }
    // send the message asynchronously
    yat::Message* msg = yat::Message::allocate(START_REQUEST_MSG);
    msg->attach_data( std::make_pair(integration_time,nb_points) );
    this->post( msg );
}

bool   PolledContinuousTimebaseAdapter::is_counting( void )
{
    yat::MutexLock lock(mutex_);
    return is_counting_;
}

void   PolledContinuousTimebaseAdapter::error_check( void )
{
    yat::MutexLock lock(mutex_);

    if (has_error_)
        throw last_error_;

    try
    {
        polled_timebase_obj_->error_check();
    }
    catch( Tango::DevFailed& df )
    {
        has_error_ = true;
        last_error_ = yat4tango::TangoYATException(df);
        throw last_error_;
    }
    catch( yat::Exception& ex )
    {
        has_error_ = true;
        last_error_ = ex;
        throw last_error_;
    }

}

void   PolledContinuousTimebaseAdapter::abort( void )
{
    yat::Message* msg = yat::Message::allocate(ABORT_REQUEST_MSG);
    this->post( msg );
}

void PolledContinuousTimebaseAdapter::handle_message (yat::Message& msg)
throw (yat::Exception)
{
    try
    {
        switch ( msg.type() )
        {
        case yat::TASK_TIMEOUT:
        {
            this->check_counting_state();
            if (!is_counting_)
            {
                this->enable_timeout_msg(false);
            }
        }
            break;
        case START_REQUEST_MSG:
        {
            std::pair<double,long>* config;
            msg.detach_data(config);
            boost::scoped_ptr< std::pair<double,long> > guard( config );

            {
                yat::MutexLock lock(mutex_);
                polled_timebase_obj_->start(config->first, config->second);
                double polling_period = polled_timebase_obj_->get_polling_period_ms();
                this->set_timeout_msg_period( static_cast<size_t>(polling_period) );
                this->enable_timeout_msg(true);

                // trigger an update immediately
                this->check_counting_state();
            }
        }
            break;
        case ABORT_REQUEST_MSG:
        {
            this->enable_timeout_msg(false);
            {
                yat::MutexLock lock(mutex_);
                polled_timebase_obj_->abort();
            }
            is_counting_ = false;
        }
            break;
        }
    }
    catch( yat::Exception& ex )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = ex;
                is_counting_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
    catch( Tango::DevFailed& ex )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = yat4tango::TangoYATException(ex);
                is_counting_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
    catch( ... )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                std::ostringstream oss;
                oss << "Unknown error on timebase "  << device_name_;
                last_error_ = yat::Exception("UNKNWON",oss.str().c_str(), "PolledContinuousTimebaseAdapter::handle_message");
                is_counting_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
}

void   PolledContinuousTimebaseAdapter::check_counting_state( void )
{
    is_counting_ = polled_timebase_obj_->is_counting();
}
}

