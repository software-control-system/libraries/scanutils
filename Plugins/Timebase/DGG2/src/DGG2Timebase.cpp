/*!
 * \file
 * \brief    Declaration of DGG2Timebase class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "DGG2Timebase.h"
#include <yat/plugin/PlugInSymbols.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::DGG2Timebase,
                          ScanUtils::DGG2TimebaseInfo);

namespace ScanUtils
{
const std::string PlugInID                ( "DGG2Timebase" );
const std::string VersionNumber           ( "1.0.0" );

// ============================================================================
// DGG2TimebaseInfo::get_plugin_id
// ============================================================================
std::string DGG2TimebaseInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// DGG2TimebaseInfo::get_interface_name
// ============================================================================
std::string DGG2TimebaseInfo::get_interface_name() const
{
    return IPolledTimebaseInterfaceName;
}

// ============================================================================
// DGG2TimebaseInfo::get_version_number
// ============================================================================
std::string DGG2TimebaseInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// DGG2TimebaseInfo::supported_classes
// ============================================================================
void DGG2TimebaseInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back("DGG2");
}


// ============================================================================
// DGG2Timebase::DGG2Timebase
// ============================================================================
DGG2Timebase::DGG2Timebase()
{
    config_.counting_state        = Tango::MOVING;
    config_.error_states.push_back(Tango::FAULT);
    config_.error_states.push_back(Tango::UNKNOWN);
    config_.polling_period_ms     = 10;
    config_.abort_cmd_name        = "Stop";
    config_.timeout_ms            = 3000;
    config_.integration_time_attr = "SampleTime";
    config_.integration_time_gain = 1.0;
    config_.start_cmd_name        = "Start";
}
}
