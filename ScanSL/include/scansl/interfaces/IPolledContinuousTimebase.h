#ifndef _SCANSERVER_IPOLLEDCONTINUOUSTIMEBASE_H
#define _SCANSERVER_IPOLLEDCONTINUOUSTIMEBASE_H

#include <scansl/ScanConfig.h>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <yat/plugin/IPlugInObject.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
const std::string IPolledContinuousTimebaseInterfaceName( "IPolledContinuousTimebase" );

struct IPolledContinuousTimebase : public yat::IPlugInObject
{
    virtual void init( std::string device_name  ) = 0;

    virtual void check_initial_condition( ) = 0;
    
    virtual std::vector<std::string> get_sensors() = 0;

    virtual std::vector<std::string> get_actuators() = 0;
    
    virtual std::vector<std::string> get_actuators_proxies() = 0;

    virtual void start( double integration_time, long nb_points ) = 0;
    
    virtual double get_polling_period_ms( void ) = 0;

    virtual bool is_counting( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual void abort( void ) = 0;
};

typedef boost::shared_ptr<IPolledContinuousTimebase> IPolledContinuousTimebasePtr;
}

#endif
