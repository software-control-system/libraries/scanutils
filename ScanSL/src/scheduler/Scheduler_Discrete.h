#ifndef _SCANSERVER_SCHEDULER_DISCRETE_H
#define _SCANSERVER_SCHEDULER_DISCRETE_H

#include <scansl/scheduler/Scheduler.h>

namespace ScanUtils
{

class Scheduler_Discrete : public Scheduler
{
public: //! structors
    Scheduler_Discrete( const ScanConfig& config );

protected: //! modifiers

    virtual bool is_specific_action( int action );

    virtual SchedulerState handle_specific_action( void );

private:
    enum
    {
        SpecificAction_READ_ACTUATORS = Scheduler::ActionType_MAX_
    };


};

}

#endif
