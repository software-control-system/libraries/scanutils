/*!
 * \file
 * \brief    Definition of AIControllerTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class AIControllerTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "AIControllerTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("AIController");
    }
};

class AIControllerTimebase : public WaitingStateChangeTimebase
{

public:
    AIControllerTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Start";
    }

    void check_initial_condition( )
    {
		WaitingStateChangeTimebase::check_file_generation_condition("nexusFileGeneration");
    }
    
    void before_run()
    {
        SCAN_DEBUG << "AIControllerTimebase::before_run" << ENDLOG;
        try{
            Tango::DeviceAttribute triggerNumber_attr("dataBufferNumber", 1UL);
            proxy_->write_attribute( triggerNumber_attr );
        }
        catch( Tango::DevFailed& df )
        {
            yat4tango::TangoYATException e(df);

            RETHROW_YAT_ERROR(e,
                    "TANGO_ERROR",
                    "AiController V2 expected",
                    "AIControllerTimebase::before_run");
        }
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::AIControllerTimebase,
                          ScanUtils::AIControllerTimebaseInfo);
