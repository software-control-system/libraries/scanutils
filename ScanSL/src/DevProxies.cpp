#include <scansl/DevProxies.h>
#include <boost/algorithm/string.hpp>

namespace ScanUtils
{
typedef std::map< std::string, ProxyP > ProxyRep;

ProxyP DevProxies::proxy( const std::string& dev_name )
{
    //std::cout << "proxy request : " << std::hex << this << std::endl;
    std::string dev_name_low = boost::to_lower_copy( dev_name );

    //- try to find "dev_name" in the list
    ProxyRep::iterator it = rep_.find( dev_name_low );
    if ( it != rep_.end() )
    {
        return (*it).second;
    }


    //std::cout << "proxy alloc : " << dev_name << std::endl;
    ProxyP p( new Proxy(dev_name_low) );

    //- maybe dev_name is an alias of a device we already have in the repository
    //- in that case, use the already allocated proxy, and register the fact that we have an alias
    //- to speed up future calls with the same dev_name
    std::string resolved_dev_name_low = boost::to_lower_copy(p->dev_name()); //- here, alias is resolved
    ProxyRep::iterator resolved_it = rep_.find( resolved_dev_name_low );
    if ( resolved_it != rep_.end() )
    {
        rep_[dev_name_low] = (*resolved_it).second; //- we will have 2 entries with the same ProxyP
        return (*resolved_it).second;
        //- p will be deleted here
    }

    //- ok assume the proxy does not exist yet, so register it
    rep_[dev_name_low] = p;
    return p;
}

void DevProxies::clear()
{
    ProxyRep new_rep;
    rep_.swap( new_rep );
}
}

