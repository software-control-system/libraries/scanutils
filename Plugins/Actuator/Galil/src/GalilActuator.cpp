/*!
 * \file
 * \brief    Definition of GalilActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

// JIRA SCAN-51 : Avoid duplication of GalilActuatorPlugin
#include "GalilActuator.h"

#include <yat/plugin/PlugInSymbols.h>
#include <scansl/interfaces/IScanPlugInInfo.h>

namespace ScanUtils
{
class GalilActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "GalilActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("GalilAxis");
        list.push_back("GalilSlit");
        list.push_back("GalilPiezoAna");
		list.push_back("GalilGearedAxes");
        list.push_back("SimulatedMotor");
        list.push_back("Diaphragme");
        list.push_back("Caesar");
        list.push_back("MCSAxis");
        list.push_back("PMACAxis"); // SIRIUS
		list.push_back("ANC350Axis");
		list.push_back("AerotechAxis");
        list.push_back("PIAxis");//SCAN-910
		//- Delta Tau PowerPMAC:
		//list.push_back("PowerPMACAxis");
	//	list.push_back("PowerPMACComposedAxis");//SCAN-729

    }


    // CF GalilActuator.h for  implementation of class GalilActuator : public WaitingStateChangeActuator
    // GalilActuator is also used in the FacadeUndulatorActuator plugin.
    // As each plugin is a stand alone library(.so) it is a way to share classe and avoid to duplicate the code.

};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::GalilActuator, ScanUtils::GalilActuatorInfo);
