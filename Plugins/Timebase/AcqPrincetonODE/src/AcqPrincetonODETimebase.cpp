/*!
 * \file
 * \brief    Definition of AcqPrincetonODETimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class AcqPrincetonODETimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "AcqPrincetonODETimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("AcqPrincetonODE");
    }
};

class AcqPrincetonODETimebase : public WaitingStateChangeTimebase
{

public:
    AcqPrincetonODETimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 20;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 20000; // CCD camera can integrate 20 seconds
        config_.integration_time_attr = "exposure";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Start";
    }

};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::AcqPrincetonODETimebase,
                          ScanUtils::AcqPrincetonODETimebaseInfo);
