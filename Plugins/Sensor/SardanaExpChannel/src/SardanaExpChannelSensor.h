/*!
 * \file
 * \brief    Declaration of SardanaExpChannelSensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_SARDANAEXPCHANNEL_H_
#define _SCANSERVER_SARDANAEXPCHANNEL_H_

#include <scansl/interfaces/ISensor.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>

namespace ScanUtils
{

class SardanaExpChannelSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};


class SardanaExpChannelSensor : public Sensor
{
public: //! structors

    SardanaExpChannelSensor();

    ~SardanaExpChannelSensor();

public: //! ISensor implementation

    virtual void init( std::string device_name , bool sync);

    virtual void before_integration( void );

    virtual void after_integration( void );

private: //! private implementation

    //! configuration
    ProxyP proxy_;
    bool must_arm_;
};

}


#endif

