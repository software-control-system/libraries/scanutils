#ifndef _SCANSERVER_ISENSOR_H
#define _SCANSERVER_ISENSOR_H

#include <scansl/ScanConfig.h>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <yat/plugin/IPlugInObject.h>
#include <yat4tango/CommonHeader.h>

namespace ScanUtils
{
const std::string ISensorInterfaceName( "ISensor" );

struct ISensor : public yat::IPlugInObject
{
    virtual void init( std::string device_name,bool sync ) = 0;
    
    virtual void check_initial_condition( ) = 0;

    virtual void ensure_supported( std::string attr_name ) = 0;

    virtual void before_integration( void ) = 0;

    virtual void after_integration( void ) = 0;

    virtual void before_run( void ) = 0;

    virtual void after_run( void ) = 0;

    virtual void abort( void ) = 0;
};

typedef boost::shared_ptr<ISensor> ISensorPtr;

}

#endif
