﻿/*!
* \file
* \brief    Definition of VacuumValveActuator class
* \author   Ludmila KLENOV - Synchrotron SOLEIL
*/
//debut SPYC-296
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/String.h>
#include <yat/utils/Logging.h>
#include <yat/utils/StringTokenizer.h>
#include <yat/time/Timer.h>
//enlever par la suite
#include <yat/threading/Thread.h> 

namespace ScanUtils
{
class VacuumValveActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "VacuumValveActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }


    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }



public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("VacuumValve");
	list.push_back("SimulatedVacuumValve");
    }
};

class VacuumValveActuator : public WaitingStateChangeActuator
{
public:
    

    VacuumValveActuator()
    {
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
	config_.error_states.push_back(Tango::DISABLE);
	// l etat du device en mouvement n existe pas
	//l etat du device en mouvement sera mise a jour dans la methode go_to
        config_.move_state        = Tango::UNKNOWN; 
        config_.abort_cmd_name    = ""; //stop command
       
    }

// ============================================================================
// init
// ============================================================================
void init( std::string device_name )
    {
        this->WaitingStateChangeActuator::init( device_name );
	std::ostringstream oss; //description d erreur possible
	//verification si les commandes Open et Close existes dans le device
        if (!check_open_close_commande())
       {
        oss << "The device " 
            	<< config_.dev_name
            	<< " : must have the commands OPEN and CLOSE to move";	
        THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"VacuumValveActuator::init");
   	}
   	
        // lecture de l attribute maxTime 
	double max_time;
        max_time = read_max_time();
	//calcul du time out 
     	timeout = max_time +3.;
   
    }//fin init

//============================================================================
 //check_initial_condition
 //============================================================================
void check_initial_condition( std::string){
	/*verification des conditions initials du device :
		device est dans l etat OPEN ou CLOSE 
	 */
	std::ostringstream oss; //description d erreur possible
	

        //verification si initial state est acceptable
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::OPEN && dev_state != Tango::CLOSE)
        {
        oss << "The device " 
            	<< config_.dev_name
            	<< " : must be OPEN or CLOSE to move";	
        THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"VacuumValveActuator::check_initial_condition");
        }
        
       
    }//fin check_initial_condition

// ============================================================================
// check_open_close_commande
// ============================================================================
bool check_open_close_commande(){
        /*verification si les commandes Open et Close existes.
          retourne : True si les commandes existes,
                     False sinon
        */
        Tango::CommandInfoList*  al = proxy_->command_list_query();
        bool check_open = false; //par defaut on considere que la command open n existe pas
	bool check_close = false;//par defaut on considere que la command close n existe pas
	bool check_open_close = false; // resultat de la verification. Par defaut les commandes open et close n existes pas
        size_t i = 0;
	//recherche des commandes Open et Close dans la liste des commandes
        while( (i < al->size()) && (check_open_close == false))
	{
		//recherche de la commande Open
		if ( (*al)[i].cmd_name == "Open" )
	   	{
            		check_open = true;
           	}
		//recherche de la commande Close
		if ( (*al)[i].cmd_name == "Close" )
	   	{
            		check_close = true;
           	}
		// les commandes Open et Close sont trouvees, alors verification d existance open close ok
		if ((check_open == true) && (check_close == true))
		{
			check_open_close = true;
		}
		i++;
	}
       return check_open_close;
}//fin check_open_close_commande
    
 //============================================================================
 //read_max_time
 //============================================================================
 
 double read_max_time(){
 /*lecture de la valeur de l attribut maxTime : le temps maximum autorise pour un mouvement ouverture/fermeture sur la valve.
 	Si l attribut est vide, sorti en erreur.
 */
 std::ostringstream oss; //description d erreur possible
 Tango::DeviceAttribute maxtime_attr;
 //lecture d attribut maxTime
 try {
   		maxtime_attr = proxy_->read_attribute("maxTime");
		}catch (Tango::DevFailed& df) {
			oss << "The device " 
            	<< config_.dev_name
            	<< df.errors[0].desc;
        		THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "VacuumValveActuator::read_max_time");
      }
   //si l attribut n est pas vide, on recuper la valeur sinon error
   double current_value; //le temps maximum autorise pour un mouvement ouverture/fermeture sur la valve
   if (! maxtime_attr.is_empty())
   	{
      	maxtime_attr >> current_value; 
        return current_value;
   	}
   else {
   		oss << "The device " 
            	<< config_.dev_name
            	<< " :  maxTime attribute is empty";	
        	THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"VacuumValveActuator::read_max_time");
   		}
 
 }//fin read_max_time

// ============================================================================
// go-to
// ============================================================================
void go_to(const std::vector<PositionRequest>& positions ){
     /*Effectuer la commande demandee.
       Les valeurs de positions possibles : 1 - la commande est OPEN,
       					    0 - la commande est CLOSE
     */
    	
     //recuperation de l argument positions
     const PositionRequest& pos_req = positions[0];

     std::ostringstream oss; //description d erreur possible
     std::string cmd_name; //definition du nom de la commande
     
	
     //recuperation de la valeur de l argument positions
     double pos = pos_req.position; 
     if(pos == 0){
     	cmd_name = "Close"; //la commande est close
	//etat du device en mouvement est OPEN 
	config_.move_state = Tango::OPEN;
	//etat finale du device est CLOSE
     	final_state = Tango::CLOSE;
     }
     else if(pos == 1){
     	cmd_name = "Open"; //la commande est open
	//etat du device en mouvement est CLOSE
	config_.move_state = Tango::CLOSE;
	//etat finale du device est OPEN
     	final_state = Tango::OPEN;
     }
     else {
     	oss << "The device " 
            	<< config_.dev_name
            	<< " : Unknown command. Only supported : 1 for OPEN, 0 for CLOSE";	
        THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"VacuumValveActuator::go_to");
     }
     
     //verification si la valve n est pas deja ouvert/fermer
     Tango::DevState current_state = proxy_->state();
     if (current_state == final_state)
	{	
	std::cout << "VacuumValveActuator::go_to. The device  is already in the required position"<< std::endl; 
	//initialisation de l objet du temps a zero car device est deja dans la position attendue
     	tmo = yat::Timeout(0., yat::Timeout::TMO_UNIT_SEC, false);
	}
     else {//initialisation de l objet du temps a timeout seconds
     	tmo = yat::Timeout(timeout, yat::Timeout::TMO_UNIT_SEC, false);
		}
     //envoie de la valve à la position demandee
     proxy_->command_inout(cmd_name);
     //l objet du temps ne doit pas etre mise a jours
     tmo.enable(false);
     
}//fin go_to


// ============================================================================
//is_moving
// ============================================================================
bool is_moving( void )
{ /*verification si le device est un mouvement.

  */
    Tango::DevState current_state = proxy_->state();
    
    //si State courante de device n est pas egale a la valeur de State de device en mouvement,
    //ou le temps maximal autorise pour le mouvement ouverture/fermeture de la valve est dépassée
    //alors le device n est pas un mouvement
    if(current_state == config_.move_state && (tmo.time_to_expiration() > 0.)){
    	return true;
    
    }
    else {
    	return false;
    }
  
}//fin is_moving


// ============================================================================
// get_limits
// ============================================================================
LimitsType get_limits( std::string attr_name )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "VacuumValveActuator::get_limits" );
}//fin get_limits

// ============================================================================
// set_limits
// ============================================================================
void set_limits( std::string attr_name, const LimitsType& limits )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "VacuumValveActuator::set_limits" );
}//fin set_limits

protected:
	double timeout; //valeur de l attribut maxTime
	yat::Timeout tmo; //le temps d attents pour un mouvement d ouverture/fermeture sur la vanne
	Tango::DevState final_state; //valeurs acceptable de State du device sont OPEN et CLOSE
        std::vector<std::string> label_list;//list des positions possibles dans le device
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::VacuumValveActuator, \
                          ScanUtils::VacuumValveActuatorInfo);
//fin SPYC-296
