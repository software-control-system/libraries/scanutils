/*!
 * \file
 * \brief    Definition of PulseGenerationContinuousTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <boost/scoped_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/plugin/WaitingStateChangeContinuousTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class PulseGenerationContinuousTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PulseGenerationContinuousTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledContinuousTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PulseGenerationV2");
    }
};


/* Explication http://jira/jira/browse/SCAN-533?focusedCommentId=616637&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-616637 */
class PulseGenerationContinuousTimebase : public WaitingStateChangeContinuousTimebase
{
public: //! structors

    PulseGenerationContinuousTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 500;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        //config_.integration_time_attr = "integrationTime"; // JIRA SCAN-533
        config_.integration_time_gain = 1.0;
        config_.nbpoint_attr          = "pulseNumber";
        config_.start_cmd_name        = "Start";
		m_is_need_extra_trigger		  = false;
    }

public: //! ITimebase implementation

    void init( std::string device_name )
    {
        this->WaitingStateChangeContinuousTimebase::init( device_name );
        Tango::DeviceAttribute counter0_attr;
        // Checking counter0Enable
		try
		{
			counter0_attr = proxy_->read_attribute( "counter0Enable" );
		}
	    catch( Tango::DevFailed& df )
	    {
			yat4tango::TangoYATException e(df);
			RETHROW_YAT_ERROR(	e,
								"TANGO_ERROR",
								"PulseGeneration error : counter0Enable cannot be read",
								"PulseGenerationContinuousTimebase::init");
	    }

	    bool counter0Enable;
	    counter0_attr >> counter0Enable;

	    if ( !counter0Enable )
	    {
			std::ostringstream oss;
			oss << "The counter0 is not enable for the device PulseGeneration : "
				<< config_.dev_name
				<< ". You must configure the counter0Enable attribut to true";
			THROW_DEVFAILED( 	"CONTINUOUS_TIMEBASE_ERROR",
								oss.str().c_str(),
								"PulseGenerationContinuousTimebase::init" );
	    }

	    // Checking idleStateCounter0
		Tango::DeviceAttribute idleStateCounter0_att;
		try
		{
			idleStateCounter0_att = proxy_->read_attribute("idleStateCounter0");
	    }
	    catch( Tango::DevFailed& df )
		{
			yat4tango::TangoYATException e(df);
			RETHROW_YAT_ERROR(	e,
								"TANGO_ERROR",
								"PulseGeneration error : idleStateCounter0 cannot be read",
								"PulseGenerationContinuousTimebase::init");
		}

		std::string stateCounter;
		idleStateCounter0_att >> stateCounter;
	    if (stateCounter.compare("LOW") != 0)
		{
			std::ostringstream oss;
			oss << "The idle state counter0 has not the good value for the device PulseGeneration : "
				<< config_.dev_name
				<< ". You must configure the idleStateCounter0 attribut to LOW";
			THROW_DEVFAILED( 	"CONTINUOUS_TIMEBASE_ERROR",
								oss.str().c_str(),
								"PulseGenerationContinuousTimebase::init" );
	    }

	    // Checking generationType
		Tango::DeviceAttribute generationType_att;
		try
		{
			generationType_att = proxy_->read_attribute("generationType");
	    }
	    catch( Tango::DevFailed& df )
		{
			yat4tango::TangoYATException e(df);
			RETHROW_YAT_ERROR(	e,
								"TANGO_ERROR",
								"PulseGeneration error : generationType cannot be read",
								"PulseGenerationContinuousTimebase::init");
		}

		std::string generationType;
		generationType_att >> generationType;
	    if (generationType.compare("FINITE") != 0)
		{
			std::ostringstream oss;
			oss << "The generation type has not the good value for the device PulseGeneration : "
				<< config_.dev_name
				<< ". You must configure the generationType attribut to FINITE";
			THROW_DEVFAILED( 	"CONTINUOUS_TIMEBASE_ERROR",
								oss.str().c_str(),
								"PulseGenerationContinuousTimebase::init" );
	    }

        // ensures that state is STANDBY
        Tango::DevState state = proxy_->state();
        if ( state != Tango::STANDBY )
        {
            std::ostringstream oss;
			oss << "The device "
				<< device_name
				<< " state is "
				<< Tango::DevStateName[state]
				<< "."
				<< std::endl
				<< "It should be STANDBY";

            THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "PulseGenerationTimebase::init" );
        }
		
        //- Get the DetectorType property
        Tango::DbData dev_prop;
        dev_prop.push_back( Tango::DbDatum("_ExpertGenerateExtraTrigger") );
        proxy_->unsafe_proxy().get_property(dev_prop);
		
		m_is_need_extra_trigger = false;
        if ( !dev_prop[0].is_empty() )
        {
            m_is_need_extra_trigger = true;
        }	
		
		SCAN_DEBUG<<(m_is_need_extra_trigger?"Need Extra Trigger":"Do Not Need Extra Trigger")<<ENDLOG;
    }

    void start( double integration_time, long nb_point )
    {
		//- Get the DetectorType property
		Tango::DbData dev_prop;
		dev_prop.push_back( Tango::DbDatum("_ExpertDelayCounter0") );
		dev_prop.push_back( Tango::DbDatum("_ExpertDelayBeforeStart") );
		proxy_->unsafe_proxy().get_property(dev_prop);
		
		double expert_delay_counter_0 = 0.0;
		bool use_expert_delay_from_property = false;
		if ( !dev_prop[0].is_empty() )
		{
			dev_prop[0]>>expert_delay_counter_0;
			use_expert_delay_from_property = true;
		}			
		
		std::vector<Tango::DeviceAttribute> attrs;
		if(use_expert_delay_from_property)
		{
			attrs.push_back( Tango::DeviceAttribute("delayCounter0", expert_delay_counter_0 ));
			attrs.push_back( Tango::DeviceAttribute("pulseWidthCounter0", (integration_time*1000-expert_delay_counter_0)));
			SCAN_DEBUG<<"delayCounter0 = "<<expert_delay_counter_0<<ENDLOG;
			SCAN_DEBUG<<"pulseWidthCounter0 = "<<(integration_time*1000-expert_delay_counter_0)<<ENDLOG;			
		}
		else
		{
			attrs.push_back( Tango::DeviceAttribute("delayCounter0", integration_time/2 ));
			attrs.push_back( Tango::DeviceAttribute("pulseWidthCounter0", integration_time/2 ));
			SCAN_DEBUG<<"delayCounter0 = "<<(integration_time/2)<<ENDLOG;
			SCAN_DEBUG<<"pulseWidthCounter0 = "<<(integration_time/2)<<ENDLOG;			
		}
			
		if(m_is_need_extra_trigger)//if need extra trigger, in order to get the same nb_point for NI6602 & Xia (scan HCS on DEIMOS)
		{
			attrs.push_back( Tango::DeviceAttribute(config_.nbpoint_attr, nb_point+1) );
		}
		else
		{
			attrs.push_back( Tango::DeviceAttribute(config_.nbpoint_attr, nb_point) );
		}
		
		proxy_->write_attributes( attrs );
			
		double expert_delay_before_start = 0.0;
		bool use_expert_delay_before_start = false;
		if ( !dev_prop[1].is_empty() )
		{
			dev_prop[1]>>expert_delay_before_start;
			use_expert_delay_before_start = true;
		}
		
		if(use_expert_delay_before_start)
		{
			yat::ThreadingUtilities::sleep( use_expert_delay_before_start, 0 );
		}
		
		proxy_->command_inout( config_.start_cmd_name );
    }


     std::vector<std::string> get_sensors()
     {
         return std::vector<std::string>();
     }

     std::vector<std::string> get_actuators()
     {
         return std::vector<std::string>();
     }

     std::vector<std::string> get_actuators_proxies()
     {
         return std::vector<std::string>();
     }


private: //! private implementation
	bool m_is_need_extra_trigger;

  };

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PulseGenerationContinuousTimebase,
                          ScanUtils::PulseGenerationContinuousTimebaseInfo);

