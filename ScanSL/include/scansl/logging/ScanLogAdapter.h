#ifndef _SCANSERVER_LOGADAPTER_H
#define _SCANSERVER_LOGADAPTER_H

#include <tango.h>
#include <iostream>
#include <scansl/ScanSL.h>
#include <scansl/Singleton.h>
#include <boost/shared_ptr.hpp>

namespace ScanUtils
{

class SCANSL_DECL ScanLogAdapter : public Singleton<ScanLogAdapter>
{
public:
    static ScanLogAdapter& instance();

    void init( Tango::DeviceImpl *dev );

    log4tango::Logger* get_logger( void );

private:
    Tango::DeviceImpl* dev_;
};


#ifdef SCAN_LOG_STDERR

#undef ENDLOG
#define ENDLOG std::endl

#define SCAN_LOG_ std::cerr

#define SCAN_DEBUG SCAN_LOG_ << "debug: "
#define SCAN_INFO SCAN_LOG_ << "info: "
#define SCAN_WARN SCAN_LOG_ << "warning: "
#define SCAN_ERROR SCAN_LOG_ << "error: "
#define SCAN_FATAL SCAN_LOG_ << "fatal: "

#else

#define SCAN_DEV_LOGGER ScanLogAdapter::instance().get_logger()

#ifndef YAT_DEBUG

#define SCAN_DEBUG \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_debug_enabled()) \
    SCAN_DEV_LOGGER->debug_stream() << log4tango::LogInitiator::_begin_log

#define SCAN_INFO \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_info_enabled()) \
    SCAN_DEV_LOGGER->info_stream() << log4tango::LogInitiator::_begin_log

#define SCAN_WARN \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_warn_enabled()) \
    SCAN_DEV_LOGGER->warn_stream() << log4tango::LogInitiator::_begin_log

#define SCAN_ERROR \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_warn_enabled()) \
    SCAN_DEV_LOGGER->error_stream() << log4tango::LogInitiator::_begin_log

#define SCAN_FATAL \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_fatal_enabled()) \
    SCAN_DEV_LOGGER->fatal_stream() << log4tango::LogInitiator::_begin_log

#else

#define SCAN_DEBUG \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_debug_enabled()) \
    SCAN_DEV_LOGGER->debug_stream() << log4tango::LogInitiator::_begin_log << "[" << __FILE__ << ":" << __LINE__ << "] "

#define SCAN_INFO \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_info_enabled()) \
    SCAN_DEV_LOGGER->info_stream() << log4tango::LogInitiator::_begin_log << "[" << __FILE__ << ":" << __LINE__ << "] "

#define SCAN_WARN \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_warn_enabled()) \
    SCAN_DEV_LOGGER->warn_stream() << log4tango::LogInitiator::_begin_log << "[" << __FILE__ << ":" << __LINE__ << "] "

#define SCAN_ERROR \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_warn_enabled()) \
    SCAN_DEV_LOGGER->error_stream() << log4tango::LogInitiator::_begin_log << "[" << __FILE__ << ":" << __LINE__ << "] "

#define SCAN_FATAL \
    if (SCAN_DEV_LOGGER && SCAN_DEV_LOGGER->is_fatal_enabled()) \
    SCAN_DEV_LOGGER->fatal_stream() << log4tango::LogInitiator::_begin_log << "[" << __FILE__ << ":" << __LINE__ << "] "

#endif

#endif

#define LOG_EXCEPTION_FATAL( e ) do { for( std::size_t i = 0; i < e.errors.size(); ++i ) { SCAN_FATAL << e.errors[i].desc << ENDLOG; } } while(0)
#define LOG_EXCEPTION_ERROR( e ) do { for( std::size_t i = 0; i < e.errors.size(); ++i ) { SCAN_ERROR << e.errors[i].desc << ENDLOG; } } while(0)

}

#endif
