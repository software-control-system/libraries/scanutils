/*!
* \file
* \brief    Definition of LimaDetectorTimebase class
* \author   Julien Malik - Synchrotron SOLEIL
*/

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class LimaDetectorTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "LimaDetectorTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("LimaDetector");
    }
};

class LimaDetectorTimebase : public WaitingStateChangeTimebase
{

public:
    LimaDetectorTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 20;
        config_.abort_cmd_name        = "Stop";
        config_.integration_time_attr = "exposureTime";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Snap";
    }
    
    void init( std::string device_name, bool sync )
    {
        // call the super class init
        WaitingStateChangeTimebase::init( device_name, sync );
        //- Get the DetectorType property
        Tango::DbData dev_prop;
        dev_prop.push_back( Tango::DbDatum("DetectorType") );
        proxy_->unsafe_proxy().get_property(dev_prop);

        std::string detector_type;
        if ( !dev_prop[0].is_empty() )
        {
            dev_prop[0] >> detector_type;
        }
        else
            THROW_YAT_ERROR("PLUGIN_ERROR","DetectorType property does not exist","LimaDetectorTimebase::init");

        if(detector_type == "XpadPixelDetector")
            config_.timeout_ms = 10000000; //- FL: arbitrary big value ...
        else if (detector_type == "Basler")
            config_.timeout_ms = 11000;  //- Basler camera can integrate 11 seconds
    }

    void check_initial_condition( )
    {
		WaitingStateChangeTimebase::check_file_generation_condition("fileGeneration");
    }
    
    void before_run()
    {
        //- Get the DetectorType property
        Tango::DbData dev_prop;
        dev_prop.push_back( Tango::DbDatum("DetectorType") );
        proxy_->unsafe_proxy().get_property(dev_prop);
        std::string detector_type;
        if ( !dev_prop[0].is_empty() )
        {
            dev_prop[0] >> detector_type;
        }
        else
        {
            THROW_YAT_ERROR("PLUGIN_ERROR","DetectorType property does not exist","LimaDetectorTimebase::before_run");
        }
        
        //Set nbFrames = 1 except for Ufxc 
        if(detector_type != "Ufxc")
        {
        //- Set the nbFrame to 1
        Tango::DeviceAttribute nbframes_attr("nbFrames", 1L);
        proxy_->write_attribute( nbframes_attr );
    }
    }

    void start( double integration_time )
    {
        //- Get the DetectorType property
        Tango::DbData dev_prop;
        dev_prop.push_back( Tango::DbDatum("DetectorType") );
        proxy_->unsafe_proxy().get_property(dev_prop);
        std::string detector_type;
		
        if ( !dev_prop[0].is_empty() )
        {
            dev_prop[0] >> detector_type;
        }
        else
        {
            THROW_YAT_ERROR("PLUGIN_ERROR","DetectorType property does not exist","LimaDetectorTimebase::start");
        }
       		        
		
        //specific job to do only for UFXC
        //get the device proxy to the specific device
        //rewrite triggerAcquisitionFrequency attribute on specific device in order to compute nbFrames with the last exposureTime set by ScanServer
        if(detector_type == "Ufxc")
        {
            std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
            std::cout << ">>>>>> specific treatment available only for UFXC <<<<<<" << std::endl;
            std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			//- Get properties
			Tango::DbData dev_prop;
			dev_prop.push_back( Tango::DbDatum("DetectorPixelDepth") );
			dev_prop.push_back( Tango::DbDatum("MemorizedTriggerMode") );
			proxy_->unsafe_proxy().get_property(dev_prop);
			std::string detector_pixel_depth;
			std::string detector_trigger_mode;            
			if ( !dev_prop[0].is_empty() )
			{
				dev_prop[0] >> detector_pixel_depth;
			}
			else
			{
				THROW_YAT_ERROR("PLUGIN_ERROR","DetectorPixelDepth property does not exist","LimaDetectorTimebase::start");
			}

			if ( !dev_prop[1].is_empty() )
			{
				dev_prop[1] >> detector_trigger_mode;
			}
			else
			{
				THROW_YAT_ERROR("PLUGIN_ERROR","MemorizedTriggerMode property does not exist","LimaDetectorTimebase::start");
			}
		
			//update the exposure time with integration_time of scanserver
			if ( !config_.integration_time_attr.empty() )
			{
				Tango::DeviceAttribute dev_attr( config_.integration_time_attr, integration_time * config_.integration_time_gain );
				proxy_->write_attribute( dev_attr );
			}
			
			//only for pump & probe mode (i.e EXTERNAL_MULTI & 2 bits)
			if(detector_trigger_mode == "EXTERNAL_MULTI" && (detector_pixel_depth == "2" || detector_pixel_depth == "2A"))
			{
				//- Get the DeviceSpecificProxyName property, created manually by user
				Tango::DbData dev_prop;
				dev_prop.push_back( Tango::DbDatum("DeviceSpecificProxyName") );
				proxy_->unsafe_proxy().get_property(dev_prop);
				std::string device_specific_proxy_name;
				if ( !dev_prop[0].is_empty() )
				{
					dev_prop[0] >> device_specific_proxy_name;
				}
				else
				{
					THROW_YAT_ERROR("PLUGIN_ERROR","DeviceSpecificProxyName property does not exist","LimaDetectorTimebase::start");
				}
					
				//read the attribute triggerAcquisitionFrequency from the Device Specific Proxy , and rewrite the same value in order to refresh the computation .
				//because, writing value on triggerAcquisitionFrequency will compute a new nbFrames according to the last exposureTime done by Scanserver on the Device Generic "LimaDetector"
				m_proxy_device_specific = new Tango::DeviceProxy(device_specific_proxy_name);
				Tango::DeviceAttribute trigger_acquisition_frequency_attr = m_proxy_device_specific->read_attribute( "triggerAcquisitionFrequency" );
				
				float trigger_acq;
				trigger_acquisition_frequency_attr>> trigger_acq;
				Tango::DeviceAttribute trigger_acq_attr("triggerAcquisitionFrequency",(Tango::DevFloat)trigger_acq);
				std::cout << ">>>>>> write ("<<trigger_acq<<") in triggerAcquisitionFrequency attribute ..." << std::endl;
				m_proxy_device_specific->write_attribute( trigger_acq_attr);

				
				//- Set the nbFrame to to the value computed by triggerAcquisitionFrequency and set into nbFrames ! 
				// not really necessary now, because already done in ufxc specific .
				Tango::DeviceAttribute nb_frames_attr = proxy_->read_attribute( "nbFrames" );
				long nb_frames;
				nb_frames_attr>> nb_frames;
				Tango::DeviceAttribute nb_frames_computed_attr("nbFrames",(Tango::DevLong)nb_frames);
				std::cout << ">>>>>> write ("<<nb_frames<<") in nbFrames attribute ..." << std::endl;
				proxy_->write_attribute( nb_frames_computed_attr);
				std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
			}
			
			//start acquisition
			if ( !config_.start_cmd_name.empty() )
			{
				proxy_->command_inout( config_.start_cmd_name );
			}				
        }
		else
		{
			//if not ufxc, use standard process
			WaitingStateChangeTimebase::start(integration_time);			
		}
    }

private :
    Tango::DeviceProxy* m_proxy_device_specific;

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::LimaDetectorTimebase,
                          ScanUtils::LimaDetectorTimebaseInfo);
