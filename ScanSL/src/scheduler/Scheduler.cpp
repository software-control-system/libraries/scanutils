#include <limits>
#include <scansl/scheduler/Scheduler.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace ScanUtils
{

// ============================================================================
// Scheduler::Scheduler
// ============================================================================
Scheduler::Scheduler( const ScanConfig& config )
    : config_( config ),
      manager_( ScanManager(config) ),
      next_action_( ActionType_SAVE_ACTUATORS_PRIOR_POS ),
      scan_to_do_( config.scan_number ),
      dim1_pt_(0),
      dim2_pt_(0),
      integration_time_(0),
      progress_manager_( config ),
      afterrun_action_ondemand_(false),
      actuators_retries( config.actuators_mngt ),
      timebases_retries( config.timebases_mngt ),
      sensors_retries( config.sensors_mngt ),
      hooks_retries( config.hooks_mngt )
{
    if ( !config_.actuators.empty() )
        last_pos_.x = config_.trajectories.shape()[1] - 1;
    else
        last_pos_.x = 0;

    if ( !config_.actuators2.empty() )
        last_pos_.y = config_.trajectories2.shape()[1] - 1;
    else
        last_pos_.y = 0;

    if ( config_.actuators.empty() )
        data_.scheduler_data.scan_type = 0;
    else if ( config_.actuators2.empty() )
        data_.scheduler_data.scan_type = 1;
    else
        data_.scheduler_data.scan_type = 2;

    next_action_ = ActionType_SAVE_ACTUATORS_PRIOR_POS;
    prev_action_ = ActionType_NONE;

    // To allow the propagation of the generated file name
    manager_.data_collector->set_scan_data(&data_);
}

// ============================================================================
// Scheduler::~Scheduler
// ============================================================================
Scheduler::~Scheduler()
{
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > Scheduler::get_actuators_attributes()
{
    return manager_.data_collector->get_actuators_attributes();
}

std::vector< boost::shared_ptr<Tango::Attr> > Scheduler::get_actuators2_attributes()
{
    return manager_.data_collector->get_actuators2_attributes();
}

std::vector< boost::shared_ptr<Tango::Attr> > Scheduler::get_sensors_attributes()
{
    return manager_.data_collector->get_sensors_attributes();
}

std::vector< boost::shared_ptr<Tango::Attr> > Scheduler::get_timestamps_attributes()
{
    return manager_.data_collector->get_timestamps_attributes();
}

ScanBuffers Scheduler::get_buffers()
{
    return manager_.data_collector->get_buffers();
}

ScanData Scheduler::get_data()
{
    yat::MutexLock guard(data_mutex_);
    //- update the progress
    data_.scheduler_data.progress = progress_manager_.get_progress();
    return data_;
}




SchedulerState Scheduler::execute_action( void )
{
    // keep track of the previous action
    int prev_action = next_action_;

    SchedulerState state;
    if ( this->is_specific_action( next_action_ ) )
        state = this->handle_specific_action( );
    else
        state = this->handle_standard_action( );

    // Memorize previous action
    prev_action_ = prev_action;

    return state;
}

SchedulerState Scheduler::handle_standard_action( void )
{
    SchedulerState state;

    switch( next_action_ )
    {
    case ActionType_ABORT_ACTUATORS:
    {
        this->abort_actuators();
        next_action_ = ActionType_SAVE_ACTUATORS_PRIOR_POS;
    }
        break;
    case ActionType_SAVE_ACTUATORS_PRIOR_POS:
    {
        this->save_actuator_prior_position();
        next_action_ = ActionType_INIT_RUN;
    }
        break;
    case ActionType_INIT_RUN:
    {
        this->init_run();
        next_action_ = ActionType_INIT_RUN_SYNCDATA;
        SCAN_DEBUG << "[Scheduler] Synchronisation with data recording..." << ENDLOG;
    }
        break;
    case ActionType_INIT_RUN_SYNCDATA:
    {
        this->sync_with_datacollector( ActionType_PRERUN_HOOK );
    }
        break;
    case ActionType_PRERUN_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_PRE_RUN,
                                              ActionType_PRERUN_HOOK,
                                              ActionType_INIT_SCAN);
    }
        break;
    case ActionType_INIT_SCAN:
    {
        this->init_scan();
        this->compute_next_actuator_pos();
        next_action_ = ActionType_INIT_SCAN_SYNCDATA;
        SCAN_DEBUG << "[Scheduler] Synchronisation with data recording..." << ENDLOG;
    }
        break;
    case ActionType_INIT_SCAN_SYNCDATA:
    {
        //this->sync_with_datacollector( ActionType_PRESCAN_HOOK );
        this->sync_with_datacollector( SpecificAction_SCAN_FIRST_ACTION );
    }
        break;
    case ActionType_SENSORS_PRE_INTEGRATION:
    {
        this->sensor_pre_integration();
        next_action_ = ActionType_START_TIMEBASES;
    }
        break;
    case ActionType_START_TIMEBASES:
    {
        this->start_timebases( ActionType_WAIT_TIMEBASES );
        SCAN_INFO << "Waiting for timebases to complete integration..." << ENDLOG;
    }
        break;
    case ActionType_WAIT_TIMEBASES:
    {
        state.must_pause = this->wait_timebases(ActionType_WAIT_TIMEBASES_DELAY,
                                                ActionType_START_TIMEBASES);
    }
        break;
    case ActionType_WAIT_TIMEBASES_DELAY:
    {
        this->wait_timebases_delay();
        next_action_ = ActionType_SENSORS_POST_INTEGRATION;
    }
        break;
    case ActionType_SENSORS_POST_INTEGRATION:
    {
        this->sensor_post_integration();
        next_action_ = ActionType_POST_INTEGRATION_HOOK;
    }
        break;
    case ActionType_POST_INTEGRATION_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_INTEGRATION,
                                              ActionType_POST_INTEGRATION_HOOK,
                                              ActionType_READ_SENSORS);
    }
        break;

    case ActionType_POST_STEP_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_STEP,
                                              ActionType_POST_STEP_HOOK,
                                              ActionType_END_OF_STEP);
    }
        break;
    case ActionType_POST_SCAN_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_SCAN,
                                              ActionType_POST_SCAN_HOOK,
                                              ActionType_END_SCAN_RECORDING);
    }
        break;
    case ActionType_END_SCAN_RECORDING:
    {
        // don't check errors here : force closing of file handle etc...
        // manager_.data_collector->check_for_error();
        manager_.data_collector->end_of_scan( true );

        SCAN_DEBUG << "[Scheduler] Synchronisation with data recording..." << ENDLOG;
        next_action_ = ActionType_END_SCAN_RECORDING_SYNC;
    }
        break;
    case ActionType_END_SCAN_RECORDING_SYNC:
    {
        this->sync_with_datacollector( ActionType_END_OF_SCAN );
    }
        break;
    case ActionType_END_OF_SCAN:
    {
        scan_to_do_--;
        SCAN_DEBUG << "[Scheduler] Remaining scan to do : " << scan_to_do_ << ENDLOG;
        if (scan_to_do_ == 0)
        {
            next_action_ = ActionType_POST_RUN_HOOK;
        }
        else
        {
            //- run is not finished -> this is the last step of the current run
            state.end_of_step = true;
            state.end_of_scan = true;
            next_action_ = ActionType_INIT_SCAN;
        }
    }
        break;
    case ActionType_POST_RUN_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_RUN,
                                              ActionType_POST_RUN_HOOK,
                                              ActionType_END_RUN_RECORDING);
    }
        break;
    case ActionType_END_RUN_RECORDING:
    {
        // don't check errors here : force closing of file handle etc...
        // manager_.data_collector->check_for_error();
        manager_.data_collector->end_of_run( );

        SCAN_DEBUG << "[Scheduler] Synchronisation with data recording..." << ENDLOG;
        next_action_ = ActionType_END_RUN_RECORDING_SYNC;
    }
        break;
    case ActionType_END_RUN_RECORDING_SYNC:
    {
        this->sync_with_datacollector( ActionType_AFTER_RUN_ACTION );
    }
        break;
    case ActionType_AFTER_RUN_ACTION:
    {
        SCAN_DEBUG << "[Scheduler] AFTER_RUN_ACTION..." << ENDLOG;
        this->after_run_action( config_.after_run_action );
        next_action_ = ActionType_WAIT_ACTUATORS_AFTER_ACTION;
    }
        break;
    case ActionType_WAIT_ACTUATORS_AFTER_ACTION:
    {
        SCAN_DEBUG << "[Scheduler] WAIT_ACTUATORS_AFTER_ACTION..." << ENDLOG;
        state.must_pause = this->wait_actuators(ActionType_END_OF_RUN,
                                                ActionType_AFTER_RUN_ACTION,
                                                ActionType_WAIT_ACTUATORS_AFTER_ACTION);
    }
        break;
    case ActionType_END_OF_RUN:
    {
        this->sensor_after_run();
        this->timebase_after_run();
        //- this is always the end of the last scan of the run when we are here
        state.end_of_step = true;
        state.end_of_scan = true;
        state.end_of_run  = true;
    }
        break;
    }
    return state;
}

void Scheduler::execute_afterrun_action( const AfterRunActionDesc& action_desc )
{
    afterrun_action_ondemand_ = true;
    this->after_run_action( action_desc );
    next_action_ = ActionType_WAIT_ACTUATORS_AFTER_ACTION;
}

void Scheduler::hard_abort_datacollector( void )
{
    SCAN_INFO << "Stop recording session on the DataRecorder device" << ENDLOG;
    manager_.data_collector->on_end_of_run();
}

void Scheduler::abort_scan_on_error( void )
{
    try
    {
        if (this->config_.hw_continuous)
            this->manager_.continuous_timebase_manager->abort();
        else
            this->manager_.timebase_manager->abort();
    }
    catch( yat::Exception& ex )
    {
        LOG_EXCEPTION_ERROR( ex );
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown exception caught while stoping timebases" << ENDLOG;
    }

    try
    {
        // don't check errors here : force closing of file handle etc...
        // manager_.data_collector->check_for_error();
        manager_.data_collector->end_of_scan( false );
    }
    catch( yat::Exception& ex )
    {
        LOG_EXCEPTION_ERROR( ex );
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown exception caught while finalizing scan" << ENDLOG;
    }

    try
    {
        // don't check errors here : force closing of file handle etc...
        // manager_.data_collector->check_for_error();
        manager_.data_collector->end_of_run();
    }
    catch( yat::Exception& ex )
    {
        LOG_EXCEPTION_ERROR( ex );
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown exception caught while finalizing run" << ENDLOG;
    }

    //- execute PostScan & PostRun Hook without error handling
    try
    {
        manager_.hooks_manager->execute( HookType_POST_SCAN );
    }
    catch( Tango::DevFailed& df )
    {
        _TANGO_TO_YAT_EXCEPTION(df, e);
        LOG_EXCEPTION_ERROR( e );
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown exception caught while executing PostScan hooks" << ENDLOG;
    }

    try
    {
        manager_.hooks_manager->execute( HookType_POST_RUN );
    }
    catch( Tango::DevFailed& df )
    {
         _TANGO_TO_YAT_EXCEPTION(df, e);
         LOG_EXCEPTION_ERROR( e );
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown exception caught while executing PostRun hooks" << ENDLOG;
    }

}

void Scheduler::abort_actuators( void )
{
    manager_.actuator_manager->abort();
    manager_.actuator_manager->restore_speed();
}

void Scheduler::abort_sensors( void )
{
    manager_.sensor_manager->abort();
}

void Scheduler::save_actuator_prior_position( void )
{
    SCAN_INFO << "Saving actuator prior positions..." << ENDLOG;

    //- cannot use actuator_manager->read() since the actuator manager will read the spectrum value
    //- of actuators instead of the "real" actuator (the scalar)

    std::vector< std::string > all_actuators;

    std::copy(config_.actuators.begin(),
              config_.actuators.end(),
              std::back_inserter(all_actuators) );

    std::copy(config_.actuators2.begin(),
              config_.actuators2.end(),
              std::back_inserter(all_actuators) );

    std::vector<double> prior_positions;

    for( std::size_t i = 0; i < all_actuators.size(); ++i )
    {
        Tango::AttributeProxy att_proxy(all_actuators[i]);
        prior_positions.push_back( Util::instance().extract_scalar_as_double( att_proxy.read(), att_proxy.get_config() ) );
    }

    manager_.data_collector->check_for_error();
    manager_.data_collector->save_prior_actuator_data( prior_positions );
}

void Scheduler::init_run( void )
{
    manager_.data_collector->check_for_error();
    manager_.data_collector->init_run();
    progress_manager_.run_start();
}

void Scheduler::init_scan( void )
{
    SCAN_INFO << "Initializing scan..." << ENDLOG;
    manager_.actuator_manager->validate_trajectories();
    manager_.data_collector->check_for_error();
    manager_.data_collector->init_scan();
    progress_manager_.scan_start();

    data_.scheduler_data.scan_nb = config_.scan_number - scan_to_do_;

    dim1_pt_ = 0;
    dim2_pt_ = 0;
    step_pos_ = ScanPosition(-1, -1);
}

void Scheduler::init_step( void )
{
    this->actuators_retries.reinit();
    this->timebases_retries.reinit();
    this->sensors_retries.reinit();
    this->hooks_retries.reinit();

    manager_.data_collector->check_for_error();
    manager_.data_collector->set_point_nb( dim1_pt_, dim2_pt_ );
    if (dim1_pt_ == 0)
    {
        manager_.data_collector->check_for_error();
        manager_.data_collector->begin_of_dim();
    }

    SCAN_INFO << "--------------------------------------------------------------------------------" << ENDLOG;
    SCAN_INFO << "Beginning of step [ " << dim1_pt_ + 1 << " , " << dim2_pt_ + 1 << " ]" << ENDLOG;
}

void Scheduler::move_actuators( void )
{
    SCAN_INFO << "Moving actuators..." << ENDLOG;
    if ( step_pos_.y == prestep_pos_.y )
    {
        //- move only one dimension
        if ( !config_.actuators.empty() )
        {
            this->move_dim1_actuators( step_pos_ );
        }
    }
    else
    {
        //- move all dimension
        if ( !config_.actuators.empty() || !config_.actuators2.empty() )
        {
            this->move_all_actuators( step_pos_ );
        }
    }
}

void Scheduler::move_actuators1( void )
{
    SCAN_INFO << "Moving dim1 actuators..." << ENDLOG;
    this->move_dim1_actuators( step_pos_ );
}

void Scheduler::move_actuators2( void )
{
    SCAN_INFO << "Moving dim2 actuators..." << ENDLOG;
    if ( step_pos_.y != prestep_pos_.y )
    {
        //- move all dimension
        if ( !config_.actuators2.empty() )
        {
            this->move_dim2_actuators( step_pos_ );
        }
    }
}

bool Scheduler::wait_actuators ( int next_action_on_success,
                                 int next_action_on_failure,
                                 int waiting_action )
{
    bool must_pause = false;
    try
    {
        actuators_retries.consume();
        bool completed = manager_.actuator_manager->move_request_completed();
        // SCAN-480 execute error_check if and only if the method move_request_completed
        // (test only if each motor is still moving) returns true
        // manager_.actuator_manager->error_check();
        if (completed)
        {
            // SCAN-480
            manager_.actuator_manager->error_check();

            SCAN_DEBUG << "[Scheduler] Move request completed" << ENDLOG;
            next_action_ = next_action_on_success;
        }
        else
        {
            //- sleep 1 ms
            yat::ThreadingUtilities::sleep(0, 1000000);
            next_action_ = waiting_action;
        }
        actuators_retries.reinit();
    }
    catch( yat::Exception& ex )
    {
        SCAN_ERROR << "Actuator error : " << ex.errors[0].desc << ENDLOG;
        if ( actuators_retries.more_try() )
        {
            SCAN_INFO << "Retry in " << config_.actuators_mngt.retry_timeout_s << " sec ( " << actuators_retries.retry_left << " retries left )" << ENDLOG;
            unsigned long secs     = static_cast<unsigned long>( config_.actuators_mngt.retry_timeout_s );
            unsigned long nanosecs = static_cast<unsigned long>( (config_.actuators_mngt.retry_timeout_s - secs) * 1E9 );
            yat::ThreadingUtilities::sleep(secs, nanosecs);
            next_action_ = next_action_on_failure;
        }
        else
        {
            actuators_retries.reinit();
            SCAN_INFO << "Retry count exhausted. Applying actuator error strategy : " << config_.actuators_mngt.strategy_name() << ENDLOG;
            switch ( config_.actuators_mngt.strategy )
            {
            case ErrorStrategy_IGNORE:
                SCAN_INFO << "Ignoring failure..." << ENDLOG;
                next_action_ = next_action_on_success;
                break;
            case ErrorStrategy_PAUSE:
                SCAN_INFO << "Pausing run because of actuator error ..." << ENDLOG;
                must_pause = true;
                next_action_ = next_action_on_failure;
                break;
            case ErrorStrategy_ABORT:
            default:
                SCAN_INFO << "Aborting run" << ENDLOG;
                RETHROW_YAT_ERROR( ex,
                                   "ERROR_STRATEGY",
                                   "Aborting run on actuator error",
                                   "Scheduler::handle_standard_action" );
                break;
            }
        }
    }
    return must_pause;
}


bool Scheduler::continuous_timebases_idle ( void )
{
    return manager_.continuous_timebase_manager->finished_counting();
}

void Scheduler::get_after_move_pos( bool save_values )
{
    int group_id = config_.actuators2.empty() ? ActuatorDim_1 : ActuatorDim_1 + ActuatorDim_2;
    AttrValues values = manager_.actuator_manager->read( group_id );
    if ( !config_.on_the_fly && save_values )
    {
        for( std::size_t i = 0; i < values.size(); ++i )
        {
            manager_.data_collector->check_for_error();
            manager_.data_collector->save_actuator_data(values[i]);
        }
        manager_.data_collector->check_for_error();
        manager_.data_collector->timestamp_actuators( );
    }
}

void Scheduler::wait_actuators_delay( int next_action )
{
    static tm::ptime start_waiting_delay;
    if( !config_.properties.timescan_actuators_delay &&
            config_.actuators.empty() && config_.actuators2.empty() )
    {
        // no actuators => no delay
        next_action_ = next_action;
    }
    else
    {
        if( prev_action_ != ActionType_WAIT_ACTUATORS_DELAY )
        {
            start_waiting_delay = tm::microsec_clock::universal_time();
            SCAN_INFO << "Waiting for actuators delay (" << config_.actuators_delay << " s) ..." << ENDLOG;
        }
        else
        {
            tm::ptime current_time = tm::microsec_clock::universal_time();
            double delta_sec = double((current_time - start_waiting_delay).total_milliseconds()) / 1000.0;
            if( delta_sec >= config_.actuators_delay )
            {
                next_action_ = next_action;
            }
            else
            {
                // sleep for 1ms
                yat::ThreadingUtilities::sleep(0, 1000000);
                next_action_ = ActionType_WAIT_ACTUATORS_DELAY;
            }
        }
    }
}

void Scheduler::sensor_pre_integration( void )
{
    SCAN_INFO << "Executing sensors pre-integration hooks..." << ENDLOG;
    manager_.sensor_manager->before_integration(integration_time_);
}

void Scheduler::start_timebases( int next_action_on_success )
{
    SCAN_INFO << "Starting timebases for " << integration_time_ << " sec..." << ENDLOG;
    if ( !config_.hw_continuous )
        manager_.timebase_manager->start( integration_time_ );
    else
    {
        manager_.continuous_timebase_manager->start( integration_time_, config_.hw_continuous_nbpt);
        //- sleep 100 ms (workaround to a bug of state a corriger dans le device NI6602)
        yat::ThreadingUtilities::sleep(0, 100000000);
    }

    next_action_ = next_action_on_success;
}

bool Scheduler::wait_timebases( int next_action_on_success,
                                int next_action_on_failure )
{
    bool must_pause = false;
    try
    {
        timebases_retries.consume();
        bool completed = manager_.timebase_manager->finished_counting();
        manager_.timebase_manager->error_check();
        if (completed)
        {
            // all done
            next_action_ = next_action_on_success;
            manager_.timebase_manager->reset_done_flags();
        }
        else
        {
            //- sleep 1 ms
            yat::ThreadingUtilities::sleep(0, 1000000);
            next_action_ = ActionType_WAIT_TIMEBASES;
        }
        timebases_retries.reinit();
    }
    catch( yat::Exception& ex )
    {
        SCAN_ERROR << "Timebase error : " << ex.errors[0].desc << ENDLOG;
        if ( timebases_retries.more_try() )
        {
            SCAN_INFO << "Retry in " << config_.timebases_mngt.retry_timeout_s << " sec ( " << timebases_retries.retry_left << " retries left )" << ENDLOG;
            unsigned long secs     = static_cast<unsigned long>( config_.timebases_mngt.retry_timeout_s );
            unsigned long nanosecs = static_cast<unsigned long>( (config_.timebases_mngt.retry_timeout_s - secs) * 1E9 );
            yat::ThreadingUtilities::sleep(secs, nanosecs);
            next_action_ = next_action_on_failure;
        }
        else
        {
            timebases_retries.reinit();
            SCAN_INFO << "Retry count exhausted. Applying timebase error strategy : " << config_.timebases_mngt.strategy_name() << ENDLOG;
            switch ( config_.timebases_mngt.strategy )
            {
            case ErrorStrategy_IGNORE:
                SCAN_INFO << "Ignoring failure..." << ENDLOG;
                next_action_ = next_action_on_success;
                break;
            case ErrorStrategy_PAUSE:
                SCAN_INFO << "Pausing run because of timebase error ..." << ENDLOG;
                must_pause = true;
                next_action_ = next_action_on_failure;
                break;
            case ErrorStrategy_ABORT:
            default:
                SCAN_INFO << "Aborting run" << ENDLOG;
                RETHROW_YAT_ERROR( ex,
                                   "ERROR_STRATEGY",
                                   "Aborting run on timebase error",
                                   "Scheduler::wait_timebases" );
                break;
            }
        }
    }
    return must_pause;
}

void Scheduler::wait_timebases_delay( void )
{
    double d = config_.timebases_delay;
    unsigned long d_s = static_cast<unsigned long>(d);
    unsigned long d_ns = static_cast<unsigned long>( 1E9 * (d - d_s) );
    if ( d_s > 0 || d_ns > 0 )
    {
        SCAN_INFO << "Waiting for timebases delay (" << d << " s) ..." << ENDLOG;
        yat::ThreadingUtilities::sleep(d_s, d_ns);
    }
}

void Scheduler::timebase_after_run( void )
{
    if (!config_.hw_continuous) {
        SCAN_INFO << "Executing timebases after_run..." << ENDLOG;
        manager_.timebase_manager->after_run();
    }
}

void Scheduler::sensor_post_integration( void )
{
    SCAN_INFO << "Executing sensors after-integration hooks..." << ENDLOG;
    manager_.sensor_manager->after_integration();
}


void Scheduler::sensor_after_run( void )
{
    SCAN_INFO << "Executing sensors after_run..." << ENDLOG;
    manager_.sensor_manager->after_run();
}

bool Scheduler::read_sensors( int next_action_on_success )
{
    SCAN_DEBUG << "entering read_sensors..." << ENDLOG;
    bool must_pause = false;
    AttrValues values;
    std::vector< std::string > sensors_without_value;
    try
    {
        sensors_retries.consume();
        manager_.sensor_manager->read();
        values = manager_.sensor_manager->wait_end( static_cast<size_t>(config_.sensors_mngt.timeout_s * 1E3) );

        if ( values.size() != manager_.sensor_manager->get_sensors_nb() )
        {
            // get attributes that are not in the values
            std::set<std::string> names = manager_.sensor_manager->get_sensors_names();
            for(AttrValues::const_iterator it = values.begin(), end = values.end() ; it != end ; ++it)
                names.erase(it->name);
            // format error message
            std::stringstream oss;
            oss << "The following sensors could not be read:";
            for(std::set<std::string>::const_iterator it = names.begin(), end = names.end() ; it != end ; ++it)
                oss << " " << *it;
        }

        size_t min_length = 0;
        std::vector<size_t> shape(2);
        for( std::size_t i = 0; i < values.size(); ++i )
        {
            SCAN_DEBUG << "sensor " << values[i].name << ENDLOG;
            manager_.data_collector->check_for_error();
            manager_.data_collector->save_sensor_data( values[i] );

            if ( config_.hw_continuous )
            {
                std::map<std::string, DataCollector::DataInfo>::iterator it;
                it = manager_.data_collector->sensor_data_.find( values[i].name );
                if( it == manager_.data_collector->sensor_data_.end() )
                {
                    SCAN_DEBUG << "sensor " << values[i].name << " not found in sensor_data_" << ENDLOG;
                    it = manager_.data_collector->actuator_data_.find( values[i].name );
                }
                if( it != manager_.data_collector->actuator_data_.end() )
                {
                    SCAN_DEBUG << "sensor " << values[i].name << " Found in actuator_data_" << ENDLOG;
                    DataCollector::DataInfo& di = it->second;

                    if      (GetShape<Tango::DevShort  >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevLong   >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevDouble >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevFloat  >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevBoolean>()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevUShort >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevULong  >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevUChar  >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevLong64 >()( di.attr_info.data_type, values[i].value, shape )) {}
                    else if (GetShape<Tango::DevULong64>()( di.attr_info.data_type, values[i].value, shape )) {}

                    if (min_length == 0)
                        min_length = shape[1];
                    else
                        min_length = min_length < shape[1] ? min_length : shape[1];
                }
                else
                {
                    SCAN_DEBUG << "sensor " << values[i].name << " not found in actuator_data_" << ENDLOG;
                }
            }
        }

        if (config_.hw_continuous)
        {
            // synchronize the data sizes of all sensors & actuators
            if (min_length == 0)
                dim1_pt_ = 0;
            else
                dim1_pt_ = min_length - 1;
        }

        if ( !config_.hw_continuous )
        {
            //- we don't timestamp in the hwcontiunous case
            //- timestamp are pre-generated because we now them in advance
            manager_.data_collector->check_for_error();
            manager_.data_collector->timestamp_sensors();
        }

        const std::vector<yat::Error>& sensor_errors = manager_.sensor_manager->get_errors();
        if( sensor_errors.size() > 0 )
        {
            // error(s) occured while reading sensors
            yat::Exception e;
            e.errors = sensor_errors;
            throw e;
        }

        sensors_retries.reinit();
        next_action_ = next_action_on_success;
    }
    catch( yat::Exception& ex )
    {
        // By pass error strategies if a data recording error is raised during sensor reading
        manager_.data_collector->check_for_error();

        SCAN_ERROR << "Sensor read error!!!" << ENDLOG;
        LOG_EXCEPTION_ERROR( ex );

        // clear errors stack
        manager_.sensor_manager->clear_errors();

        if ( sensors_retries.more_try() )
        {
            SCAN_INFO << "Retry in " << config_.sensors_mngt.retry_timeout_s << " sec ( " << sensors_retries.retry_left << " retries left )" << ENDLOG;
            unsigned long secs     = static_cast<unsigned long>( config_.sensors_mngt.retry_timeout_s );
            unsigned long nanosecs = static_cast<unsigned long>( (config_.sensors_mngt.retry_timeout_s - secs) * 1E9 );
            yat::ThreadingUtilities::sleep(secs, nanosecs);
            next_action_ = ActionType_READ_SENSORS;
        }
        else
        {
            sensors_retries.reinit();
            SCAN_INFO << "Retry count exhausted. Applying sensor error strategy : " << config_.sensors_mngt.strategy_name() << ENDLOG;
            switch ( config_.sensors_mngt.strategy )
            {
            case ErrorStrategy_IGNORE:
                SCAN_INFO << "Ignoring failure..." << ENDLOG;
                sensors_without_value = config_.sensors;
                for( std::size_t i = 0; i < values.size(); ++i )
                {
                    manager_.data_collector->check_for_error();
                    manager_.data_collector->save_sensor_data( values[i] );
                    std::vector< std::string >::iterator it = sensors_without_value.begin();
                    for (; it != sensors_without_value.end(); it++)
                    {
                        if ( (*it) == values[i].name )
                        {
                            sensors_without_value.erase( it );
                            break;
                        }
                    }
                }

            /* SCAN-11
                for( std::size_t i = 0; i < sensors_without_value.size(); ++i )
                {
                    SCAN_INFO << "Sensor without value..." << ENDLOG;
                    AttrValue attr_value = AttrValue(sensors_without_value[i], std::numeric_limits<double>::signaling_NaN());
                    SCAN_INFO << "call data_collector::check_for_error..." << ENDLOG;
                    manager_.data_collector->check_for_error();
                    SCAN_INFO << "call data_collector::save_sensor_data..." << ENDLOG;
                    manager_.data_collector->save_sensor_data( attr_value );
                    SCAN_INFO << "done." << ENDLOG;
                }
            */

                manager_.data_collector->check_for_error();
                manager_.data_collector->timestamp_sensors();
                next_action_ = next_action_on_success;
                break;
            case ErrorStrategy_PAUSE:
                SCAN_INFO << "Pausing run because of sensor error ..." << ENDLOG;
                must_pause = true;
                next_action_ = ActionType_READ_SENSORS;
                break;
            case ErrorStrategy_ABORT:
            default:
                SCAN_INFO << "Aborting run" << ENDLOG;
                RETHROW_YAT_ERROR( ex,
                                   "ERROR_STRATEGY",
                                   "Aborting run on sensor error",
                                   "Scheduler::read_sensors" );
                break;
            }


            // sensor devices in error have to be desabled
            manager_.sensor_manager->disable_failed_sensors(step_pos_.x, step_pos_.y);
            manager_.sensor_manager->clear_error_flag();
        }
    }
    SCAN_DEBUG << "exiting read_sensors" << ENDLOG;
    return must_pause;
}

void Scheduler::end_of_step( void )
{
    manager_.data_collector->check_for_error();
    manager_.data_collector->end_of_step();
    progress_manager_.step_end();
}

void Scheduler::after_run_action( const AfterRunActionDesc& action_desc )
{
    SCAN_INFO << "Executing after-run action..." << ENDLOG;

    //- exit if there is no actuators
    if ( config_.actuators.empty() )
        return;

    manager_.after_run_action->configure( manager_.data_collector,
                                          manager_.actuator_manager,
                                          manager_.continuous_timebase_manager );
    manager_.after_run_action->execute( action_desc );
}




bool Scheduler::handle_hook( HookType hook_type,
                             int hook_action,
                             int next_action )
{
    const std::string hook_desc = HooksManager::get_hook_type_name( hook_type );
    bool must_pause = false;
    try
    {
        hooks_retries.consume();
        manager_.hooks_manager->execute( hook_type );
        hooks_retries.reinit();
        next_action_ = next_action;
    }
    catch( yat::Exception& ex )
    {
        SCAN_ERROR << hook_desc << " hook error : " << ex.errors[0].desc << ENDLOG;
        if ( hooks_retries.more_try() )
        {
            SCAN_INFO << "Retry in " << config_.hooks_mngt.retry_timeout_s << " sec ( " << hooks_retries.retry_left << " retries left )" << ENDLOG;
            unsigned long secs     = static_cast<unsigned long>( config_.hooks_mngt.retry_timeout_s );
            unsigned long nanosecs = static_cast<unsigned long>( (config_.hooks_mngt.retry_timeout_s - secs) * 1E9 );
            yat::ThreadingUtilities::sleep(secs, nanosecs);
            next_action_ = hook_action;
        }
        else
        {
            hooks_retries.reinit();
            SCAN_INFO << "Retry count exhausted. Applying hook error strategy : " << config_.hooks_mngt.strategy_name() << ENDLOG;
            switch ( config_.hooks_mngt.strategy )
            {
            case ErrorStrategy_IGNORE:
                SCAN_INFO << "Ignoring failure..." << ENDLOG;
                next_action_ = next_action;
                break;
            case ErrorStrategy_PAUSE:
                SCAN_INFO << "Pausing run because of PRE STEP hook error ..." << ENDLOG;
                must_pause = true;
                next_action_ = hook_action;
                break;
            case ErrorStrategy_ABORT:
            default:
                SCAN_INFO << "Aborting run" << ENDLOG;
                RETHROW_YAT_ERROR( ex,
                                   "ERROR_STRATEGY",
                                   "Aborting run on hook failure",
                                   "Scheduler::handle_hook" );
                break;
            }
        }
    }
    return must_pause;
}

void Scheduler::sync_with_datacollector( int next_action_on_success )
{
    manager_.data_collector->check_for_error();

    if (manager_.data_collector->idle())
        next_action_ = next_action_on_success;
    else
        yat::ThreadingUtilities::sleep(0, 500000); // 500 microsec
}

void Scheduler::compute_next_actuator_pos( void )
{
#define LOG( p )
#define LOGG( p )
    /*
#define LOG( p ) std::cout << p << std::endl
#define LOGG( p ) std::cout << #p << p << std::endl
*/

    prestep_pos_ = step_pos_;
    LOGG( prestep_pos_ );
    LOGG( step_pos_ );

    /*
    if ( prestep_pos_ == ScanPosition(-1, -1)
         || ScanPosition(dim1_pt_, dim2_pt_) == last_pos_ )
         */
    if ( prestep_pos_ == ScanPosition(-1, -1) )
    {
        LOGG( (prestep_pos_ == ScanPosition(-1, -1)) );
        LOGG( (prestep_pos_ == last_pos_) );
        //- first step of scan
        step_pos_ = ScanPosition(0, 0);
        LOGG( step_pos_ );
    }
    else if ( prestep_pos_.x == last_pos_.x )
    {
        LOGG( (prestep_pos_.x == last_pos_.x) );
        //- first dim complete
        //- iterate on next dim
        step_pos_ = ScanPosition(0, prestep_pos_.y + 1);
        LOGG( step_pos_ );
    }
    else
    {
        LOG( "inside 'else'" );
        if (config_.on_the_fly)
        {
            //- go directly to the last position
            step_pos_ = ScanPosition(last_pos_.x, prestep_pos_.y);
            LOGG( step_pos_ );
        }
        else
        {
            step_pos_ = ScanPosition(prestep_pos_.x + 1, prestep_pos_.y);
            LOGG( step_pos_ );
        }
    }
}

bool Scheduler::more_step( void )
{
    if (!config_.on_the_fly && !config_.hw_continuous)
    {
        if (config_.actuators.size() == 0)
        {
            //- time-scan
            return dim1_pt_ != (config_.integration_times.shape()[0] - 1);
        }
        else
        {
            return step_pos_.y != last_pos_.y
                    || dim1_pt_ != (config_.integration_times.shape()[0] - 1);
        }
    }
    else
    {
        return step_pos_ != last_pos_;
    }
}

void Scheduler::move_all_actuators( ScanPosition pos )
{
    std::vector<double> pos_to_go;
    for (size_t i = 0; i < config_.actuators.size(); i++)
    {
        size_t xpos = ( !config_.zig_zag || dim2_pt_ % 2 == 0 )
                ? pos.x
                : config_.trajectories.shape()[1] - pos.x - 1;

        pos_to_go.push_back( config_.trajectories[i][xpos] );
    }

    if ( config_.actuators2.empty() )
    {
        manager_.actuator_manager->go_to( ActuatorDim_1, pos_to_go );
        return;
    }

    for (size_t i = 0; i < config_.actuators2.size(); i++)
    {
        pos_to_go.push_back( config_.trajectories2[i][pos.y] );
    }

    manager_.actuator_manager->go_to( ActuatorDim_1 + ActuatorDim_2, pos_to_go );
}

void Scheduler::move_dim1_actuators( ScanPosition pos )
{
    std::vector<double> pos_to_go;

    for (size_t i = 0; i < config_.actuators.size(); i++)
    {
        size_t xpos = ( !config_.zig_zag || dim2_pt_ % 2 == 0 )
                ? pos.x
                : config_.trajectories.shape()[1] - pos.x - 1;


        pos_to_go.push_back( config_.trajectories[i][xpos] );
    }
    manager_.actuator_manager->go_to( ActuatorDim_1, pos_to_go );
}

void Scheduler::move_dim2_actuators( ScanPosition pos )
{
    std::vector<double> pos_to_go;

    for (size_t i = 0; i < config_.actuators2.size(); i++)
    {
        pos_to_go.push_back( config_.trajectories2[i][pos.y] );
    }
    manager_.actuator_manager->go_to( ActuatorDim_2, pos_to_go );
}

void Scheduler::set_scan_speed( void )
{
    this->manager_.actuator_manager->set_scan_speed();
}

void Scheduler::restore_speed( void )
{
    this->manager_.actuator_manager->restore_speed();
}

}
