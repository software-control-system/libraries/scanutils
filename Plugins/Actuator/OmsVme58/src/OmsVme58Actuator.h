/*!
 * \file
 * \brief    Declaration of OmsVme58Actuator class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_OMSVME58_ACTUATOR_H_
#define _SCANSERVER_OMSVME58_ACTUATOR_H_

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>

namespace ScanUtils
{
class OmsVme58ActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};

class OmsVme58Actuator : public WaitingStateChangeActuator
{
public:
    OmsVme58Actuator();
};
}

#endif
