#include "Scheduler_OnTheFly.h"

namespace ScanUtils
{

Scheduler_OnTheFly::Scheduler_OnTheFly( const ScanConfig& config )
    : Scheduler(config),
      last_move_(0)
{
}

bool Scheduler_OnTheFly::is_specific_action( int action )
{
    switch ( action )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    case ActionType_PRESCAN_HOOK:
    case SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE:
    case SpecificAction_MOVE_ACTUATOR_END_OF_LINE:
    case ActionType_WAIT_ACTUATORS:
    case ActionType_POST_ACTUATOR_MOVE_HOOK:
    case SpecificAction_END_OF_ACTUATOR_MOVE:
    case SpecificAction_READ_ACTUATORS_BEFORE_INTEGRATION:
    case ActionType_INIT_STEP:
    case ActionType_PRESTEP_HOOK:
    case ActionType_READ_SENSORS:
    case SpecificAction_READ_ACTUATORS_AFTER_INTEGRATION:
    case ActionType_END_OF_STEP:
        return true;
    default:
        return false;
    }
}

SchedulerState Scheduler_OnTheFly::handle_specific_action( void )
{
    SchedulerState state;

    switch( next_action_ )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    {
        // this action is just a delegate implemented in all Scheduler derived classes
        next_action_ = SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE;
        break;
    }
    case SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE:
    {
        this->move_actuators();
        last_move_ = SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE;
        next_action_ = ActionType_WAIT_ACTUATORS;
    }
        break;
    case SpecificAction_MOVE_ACTUATOR_END_OF_LINE:
    {
        this->set_scan_speed();
        this->move_actuators();
        last_move_ = SpecificAction_MOVE_ACTUATOR_END_OF_LINE;
        next_action_ = ActionType_INIT_STEP;
    }
        break;
    case ActionType_WAIT_ACTUATORS:
    {
        int next_action;
        if ( last_move_ == SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE
             && dim1_pt_ == 0
             && dim2_pt_ == 0 )
        {
            // mantis 12227 : prescan hook must be executed only when actuators are at their first position
            next_action = ActionType_PRESCAN_HOOK;
        }
        else
        {
            next_action = ActionType_POST_ACTUATOR_MOVE_HOOK;
        }

        state.must_pause = this->wait_actuators(next_action,
                                                last_move_,
                                                ActionType_WAIT_ACTUATORS);
    }
        break;
    case ActionType_PRESCAN_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_PRE_SCAN,
                                              ActionType_PRESCAN_HOOK,
                                              ActionType_POST_ACTUATOR_MOVE_HOOK);
    }
        break;
    case ActionType_POST_ACTUATOR_MOVE_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_ACTUATOR_MOVE,
                                              ActionType_POST_ACTUATOR_MOVE_HOOK,
                                              SpecificAction_END_OF_ACTUATOR_MOVE);
    }
        break;
    case SpecificAction_END_OF_ACTUATOR_MOVE:
    {
        if (last_move_ == SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE)
        {
            next_action_ = ActionType_INIT_STEP;
        }
        else
        {
            //- last step of line
            this->restore_speed();
            manager_.data_collector->check_for_error();
            manager_.data_collector->end_of_dim();
            if ( this->more_step() )
            {
                this->compute_next_actuator_pos();
                dim1_pt_ = 0;
                dim2_pt_ ++;
                state.end_of_step = true;
                next_action_ = SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE;
            }
            else
            {
                next_action_ = ActionType_POST_SCAN_HOOK;
            }
        }
    }
        break;
    case ActionType_INIT_STEP:
    {
        this->init_step();
        integration_time_ = config_.integration_times[ 0 ];
        next_action_ = ActionType_PRESTEP_HOOK;
    }
        break;
    case ActionType_PRESTEP_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_PRE_STEP,
                                              ActionType_PRESTEP_HOOK,
                                              SpecificAction_READ_ACTUATORS_BEFORE_INTEGRATION);
    }
        break;
    case SpecificAction_READ_ACTUATORS_BEFORE_INTEGRATION:
    {
        if ( dim1_pt_ != 0 )
        {
            this->save_actuator_values_1d();
        }
        next_action_ = ActionType_SENSORS_PRE_INTEGRATION;
    }
        break;
    case ActionType_READ_SENSORS:
    {
        state.must_pause = this->read_sensors( SpecificAction_READ_ACTUATORS_AFTER_INTEGRATION );
    }
        break;
    case SpecificAction_READ_ACTUATORS_AFTER_INTEGRATION:
    {
        if ( dim1_pt_ == 0 )
        {
            //- first point : actuators are not moving
            //- just read all actuator values
            int group_id = (dim1_pt_ == 0 && !config_.actuators2.empty()) ? (ActuatorDim_1 + ActuatorDim_2) : ActuatorDim_1;

            this->read_actuators( group_id );
        }
        else
        {
            //- only read 1d, do the mean, send the result to DataCollector
            this->read_actuators_1d_after_integration();
        }
        next_action_ = ActionType_POST_STEP_HOOK;
    }
        break;
    case ActionType_END_OF_STEP:
    {
        this->end_of_step();

        if (last_move_ == SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE)
        {
            //- 1st step : move actuator to end of line
            this->set_scan_speed();
            this->compute_next_actuator_pos();
            dim1_pt_++;
            state.end_of_step = true;
            next_action_ = SpecificAction_MOVE_ACTUATOR_END_OF_LINE;
        }
        else if ( manager_.actuator_manager->move_request_completed() )
        {
            try
            {
                manager_.actuator_manager->error_check();
                next_action_ = ActionType_POST_ACTUATOR_MOVE_HOOK;
            }
            catch( yat::Exception& ex )
            {
                SCAN_ERROR << "Actuator error : " << ex.errors[0].desc << ENDLOG;
                SCAN_INFO << "Cannot retry here. Applying actuator error strategy : " << config_.actuators_mngt.strategy_name() << ENDLOG;
                switch ( config_.actuators_mngt.strategy )
                {
                case ErrorStrategy_IGNORE:
                    SCAN_INFO << "Ignoring failure..." << ENDLOG;
                    next_action_ = ActionType_POST_ACTUATOR_MOVE_HOOK;
                    break;
                case ErrorStrategy_PAUSE:
                    SCAN_INFO << "Pausing run because of actuator error ..." << ENDLOG;
                    state.must_pause = true;
                    next_action_ = SpecificAction_MOVE_ACTUATOR_END_OF_LINE;
                    break;
                case ErrorStrategy_ABORT:
                default:
                    SCAN_INFO << "Aborting run" << ENDLOG;
                    RETHROW_YAT_ERROR( ex,
                                       "ERROR_STRATEGY",
                                       "Aborting run on actuator error",
                                       "Scheduler::handle_standard_action" );
                    break;
                }
            }
        }
        else
        {
            //- launch next step
            dim1_pt_++;
            state.end_of_step = true;
            next_action_ = ActionType_INIT_STEP;
        }
    }
        break;
    default:
    {
        //- we should never be here : end the scan immediately
        SCAN_ERROR << "Internal Error : Scan aborted" << ENDLOG;
        state.end_of_step = true;
        state.end_of_scan = true;
        state.end_of_run  = true;
    }
        break;
    }

    return state;
}


void Scheduler_OnTheFly::read_actuators( int group_id )
{
    AttrValues values = manager_.actuator_manager->read( group_id );
    for( std::size_t i = 0; i < values.size(); ++i )
    {
        manager_.data_collector->check_for_error();
        manager_.data_collector->save_actuator_data(values[i]);
    }
    manager_.data_collector->check_for_error();
    manager_.data_collector->timestamp_actuators( );
}

void Scheduler_OnTheFly::save_actuator_values_1d( void )
{
    actuators_values_before_integration_ = manager_.actuator_manager->read( ActuatorDim_1 );
}

void Scheduler_OnTheFly::read_actuators_1d_after_integration( void )
{
    AttrValues values = manager_.actuator_manager->read( ActuatorDim_1 );

    AttrValues::iterator i1, i2, iend;
    i1 = values.begin();
    i2 = actuators_values_before_integration_.begin();
    iend = values.end();

    for( ; i1 != iend; i1++, i2++ )
    {
        double mean = 0.5 * (boost::any_cast<double>((*i1).value) + boost::any_cast<double>((*i2).value));
        (*i1).value = mean;
    }
    for( std::size_t i = 0; i < values.size(); ++i )
    {
        manager_.data_collector->check_for_error();
        manager_.data_collector->save_actuator_data(values[i]);
    }
    manager_.data_collector->check_for_error();
    manager_.data_collector->timestamp_actuators( );
}

}

