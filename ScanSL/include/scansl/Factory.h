#ifndef _SCANSERVER_FACTORY_H
#define _SCANSERVER_FACTORY_H

#include <scansl/ScanSL.h>
#include <scansl/Util.h>
#include <scansl/actors/GenericActuator.h>
#include <scansl/actors/GenericSensor.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/shared_ptr.hpp>
#include <yat/plugin/PlugInManager.h>

namespace ScanUtils
{

class SCANSL_DECL Factory
{
public:
    Factory( const std::string& interface_name );
    virtual ~Factory();

    void init( std::string plugin_path_str );

protected:
    struct Entry
    {
        yat::IPlugInInfo*    info;
        yat::IPlugInFactory* factory;
    };
    
    std::string interface_name_;
    std::map< std::string,  Entry > factory_rep_;
    yat::PlugInManager manager_;
};


template<class Interface>
class DefaultFactory : public Factory
{
public:
    DefaultFactory( const std::string& interface_name )
        : Factory(interface_name)
    {}

    virtual boost::shared_ptr<Interface> create( const std::string& device_name, std::string *id = NULL)
    {
        std::string device_class = Util::instance().get_class( device_name );
        std::map< std::string,  Entry >::const_iterator it;
        it = factory_rep_.find( device_class );

        yat::OSStream oss;
        oss << "Creating " << interface_name_ << " for " << device_name << ": ";

        boost::shared_ptr<Interface> obj_ptr;

        if ( it == factory_rep_.end() )
        {
            oss << "no plugin found";
            // leaving result as a nullptr
        }
        else
        {
            oss << "using plugin " << (*it).second.info->get_plugin_id();
            yat::IPlugInFactory* factory = (*it).second.factory;

            yat::IPlugInObject* obj;
            factory->create(obj);

            Interface* typed_obj;
            try
            {
                typed_obj = static_cast<Interface*>( obj );
                if (typed_obj == 0)
                    throw std::bad_cast();
            }
            catch( std::bad_cast& )
            {
                THROW_YAT_ERROR("BAD_CAST",
                                "The created plugin object is not of the expected type",
                                "DefaultFactory::create");
            }
            if (id)
                *id = (*it).second.info->get_plugin_id();
            obj_ptr.reset( typed_obj );
        }
        SCAN_DEBUG << oss.str() << ENDLOG;
        return obj_ptr;
    }
};

template <class Interface, class GenericImpl>
class GenericFactory : public DefaultFactory<Interface>
{
public:
    GenericFactory( const std::string& interface_name )
        : DefaultFactory<Interface>(interface_name)
    {}

    boost::shared_ptr<Interface> create( const std::string& device_name, std::string *id = NULL)
    {
        boost::shared_ptr<Interface> obj_ptr;
        obj_ptr = DefaultFactory<Interface>::create(device_name, id);
        if (!obj_ptr)
            obj_ptr.reset(new GenericImpl());
        return obj_ptr;
    }

};

}

#endif
