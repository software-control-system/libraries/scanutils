file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(ImgGrabberTimebase MODULE ${sources})
target_include_directories(ImgGrabberTimebase PRIVATE ${includedirs})
target_link_libraries(ImgGrabberTimebase PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(ImgGrabberTimebase PROPERTIES OUTPUT_NAME "ImgGrabberTimebase-${PROJECT_VERSION}")
endif()

install(TARGETS ImgGrabberTimebase LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
