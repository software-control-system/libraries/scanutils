/*!
 * \file
 * \brief    Definition of UndulatorsToEnergyActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/String.h>
#include <devapi.h>
#include <scansl/logging/ScanLogAdapter.h>

namespace ScanUtils
{
  class UndulatorsToEnergyActuatorInfo : public IScanPlugInInfo
  {
  public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
      return "UndulatorsToEnergyActuator";
    }

    virtual std::string get_interface_name(void) const
    {
      return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
      return "1.1.0";
    }

  public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
      list.push_back("Apple2ToEnergy"); //- deprecated
	  list.push_back("BeamlineInVacuumUndulator"); //- deprecated
	  list.push_back("HU256ToEnergy");
      list.push_back("GMID2Energy");
    }
  }; 
  
  class UndulatorsToEnergyActuator : public WaitingStateChangeActuator
  {
  public:
    UndulatorsToEnergyActuator()
    {
      config_.move_state        = Tango::MOVING;
      config_.error_states.push_back(Tango::FAULT);
      config_.error_states.push_back(Tango::UNKNOWN);
      config_.polling_period_ms = 50;
      config_.abort_cmd_name    = ""; // no stop command
    }

    void init( std::string device_name )
    {
            device_class_ = Util::instance().get_class( device_name );
            if (device_class_ == "GMID2Energy")
            {
                config_.abort_cmd_name = "Stop";
            }

            this->WaitingStateChangeActuator::init( device_name );
    }

    void go_to( const std::vector<PositionRequest>& positions )
    {
    	if(device_class_ != "GMID2Energy")
    	{
    		this->WaitingStateChangeActuator::go_to(positions);
    	}
    	else
    	{
        //Creation of a working copy.
        std::vector<PositionRequest> positions_copy(positions);

        // Retreiving the undulator type.
        Tango::DbData db_data;
        proxy_->unsafe_proxy().get_property(undulatorTypePropName, db_data);
        if( db_data.size()>0 )
        {
          std::string undulatorTypeStr;
          db_data[0] >> undulatorTypeStr;
          yat::String undulatorType = undulatorTypeStr;
          if( undulatorType.is_equal_no_case("ELECTROMAGNET"))
          {
              //If it's an electromagnet, the polarisation must be treated apart.
              std::vector<PositionRequest> not_freezed_positions;
              std::vector<int> to_remove;
              for( std::size_t i = 0; i < positions_copy.size(); ++i )
              {
                  const PositionRequest& pos_req = positions_copy[i];
                  const Tango::AttributeInfo& attr_info = *pos_req.attribute_info;
                  yat::String attr_name = attr_info.name;
                  if( attr_name.is_equal_no_case("polarisation") || attr_name.is_equal_no_case("polarisationName") )
                  {
                    to_remove.insert(to_remove.begin(), i);
                    not_freezed_positions.push_back(positions_copy[i]);
                  }
              }
              for( std::size_t i = 0; i < to_remove.size(); ++i )
              {
                positions_copy.erase(positions_copy.begin()+to_remove[i]);
              }

              //Treating the polarisation.
              if( !not_freezed_positions.empty() )
              {
                std::vector<Tango::DeviceAttribute> dev_attrs;
                for( std::size_t i = 0; i < not_freezed_positions.size(); ++i )
                {
                    const PositionRequest& pos_req = not_freezed_positions[i];
                    dev_attrs.push_back( Util::instance().create_device_attribute(*pos_req.attribute_info, pos_req.position) );

                }
        
                for( std::size_t i = 0; i < dev_attrs.size(); ++i )
                {
                  proxy_->write_attribute( dev_attrs[i]);
                }


                //Changing the polarisation should take a while but it doesn't start immediatly
                while( !is_moving() )
                {
                  SCAN_INFO << "Waiting for polarization start to change." << config_.dev_name << ENDLOG;
                  yat::Thread::sleep(get_polling_period_ms());
                }

              }

          }
        }
        if( !positions_copy.empty() )
        {
          Tango::DeviceAttribute freeze_attr("freeze",true);
          proxy_->write_attribute( freeze_attr);
              
          // Update due to CTRLDESK-931
          std::vector<Tango::DeviceAttribute> dev_attrs;
          for( std::size_t i = 0; i < positions_copy.size(); ++i )
          {
              const PositionRequest& pos_req = positions_copy[i];
              dev_attrs.push_back( Util::instance().create_device_attribute(*pos_req.attribute_info, pos_req.position) );

          }

  
          for( std::size_t i = 0; i < dev_attrs.size(); ++i )
          {
            proxy_->write_attribute( dev_attrs[i]);
          }

          freeze_attr << false ;
          proxy_->write_attribute( freeze_attr);
        }

    	}

    }

  private :      
      std::string device_class_;
      static std::string undulatorTypePropName;

  };

  std::string UndulatorsToEnergyActuator::undulatorTypePropName = "UndulatorType";

}


EXPORT_SINGLECLASS_PLUGIN(ScanUtils::UndulatorsToEnergyActuator, \
                          ScanUtils::UndulatorsToEnergyActuatorInfo);
