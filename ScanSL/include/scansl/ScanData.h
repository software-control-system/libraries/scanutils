#ifndef _SCANSERVER_SCAN_DATA_H
#define _SCANSERVER_SCAN_DATA_H

#include <tango.h>
#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/Progress.h>
#include <boost/any.hpp>

namespace ScanUtils
{

struct SCANSL_DECL ScanBuffers
{
    ScanBuffers() : pt_nb(0), pt_nb2(0)
    {}

    long      pt_nb;
    long      pt_nb2;
    boost::shared_ptr< std::vector<double> > actuators_prior_pos;
    // IntBufPtr line_pt_nb; //- the number of point on each line of scan (can differ in on-the-fly mode)
};

struct SCANSL_DECL ScanLogs
{
    Tango::DevVarStringArray string_array;
    long x_size;
    long y_size;
};

struct SCANSL_DECL SchedulerData
{
    SchedulerData() : scan_type(0), scan_nb(0)
    {}

    long scan_type;
    long scan_nb;

    Progress progress;
};

struct SCANSL_DECL ScanData
{
    SchedulerData scheduler_data;
    ScanLogs      logs;
    
    std::string generated_file_name_;
};

}

#endif
