﻿/*!
* \file
* \brief    Definition of TangoParser class
* \author   Alain BUTEAU - Synchrotron SOLEIL
*/

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/String.h>

namespace ScanUtils
{
class TangoParserInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "TangoParser";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

//debut SCAN-541
    virtual std::string get_version_number(void) const
    {
        return "1.0.1";
    }

//fin SCAN-541

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("TangoParser");
    }
};

class TangoParser : public WaitingStateChangeActuator
{
public:
    TangoParser()
    {
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = ""; // Stop command TANGODEVIC-1495,  TANGODEVIC-1416
    }

//debut SCAN-629
// ============================================================================
// init
// ============================================================================
void init( std::string device_name )
    {
        this->WaitingStateChangeActuator::init( device_name );
	//debut SCAN-643
	//verification si la commande Stop existe dans le device
        if (check_cmd_exists("StopAll"))
       {
        config_.abort_cmd_name = "StopAll";
   		}
   		else config_.abort_cmd_name = "";	
	//fin SCAN-643	
    }//fin init

//fin SCAN-629
//debut SCAN-541
/// ============================================================================
// check_initial_condition : 
//device doit avoir les attributs NON vides suivants :
// - MovingState,
// - ScanMode.
// L attribut MovingState doit avoir un valeur predefinie,
// L attribut ScanMode doit avoir la valeur TRUE
// ============================================================================
    void check_initial_condition( std::string /*attr_name*/ ) {

        // Lire la propriete MOVINGState du device TangoPArser
        Tango::DbData dev_prop;
        dev_prop.push_back( Tango::DbDatum("MovingState") );
        proxy_->unsafe_proxy().get_property(dev_prop);

        yat::String moving_state;
        if ( !dev_prop[0].is_empty() )
        {
            dev_prop[0] >> moving_state;
        }
        else
            THROW_YAT_ERROR("PLUGIN_ERROR","MovingState property does not exist","TangoParser::check_initial_condition");

			
        //- Moving State
        if (moving_state.is_equal_no_case(yat::String("MOVING")))		config_.move_state = Tango::MOVING;
        else if(moving_state.is_equal_no_case(yat::String("RUNNING")))	config_.move_state = Tango::RUNNING;
        else if(moving_state.is_equal_no_case(yat::String("EXTRACT")))	config_.move_state = Tango::EXTRACT;
        else if(moving_state.is_equal_no_case(yat::String("OPEN")))		config_.move_state = Tango::OPEN;
        else if(moving_state.is_equal_no_case(yat::String("DISABLE")))	config_.move_state = Tango::DISABLE;
        else if(moving_state.is_equal_no_case(yat::String("ON")))		config_.move_state = Tango::ON;
        else THROW_YAT_ERROR("PLUGIN_ERROR","MovingState property value is not yet supported","TangoParser::check_initial_condition");

        //
        // Lire la propriété ScanMode du device TangoPArser
        bool scan_mode;

        Tango::DbData dev_prop1;
        dev_prop1.push_back( Tango::DbDatum("ScanMode") );

        proxy_->unsafe_proxy().get_property(dev_prop1);
        if ( !dev_prop1[0].is_empty() )
        {
            dev_prop1[0] >> scan_mode;
        }
        else
            THROW_YAT_ERROR("PLUGIN_ERROR","ScanMode property does not exist","TangoParser::check_initial_condition");

        if(scan_mode != true )
            THROW_YAT_ERROR("PLUGIN_ERROR","ScanMode property must be set to TRUE","TangoParser::check_initial_condition");

    }//fin check_initial_condition
//fin SCAN-541
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::TangoParser, \
                          ScanUtils::TangoParserInfo);
