/*!
 * \file
 * \brief    Definition of MK2Actuator class
 * \author   FL - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>


namespace ScanUtils
{

class MK2ActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "MK2Actuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("MK2");
    }
};

class MK2Actuator : public WaitingStateChangeActuator
{
public:
    MK2Actuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::ALARM);
        config_.polling_period_ms = 200;
        config_.abort_cmd_name    = "";
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::MK2Actuator,
                          ScanUtils::MK2ActuatorInfo);

