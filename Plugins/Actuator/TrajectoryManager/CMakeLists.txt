file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(TrajectoryManagerActuator MODULE ${sources})
target_include_directories(TrajectoryManagerActuator PRIVATE ${includedirs})
target_link_libraries(TrajectoryManagerActuator PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(TrajectoryManagerActuator PROPERTIES OUTPUT_NAME "TrajectoryManagerActuator-${PROJECT_VERSION}")
endif()

install(TARGETS TrajectoryManagerActuator LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
