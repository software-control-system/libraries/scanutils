/*!
 * \file
 * \brief    Declaration of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */
#ifndef _SCANSERVER_SENSOR_H_
#define _SCANSERVER_SENSOR_H_

#include <scansl/interfaces/ISensor.h>
#include <scansl/plugin/MParameters.h>

namespace ScanUtils
{
class SCANSL_DECL Sensor : public ISensor, public MParameters
{
public: //! structors
    Sensor();

    virtual ~Sensor();

public: //! ISensor implementation

    virtual void init( std::string, bool );
    
    virtual void check_initial_condition( );

    virtual void ensure_supported( std::string );

    virtual void before_integration( void );

    virtual void after_integration( void );

    virtual void before_run( void );

    virtual void after_run( void );

    virtual void abort( void );

protected:

    void check_file_generation_condition( std::string );
    
    ProxyP proxy_;
    std::string device_name_;
    
    
};
}

#endif
