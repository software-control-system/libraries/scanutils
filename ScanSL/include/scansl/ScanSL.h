#ifndef _SCANSL_H
#define _SCANSL_H

#include <tango.h>  // ensure tango is included before yat
#include <yat/CommonHeader.h>

#if defined(YAT_WIN32)
#   if defined (SCANSL_BUILD)
#     define SCANSL_DECL __declspec(dllexport)
#   else
#     define SCANSL_DECL __declspec(dllimport)
#   endif
#else
#     define SCANSL_DECL
#endif

#endif
