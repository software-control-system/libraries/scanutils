//*****************************************************************************
// Synchrotron SOLEIL
//
// NeXus Access API through DataRecorder device
//
// Création : 19/06/2008
// Auteur   : S. Poirier
//
//*****************************************************************************

// C++ stantard
#include <iostream>

// STL headers
#include <string>
#include <map>
#include <exception>

// YAT
#include "yat/time/Time.h"
#include "yat/utils/String.h"
#include "yat/utils/URI.h"
#include "yat/utils/Dictionary.h"

// YAT4Tango
#include "yat4tango/ExceptionHelper.h"
// Tango
#include <tango.h>

#include <scansl/actors/nexus4tango.h>

//----------------------------------------------------------------------------
// DataRecorderWriteLock::DataRecorderWriteLock
//----------------------------------------------------------------------------
DataRecorderWriteLock::DataRecorderWriteLock()
{
	m_lock_msg = 0;
	m_private_data = NULL;
}

//----------------------------------------------------------------------------
// DataRecorderWriteLock::Instance
//----------------------------------------------------------------------------
DataRecorderWriteLock &DataRecorderWriteLock::Instance()
{
	static DataRecorderWriteLock* unique_instance_ptr = NULL;

	if( !unique_instance_ptr )
		unique_instance_ptr = new DataRecorderWriteLock();

	return *unique_instance_ptr;
}

//----------------------------------------------------------------------------
// DataRecorderWriteLock::SetDataRecorder
//----------------------------------------------------------------------------
void DataRecorderWriteLock::SetDataRecorder(const std::string &datarecorder_name, bool is_recording_manager)
{
	if( m_private_data )
	{
		// Delete previously allocated proxy
		delete (Tango::DeviceProxy *)m_private_data;
		m_private_data = NULL;
	}

	m_is_recording_manager = is_recording_manager;

	// Try to get a proxy on datarecorder device
	Tango::DeviceProxy *pProxy = NULL;
	try
	{
		pProxy = new Tango::DeviceProxy(PSZ(datarecorder_name));
	}
	catch(Tango::DevFailed e)
	{
		throw e;
	}
	catch(...)
	{
		throw Exception("Unknow error occured when trying to get a proxy on DataRecorder device");
	}
	m_private_data = (void *)pProxy;
}

//----------------------------------------------------------------------------
// DataRecorderWriteLock::PrivRequestAccess
//----------------------------------------------------------------------------
bool DataRecorderWriteLock::PrivRequestAccess(int life_time)
{
	if( m_is_recording_manager )
	{
	  std::string file_uri;
		Tango::DeviceProxy *pProxy = (Tango::DeviceProxy *)m_private_data;
	  Tango::DeviceAttribute da = pProxy->read_attribute("currentDestination");
	  da >> file_uri;

	  try
	  {
	    if( !file_uri.empty() )
	    {
	      yat::URI uri = yat::URI(file_uri);
	      m_file_path = uri.get(yat::URI::PATH);
	      m_nxentry_name = yat::StringDictionary(uri.get(yat::URI::QUERY), ',', '=').at("entry");
	      return true;
	    }
	    else
	    	return false;
	  }
	  catch(const yat::Exception& e)
	  {
	    THROW_YAT_TO_TANGO_EXCEPTION(e);
	  }
	}
	else // Old DataRecorder
	{
		Tango::DeviceData arg_data, reply_data;

		// Prepare input arguments
		vector<long> argin_vector;
		argin_vector.push_back(life_time);
		argin_vector.push_back(m_lock_msg);
		arg_data << argin_vector;

		Tango::DeviceProxy *pProxy = (Tango::DeviceProxy *)m_private_data;
		reply_data = pProxy->command_inout("RequestWriteAccess", arg_data);

		// Response
		vector<Tango::DevLong> response_msg_vector_only_one_element; // changed by Yves Acremann; type was vector<long>
		vector<string> response_vector_nxentry_file;
		reply_data.extract(response_msg_vector_only_one_element, response_vector_nxentry_file);

		// Checking response
		if( response_msg_vector_only_one_element.size() )
		{
			m_lock_msg = response_msg_vector_only_one_element[0];
			if( response_vector_nxentry_file.size() < 2 )
				// Access is refused
				return false;
			m_file_path = response_vector_nxentry_file[0];
			m_nxentry_name = response_vector_nxentry_file[1];
			return true;
		}
		return false;
	}
}

//----------------------------------------------------------------------------
// DataRecorderWriteLock::RequestAccess
//----------------------------------------------------------------------------
bool DataRecorderWriteLock::RequestAccess(int life_time, long time_out)
{
	if( m_is_recording_manager )
	{
		return PrivRequestAccess();
	}
	else
	{
		bool access_granted = false;
		long current_time = yat::CurrentTime().long_unix();
		long end_time = current_time + time_out;
		while( current_time <= end_time && !access_granted )
		{
			access_granted = PrivRequestAccess(life_time);

			current_time = yat::CurrentTime().long_unix();
			if( time_out != 0 && !access_granted )
			{
				// wait 0.1 sec. util next try
	#ifdef WIN32
				::Sleep(100);
	#else
				usleep(100000);
	#endif
			}
		}
		return access_granted;
	}
}

//----------------------------------------------------------------------------
// DataRecorderWriteLock::ReleaseAccess
//----------------------------------------------------------------------------
void DataRecorderWriteLock::ReleaseAccess()
{
	if( !m_is_recording_manager )
	{
		Tango::DeviceData arg_data, replyData;
		arg_data << m_lock_msg;
		Tango::DeviceProxy *pProxy = (Tango::DeviceProxy *)m_private_data;
		pProxy->command_inout("ReleaseWriteAccess", arg_data);
		m_lock_msg = 0;
	}
}

