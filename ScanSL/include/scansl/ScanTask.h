#ifndef _SCANSERVER_SCAN_TASK_H
#define _SCANSERVER_SCAN_TASK_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/ScanData.h>
#include <scansl/logging/ILogReceiver.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <yat/threading/Task.h>
#include <yat4tango/CommonHeader.h>

namespace ScanUtils
{
class ScanState;
class Scheduler;
typedef boost::shared_ptr<Scheduler> SchedulerPtr;

class ContextValidation;
typedef boost::shared_ptr<ContextValidation> ContextValidationPtr;

class SCANSL_DECL ScanTask : public yat::Task, private boost::noncopyable
{
public: //! structors
    ScanTask( void );

    ~ScanTask( void );

public:
    //! get attributes list
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators2_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_sensors_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_timestamps_attributes();

    //! start a run
    void start( const ScanConfig& );

    //! pause the current run
    void pause( void );

    //! resume the current run
    void resume( void );

    //! abort the run immediately
    void abort( void );
    
    //! execute after run action
    void execute_afterrun_action( AfterRunActionDesc& );

    //! get the state and status (in a thread safe way)
    void get_state_status( Tango::DevState& state, std::string& status );

    //! get data
    ScanData get_data( void );

    //! get data
    ScanBuffers get_buffers( void );

protected:
    virtual void handle_message (yat::Message& msg);

private: //! msg handling routines
    
    void on_init( void );

    void on_exit( void );

    void on_start( const ScanConfig& );

    void on_pause( bool on_context_error );

    void on_resume( void );

    void on_execute_afterrun_action( const AfterRunActionDesc& );

    void on_check_context( void );

    void on_next_action( void );

    void on_abort_on_error( yat::Exception& );
    void on_abort_by_user( void );
    void on_complete_scan( void );

private:
    ScanConfig           config_;
    ScanState*           state_;
    SchedulerPtr         scheduler_;
    ContextValidationPtr context_validation_;
    bool                 acquisition_started_;
};

typedef boost::shared_ptr<ScanTask> ScanTaskPtr;

}

#endif
