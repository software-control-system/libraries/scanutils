#ifndef _SCANSERVER_DATA_COLLECTOR_H
#define _SCANSERVER_DATA_COLLECTOR_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/ScanData.h>
#include <scansl/actors/ContinuousTimebaseManager.h>
#include <scansl/actors/DataRecorder.h>
#include <scansl/ScanAttr.h>
#include <boost/noncopyable.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <yat/threading/Task.h>


namespace ScanUtils
{

namespace fs = boost::filesystem;
namespace dt = boost::gregorian;
namespace tm = boost::posix_time;

template <typename T>
struct GetShape
{
    bool operator()( int& data_type, boost::any& any_array, std::vector<size_t>& shape  )
    {
        if (data_type == TangoTraits<T>::type_id)
        {
            typename ScanArray<T>::ImgP& buffer = boost::any_cast<typename ScanArray<T>::ImgP&>( any_array );
            if (buffer)
            {
                shape[0] = buffer->shape()[0];
                shape[1] = buffer->shape()[1];
            }
            else
            {
                shape[0] = 0;
                shape[1] = 0;
            }
            return true;
        }
        else
            return false;
    }
};

class SCANSL_DECL DataCollector : public yat::Task,
        private boost::noncopyable
{
    friend class Scheduler;
    friend class AfterRunAction;
    friend class DataRecorder;

public:
    // some types that need to be public
    typedef enum
    {
        ACTUATOR,
        SENSOR,
        TIMESTAMP
    } DataType;

    struct DataInfo
    {
        DataType type;
        size_t scan_dim; // 1 for actuators1d & sensors, 2 for actuators2d,...
        size_t index; // idx of actuator or sensor (0-based)
        //# BEGIN PROBLEM-1454
        bool no_data; // No data to save
        bool dataset_created;
        //# END PROBLEM-1454

        int src_dims;     // data nb dims when we read it (0 for scalar, 1 for spectrum, 2 for image)
        int display_dims; // nb dims of the corresponding attributes of the ScanServer
        int storage_dims; // nb dims when we store it (should be src_dims + nb dims of scan)

        std::string attr_name;

        // JIRA EXPDATA-114: Ajouter l'alias d'attribut dans les informations
        std::string attr_alias;

        Tango::AttributeInfoEx attr_info; // the attribute info of the undernying attr to read
        boost::shared_ptr<DataSetter> datasetter;
        std::string dataset_name;
        DataInfo() : no_data(true), dataset_created(false)  {}

        // Called between each scan of a run
        void reset()
        {
          no_data = true;
          dataset_created = false;
        }
    };


public: //! structors
    DataCollector( const ScanConfig& config, ContinuousTimebaseManagerPtr cont_tb_manager );

    ~DataCollector();

public: //! modifiers

    //! get data attributes list
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators2_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_sensors_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_timestamps_attributes();

    //! do preliminary things before a new run
    void init_run( void );

    //! do preliminary things before a new scan (clean buffers, create data file...)
    void init_scan( void );

    //! tell the data collector where we are in the scan
    void set_point_nb( size_t dim1_pt, size_t dim2_pt );

    //! tell the data collector we are beginning a dimension
    void begin_of_dim( );

    //! save_prior_actuator_data
    void save_prior_actuator_data( std::vector<double>& positions );

    //! save_actuator_data
    void save_actuator_data( AttrValue& value );

    //! save_sensor_data
    void save_sensor_data( AttrValue& value );

    //! timestamp actuators
    void timestamp_actuators( void );

    //! timestamp sensors
    void timestamp_sensors( void );

    //! write the data of the current step to the data file
    void end_of_step( void );

    //! tell the DataCollector that the dimension is finished
    void end_of_dim( void );

    //! notify the DataCollector that the current scan has ended (close data file...)
    void end_of_scan( bool runsuccessfull );

    //! notify the DataCollector that the current run has ended
    void end_of_run( void );

    //! return the buffers
    ScanBuffers get_buffers( void );

    //! sets the pointer to ScanData object in order to propagate the generated file name
    void set_scan_data(ScanData* data_p) ;

    // true if there is no message in the queue
    bool idle();

    // does nothing if no error found, else throws an exception
    void check_for_error( void )
    throw(yat::Exception);

protected: //! [yat::Task implementation]
    virtual void handle_message( yat::Message& msg )
    throw(yat::Exception);

private:
    void on_init_run( void );

    void on_init_scan( void );

    void on_set_point_nb( yat::Message& msg );

    void on_begin_of_dim( void );

    void on_save_prior_actuator_data( yat::Message& msg );

    void on_save_actuator_data( yat::Message& msg );

    void on_save_sensor_data( yat::Message& msg );

    void on_end_of_step( void );

    void on_end_of_dim( void );

    void on_end_of_scan( void );

    void on_end_of_run( void );

    void clear_buffers( void );

    void register_data( const std::vector< std::string >& attr_names,
                        DataType type,
                        int scan_dim,
                        std::string data_attr_basename,
                        std::vector< boost::shared_ptr<Tango::Attr> >& attr_list,
                        std::map<std::string, DataInfo>& data_                        );

    void register_timestamps_attr( void );
    void register_relative_timestamps( void );

    void increment_msgcntr( void );
    void decrement_msgcntr( void );

    void set_error( const yat::Exception& ex );

private:

    const ScanConfig& config_;
    ScanData *scan_data_p_; // ugly cross reference !

    yat::Mutex msg_counter_mutex_;
    size_t     msg_counter_;

    class MsgCntrDecrement
    {
    public:
        MsgCntrDecrement( DataCollector* dc, size_t msg_type )
            : dc_(dc), msg_type_(msg_type)
        {}

        ~MsgCntrDecrement()
        {
            if (msg_type_ >= yat::FIRST_USER_MSG)
            {
                yat::MutexLock guard(dc_->msg_counter_mutex_);
                dc_->msg_counter_--;
                //SCAN_DEBUG << "DataCollector msg_counter = " << dc_->msg_counter_;
            }
        }
    private:
        DataCollector* dc_;
        size_t         msg_type_;
    };


    size_t scan_index_;
    size_t dim1_pt_;
    size_t dim2_pt_;

    bool continuous_;
    size_t capacity_;

    yat::Mutex  buffers_mutex_;
    ScanBuffers buffers_;

    std::map<std::string, DataInfo> actuator_data_;
    std::map<std::string, DataInfo> sensor_data_;
    std::vector< boost::shared_ptr<Tango::Attr> > actuators_attr_list_;
    std::vector< boost::shared_ptr<Tango::Attr> > actuators2_attr_list_;
    std::vector< boost::shared_ptr<Tango::Attr> > sensors_attr_list_;
    std::vector< boost::shared_ptr<Tango::Attr> > timestamps_attr_list_;

    tm::ptime init_scan_timestamp_;

    DataRecorderPtr data_recorder_;

    // Scheduler will check for errors each time before sending new commands to the thread
    bool has_error_;
    yat::Mutex errors_mutex_;
    yat::Exception last_error_;

};

typedef boost::shared_ptr<DataCollector> DataCollectorPtr;

}

#endif
