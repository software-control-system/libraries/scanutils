/*!
 * \file
 * \brief    Definition of NI6602Timebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class NI6602TimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "NI6602Timebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("NI6602");
    }
};

class NI6602Timebase : public WaitingStateChangeTimebase
{

public:
    NI6602Timebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1.0;
        config_.start_cmd_name        = "Start";
    }

    void init( std::string device_name, bool sync )
    {
      // call the super class init
      WaitingStateChangeTimebase::init( device_name, sync );
      device_name_ = device_name;
    }
    
	void before_run()
    {
      // check that we are in scalar counting mode
      Tango::DbData prop_data;
      prop_data.push_back( Tango::DbDatum("Buffered") );
      prop_data.push_back( Tango::DbDatum("IsMaster") );
      proxy_->unsafe_proxy().get_property( prop_data );
      
      // checking if 'Buffered' is false
      if ( prop_data[0].is_empty() )
      {
        std::ostringstream oss;
        oss << "The device "
            << device_name_
            << " has no property named 'Buffered'. This is abnormal and the device NI6602 is not usable as is";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }

      bool buffered_;
      prop_data[0] >> buffered_;

      // checking if 'Buffered' is false
      if (buffered_)
      {
        std::ostringstream oss;
        oss << "The device NI6602 : "
            << device_name_
            << " is configured for hardware continuous scans. The 'Buffered' property should be set to 'false' and the device should be reinitialized";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }


      // checking if 'IsMaster' is true
      if ( prop_data[1].is_empty() )
      {
        std::ostringstream oss;
        oss << "The device NI6602 : "
            << device_name_
            << " has no property named 'IsMaster'. This is abnormal and the device is not usable as is";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }

      bool is_master;
      prop_data[1] >> is_master;

      if ( !is_master )
      {
        std::ostringstream oss;
        oss << "The device NI6602 : "
            << device_name_
            << " handles a SLAVE board. You should configure only the master board in the 'Timebase' parameter";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }

      // ensures that state is STANDBY
      Tango::DevState state = proxy_->state();
      if ( state != Tango::STANDBY )
      {
        std::ostringstream oss;
        oss << "The device NI6602 : "
            << device_name_
            << " state is "
            << Tango::DevStateName[state]
            << "."
            << std::endl
            << "It should be STANDBY";

        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }
	  
      //backup ratemeter value (buffered_ property is false)
      Tango::DeviceAttribute ratemeter_attr = proxy_->read_attribute( "continuous" );
      bool ratemeter_;
      ratemeter_attr >> ratemeter_;

      if ( ratemeter_ )
      {
        std::ostringstream oss;
        oss << "The device NI6602 : "
            << device_name_
            << " is configured in ratemeter mode. You must configure it at false";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
                         oss.str().c_str(),
                         "NI6602Timebase::before_run" );
      }

      // JIRA : SPYC-162
 /*     //if sync, force ratemeter to false
      if(sync)
      {
        //force ratemeter to false
        Tango::DeviceAttribute devattr( "continuous", false );
        proxy_->write_attribute( devattr );
      }
      */
    }
    
    void after_run()
    {
    	// JIRA : SPYC-162
        /*//restore ratemeter value only when buffered is false !!
        if(buffered_== false)
        {
            Tango::DeviceAttribute devattr( "continuous", ratemeter_ );
            proxy_->write_attribute( devattr );
        }*/
    }
private: //! private implementation

    //! configuration
    std::string device_name_;

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::NI6602Timebase,
                          ScanUtils::NI6602TimebaseInfo);
