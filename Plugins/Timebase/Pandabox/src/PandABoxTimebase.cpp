/*!
 * \file
 * \brief    Definition of PandABoxTimebase class
 * \author   F. Langlois - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class PandABoxTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PandABoxTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PandABoxTimebase");
    }
};

class PandABoxTimebase : public WaitingStateChangeTimebase
{

  public:

    PandABoxTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Abort";
        config_.timeout_ms            = 10000;
        config_.integration_time_attr = "pulsePeriod";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Start";
    }

    void before_run()
    {
        //- Set the sequenceLength to 1 (TANGODEVIC-2317)
        Tango::DeviceAttribute sequenceLength_attr("sequenceLength", 1UL);
        proxy_->write_attribute( sequenceLength_attr );
    }

    void start(double integration_time)
    {
        double integ_time_ms = integration_time * config_.integration_time_gain;
        //- ECA recommands to write pulsePeriod with : integrationTime (ie pulsePeriod) plus 100 ns
        Tango::DeviceAttribute pulse_period_attr(config_.integration_time_attr, integ_time_ms + 1e-4); //- 1e-4 ms = 100 ns
        proxy_->write_attribute(pulse_period_attr);
        Tango::DeviceAttribute pulse_width_attr("pulseWidth", integ_time_ms);
        proxy_->write_attribute(pulse_width_attr);

        //- Call the Prepare command
        proxy_->command_inout("Prepare");

        //- Call the Start command
        proxy_->command_inout( config_.start_cmd_name );
    }
};
} // end namespace ScanUtils

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PandABoxTimebase,
                          ScanUtils::PandABoxTimebaseInfo);
