/*!
 * \file
 * \brief    Declaration of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */
#ifndef _SCANSERVER_WAITING_STATE_CHANGE_CONT_TIMEBASE_H_
#define _SCANSERVER_WAITING_STATE_CHANGE_CONT_TIMEBASE_H_

#include <scansl/interfaces/IPolledContinuousTimebase.h>
#include <scansl/DevProxies.h>


namespace ScanUtils
{
struct WaitingStateChangeContinuousTimebaseConfig
{
    WaitingStateChangeContinuousTimebaseConfig();

    std::string     dev_name;

    Tango::DevState counting_state;
    std::vector<Tango::DevState> error_states;
    size_t          polling_period_ms;
    int             timeout_ms;

    std::string     integration_time_attr;
    double          integration_time_gain;

    std::string     nbpoint_attr;

    std::string     start_cmd_name;
    std::string     abort_cmd_name;


};

class SCANSL_DECL WaitingStateChangeContinuousTimebase : public IPolledContinuousTimebase
{
public: //! structors
    WaitingStateChangeContinuousTimebase();

    ~WaitingStateChangeContinuousTimebase();

public: //! IPolledActuator implementation

    void   init( std::string device_name );

    void   check_initial_condition( );

    void   start( double integration_time, long nb_point );

    bool   is_counting( void );

    void   error_check( void );

    double get_polling_period_ms( void );

    void   abort( void );

protected:

    void check_file_generation_condition(std::string );

    WaitingStateChangeContinuousTimebaseConfig config_;
    ProxyP proxy_;

};
}

#endif
