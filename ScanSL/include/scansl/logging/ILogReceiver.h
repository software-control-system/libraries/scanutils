#ifndef _SCANSERVER_ILOGRECEIVER_H_
#define _SCANSERVER_ILOGRECEIVER_H_

#include <tango.h>
#include <scansl/ScanSL.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace ScanUtils
{
namespace dt = boost::gregorian;
namespace tm = boost::posix_time;

struct SCANSL_DECL FormattedLogEvent
{
    FormattedLogEvent(const log4tango::LoggingEvent& event);

    std::string timestamp;
    std::string level;
    std::string logger_name;
    std::string message;
    std::string thread_info;
};
typedef boost::shared_ptr<FormattedLogEvent> FormattedLogEventP;


struct ILogReceiver
{

    virtual ~ILogReceiver();

    virtual void append_log( FormattedLogEventP log_event ) = 0;

};


}


#endif
