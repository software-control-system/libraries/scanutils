/*!
 * \file
 * \brief    Definition of Xspress3Sensor class
 * \author   Arafat NOUREDDINE - Synchrotron SOLEIL
 */


#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
class Xspress3SensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation
    virtual std::string get_plugin_id(void) const
    {
        return "Xspress3Sensor";
    }
    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }
    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation
    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Xspress3");
    }
} ;

class Xspress3Sensor : public Sensor
{
public: //! structors
    Xspress3Sensor()
    {
    }

public: //! ISensor implementation
    virtual void init( std::string device_name, bool sync )
    {
    	// This plugin has been developped to be used with a Xspress3 in a trigger mode
        try
        {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );		
        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }

    }

    virtual void check_initial_condition( )
    {
    	Sensor::check_file_generation_condition("fileGeneration");
    }

    virtual void before_run(void)
    {
    }


    virtual void before_integration( void )
    {
        try
        {
			//- Start
			proxy_->command_inout("Snap");
        }
        catch (Tango::DevFailed& df)
        {
             throw yat4tango::TangoYATException(df);
        }

    }


    virtual void after_integration( void )
    {
		try
		{
			//- Stop
			proxy_->command_inout( "Stop" );

			//- Wait end of running State
			Tango::DevState state = Tango::RUNNING;
			while( state == Tango::RUNNING )
			{
				state = proxy_->state();
				// wait 10 msec
				yat::ThreadingUtilities::sleep( 0, 10000000 );
			}
		}
        catch (Tango::DevFailed& df)
        {
             throw yat4tango::TangoYATException(df);
        }		
    }


    virtual void abort( void )
    {
        try
        {
        	proxy_->command_inout( "Stop" );
        }
        catch (Tango::DevFailed& df)
        {
             throw yat4tango::TangoYATException(df);
        }
    }

private: //! private implementation

    //! configuration
    std::string m_current_mode;
} ;
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::Xspress3Sensor,
                          ScanUtils::Xspress3SensorInfo);
