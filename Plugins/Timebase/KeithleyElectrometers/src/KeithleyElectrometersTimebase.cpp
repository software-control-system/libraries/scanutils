/*!
 * \file
 * \brief    Definition of KeithleyElectrometersTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class KeithleyElectrometersTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "KeithleyElectrometersTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("KeithleyElectrometers");
    }
};

class KeithleyElectrometersTimebase : public WaitingStateChangeTimebase
{

public:
    KeithleyElectrometersTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 100;
        config_.abort_cmd_name        = "Abort";
        config_.timeout_ms            = 5000;
        config_.integration_time_attr = "integrationTime";
        config_.start_cmd_name        = "Start";
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::KeithleyElectrometersTimebase,
                          ScanUtils::KeithleyElectrometersTimebaseInfo);
