/*!
* \file
* \brief    Definition of MCADxpXmapSensor class
* \author   Julien Malik - Synchrotron SOLEIL
*/


#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class MCADxpXmapSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "MCADxpXmapSensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("MCADxpXmap");
    }
};



class MCADxpXmapSensor : public Sensor
{
public: //! structors

    MCADxpXmapSensor()
    {
    }

public: //! ISensor implementation

    virtual void init( std::string device_name,bool sync )
    {
            //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
    }

    virtual void before_integration( void )
    {
        //- Refresh State
        proxy_->state();
        //- Start
        proxy_->command_inout("Start");
        //- Wait state Running
        Tango::DevState state = Tango::STANDBY;
        while( state != Tango::RUNNING )
        {
            state = proxy_->state();
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
        }
    }

    virtual void after_integration( void )
    {
        //- Abort
        proxy_->command_inout( "Abort" );
        //- Wait end of running State
        Tango::DevState state = Tango::RUNNING;
        while( state == Tango::RUNNING )
        {
            state = proxy_->state();
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
        }
    }

    virtual void abort( void )
    {
        proxy_->command_inout( "Abort" );
    }

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::MCADxpXmapSensor,
                          ScanUtils::MCADxpXmapSensorInfo);
