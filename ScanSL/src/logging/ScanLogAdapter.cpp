#include <scansl/logging/ScanLogAdapter.h>

namespace ScanUtils
{

void ScanLogAdapter::init( Tango::DeviceImpl *dev )
{
    dev_ = dev;
}

ScanLogAdapter& ScanLogAdapter::instance( void )
{
    return Singleton<ScanLogAdapter>::instance();
}


log4tango::Logger* ScanLogAdapter::get_logger( void )
{
    if (dev_)
        return dev_->get_logger();
    else
        return 0;
}

}

