#include <scansl/Util.h>
#include <scansl/actors/TimebaseManager.h>
#include <scansl/actors/PolledTimebaseAdapter.h>
#include <scansl/ThreadExiter.h>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <yat/utils/String.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

ITimebase::~ITimebase()
{
}

// ============================================================================
// TFactory::TFactory
// ============================================================================
TFactory::TFactory()
    : Factory( IPolledTimebaseInterfaceName )
{
}

// ============================================================================
// TFactory::~TFactory
// ============================================================================
TFactory::~TFactory()
{
}

// ============================================================================
// TFactory::create_hard_timebase
// ============================================================================
ITimebasePtr TFactory::create_hard_timebase( std::string device_name ) const
{
    std::string device_class = Util::instance().get_class( device_name );
    std::map< std::string, Entry >::const_iterator it;
    it = factory_rep_.find( device_class );


    ITimebasePtr obj_ptr;
    if ( it == factory_rep_.end() )
    {
        THROW_YAT_ERROR("TIMEBASE_ERROR",
                        "Timebase not supported",
                        "Timebase::Factory::create_hard_timebase");
    }
    else
    {
        SCAN_DEBUG << "Creating " << interface_name_ << " for " << device_name << " : "
                   << "using plugin " << (*it).second.info->get_plugin_id() << ENDLOG;
        yat::IPlugInFactory* factory = (*it).second.factory;

        yat::IPlugInObject* obj;
        factory->create(obj);

        IPolledTimebase* typed_obj;
        try
        {
            typed_obj = static_cast<IPolledTimebase*>( obj );
            if (typed_obj == 0)
                throw std::bad_cast();
        }
        catch( std::bad_cast& )
        {
            THROW_YAT_ERROR("TIMEBASE_ERROR",
                            "Timebase not supported",
                            "TFactory::create_hard_timebase");
        }
        IPolledTimebasePtr typed_obj_ptr( typed_obj );
        obj_ptr.reset( new PolledTimebaseAdapter(typed_obj_ptr), ThreadExiter() );
    }
    return obj_ptr;
}

// ============================================================================
// TFactory::create_soft_timebase
// ============================================================================
ITimebasePtr TFactory::create_soft_timebase( void ) const
{
    return ITimebasePtr( new SoftTimebase(), ThreadExiter() );
}

// ============================================================================
// TimebaseManager::TimebaseManager
// ============================================================================
TimebaseManager::TimebaseManager( const ScanConfig& config )
    : config_(config)
{
    SCAN_DEBUG << "Initializing timebase factory..." << ENDLOG;
    boost::filesystem::path plugin_path( config.properties.plugin_root_path );
    plugin_path /= "timebase";
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
    std::string path_str = plugin_path.string();
#else
    std::string path_str = plugin_path.file_string();
#endif
    factory_.init(path_str);
    SCAN_DEBUG << "Timebase factory initialized" << ENDLOG;

    //- register hardware timebases
    this->register_hard_timebases( config.timebases );

    //- register software timebases
    if ( config.timebases.empty() )
        this->register_soft_timebase();

}

// ============================================================================
// TimebaseManager::~TimebaseManager
// ============================================================================
TimebaseManager::~TimebaseManager()
{
}

// ============================================================================
// TimebaseManager::register_timebase
// ============================================================================
void TimebaseManager::register_timebase(const std::string& name, ITimebasePtr timebase)
{
    timebase->init(name, config_.properties.synchronize_data);
    timebase->check_initial_condition();
    timebase->before_run();
    timebases_.push_back(timebase);
    timebases_names_.push_back(name);

    if( !yat::StringUtil::is_equal(name, "SOFT") )
    { // Introduce a delay before start Ni6602 timebase
        Tango::Database db;
        // Get the device class name
        std::string dev_name = name;
        std::string class_name = db.get_class_for_device( dev_name );
        SCAN_DEBUG << "timebases device class: " << class_name << ENDLOG;

        // SCAN-736
        if( class_name.find("6602") != std::string::npos ||
            yat::StringUtil::is_equal_no_case(class_name, "pulsegeneration") ||
            yat::StringUtil::is_equal_no_case(class_name, "pulsecounting") )
        {
            SCAN_DEBUG << name << " is a Ni6602(-like) device" << ENDLOG;
            is_timebases_ni6602_.push_back(true);
        }
        else
            is_timebases_ni6602_.push_back(false);
    }
    else
        is_timebases_ni6602_.push_back(false);
}

// ============================================================================
// TimebaseManager::register_hard_timebases
// ============================================================================
void TimebaseManager::register_hard_timebases ( const std::vector<std::string>& device_names )
{
    for( std::size_t i = 0; i < device_names.size(); ++i )
    {
        try
        {
            std::string name = Util::instance().resolve_device_name( device_names[i] );
            register_timebase(name, factory_.create_hard_timebase(name));
        }
        catch( yat::Exception& )
        {
            //- TODO : catch it or not ?
            throw;
        }
    }
}

// ============================================================================
// TimebaseManager::register_soft_timebase
// ============================================================================
void TimebaseManager::register_soft_timebase ( void )
{
    SCAN_DEBUG << "register SOFT timebase" << ENDLOG;
    register_timebase("SOFT", factory_.create_soft_timebase());
}

// ============================================================================
// TimebaseManager::start
// ============================================================================
void TimebaseManager::start( double integration_time )
throw( yat::Exception )
{
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        if( done_timebases_.find( timebases_names_[i] ) == done_timebases_.end() )
        {
            SCAN_DEBUG << "Starting timebase: " << timebases_names_[i] << ENDLOG;
            {
                if( config_.properties.Ni6602_start_timebase_delay > 0 &&
                    is_timebases_ni6602_[i] &&
                    timebases_.size() > 1 )
                {
                  SCAN_DEBUG << "Inserting delay because it's a Ni6602 device: " << config_.properties.Ni6602_start_timebase_delay << "ms" << ENDLOG;
                  yat::Thread::sleep(config_.properties.Ni6602_start_timebase_delay);
                }
            }
            timebases_[i]->start( integration_time );
        }
    }
}

// ============================================================================
// TimebaseManager::start
// ============================================================================
void TimebaseManager::abort( void )
throw( yat::Exception )
{
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        SCAN_INFO << "Aborting count on Timebase " << timebases_names_[i] << "..." << ENDLOG;
        try
        {
            timebases_[i]->abort();
        }
        catch( Tango::DevFailed& df )
        {
            _TANGO_TO_YAT_EXCEPTION(df, ex);
            for( std::size_t j = 0; j < ex.errors.size(); ++j )
            {
                SCAN_ERROR << ex.errors[i].desc << ENDLOG;
            }

            SCAN_ERROR << "An error occured while aborting acquisition on timebase " << timebases_names_[i] << "..." << ENDLOG;
        }
        catch( yat::Exception& ex )
        {
            for( std::size_t j = 0; j < ex.errors.size(); ++j )
            {
                SCAN_ERROR << ex.errors[j].desc << ENDLOG;
            }
            SCAN_ERROR << "An error occured while aborting acquisition on timebase " << timebases_names_[i] << "..." << ENDLOG;
        }
        catch( ... )
        {
            SCAN_ERROR << "An unknown error occured while aborting acquisition on timebase " << timebases_names_[i] << "..." << ENDLOG;
        }
    }
}

// ============================================================================
// TimebaseManager::finished_counting
// ============================================================================
bool TimebaseManager::finished_counting( void )
{
    // counting is completed unless one of the timebase is still counting
    bool still_counting = false;
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        still_counting = ( still_counting || timebases_[i]->is_counting() );
    }

    return !still_counting;
}

// ============================================================================
// TimebaseManager::error_check
// ============================================================================
void TimebaseManager::error_check( void )
{
    yat::Exception all_errors;
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        try
        {
            if( done_timebases_.find( timebases_names_[i] ) == done_timebases_.end() )
            {
                timebases_[i]->error_check();
                done_timebases_.insert( timebases_names_[i] );
            }
        }
        catch( Tango::DevFailed& df )
        {
            _TANGO_TO_YAT_EXCEPTION(df, e);
            all_errors.errors.insert( all_errors.errors.end(), e.errors.begin(), e.errors.end() );
        }
        catch( yat::Exception& e )
        {
            all_errors.errors.insert( all_errors.errors.end(), e.errors.begin(), e.errors.end() );
        }
        catch( std::exception& e )
        {
            all_errors.errors.push_back( yat::Error("ERROR", e.what(), "TimebaseManager::error_check") );
        }
        catch( ... )
        {
            all_errors.errors.push_back( yat::Error("ERROR", std::string("Unknown error occured on timebase object ") + timebases_names_[i], "TimebaseManager::error_check") );
        }
    }
    if( all_errors.errors.size() > 0 )
        throw all_errors;
}

// ============================================================================
// TimebaseManager::after_run
// ============================================================================
void TimebaseManager::after_run( void )
{
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        timebases_[i]->after_run();
    }
}

}
