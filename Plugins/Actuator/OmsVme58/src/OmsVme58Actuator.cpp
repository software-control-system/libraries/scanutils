/*!
 * \file
 * \brief    Definition of OmsVme58Actuator class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "OmsVme58Actuator.h"
#include <yat/plugin/PlugInSymbols.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::OmsVme58Actuator, \
                          ScanUtils::OmsVme58ActuatorInfo);

namespace ScanUtils
{
const std::string PlugInID                ( "OmsVme58Actuator" );
const std::string VersionNumber           ( "1.0.0" );


// ============================================================================
// OmsVme58ActuatorInfo::get_plugin_id
// ============================================================================
std::string OmsVme58ActuatorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// OmsVme58ActuatorInfo::get_interface_name
// ============================================================================
std::string OmsVme58ActuatorInfo::get_interface_name() const
{
    return IPolledActuatorInterfaceName;
}

// ============================================================================
// OmsVme58ActuatorInfo::get_version_number
// ============================================================================
std::string OmsVme58ActuatorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// OmsVme58ActuatorInfo::supported_classes
// ============================================================================
void OmsVme58ActuatorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back("OmsVme58");
}



// ============================================================================
// OmsVme58Actuator::OmsVme58Actuator
// ============================================================================
OmsVme58Actuator::OmsVme58Actuator()
{
    config_.move_state        = Tango::MOVING;
    config_.error_states.push_back(Tango::FAULT);
    config_.polling_period_ms = 50;
    config_.abort_cmd_name    = "StopMove";
}

}
