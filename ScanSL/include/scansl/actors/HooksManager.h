#ifndef _SCANSERVER_HOOKS_MANAGER_H
#define _SCANSERVER_HOOKS_MANAGER_H


#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/Factory.h>
#include <scansl/AttrValue.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>


namespace ScanUtils
{
struct IHook; //- opaque Hook interface
typedef boost::shared_ptr<IHook> IHookPtr;


class SCANSL_DECL HooksManager : private boost::noncopyable
{

public:
    HooksManager( const ScanConfig& config );
    ~HooksManager( );

    void execute( HookType ht );

    static std::string get_hook_type_name( HookType ht );

private:
    typedef std::multimap< HookType, IHookPtr > HooksMap;
    HooksMap hooks_map_;

};

typedef boost::shared_ptr<HooksManager> HooksManagerPtr;
}

#endif
