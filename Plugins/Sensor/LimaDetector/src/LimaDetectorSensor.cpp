/*!
* \file
* \brief    Definition of LimaDetectorSensor class
* \author   Julien Malik - Synchrotron SOLEIL
*/


#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/ScanConfig.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/XString.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class LimaDetectorSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "LimaDetectorSensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("LimaDetector");
    }
};


//----------------------------------------------------------------------
//-
//----------------------------------------------------------------------
class LimaDetectorSensor : public Sensor
{
public: //! structors

    LimaDetectorSensor()
    {
        m_use_shutter = true;
		m_current_frame = 0;
		m_nb_frames = 1;
    }

    virtual void init( std::string device_name,bool sync )
    {
        try
        {

            SCAN_DEBUG << "LimaDetectorSensor: in init"<< ENDLOG;

 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );

            //--------------------------------------------------
            //- Check if Command exist for Open Shutter
            std::string prop_name = "OpenShutterCommand";
            std::string open_shutter_command_fqn;

            Tango::DbData prop_data;
            proxy_->unsafe_proxy().get_property( prop_name, prop_data );

            // checking if property 'OpenShutterCommand' exist
            if ( prop_data[0].is_empty() )
            {
                //- property does not exist, but this could be normal if user does not want to use a shutter
                m_use_shutter = false;
            }
            else
            {
                prop_data[0] >> open_shutter_command_fqn;

                size_t pos = open_shutter_command_fqn.find_last_of('/');
                //- extract device name
                std::string open_shutter_device_name = open_shutter_command_fqn.substr(0, pos);
                // get DeviceProxy
                m_proxy_open_device = new Tango::DeviceProxy(open_shutter_device_name);

                //- extract command name
                m_open_shutter_command_name = open_shutter_command_fqn.substr(pos+1,open_shutter_command_fqn.size() );
            }

            //--------------------------------------------------
            //- get the total number of acquisition points
            std::size_t sbs_nbpt = 0;
            if( has_parameter("sbs_nbpt") )
            {
              std::string str  = parameter_value("sbs_nbpt");
              sbs_nbpt = std::atol(str.c_str());
			  m_nb_frames = sbs_nbpt;
            }

            //--------------------------------------------------
            //- Then Check if Command exist for Close Shutter
            prop_name = "CloseShutterCommand" ;
            std::string close_shutter_command_fqn;

            proxy_->unsafe_proxy().get_property( prop_name, prop_data );

            // checking if property 'CloseShutterCommand' exist
            if ( prop_data[0].is_empty() )
            {
                //- property does not exist, but this could be normal if user does not want to use a shutter
                m_use_shutter = false;
            }
            else
            {
                prop_data[0] >> close_shutter_command_fqn;

                size_t pos = close_shutter_command_fqn.find_last_of('/');
                //- extract device name
                string close_shutter_device_name = close_shutter_command_fqn.substr(0, pos);
                // get DeviceProxy
                m_proxy_close_device = new Tango::DeviceProxy(close_shutter_device_name);

                //- extract command name
                m_close_shutter_command_name = close_shutter_command_fqn.substr(pos+1,close_shutter_command_fqn.size() );
            }
        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }

    virtual void ensure_supported( std::string )
    {
        //- nothing to do here
        SCAN_DEBUG << "LimaDetectorSensor: in ensure_supported"<< ENDLOG;
    }

    virtual void check_initial_condition( ) {
    	Sensor::check_file_generation_condition("fileGeneration");
    }

    virtual void before_integration( void )
    {
        SCAN_DEBUG << "LimaDetectorSensor: in before_integration"<< ENDLOG;

        //- Open shutter if needed
        if(m_use_shutter == true)
        {
            //   We must send the Open Shutter command
            m_proxy_open_device->command_inout(m_open_shutter_command_name );
            //- Wait shutter to be open
            Tango::DevState state = Tango::CLOSE;
            while( state != Tango::OPEN )
            {
                // wait 20 msec
                yat::ThreadingUtilities::sleep( 0, 20000000 );
                state = m_proxy_open_device->state();
            }
        }
		
		//- manage the "INTERNAL_MULTI" trigger mode 
		Tango::DeviceAttribute trigger_mode_attr = proxy_->read_attribute( "triggerMode" );
		std::string trigger_mode;
		trigger_mode_attr>> trigger_mode;
		if(trigger_mode == "INTERNAL_MULTI")
		{
			Tango::DeviceAttribute current_frame_attr = proxy_->read_attribute( "currentFrame" );
			current_frame_attr>> m_current_frame;
			SCAN_INFO << "LimaDetectorSensor: Snap ..."<<ENDLOG;
			proxy_->command_inout( "Snap" );
		}
    }

    virtual void after_integration( void )
    {
        SCAN_DEBUG << "LimaDetectorSensor: in after_integration..."<< ENDLOG;

        //- here we assume that the integration time is elapsed
        if(m_use_shutter == true)
        {
            //   We must send the Close Shutter command
            m_proxy_close_device->command_inout(m_close_shutter_command_name );
            //- Wait shutter to be closed
            Tango::DevState state = Tango::OPEN;
            while( state != Tango::CLOSE )
            {
                // wait 20 msec
                yat::ThreadingUtilities::sleep( 0, 20000000 );
                state = m_proxy_close_device->state();
            }
        }

		//- manage the "INTERNAL_MULTI" trigger mode 
		Tango::DeviceAttribute trigger_mode_attr = proxy_->read_attribute( "triggerMode" );
		std::string trigger_mode;
		trigger_mode_attr>> trigger_mode;
		if(trigger_mode == "INTERNAL_MULTI")
		{
			//we ensure that a new frame is acquired, by checking if currentFrame is changed
			Tango::DeviceAttribute current_frame_attr = proxy_->read_attribute( "currentFrame" );
			Tango::DevULong acquired_frame;
			current_frame_attr>> acquired_frame;	
			SCAN_INFO << "LimaDetectorSensor: wait until currentFrame has changed value ..."<<ENDLOG;			
			while(acquired_frame <= m_current_frame)
			{
				current_frame_attr = proxy_->read_attribute( "currentFrame" );
				current_frame_attr>> acquired_frame;
				// wait 1 msec
				yat::ThreadingUtilities::sleep( 0, 1000000 );				
			}
			m_current_frame = acquired_frame;
		}
    }


		
	virtual void before_run( void )
    {
        SCAN_DEBUG << "LimaDetectorSensor: in before_run..."<< ENDLOG;
		//- manage the "INTERNAL_MULTI" trigger mode 
		Tango::DeviceAttribute trigger_mode_attr = proxy_->read_attribute( "triggerMode" );
		std::string trigger_mode;
		trigger_mode_attr>> trigger_mode;
		//- execute Prepare command
		if(trigger_mode == "INTERNAL_MULTI")
		{
			SCAN_INFO << "LimaDetectorSensor: Write nb_step_points into nbFrames attribute ..."<<ENDLOG;
			Tango::DeviceAttribute nb_frames_attr("nbFrames",(Tango::DevLong)m_nb_frames);
			proxy_->write_attribute( nb_frames_attr);
				
			SCAN_INFO << "LimaDetectorSensor: Prepare ..."<<ENDLOG;
			proxy_->command_inout( "Prepare" );
			m_current_frame = 0;
		}
    }

    virtual void after_run( void )
    {
        SCAN_DEBUG << "LimaDetectorSensor: in after_run..."<< ENDLOG;
 		//- manage the "INTERNAL_MULTI" trigger mode 
		Tango::DeviceAttribute trigger_mode_attr = proxy_->read_attribute( "triggerMode" );
		std::string trigger_mode;
		trigger_mode_attr>> trigger_mode;
		//- execute Stop command
		if(trigger_mode == "INTERNAL_MULTI")
		{
			SCAN_INFO << "LimaDetectorSensor: Stop ..."<<ENDLOG;
			proxy_->command_inout( "Stop" );
		}
    }

    virtual void abort( void )
    {
        SCAN_DEBUG << "LimaDetectorSensor: in abort..."<< ENDLOG;

        if(m_use_shutter == true)
        {
            m_proxy_close_device->command_inout(m_close_shutter_command_name );
        }
		
 		//- manage the "INTERNAL_MULTI" trigger mode 
		Tango::DeviceAttribute trigger_mode_attr = proxy_->read_attribute( "triggerMode" );
		std::string trigger_mode;
		trigger_mode_attr>> trigger_mode;
		//- execute Stop command
		if(trigger_mode == "INTERNAL_MULTI")
		{
			SCAN_INFO << "LimaDetectorSensor: Stop ..."<<ENDLOG;
			proxy_->command_inout( "Stop" );
		}		
    }

private: //! private implementation

    //! configuration
    std::string m_open_shutter_command_name;
    std::string m_close_shutter_command_name;

    Tango::DeviceProxy* m_proxy_open_device;
    Tango::DeviceProxy* m_proxy_close_device;

    ScanArray<double>::Buf m_integration_times;

    bool m_use_shutter;
	unsigned long m_current_frame;
	unsigned long m_nb_frames;

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::LimaDetectorSensor,
                          ScanUtils::LimaDetectorSensorInfo);
