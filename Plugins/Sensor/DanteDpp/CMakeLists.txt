file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(DanteDppSensor MODULE ${sources})
target_include_directories(DanteDppSensor PRIVATE ${includedirs})
target_link_libraries(DanteDppSensor PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(DanteDppSensor PROPERTIES OUTPUT_NAME "DanteDppSensor-${PROJECT_VERSION}")
endif()

install(TARGETS DanteDppSensor LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

