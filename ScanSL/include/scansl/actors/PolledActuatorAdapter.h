#ifndef _SCANSERVER_POLLED_ACTUATOR_ADAPTER_H
#define _SCANSERVER_POLLED_ACTUATOR_ADAPTER_H

#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/actors/IActuator.h>
#include <yat/threading/Task.h>

namespace ScanUtils
{

class SCANSL_DECL PolledActuatorAdapter : public IActuator,
        public yat::Task
{
public:
    PolledActuatorAdapter( IPolledActuatorPtr polled_actuator_obj );

    virtual void init( std::string device_name );

    virtual void check_initial_condition( std::string attr_name );

    virtual void ensure_supported( std::string attr_name );

    virtual void set_position( std::string attr_name, double position );

    virtual bool is_enabled( std::string attr_name );

    virtual void set_enabled( std::string attr_name, bool enable );

    virtual LimitsType get_limits( std::string attr_name );

    virtual void set_limits( std::string attr_name, const LimitsType& limits);

    virtual double get_speed( std::string attr_name );

    virtual void set_speed( std::string attr_name, double speed );

    virtual void go_to( const std::vector<PositionRequest>& positions );

    virtual bool is_moving( void );
    
    virtual void error_check( void );

    virtual void   abort( void );

    //! \brief set a parameter value
    void set_parameter( const std::string &name, const string &value);


protected:
    virtual void handle_message (yat::Message& msg)
    throw (yat::Exception);

private:
    void check_moving_state( void );

    std::string device_name_;

    IPolledActuatorPtr polled_actuator_obj_;

    yat::Mutex mutex_;
    bool is_moving_;


    bool has_error_;
    yat::Exception last_error_;

};

}

#endif
