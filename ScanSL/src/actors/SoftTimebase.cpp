#include <scansl/ScanSL.h>
#include <scansl/ThreadExiter.h>
#include <scansl/actors/SoftTimebase.h>
#include <boost/scoped_ptr.hpp>

namespace ScanUtils
{

const size_t COUNTING_MSG    = yat::FIRST_USER_MSG + 1;
const size_t ABORT_MSG       = yat::FIRST_USER_MSG + 2;

const size_t TIME_LOOP_LIMIT = 4;
const size_t TIME_LOOP_STEP  = 1;

SoftTimebase::SoftTimebase( )
    : is_counting_(false)

{
    this->go();
}

void SoftTimebase::init( std::string, bool )
{
}

void SoftTimebase::check_initial_condition()
{
}

void SoftTimebase::start( double integration_time )
{
    // ensures that if is_counting() is called just after this method, it will return true
    is_counting_ = true;
    timer_.restart();
    int_time_ = integration_time;

    // send the message asynchronously
    post( COUNTING_MSG );
}

bool SoftTimebase::is_counting( void )
{
    return is_counting_;
}

void SoftTimebase::error_check( void )
{
}

void SoftTimebase::abort( void )
{
    yat::Message* msg = yat::Message::allocate(ABORT_MSG, MAX_USER_PRIORITY);
    this->post( msg );
}

void SoftTimebase::handle_message (yat::Message& msg)
throw (yat::Exception)
{
    switch ( msg.type() )
    {
    case COUNTING_MSG:
    {
        double remaining_time = int_time_ - timer_.elapsed_sec();
        if( remaining_time > TIME_LOOP_LIMIT )
        {
            yat::ThreadingUtilities::sleep(TIME_LOOP_STEP);
            post(COUNTING_MSG);
        }
        else
        {   // Less than TIME_LOOP_LIMIT seconds
            if( remaining_time > 0 )
            {
                unsigned long secs     = static_cast<unsigned long>( remaining_time );
                unsigned long nanosecs = static_cast<unsigned long>( (remaining_time - secs) * 1E9 );
                yat::ThreadingUtilities::sleep(secs, nanosecs);
            }
            is_counting_ = false;
        }
    }
    break;

    case ABORT_MSG:
    {
        is_counting_ = false;
    }
    break;
    }
}

void SoftTimebase::before_run( void )
{
    //NOP
}

void SoftTimebase::after_run( void )
{
    //NOP
}
}

