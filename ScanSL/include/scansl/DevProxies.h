#ifndef _SCANSERVER_DEVPROXIES_H
#define _SCANSERVER_DEVPROXIES_H

#include <scansl/ScanSL.h>
#include <boost/shared_ptr.hpp>
#include <yat/utils/Singleton.h>
#include <yat4tango/ThreadSafeDeviceProxy.h>

namespace ScanUtils
{

typedef yat4tango::ThreadSafeDeviceProxy Proxy;
typedef boost::shared_ptr<Proxy> ProxyP;

class SCANSL_DECL DevProxies : public yat::Singleton<DevProxies>
{
public:
    //static DevProxies& instance();

    ProxyP proxy( const std::string& );

    void clear();

private:
    std::map< std::string, ProxyP > rep_;
};
}

#endif
