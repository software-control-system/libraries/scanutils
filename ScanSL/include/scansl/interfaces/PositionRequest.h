#ifndef _SCANSERVER_POSITION_REQUEST_H
#define _SCANSERVER_POSITION_REQUEST_H

#include <vector>
#include <tango.h>
#include <boost/shared_ptr.hpp>

namespace ScanUtils
{

struct PositionRequest
{
    std::string attribute_name;
    double      position;
    boost::shared_ptr<Tango::AttributeInfoEx> attribute_info;
};

typedef std::pair<std::string, std::string> LimitsType;

}

#endif
