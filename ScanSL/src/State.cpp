#include <scansl/State.h>

namespace ScanUtils
{

// ============================================================================
// State
//
// By default all implementation returns *this
// ============================================================================
ScanState::ScanState()
{
}

ScanState::~ScanState()
{
}

ScanState& ScanState::start( void )
{
    return *this;
}

ScanState& ScanState::pause( bool /* on_context_error */ )
{
    return *this;
}

ScanState& ScanState::resume( void )
{
    return *this;
}

ScanState& ScanState::abort_on_successfullscan( void )
{
    return *this;
}

ScanState& ScanState::abort_by_user( void )
{
    return *this;
}

ScanState& ScanState::abort_on_error( yat::Exception& )
{
    return *this;
}

ScanState& ScanState::end_of_point( bool )
{
    return *this;
}

ScanState& ScanState::end_of_scan( void )
{
    return *this;
}

ScanState& ScanState::end_of_run( void )
{
    return *this;
}

bool ScanState::is_next_action_allowed( void )
{
    return false;
}


// ============================================================================
// State_RUNNING
// ============================================================================
Tango::DevState State_RUNNING::get_tango_state( void )
{
    return Tango::MOVING;
}

std::string State_RUNNING::get_tango_status( void )
{
    return "Scan in progress";
}

ScanState& State_RUNNING::pause( bool on_context_error )
{
    if (on_context_error)
        return State_PAUSED_ON_CONTEXT::instance();
    else
        return State_PAUSED_UNDEFINITELY::instance();
}

ScanState& State_RUNNING::abort_on_successfullscan( void )
{
    return State_ON::instance();
}

ScanState& State_RUNNING::abort_by_user( void )
{
    return State_ABORTED_BY_USER::instance();
}

ScanState& State_RUNNING::abort_on_error( yat::Exception& ex )
{
    State_ABORTED_ON_ERROR& aborted_state = State_ABORTED_ON_ERROR::instance();
    aborted_state.set_error( ex );
    return aborted_state;
}

ScanState& State_RUNNING::end_of_point( bool manual )
{
    if ( manual )
        return State_PAUSED_UNDEFINITELY::instance();
    else
        return State_RUNNING::instance();
}

ScanState& State_RUNNING::end_of_run( void )
{
    return State_ON::instance();
}

bool State_RUNNING::is_next_action_allowed( void )
{
    return true;
}


// ============================================================================
// State_STANDBY
// ============================================================================
Tango::DevState State_STANDBY::get_tango_state( void )
{
    return Tango::STANDBY;
}

ScanState& State_STANDBY::resume( void )
{
    return State_RUNNING::instance();
}

ScanState& State_STANDBY::abort_on_successfullscan( void )
{
    return State_ON::instance();
}

ScanState& State_STANDBY::abort_by_user( void )
{
    return State_ABORTED_BY_USER::instance();
}

ScanState& State_STANDBY::abort_on_error( yat::Exception& ex )
{
    State_ABORTED_ON_ERROR& aborted_state = State_ABORTED_ON_ERROR::instance();
    aborted_state.set_error( ex );
    return aborted_state;
}

std::string State_PAUSED_UNDEFINITELY::get_tango_status( void )
{
    return "Scan is paused. Call RESUME to continue, or ABORT to end the scan.";
}

std::string State_PAUSED_ON_CONTEXT::get_tango_status( void )
{
    return "Scan is paused because context is invalid. It will RESUME automatically when context will be valid again";
}






// ============================================================================
// State_READYFORSCAN
// ============================================================================
ScanState& State_READYFORSCAN::start( void )
{
    return State_RUNNING::instance();
}



// ============================================================================
// State_ON
// ============================================================================
Tango::DevState State_ON::get_tango_state( void )
{
    return Tango::ON;
}

std::string State_ON::get_tango_status( void )
{
    return "ScanServer is ready for a new scan. Call START to begin a new scan";
}

// ============================================================================
// State_ABORTED_BY_USER
// ============================================================================
Tango::DevState State_ABORTED_BY_USER::get_tango_state( void )
{
    return Tango::ALARM;
}

std::string State_ABORTED_BY_USER::get_tango_status( void )
{
    return "The last scan has been aborted at user request. Call START to begin a new scan";
}


// ============================================================================
// State_ABORTED_ON_ERROR
// ============================================================================
Tango::DevState State_ABORTED_ON_ERROR::get_tango_state( void )
{
    return Tango::FAULT;
}

std::string State_ABORTED_ON_ERROR::get_tango_status( void )
{
    return last_error_.to_string();
}

void State_ABORTED_ON_ERROR::set_error( yat::Exception& ex )
{
    last_error_ = ex;
}


}

