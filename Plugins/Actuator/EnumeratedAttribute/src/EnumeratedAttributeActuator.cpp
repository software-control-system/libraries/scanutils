﻿/*!
* \file
* \brief    Definition of EnumeratedAttributeActuator class
* \author   Ludmila KLENOV - Synchrotron SOLEIL
*/
//debut SPYC-155
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/String.h>
#include <yat/utils/Logging.h>
#include <yat/utils/StringTokenizer.h>



namespace ScanUtils
{
class EnumeratedAttributeActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "EnumeratedAttributeActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }


    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }



public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("EnumeratedAttribute");
    }
};

class EnumeratedAttributeActuator : public WaitingStateChangeActuator
{
public:
    

    EnumeratedAttributeActuator()
    {
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.abort_cmd_name    = ""; //stop command cf TANGODEVIC-1416
	config_.selected_attr_name= "selectedAttributeName"; //nom de l attribute qui contient le nom de la position selectionnee. Utiliser dans WaitingStateChangeActuator
       
    }

// ============================================================================
// init
// ============================================================================
void init( std::string device_name )
    {
        this->WaitingStateChangeActuator::init( device_name );

	final_position = -1; //index de la liste des positions possibles
	enum_short_value = -1; //la position finale du moteur
       // Lire la propriete EnumeratedLabelList du device 	
        label_list = read_fixed_pos_list();
        //debut TANGODEVIC-1416
	//verification si la commande Stop existe dans le device
        if (check_cmd_exists("Stop"))
       {
        config_.abort_cmd_name = "Stop";
   	}
   	else config_.abort_cmd_name = "";
    //fin TANGODEVIC-1416

	//debut SPYC-298
	std::ostringstream oss; //description d erreur possible
	Tango::DeviceData get_motor_name; //recuperation de l objet qui contient le nom du moteur
   	//execution de la commande qui donne le nom du moteur
    	try {
           	get_motor_name = proxy_->command_inout("GetEnumAttributeName"); 
    	} catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                		THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "EnumeratedAttributeActuator::init");
      }
   
        //si le resultat de la commande est vide, alors erreur
        if (!get_motor_name.is_empty())
            get_motor_name >> current_value; //extraction vers le nom courant du moteur
        else {
		oss << "The device " 
            		<< config_.dev_name
            		<< " : motor name is empty";
		THROW_YAT_ERROR("PLUGIN_ERROR",
                               oss.str().c_str(),
                              "EnumeratedAttributeActuator::init");
        }
	//fin SPYC-298
    }//fin init


// ============================================================================
// read_fixed_pos_list
// ============================================================================
//debut TANGODEVIC-1413
std::vector<std::string> read_fixed_pos_list() {
	/*lecture de la liste des positions possibles dans le device.*/
    std::ostringstream oss; //description d erreur possible
    Tango::DeviceData get_label_list;
    std::vector<std::string> label_list; //la liste des positions possibles
    //execution de la commande qui donne la liste des positions
    try {
           get_label_list = proxy_->command_inout("GetEnumeratedLabelList"); 
    } catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                		THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "EnumeratedAttributeActuator::read_fixed_pos_list");
      }
      
      //extraction de la liste des positions possibles
        //si le resultat de la commande est vide, alors erreur
        if (!get_label_list.is_empty())
            get_label_list >> label_list; //extraction vers la liste
        else {
		oss << "The device " 
            		<< config_.dev_name
            		<< " : Enumerated label list is empty";
		THROW_YAT_ERROR("PLUGIN_ERROR",
                               oss.str().c_str(),
                              "EnumeratedAttributeActuator::read_fixed_pos_list");
        }
        return label_list;

}//fin read_fixed_pos_list
//fin TANGODEVIC-1413

// ============================================================================
// read_current_value
// ============================================================================
//debut TANGODEVIC-1422
std::string read_current_value(){
/*lecture de la valeur courant.
    Les etapes de lecture sont les suivants :
    1. Recuperation du contenu(la position du motor) de l attribut qui map avec le nom du motor,
    2. Retourn la position courant du motor ou vide si l attribut du meme nome n existe pas.
  */
   double current_position; //la position courante du moteur
   std::ostringstream oss; //description d erreur possible
   Tango::DeviceData get_motor_name; //recuperation du nom du moteur
  
   //recuperation de la position du motor (de l attribut qui map avec le nom du motor)
   Tango::DeviceAttribute motor_attr = proxy_->read_attribute(current_value);

   //si l attribut n est pas vide, on retourne la valeur sinon vide
   if (! motor_attr.is_empty())
   	{
      	motor_attr >> current_position;
        
        //current_position est double, conversion en string
        std::stringstream ss;
	ss << current_position; 
        return ss.str();
   	}
   else return "";   
}//fin read_current_value
//fin TANGODEVIC-1422

// ============================================================================
// go-to
// ============================================================================
void go_to(const std::vector<PositionRequest>& positions ){
     /*Envoyer le moteur a la position demandee.
       Parametre positions contient l index de la liste des positions possibles.
       Les valeurs de positions possibles : de 1 a la longeur de la liste.
     */
    	
     //recuperation de l index de la liste des positions possibles
     const PositionRequest& pos_req = positions[0];

     std::ostringstream oss; //description d erreur possible
     
     //l index de la liste des positions possibles.
	// index commence a 1, la liste commence a 0
     double pos = pos_req.position - 1; 
     final_position = pos;
     //si l index est plus petit que zero, erreur
     if(pos < 0){
	oss << "The device " 
            	<< config_.dev_name
            	<< " : Index of the EnumeratedLabelList is smaller than zero";	
        THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"EnumeratedAttributeActuator::go_to");
     }


     //si l index est plus grand que la longeur de la liste, erreur
     if((size_t)pos >= label_list.size()){	
	
	oss << "The device " 
            	<< config_.dev_name
            	<< " : Index of the EnumeratedLabelList is greater than its length";

        THROW_YAT_ERROR("PLUGIN_ERROR",
		oss.str().c_str(),
            	"EnumeratedAttributeActuator::go_to");
     }
     
     
     //ecriture dans l attribut et envoie du moteur à la position demandee.
     try {
     Tango::DeviceAttribute enum_attr(label_list[(size_t)pos].c_str(), true);
     proxy_->write_attribute(enum_attr);
	}catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "EnumeratedAttributeActuator::go_to");
        }
   
}//fin go_to


// ============================================================================
//is_moving
// ============================================================================
bool is_moving( void )
{ /*verification si le device est un mouvement.
	:retourne :: True si le device est un mouvement,
		    	 False sinon.
	*/
	bool result_is_moving; //le resultat de la methode
    std::ostringstream oss; //description d erreur possible

    Tango::DevState current_state = proxy_->state();
    
    //si l index de la liste des positions possibles <0 ou 
    //plus grand que la longeur de la liste des positions possibles
    //alors device n est pas un mouvement
    if (final_position < 0 || final_position >= label_list.size()){
       result_is_moving = false;
   }
   //verification si device est en mouvement
    else{
    
		//lecture de l attribut(qui donne le numero de la position finale)  du device  
		Tango::DeviceAttribute enumvalue_attr;
		try{
			enumvalue_attr = proxy_->read_attribute("EnumShortValue");
		} catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                		THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "EnumeratedAttributeActuator::is_moving");
		}//fin catch

		//si l attribut n est pas vide, on recuper la valeur sinon erreur
   		if (! enumvalue_attr.is_empty())
   			{
      			enumvalue_attr >> enum_short_value;
        
   			}
   		else {
			oss << "The device " 
            		<< config_.dev_name
            		<< " : enum short value is empty";
						THROW_YAT_ERROR("PLUGIN_ERROR",
                               oss.str().c_str(),
                              "EnumeratedAttributeActuator::is_moving");
        	}

 		//comparaison de la valeur de destination finale avec la valeur courant de l attribut du device
		//si la valeur de destination finale egale a la valeur de l attribut du device
		//device n est pas en mouvement

 		if(final_position == double(enum_short_value) )
 		{
   			result_is_moving = false;
 		}
		else
 		{
    		result_is_moving = true;
 		}
	}//fin else
 	return result_is_moving;
}//fin is_moving

//debut TANGODEVIC-1617
// ============================================================================
// abort
// ============================================================================
void abort( void )
{
    if ( ! config_.abort_cmd_name.empty() )
	{
        proxy_->command_inout( config_.abort_cmd_name );
		//on donne la valeur initial. On indique que le device n est plus en mouvement 
		final_position = -1;
	}
}
//fin TANGODEVIC-1617
// ============================================================================
// get_limits
// ============================================================================
LimitsType get_limits( std::string attr_name )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "EnumeratedAttributeActuator::get_limits" );
}//fin get_limits

// ============================================================================
// set_limits
// ============================================================================
void set_limits( std::string attr_name, const LimitsType& limits )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "EnumeratedAttributeActuator::set_limits" );
}//fin set_limits

protected:
	double final_position; //index de la liste des positions possibles
        std::vector<std::string> label_list;//list des positions possibles dans le device
	std::string current_value; // le nom de l attribut 
	short enum_short_value; //la position finale du motor
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::EnumeratedAttributeActuator, \
                          ScanUtils::EnumeratedAttributeActuatorInfo);
//fin SPYC-155
