#ifndef _SCANSERVER_CONTINUOUSTIMEBASE_MANAGER_H
#define _SCANSERVER_CONTINUOUSTIMEBASE_MANAGER_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/Factory.h>
#include <scansl/AttrValue.h>
#include <scansl/interfaces/IPolledContinuousTimebase.h>
#include <scansl/actors/IContinuousTimebase.h>
#include <boost/noncopyable.hpp>

namespace ScanUtils
{
class SCANSL_DECL ContTBFactory : public Factory
{
public:
    ContTBFactory();

    ~ContTBFactory();

    IContinuousTimebasePtr create_hard_timebase( std::string device_name ) const;
};

class SCANSL_DECL ContinuousTimebaseManager : private boost::noncopyable
{
public: //! structors

    ContinuousTimebaseManager( const ScanConfig& config );

    ~ContinuousTimebaseManager( );

public: //! modifers

    //! register a continuous scan device
    void register_hard_timebases ( const std::vector<std::string>& device_names );

    //! given an actuator device, get the corresponding spectrum sensor
    std::string get_actuator_value_attr( std::string actuator_device );
    
    //! start all the timebases
    void start( double integration_time, long nb_points )
    throw ( yat::Exception );

    //! true if timebases are in standby
    bool finished_counting( void );

    //! abort immediately the acquisition
    void abort( void )
    throw ( yat::Exception );
    
    // throws if any error occurred, does nothing otherwise
    void error_check( void );

private:
    //! the factory used to create the IContinuousTimebase implementation
    ContTBFactory factory_;

    //! the timebase objects list
    std::vector<IContinuousTimebasePtr> timebases_;
    std::vector<std::string> timebases_names_;
    std::vector<bool> is_timebases_ni6602_;

    //! map of <actuator attribute, actuator value attribute>
    std::map<std::string, std::string> actuators_values_;

    const ScanConfig& config_;



};
typedef boost::shared_ptr<ContinuousTimebaseManager> ContinuousTimebaseManagerPtr;

}

#endif
