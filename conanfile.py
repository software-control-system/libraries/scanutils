from conan import ConanFile

class scanutilsRecipe(ConanFile):
    name = "scanutils"
    version = "3.5.19"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"

    license = "GPL-2"
    author = "Stéphane Poirier", "Julien Berthault", "Sandra Pierre-Joseph Zephir"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/scanutils.git"
    description = "scanutils library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "ScanSL/*", "Plugins/*"

    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("nexuscpp/[>=1.0]@soleil/stable")
        self.requires("boostlibraries/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")

    def package_info(self):
        self.cpp_info.components["scansl"].set_property("cmake_target_name", "scanutils::scansl")
        self.cpp_info.components["scansl"].libs = ["scansl"]
