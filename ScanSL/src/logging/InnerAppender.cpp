#include <iomanip>
#include <scansl/logging/InnerAppender.h>
#include <yat/threading/Thread.h>

#define LOG_NUMBER_LIMIT 2048

namespace ScanUtils
{

FormattedLogEvent::FormattedLogEvent(const log4tango::LoggingEvent& event)
{
    //- TIMESTAMP
    this->timestamp = tm::to_iso_extended_string( tm::microsec_clock::local_time() );

    //- LEVEL
    this->level = log4tango::Level::get_name(event.level);

    //- LOGGER_NAME
    this->logger_name = event.logger_name;

    //- MESSAGE
    this->message = event.message;

    //- THREAD_INFO
    std::ostringstream oss_tid;
    oss_tid << std::hex << event.thread_id;
    this->thread_info = oss_tid.str();
}


ILogReceiver::~ILogReceiver()
{
}


InnerAppender::InnerAppender (const std::string& name,
                              bool open_connection)
    : log4tango::Appender(name)
{
    if( open_connection )
    {
        reopen();
    }
}

InnerAppender::~InnerAppender ()
{
    close();
}

bool InnerAppender::requires_layout (void) const
{
    return false;
}

void InnerAppender::set_layout (log4tango::Layout*)
{
    // no-op
}

bool InnerAppender::is_valid (void) const
{
    return true;
}

int InnerAppender::_append (const log4tango::LoggingEvent& event)
{
    //------------------------------------------------------------
    //- DO NOT LOG FROM THIS METHOD !!!
    //------------------------------------------------------------
    try
    {
        yat::MutexLock scoped_lock(mutex);
        logs.push_front( FormattedLogEventP(new FormattedLogEvent(event)) );
        if (logs.size() > LOG_NUMBER_LIMIT)
            logs.pop_back();
    }
    catch (...) {
        close();
        return -1;
    }
    return 0;
}

bool InnerAppender::reopen (void)
{
    bool result = true;
    try {
        close();
        try {
            this->reinit_logs();
        }
        catch (...) {

        }
    }
    catch (...) {
        close();
        result = false;
    }
    return result;
}

void InnerAppender::close (void)
{
    this->reinit_logs();
}

void InnerAppender::get_logs( Logs& _logs )
{
    yat::MutexLock scoped_lock(mutex);
    _logs = this->logs;
}

void InnerAppender::reinit_logs( void )
{
    yat::MutexLock scoped_lock(mutex);

    //- empty the log list
    // Logs empty;
    logs.clear() ;
}


} // namespace ScanServer_ns
