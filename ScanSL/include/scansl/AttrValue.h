#ifndef _SCANSERVER_ATTRVALUE_H
#define _SCANSERVER_ATTRVALUE_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <boost/any.hpp>
#include <yat/CommonHeader.h>

namespace ScanUtils
{
struct SCANSL_DECL AttrValue
{
    AttrValue(const std::string& s, boost::any val, bool b=false)
        : name(s), value(val), no_value(b)
    {
    }

    std::string name;
    boost::any  value;
    bool no_value;
};

typedef std::vector<AttrValue> AttrValues;

}

#endif
