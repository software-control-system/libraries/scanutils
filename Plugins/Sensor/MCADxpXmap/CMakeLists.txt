file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(MCADxpXmapSensor MODULE ${sources})
target_include_directories(MCADxpXmapSensor PRIVATE ${includedirs})
target_link_libraries(MCADxpXmapSensor PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(MCADxpXmapSensor PROPERTIES OUTPUT_NAME "MCADxpXmapSensor-${PROJECT_VERSION}")
endif()

install(TARGETS MCADxpXmapSensor LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
