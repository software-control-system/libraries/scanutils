#include <scansl/scheduler/SchedulerFactory.h>
#include "Scheduler_Discrete.h"
#include "Scheduler_OnTheFly.h"
#include "Scheduler_HWContinuous.h"

namespace ScanUtils
{

SchedulerPtr SchedulerFactory::create( const ScanConfig& config )
{
    SchedulerPtr scheduler;

    if ( config.hw_continuous )
        scheduler.reset( new Scheduler_HWContinuous( config ) );
    else if ( config.on_the_fly )
        scheduler.reset( new Scheduler_OnTheFly( config ) );
    else
        scheduler.reset( new Scheduler_Discrete( config ) );

    return scheduler;
}

}
