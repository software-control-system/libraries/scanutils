#include "Scheduler_Discrete.h"

namespace ScanUtils
{

Scheduler_Discrete::Scheduler_Discrete( const ScanConfig& config )
    : Scheduler(config)
{
}

bool Scheduler_Discrete::is_specific_action( int action )
{
    switch ( action )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    case ActionType_PRESCAN_HOOK:
    case ActionType_INIT_STEP:
    case ActionType_PRESTEP_HOOK:
    case ActionType_MOVE_ACTUATORS:
    case ActionType_MOVE_ACTUATORS1:
    case ActionType_MOVE_ACTUATORS2:
    case ActionType_WAIT_ACTUATORS:
    case ActionType_WAIT_ACTUATORS1:
    case ActionType_WAIT_ACTUATORS2:
    case ActionType_WAIT_ACTUATORS_DELAY:
    case SpecificAction_READ_ACTUATORS:
    case ActionType_POST_ACTUATOR_MOVE_HOOK:
    case ActionType_READ_SENSORS:
    case ActionType_END_OF_STEP:
        return true;
    default:
        return false;
    }
}


SchedulerState Scheduler_Discrete::handle_specific_action( void )
{
    SchedulerState state;

    switch( next_action_ )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    {
        // this action is just a delegate implemented in all Scheduler derived classes
        next_action_ = ActionType_INIT_STEP;
        break;
    }
    case ActionType_INIT_STEP:
    {
        integration_time_ = config_.integration_times[ dim1_pt_ ];
        this->init_step();
        next_action_ = ActionType_PRESTEP_HOOK;
    }
        break;
    case ActionType_PRESTEP_HOOK:
    {
        if( !config_.properties.move_dim1_then_dim2_actuators || config_.actuators2.empty() )
        {
            state.must_pause = this->handle_hook( HookType_PRE_STEP,
                                                  ActionType_PRESTEP_HOOK,
                                                  ActionType_MOVE_ACTUATORS);
        }
        else
        {
            state.must_pause = this->handle_hook( HookType_PRE_STEP,
                                                  ActionType_PRESTEP_HOOK,
                                                  ActionType_MOVE_ACTUATORS1);
        }

    }
        break;
    case ActionType_MOVE_ACTUATORS:
    {
        this->move_actuators();
        next_action_ = ActionType_WAIT_ACTUATORS;
    }
        break;
    case ActionType_MOVE_ACTUATORS1:
    {
        this->move_actuators1();
        next_action_ = ActionType_WAIT_ACTUATORS1;
    }
        break;
    case ActionType_MOVE_ACTUATORS2:
    {
        this->move_actuators2();
        next_action_ = ActionType_WAIT_ACTUATORS2;
    }
        break;
    case ActionType_WAIT_ACTUATORS:
    {
        state.must_pause = this->wait_actuators(ActionType_WAIT_ACTUATORS_DELAY,
                                                ActionType_MOVE_ACTUATORS,
                                                ActionType_WAIT_ACTUATORS);
    }
        break;
    case ActionType_WAIT_ACTUATORS1:
    {
        state.must_pause = this->wait_actuators(ActionType_MOVE_ACTUATORS2,
                                                ActionType_MOVE_ACTUATORS1,
                                                ActionType_WAIT_ACTUATORS1);
    }
        break;
    case ActionType_WAIT_ACTUATORS2:
    {
        state.must_pause = this->wait_actuators(ActionType_WAIT_ACTUATORS_DELAY,
                                                ActionType_MOVE_ACTUATORS2,
                                                ActionType_WAIT_ACTUATORS2);
    }
        break;
    case ActionType_WAIT_ACTUATORS_DELAY:
    {
        int next_action = 0;
        if (dim1_pt_ == 0 && dim2_pt_== 0)
        {
            // mantis 12227 : prescan hook must be executed only when actuators are at their first position
            next_action = ActionType_PRESCAN_HOOK;
        }
        else
        {
            next_action = ActionType_POST_ACTUATOR_MOVE_HOOK;
        }
        this->wait_actuators_delay(next_action);
    }
        break;
    case ActionType_PRESCAN_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_PRE_SCAN,
                                              ActionType_PRESCAN_HOOK,
                                              ActionType_POST_ACTUATOR_MOVE_HOOK);
    }
        break;
    case ActionType_POST_ACTUATOR_MOVE_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_POST_ACTUATOR_MOVE,
                                              ActionType_POST_ACTUATOR_MOVE_HOOK,
                                              SpecificAction_READ_ACTUATORS);
    }
        break;
    case SpecificAction_READ_ACTUATORS:
    {
        this->get_after_move_pos( true );
        next_action_ = ActionType_SENSORS_PRE_INTEGRATION;
    }
        break;
    case ActionType_READ_SENSORS:
    {
        state.must_pause =  this->read_sensors( ActionType_POST_STEP_HOOK );
    }
        break;
    case ActionType_END_OF_STEP:
    {
        this->end_of_step();

        if (dim1_pt_ == config_.integration_times.shape()[0] - 1)
        {
            manager_.data_collector->check_for_error();
            manager_.data_collector->end_of_dim();
        }

        if ( this->more_step() )
        {
            this->compute_next_actuator_pos();
            dim1_pt_++;
            if ( dim1_pt_ >= config_.integration_times.shape()[0] )
            {
                dim1_pt_ = 0;
                dim2_pt_++;
            }
            state.end_of_step = true;
            next_action_ = ActionType_INIT_STEP;
        }
        else
        {
            next_action_ = ActionType_POST_SCAN_HOOK;
        }
    }
        break;
    default:
    {
        //- we should never be here : end the scan immediately
        SCAN_ERROR << "Internal Error : Scan aborted" << ENDLOG;
        state.end_of_step = true;
        state.end_of_scan = true;
        state.end_of_run  = true;
    }
        break;
    }

    return state;
}


}

