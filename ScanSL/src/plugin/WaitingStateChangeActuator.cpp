/*!
 * \file
 * \brief    Definition of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <limits>
#include <boost/scoped_ptr.hpp>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/ThreadExiter.h>
#include <scansl/Util.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

WaitingStateChangeActuatorConfig::WaitingStateChangeActuatorConfig()
    : move_state(Tango::RUNNING),
      polling_period_ms(100),
      timeout_ms(3000)
{
}

// ============================================================================
// WaitingStateChangeActuator::WaitingStateChangeActuator
// ============================================================================
WaitingStateChangeActuator::WaitingStateChangeActuator()
{

}

// ============================================================================
// WaitingStateChangeActuator::~WaitingStateChangeActuator
// ============================================================================
WaitingStateChangeActuator::~WaitingStateChangeActuator()
{
}

// ============================================================================
// WaitingStateChangeActuator::init
// ============================================================================
void WaitingStateChangeActuator::init( std::string device_name )
{
    config_.dev_name = device_name;
    proxy_ = DevProxies::instance().proxy(device_name);
    proxy_->set_timeout_millis( config_.timeout_ms );
}

// ============================================================================
// WaitingStateChangeActuator::check_initial_condition
// ============================================================================
void WaitingStateChangeActuator::check_initial_condition( std::string ) {
    // no-op
}

// ============================================================================
// WaitingStateChangeActuator::ensure_supported
// ============================================================================
void WaitingStateChangeActuator::ensure_supported( std::string )
{
    //- do nothing : accept all attributes by default
}

// ============================================================================
// WaitingStateChangeActuator::set_position
// ============================================================================
void WaitingStateChangeActuator::set_position( std::string, double )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "offset feature is not supported",
                     "WaitingStateChangeActuator::set_position" );
}

// ============================================================================
// WaitingStateChangeActuator::is_enabled
// ============================================================================
bool WaitingStateChangeActuator::is_enabled( std::string )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "on/off feature is not supported",
                     "WaitingStateChangeActuator::is_enabled" );
}

// ============================================================================
// WaitingStateChangeActuator::set_enabled
// ============================================================================
void WaitingStateChangeActuator::set_enabled( std::string, bool )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "on/off feature is not supported",
                     "WaitingStateChangeActuator::set_enabled" );
}

// ============================================================================
// WaitingStateChangeActuator::get_limits
// ============================================================================
LimitsType WaitingStateChangeActuator::get_limits( std::string attr_name )
{
    Tango::DeviceProxy& px = proxy_->unsafe_proxy();
    Tango::AttributeInfoEx info = px.get_attribute_config(attr_name);
    return LimitsType(info.min_value, info.max_value);
}

// ============================================================================
// WaitingStateChangeActuator::set_limits
// ============================================================================
void WaitingStateChangeActuator::set_limits( std::string attr_name, const LimitsType& limits )
{
    Tango::DeviceProxy& px = proxy_->unsafe_proxy();
    Tango::AttributeInfoEx info = px.get_attribute_config(attr_name);
    info.min_value = limits.first;
    info.max_value = limits.second;
    Tango::AttributeInfoListEx infos(1, info);
    px.set_attribute_config(infos);
}

// ============================================================================
// WaitingStateChangeActuator::get_speed
// ============================================================================
double WaitingStateChangeActuator::get_speed( std::string )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "speed feature is not supported",
                     "WaitingStateChangeActuator::get_speed" );
}

// ============================================================================
// WaitingStateChangeActuator::set_speed
// ============================================================================
void WaitingStateChangeActuator::set_speed( std::string, double )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "speed feature is not supported",
                     "WaitingStateChangeActuator::set_speed" );
}

// ============================================================================
// WaitingStateChangeActuator::go_to
// ============================================================================
void WaitingStateChangeActuator::go_to( const std::vector<PositionRequest>& positions )
{
    std::vector<Tango::DeviceAttribute> dev_attrs;
    for( std::size_t i = 0; i < positions.size(); ++i )
    {
        const PositionRequest& pos_req = positions[i];
        dev_attrs.push_back( Util::instance().create_device_attribute(*pos_req.attribute_info, pos_req.position) );
    }

    //- we get a more descriptive error message if we use write_attribute
    //- when there is only one attribute to write (which is the majority of cases for actuator)
    if (dev_attrs.size() == 1)
        proxy_->write_attribute( dev_attrs[0] );
    else
        proxy_->write_attributes( dev_attrs );
}




// ============================================================================
// WaitingStateChangeActuator::is_moving
// ============================================================================
bool WaitingStateChangeActuator::is_moving( void )
{
    Tango::DevState current_state = proxy_->state();
    return (current_state == config_.move_state);
}

// ============================================================================
// WaitingStateChangeActuator::error_check
// ============================================================================
void WaitingStateChangeActuator::error_check( void )
{
    Tango::DevState current_state = Tango::UNKNOWN;
    try
    {
        current_state = proxy_->state();
    }
    catch( Tango::DevFailed& df )
    {
        throw yat4tango::TangoYATException( df );
    }

    if( std::find(config_.error_states.begin(), config_.error_states.end(), current_state) != config_.error_states.end() )
    {
        std::ostringstream oss;
        oss << proxy_->dev_name()
            << " has an invalid state : "
            << Tango::DevStateName[current_state];
        THROW_YAT_ERROR( "INVALID_STATE",
                         oss.str().c_str(),
                         "WaitingStateChangeActuator::error_check" );
    }
}


// ============================================================================
// WaitingStateChangeActuator::get_polling_period_ms
// ============================================================================
double WaitingStateChangeActuator::get_polling_period_ms( void )
{
    return config_.polling_period_ms;
}

// ============================================================================
// WaitingStateChangeActuator::abort
// ============================================================================
void WaitingStateChangeActuator::abort( void )
{
    if ( ! config_.abort_cmd_name.empty() )
        proxy_->command_inout( config_.abort_cmd_name );
}

//debut SPYC-294 SCAN-643
// ============================================================================
// WaitingStateChangeActuator::check_cmd_exists
// ============================================================================
bool WaitingStateChangeActuator::check_cmd_exists(std::string command_name){
        /*verification si la commande donnee en argument existe.
          retourne : True si la commande existe,
                     False sinon
        */
        bool check_cmd = false; //le resultat de la verification si la commande donnee en argument existe
        std::string current_cmd_name; // le nom de la commande dans la liste des commandes
        Tango::CommandInfoList*  al;
        
        //transformation en lower les lettres de command_name
        std::transform(command_name.begin(),command_name.end(),command_name.begin(),::tolower);
        
		try
    	{
          al = proxy_->command_list_query();
        }
    	catch( Tango::DevFailed& df )
    	{
        throw yat4tango::TangoYATException( df );
    	}
        
        size_t i = 0;
	//recherche de la commande donnee en argument dans la liste des commandes
        while( (i < al->size()) && (check_cmd == false) )
	{
		current_cmd_name = (*al)[i].cmd_name;
		//transformation en lower les lettres de current_cmd_name
		std::transform(current_cmd_name.begin(),current_cmd_name.end(),current_cmd_name.begin(),::tolower);

		if ( current_cmd_name == command_name )
	   	{
            check_cmd = true;
        }
		
		i++;
	}
       return check_cmd;
}//fin check_cmd_exists
//fin SPYC-294 SCAN-643

// ============================================================================
// WaitingStateChangeActuator::read_fixed_pos_list
// ============================================================================
std::vector< std::string > WaitingStateChangeActuator::read_fixed_pos_list(void)
{     /*lecture de la liste des positions possibles d un device*/
     THROW_YAT_ERROR( "NOT IMPLEMENTED",
                      "read fixed position list is not supported",
                     "WaitingStateChangeActuator::read_fixed_pos_list" );
}//fin read_fixed_pos_list

// ============================================================================
// WaitingStateChangeActuator::read_current_value
// ============================================================================
std::string WaitingStateChangeActuator::read_current_value(void)
{      /*lecture de la valeur courante d un moteur d un device*/
      THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read current attribute value is not supported",
                     "WaitingStateChangeActuator::read_fixed_pos_list" );
}//fin read_current_value
//fin SPYC-155


//Debut SPYC-300
// ============================================================================
//WaitingStateChangeActuator::read_selected_position
// ============================================================================
std::string WaitingStateChangeActuator::read_selected_position(){
    /*retourne le nom de la destination finale ou l equipement doit etre deplace.
	valeurs de retour :
		nom de la destination finale (string),
		vide si l attribute n existe pas
	*/
    std::string selected_position_value; //nom de la destination finale
    //si l attribute n existe pas, retourne la valeur vide
    if (config_.selected_attr_name.empty() )
	{
	selected_position_value = "";
	return selected_position_value;
	}

    //l attribute existe
    std::ostringstream oss; //description d erreur possible
    Tango::DeviceAttribute selected_position_name; //attribute du device

   //lecture de l attribut de la valeur de destination finale
   try {
   	selected_position_name = proxy_->read_attribute(config_.selected_attr_name);
	}catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "WaitingStateChangeActuator::read_selected_position");
        }
   	
   //si l attribut n est pas vide, extraction de la valeur, sinon erreur
   	if (! selected_position_name.is_empty())
   		{
      		selected_position_name >> selected_position_value;
		return selected_position_value;
   		}
   	else {
		oss << "The device " 
            		<< config_.dev_name
			<< " attribute "
            		<< config_.selected_attr_name
			<< " is empty ";
                THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "WaitingStateChangeActuator::read_selected_position");
		}
   

}//fin read_selected_position
//fin SPYC-300


}
