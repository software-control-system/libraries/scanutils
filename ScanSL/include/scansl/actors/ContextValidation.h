#ifndef _SCANSL_CONTEXTVALIDATION_H
#define _SCANSL_CONTEXTVALIDATION_H

#include <tango.h>
#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <boost/shared_ptr.hpp>
#include <yat/threading/Task.h>

namespace ScanUtils
{

class ContextValidationTask;

class SCANSL_DECL ContextValidation
{
public:
    ContextValidation( const ScanConfig& config );
    ~ContextValidation();

    void start_monitoring( void );

    bool is_valid( void );

    void stop_monitoring( void );

private:
    boost::shared_ptr<ContextValidationTask> task_;
};

}

#endif
