#ifndef _SCANSERVER_GENERICACTUATOR_H
#define _SCANSERVER_GENERICACTUATOR_H

#include <scansl/ScanSL.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/DevProxies.h>
#include <boost/shared_ptr.hpp>

namespace ScanUtils
{
class SCANSL_DECL GenericActuator : public IPolledActuator
{
public : //! structors
    GenericActuator();

    ~GenericActuator();

public : //! IActuator implementation
    
    virtual void init( std::string device_name );

    void check_initial_condition( std::string attr_name );

    virtual void ensure_supported( std::string attr_name );

    virtual void set_position( std::string attr_name, double position );

    virtual bool is_enabled( std::string attr_name );

    virtual void set_enabled( std::string attr_name, bool enable );

    virtual LimitsType get_limits( std::string attr_name );

    virtual void set_limits( std::string attr_name, const LimitsType& limits);

    virtual double get_speed( std::string attr_name );

    virtual void set_speed( std::string attr_name, double speed );

    virtual void go_to( const std::vector<PositionRequest>& positions );
    
    virtual bool is_moving( void );
    
    virtual void error_check( void );

    virtual double get_polling_period_ms( void );

    virtual void abort( void );
//debut SPYC-155
    //SpycStates
    virtual std::vector< std::string > read_fixed_pos_list(void);/*lecture de la liste des positions possibles d un device*/
    virtual std::string read_current_value(void); /*lecture de la valeur courante d un moteur d un device*/
    
//fin SPYC-155
//debut SPYC-300
	//SpycStates
    virtual std::string read_selected_position(void); /*lecture du nom de la destination finale ou l equipement doit etre deplace*/
//fin SPYC-300

private: //! private implementation

    //! configuration
    std::string device_name_;
    ProxyP proxy_;
    boost::shared_ptr<Tango::AttributeInfoList>  attr_info_;
};
}

#endif
