#ifndef _SCANSERVER_I_TIMEBASE_H
#define _SCANSERVER_I_TIMEBASE_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <boost/noncopyable.hpp>
#include <boost/smart_ptr.hpp>

namespace ScanUtils
{
struct SCANSL_DECL ITimebase : private boost::noncopyable
{
    virtual ~ITimebase();

    virtual void init( std::string device_name, bool sync ) = 0;

    virtual void check_initial_condition( ) = 0;

    virtual void start( double integration_time ) = 0;

    virtual bool is_counting( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual void abort( void ) = 0;

    virtual void before_run( void ) = 0;

    virtual void after_run( void ) = 0;

};

typedef boost::shared_ptr<ITimebase> ITimebasePtr;
}

#endif
