#ifndef _SCANSERVER_ACTUATOR_MANAGER_H
#define _SCANSERVER_ACTUATOR_MANAGER_H

#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/actors/IActuator.h>
#include <scansl/ScanConfig.h>
#include <scansl/Factory.h>
#include <scansl/AttrValue.h>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>


namespace ScanUtils
{
class SCANSL_DECL ActuatorManager : private boost::noncopyable
{
    //! a subgroup correspond to the list of attributes in a group attached to the same device
    struct SubGroup
    {
        std::string              device_name;
        IActuatorPtr             actuator;
        std::vector<std::string> attr_names;
        std::vector< boost::shared_ptr<Tango::AttributeInfoEx> > attr_info;
        std::vector<int>         index;
        std::vector<double>      normal_speed;
        std::vector<double>      scan_speed;
        bool                     actuator_moved;
    };
    //! a group is just a collection of subgroup
    typedef std::vector<SubGroup> Group;
    //! group shared_ptr definition to avoid deep copy when handled in a map
    typedef boost::shared_ptr<Group> GroupPtr;


public:
    ActuatorManager( const ScanConfig& config );
    ~ActuatorManager( );

    //! check that given trajectories fit in the limits
    void validate_trajectories(const std::vector<std::string>& actuator_names, const ScanArray<double>::Img& trajectories);

    //! check that all trajectories fit in the limits
    void validate_trajectories();

    //! register a group of actuators
    void register_group ( int id, const std::vector<std::string>& attr_names );

    //! launch a move request on a group
    void go_to( int id, const std::vector<double>& position );

    //! configure the actuator speed
    void set_scan_speed( void );
    
    //! restore the speed bedore it was configured
    void restore_speed( void );

    //! aborts the movements of all actuators
    void abort( void );

    //! just read the attributes
    AttrValues read( int group_id );

    //! have all actuators finished their movement ?
    bool move_request_completed( void );

    // throws if any error occurred, does nothing otherwise
    void error_check( void );

private:

    const ScanConfig& config_;

    //! the factory used to create the IPolledActuator implementation
    GenericFactory<IPolledActuator, GenericActuator> factory_;

    //! the different actuator handlers (IActuator implementation).
    //! each one can be used in several groups
    std::map<std::string, IActuatorPtr> actuators_;
    
    //! the group collection, indexed by the group id
    std::map<int, GroupPtr> groups_;

    int moving_group_id_;
};

typedef boost::shared_ptr<ActuatorManager> ActuatorManagerPtr;
}

#endif
