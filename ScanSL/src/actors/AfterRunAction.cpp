#include <scansl/ScanSL.h>
#include <scansl/actors/AfterRunAction.h>
#include <scansl/actors/DataCollector.h>
#include <scansl/Util.h>
//#include <limits>
#include <numeric>
#include <math.h>
#include <float.h>

namespace ScanUtils
{
const double NaN = ::sqrt( -1.0 );

int is_NaN( double value )
{
#ifdef YAT_WIN32
    return _isnan( value );
#else
    return  std::isnan( value ); // modified by Yves Acremann, 24.6.2014; was: return isnan( value );
#endif
}

struct ComputationResult
{
    std::vector<size_t> scan_pos;
    double value;
};

template <typename T>
std::vector<size_t> get_scanpos( T* it, typename ScanArray<T>::ImgP& dataset )
{
    std::vector<size_t> pos(2);
    std::vector<size_t> shape(2);
    std::copy( dataset->shape(), dataset->shape() + 2, shape.begin() );
    int dist = std::distance( dataset->data(), it );
    pos[0] = dist / shape[1];
    pos[1] = dist % shape[1];
    return pos;
}

struct MaxWrapper
{
    template <typename Iterator>
    Iterator operator() ( Iterator first, Iterator last )
    {
        return std::max_element(first, last);
    }
};

struct MinWrapper
{
    template <typename Iterator>
    Iterator operator() ( Iterator first, Iterator last )
    {
        return std::min_element(first, last);
    }
};

double maxx( const double& first, const double& second )
{
    return max( first, second);
}

double minn( const double& first, const double& second )
{
    return min( first, second);
}


template <typename T, typename Algorithm>
struct Process
{
    typedef typename ScanArray<T>::ImgP imagep_t;

    bool operator() ( DataCollector::DataInfo& datainfo, ComputationResult& result )
    {
        if (datainfo.attr_info.data_type == TangoTraits<T>::type_id)
        {
            boost::any buffer_any = datainfo.datasetter->get();
            imagep_t& buffer = boost::any_cast<imagep_t&>( buffer_any );
            T* result_elem = Algorithm()( buffer->data(), buffer->data() + buffer->num_elements() );
            result.value = static_cast<double>(*result_elem);
            result.scan_pos = get_scanpos( result_elem, buffer );
            return true;
        }
        else
            return false;
    }
};

template <typename T, typename U>
struct Derivative2
{
    typedef typename ScanArray<T>::ImgP imagep_t;
    typedef typename ScanArray<U>::ImgP imagep_u;


    template <class min_or_max>
    bool operator() ( imagep_t& dataset_sensor,
                      DataCollector::DataInfo& datainfo_actuator,
                      ComputationResult& result,
                      min_or_max f )
    {
        if (datainfo_actuator.attr_info.data_type == TangoTraits<U>::type_id)
        {
            boost::any buffer_any = datainfo_actuator.datasetter->get();
            imagep_u& dataset_actuator = boost::any_cast<imagep_u&>( buffer_any );
            if ( !std::equal(dataset_sensor->shape(), dataset_sensor->shape() + 2, dataset_actuator->shape()) )
            {
                SCAN_ERROR << "Data don't have the same shape" << ENDLOG;
                THROW_YAT_ERROR( "ARGUMENT_ERROR", "Data don't have the same shape", "Derivative2::operator()");
            }
            else
            {
                bool initialized = false;
                double optimum = 0;
                T* optimum_pos = 0;
                for (size_t i = 0; i < dataset_sensor->num_elements() - 1; i++)
                {
                    T* sensor1 = dataset_sensor->data() + i;
                    T* sensor2 = dataset_sensor->data() + i + 1;

                    U* actuator1 = dataset_actuator->data() + i;
                    U* actuator2 = dataset_actuator->data() + i + 1;

                    if (    !is_NaN(*sensor1)
                            && !is_NaN(*sensor2)
                            && !is_NaN(*actuator1)
                            && !is_NaN(*actuator2)
                            && *actuator1 != *actuator2 )
                    {
                        double derivative = (*sensor2 - *sensor1) / (*actuator2 - *actuator1 );
                        if ( !initialized )
                        {
                            optimum = derivative;
                            initialized = true;
                            optimum_pos = sensor1;
                        }
                        else
                        {
                            optimum = f( optimum, derivative );
                            if ( ::fabs(optimum - derivative) < DBL_EPSILON )
                                optimum_pos = sensor1;
                        }
                    }
                }
                result.value = optimum;
                result.scan_pos = get_scanpos( optimum_pos, dataset_sensor );
            }
            return true;
        }
        else
            return false;
    }
};

template <typename T>
struct Derivative
{
    typedef typename ScanArray<T>::ImgP imagep_t;

    template <class min_or_max>
    bool operator() ( DataCollector::DataInfo& datainfo_sensor,
                      DataCollector::DataInfo& datainfo_actuator,
                      ComputationResult& result,
                      min_or_max f)
    {
        if (datainfo_sensor.attr_info.data_type == TangoTraits<T>::type_id)
        {
            boost::any buffer_any = datainfo_sensor.datasetter->get();
            imagep_t& buffer = boost::any_cast<imagep_t&>( buffer_any );
            if      ( Derivative2<T, Tango::DevShort   >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevLong    >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevDouble  >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevFloat   >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevBoolean >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevUShort  >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevULong   >()( buffer, datainfo_actuator, result, f ) ) { return true; }
            else if ( Derivative2<T, Tango::DevUChar   >()( buffer, datainfo_actuator, result, f ) ) { return true; }
        }
        return false;
    }
};

struct DoubleConverter : public yat::Singleton<DoubleConverter>
{
public:
    typedef ScanArray<double>::Img  dbl_image_t;
    typedef ScanArray<double>::ImgP dbl_imagep_t;
private:

    struct DoubleConverterEngine
    {
        virtual dbl_imagep_t convert( DataCollector::DataInfo& datainfo ) = 0;
    };

    template <typename T>
    struct DoubleConverterEngineT : public DoubleConverterEngine
    {
        typedef typename ScanArray<T>::ImgP imagep_t;

        virtual dbl_imagep_t convert( DataCollector::DataInfo& datainfo )
        {
            boost::any input_any = datainfo.datasetter->get();
            imagep_t& input = boost::any_cast<imagep_t&>( input_any );
            dbl_imagep_t output( new dbl_image_t( boost::extents[input->shape()[0]][input->shape()[1]] ));
            std::copy( input->data(), input->data() + input->num_elements(), output->data() );
            return output;
        }
    };

    typedef boost::shared_ptr< DoubleConverterEngine > DoubleConverterEngineP;

    std::map< int , DoubleConverterEngineP > converters_;

public:
    DoubleConverter()
    {
        converters_[ Tango::DEV_BOOLEAN ].reset( new DoubleConverterEngineT<Tango::DevBoolean>() );
        converters_[ Tango::DEV_SHORT   ].reset( new DoubleConverterEngineT<Tango::DevShort>() );
        converters_[ Tango::DEV_LONG    ].reset( new DoubleConverterEngineT<Tango::DevLong>() );
        converters_[ Tango::DEV_DOUBLE  ].reset( new DoubleConverterEngineT<Tango::DevDouble>() );
        converters_[ Tango::DEV_FLOAT   ].reset( new DoubleConverterEngineT<Tango::DevFloat>() );
        converters_[ Tango::DEV_USHORT  ].reset( new DoubleConverterEngineT<Tango::DevUShort>() );
        converters_[ Tango::DEV_ULONG   ].reset( new DoubleConverterEngineT<Tango::DevULong>() );
        converters_[ Tango::DEV_UCHAR   ].reset( new DoubleConverterEngineT<Tango::DevUChar>() );
    }


    dbl_imagep_t convert( DataCollector::DataInfo& datainfo )
    {
        return converters_[datainfo.attr_info.data_type]->convert( datainfo );
    }

};






AfterRunAction::AfterRunAction( const ScanConfig& config )
    : config_( &config )
{
}

void AfterRunAction::configure( DataCollectorPtr data_collector,
                                ActuatorManagerPtr act_manager,
                                ContinuousTimebaseManagerPtr ctb_manager )
{
    this->data_collector_ = data_collector;
    this->actuator_manager_ = act_manager;
    this->ctb_manager_ = ctb_manager;
}

void AfterRunAction::execute( AfterRunActionDesc action )
{
    this->action_ = action;

    switch (action.type)
    {
    // Go to the first scan pos (no sensor data required)
    case AfterRunActionType_FIRST:
        this->first_pos();
        break;
        // Go to the last before scan position (no sensor data required)
    case AfterRunActionType_PRIOR:
        this->prior_pos();
        break;
        // Go to the position of the max sensor value
    case AfterRunActionType_PEAK:
        this->maximum();
        break;
        // Go to the position of the min sensor value
    case AfterRunActionType_VALLEY:
        this->minimum();
        break;
        // Go to the position of the max derivated sensor value
    case AfterRunActionType_MAX_OF_DERIVATIVE:
        this->max_derivative();
        break;
        // Go to the position of the min derivated sensor value
    case AfterRunActionType_MIN_OF_DERIVATIVE:
        this->min_derivative();
        break;
        // Go to the position of the center of mass position
    case AfterRunActionType_CENTER_OF_MASS:
        this->center_of_mass();
        break;
        /*
    case AfterRunActionType_CENTER_OF_GAUSSIAN_FIT:
      this->fit( "gaussian" );
      break;
    case AfterRunActionType_CENTER_OF_GAUSSIAN_FIT_BG:
      this->fit( "gaussianb" );
      break;
*/
    case AfterRunActionType_GOTO_POS:
        this->goto_pos();
        break;
    case AfterRunActionType_NO_ACTION:
    default:
        break;
    }
}

void AfterRunAction::move_actuators_to_scan_pos( int x, int y )
{
    std::vector<double> pos_to_go;
    int group_id;

    for (size_t i = 0; i < config_->actuators.size(); i++)
    {
        if ( !config_->hw_continuous )
        {
            pos_to_go.push_back( config_->trajectories[i][x] );
        }
        else //## Mettre en commentaire ??
        {
            std::string actuator_full_name = Util::instance().complete_attr_name( config_->actuators[i] );
            std::string actuator_data_attr = ctb_manager_->get_actuator_value_attr( actuator_full_name );
            DataCollector::DataInfo& datainfo_actuator = data_collector_->actuator_data_[ actuator_data_attr ];

            // TODO supported only for Double data (which is always the case today)
            if ( datainfo_actuator.attr_info.data_type != Tango::DEV_DOUBLE )
            {
                THROW_YAT_ERROR("BAD_CAST",
                                "Actuator data are not of type DevDouble. Afterrun action not supported",
                                "AfterRunAction::move_actuators_to_scan_pos");
            }

            boost::any buffer_any = datainfo_actuator.datasetter->get();
            ScanArray<double>::ImgP& bufferp = boost::any_cast<ScanArray<double>::ImgP&>( buffer_any );
            ScanArray<double>::Img& buffer = *bufferp;
            pos_to_go.push_back( buffer[0][x] );
        }
    }

    if ( config_->actuators2.empty() )
    {
        group_id = ActuatorDim_1;
    }
    else
    {
        group_id = ActuatorDim_1 + ActuatorDim_2;
        for (size_t i = 0; i < config_->actuators2.size(); i++)
        {
            pos_to_go.push_back( config_->trajectories2[i][y] );
        }
    }

    actuator_manager_->go_to( group_id, pos_to_go );
}

// moves the actuator given by actuator_idx to actuator_value
// moves all others to the corresponding positions computed by linear interpolation
// supported only for 1D scans (step by step, on-the-fly or HW)
void AfterRunAction::move_actuators_to_absolute_pos( int actuator_idx, double actuator_value )
{

    // indexes in actuator data spectrum before and after the point
    size_t index_before, index_after;

    // get a double image from the actuator data
    DataCollector::DataInfo& datainfo_actuator = this->get_actuator_dataset( actuator_idx );
    ScanArray<double>::ImgP actuator_datap = DoubleConverter::instance().convert( datainfo_actuator );
    ScanArray<double>::Img& actuator_data = *actuator_datap;
    size_t nb_pt = actuator_data.num_elements();

    if (actuator_data.shape()[0] != 1 || nb_pt == 0)
        THROW_YAT_ERROR("WRONG_SHAPE",
                        "unexpected actuators data shape",
                        "AfterRunAction::move_actuators_to_absolute_pos");

    if ( actuator_data[0][0] > actuator_data[0][nb_pt - 1] )
    {
        // here we have a decreasing trajectory

        // handle some special cases :
        if (actuator_data[0][0] < actuator_value)
        {
            // all data are < actuator_value
            this->move_actuators_to_scan_pos(0, 0);
            return;
        }

        if (actuator_data[0][nb_pt - 1] > actuator_value)
        {
            // all data are > actuator_value
            this->move_actuators_to_scan_pos(nb_pt - 1, 0);
            return;
        }

        // handle normal case :
        for (index_after = 1; index_after < nb_pt - 1; index_after++)
        {
            if (actuator_data[0][index_after] < actuator_value)
                break;
        }

    }
    else
    {
        // here we have an increasing trajectory

        // handle some special cases :
        if (actuator_data[0][0] > actuator_value)
        {
            // all data are < actuator_value
            this->move_actuators_to_scan_pos(0, 0);
            return;
        }

        if (actuator_data[0][nb_pt - 1] < actuator_value)
        {
            // all data are > actuator_value
            this->move_actuators_to_scan_pos(nb_pt - 1, 0);
            return;
        }

        // handle normal case :
        for (index_after = 1; index_after < nb_pt - 1; index_after++)
        {
            if (actuator_data[0][index_after] > actuator_value)
                break;
        }
    }

    index_before = index_after - 1;
    double value_before = actuator_data[0][index_before];
    double value_after = actuator_data[0][index_after];

    if (value_before == value_after) // just in case
        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        "can't compute slope",
                        "AfterRunAction::move_actuators_to_absolute_pos");

    double interp_factor = (actuator_value - value_before) / (value_after - value_before);

    std::vector<double> pos_to_go;
    for ( size_t i = 0; i < config_->actuators.size(); i++ )
    {
        if (i == size_t(action_.actuator))
        {
            pos_to_go.push_back( actuator_value );
        }
        else
        {
            DataCollector::DataInfo& actuator_i_dataset = this->get_actuator_dataset( i );
            ScanArray<double>::ImgP  actuator_i_datap = DoubleConverter::instance().convert( actuator_i_dataset );
            ScanArray<double>::Img&  actuator_i_data = *actuator_i_datap;

            pos_to_go.push_back( (1-interp_factor)  * actuator_i_data[0][index_before]
                    + interp_factor   * actuator_i_data[0][index_after] );
        }
    }

    actuator_manager_->go_to( ActuatorDim_1, pos_to_go );
}



DataCollector::DataInfo& AfterRunAction::get_sensor_dataset( long sensor_idx )
{
    if ( sensor_idx < 0 || size_t(sensor_idx) > config_->sensors.size() - 1 )
    {
        THROW_YAT_ERROR( "BAD_INDEX",
                         "Wrong sensor index selected for Post Scan Behavior",
                         "AfterRunAction::get_sensor_dataset" );
    }
    std::string sensor_full_name = Util::instance().complete_attr_name( config_->sensors[sensor_idx] );
    DataCollector::DataInfo& datainfo = data_collector_->sensor_data_[ sensor_full_name ];
    return datainfo;
}

DataCollector::DataInfo& AfterRunAction::get_actuator_dataset( long actuator_idx )
{
    if ( actuator_idx < 0 || size_t(actuator_idx) > config_->actuators.size() - 1 )
    {
        THROW_YAT_ERROR( "BAD_INDEX",
                         "Wrong actuator index selected for Post Scan Behavior",
                         "AfterRunAction::get_sensor_dataset" );
    }
    
    std::string actuator_full_name = Util::instance().complete_attr_name( config_->actuators[actuator_idx] );
    std::string actuator_data_attr = actuator_full_name;

    if (config_->hw_continuous)
    {
        // get the attribute where the data of actuator_full_name are available from the ContinuousTimebaseManager
        actuator_data_attr = ctb_manager_->get_actuator_value_attr( actuator_full_name );
    }

    DataCollector::DataInfo& datainfo_actuator = data_collector_->actuator_data_[ actuator_data_attr ];

    return datainfo_actuator;
}


void AfterRunAction::first_pos( void )
{
    SCAN_INFO << "Moving to first scan position..." << ENDLOG;
    this->move_actuators_to_scan_pos( 0, 0 );
}

void AfterRunAction::prior_pos( void )
{
    SCAN_INFO << "Moving to prior scan position..." << ENDLOG;
    std::vector<double> pos_to_go;
    int group_id = config_->actuators2.empty() ? ActuatorDim_1 : ActuatorDim_1 + ActuatorDim_2;
    boost::shared_ptr< std::vector<double> > prior_pos = data_collector_->get_buffers().actuators_prior_pos;
    std::copy( prior_pos->begin(), prior_pos->end(), std::back_inserter(pos_to_go) );
    actuator_manager_->go_to( group_id, pos_to_go );
}

void AfterRunAction::maximum( void )
{
    if ( config_->hw_continuous )
    {
        SCAN_ERROR << "The 'MAX' after run action is not supported for hardware scan" << ENDLOG;
        return;
    }

    SCAN_INFO << "Moving actuators to maximum of sensor "
              << config_->sensors[action_.sensor]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo = this->get_sensor_dataset( action_.sensor );
    
    if( datainfo.src_dims != 0 && !config_->hw_continuous )
    {
        SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
        return;
    }

    ComputationResult result;
    if      (Process<Tango::DevShort  , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevLong   , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevDouble , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevFloat  , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevBoolean, MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevUShort , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevULong  , MaxWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevUChar  , MaxWrapper>()( datainfo, result )) {}

    SCAN_INFO << "Maximum found on scan position [" << result.scan_pos[0] << " , " << result.scan_pos[1] << "] with value " << result.value << ENDLOG;

    if (config_->on_the_fly)
    {
        // for on the fly scans, it is mandatory to use the actuator dataset
        DataCollector::DataInfo& datainfo = this->get_actuator_dataset( 0 );
        DoubleConverter::dbl_imagep_t dataset_actuator = DoubleConverter::instance().convert( datainfo );

        this->move_actuators_to_absolute_pos( 0, (*dataset_actuator)[ result.scan_pos[0] ][ result.scan_pos[1] ] );
    }
    else
    {
        this->move_actuators_to_scan_pos( result.scan_pos[1], result.scan_pos[0] );
    }
}

void AfterRunAction::minimum( void )
{
    if ( config_->hw_continuous )
    {
        SCAN_ERROR << "The 'MIN' after run action is not supported for hardware scan" << ENDLOG;
        return;
    }

    SCAN_INFO << "Moving actuators to minimum of sensor "
              << config_->sensors[action_.sensor]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo = this->get_sensor_dataset( action_.sensor );
    
    if( datainfo.src_dims != 0 && !config_->hw_continuous )
    {
        SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
        return;
    }

    ComputationResult result;
    if      (Process<Tango::DevShort  , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevLong   , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevDouble , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevFloat  , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevBoolean, MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevUShort , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevULong  , MinWrapper>()( datainfo, result )) {}
    else if (Process<Tango::DevUChar  , MinWrapper>()( datainfo, result )) {}

    SCAN_INFO << "Minimum found on scan position [" << result.scan_pos[0] << " , " << result.scan_pos[1] << "] with value " << result.value << ENDLOG;
    if (config_->on_the_fly)
    {
        // for on the fly scans, it is mandatory to use the actuator dataset
        DataCollector::DataInfo& datainfo = this->get_actuator_dataset( 0 );
        DoubleConverter::dbl_imagep_t dataset_actuator = DoubleConverter::instance().convert( datainfo );

        this->move_actuators_to_absolute_pos( 0, (*dataset_actuator)[ result.scan_pos[0] ][ result.scan_pos[1] ] );
    }
    else
    {
        this->move_actuators_to_scan_pos( result.scan_pos[1], result.scan_pos[0] );
    }
}



void AfterRunAction::max_derivative( void )
{
    if (config_->actuators2.size() > 0)
    {
        SCAN_ERROR << "The 'MAX_OF_DERIVATIVE' after run action is not supported for a 2D scan" << ENDLOG;
        return;
    }

    if ( config_->hw_continuous )
    {
        SCAN_ERROR << "The 'MAX_OF_DERIVATIVE' after run action is not supported for hardware scan" << ENDLOG;
        return;
    }


    SCAN_INFO << "Moving actuators to maximum of derivative of sensor "
              << config_->sensors[action_.sensor]
              << " versus actuator "
              << config_->actuators[action_.actuator]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo_sensor = this->get_sensor_dataset( action_.sensor );
    DataCollector::DataInfo& datainfo_actuator = this->get_actuator_dataset( action_.actuator );
    
    if( datainfo_sensor.src_dims != 0 && !config_->hw_continuous )
    {
        SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
        return;
    }

    ComputationResult result;

    if      (Derivative<Tango::DevShort  >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevLong   >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevDouble >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevFloat  >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevBoolean>()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevUShort >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevULong  >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}
    else if (Derivative<Tango::DevUChar  >()( datainfo_sensor, datainfo_actuator, result, maxx )) {}

    SCAN_INFO << "Maximum of derivative found on scan position [" << result.scan_pos[0] << " , " << result.scan_pos[1] << "] with value " << result.value << ENDLOG;
    if (config_->on_the_fly)
    {
        // for on the fly scans, it is mandatory to use the actuator dataset
        DataCollector::DataInfo& datainfo = this->get_actuator_dataset( 0 );
        DoubleConverter::dbl_imagep_t dataset_actuator = DoubleConverter::instance().convert( datainfo );

        this->move_actuators_to_absolute_pos( 0, (*dataset_actuator)[ result.scan_pos[0] ][ result.scan_pos[1] ] );
    }
    else
    {
        this->move_actuators_to_scan_pos( result.scan_pos[1], result.scan_pos[0] );
    }
}

void AfterRunAction::min_derivative( void )
{
    if (config_->actuators2.size() > 0)
    {
        SCAN_ERROR << "The 'MIN_OF_DERIVATIVE' after run action is not supported for a 2D scan" << ENDLOG;
        return;
    }

    if ( config_->hw_continuous )
    {
        SCAN_ERROR << "The 'MIN_OF_DERIVATIVE' after run action is not supported for hardware scan" << ENDLOG;
        return;
    }

    SCAN_INFO << "Moving actuators to minimum of derivative of sensor "
              << config_->sensors[action_.sensor]
              << " versus actuator "
              << config_->actuators[action_.actuator]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo_sensor = this->get_sensor_dataset( action_.sensor );
    DataCollector::DataInfo& datainfo_actuator = this->get_actuator_dataset( action_.actuator );

    if( datainfo_sensor.src_dims != 0 && !config_->hw_continuous )
    {
        SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
        return;
    }

    ComputationResult result;

    if      (Derivative<Tango::DevShort  >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevLong   >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevDouble >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevFloat  >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevBoolean>()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevUShort >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevULong  >()( datainfo_sensor, datainfo_actuator, result, minn )) {}
    else if (Derivative<Tango::DevUChar  >()( datainfo_sensor, datainfo_actuator, result, minn )) {}

    SCAN_INFO << "Minimum of derivative found on scan position [" << result.scan_pos[0] << " , " << result.scan_pos[1] << "] with value " << result.value << ENDLOG;
    if (config_->on_the_fly)
    {
        // for on the fly scans, it is mandatory to use the actuator dataset
        DataCollector::DataInfo& datainfo = this->get_actuator_dataset( 0 );
        DoubleConverter::dbl_imagep_t dataset_actuator = DoubleConverter::instance().convert( datainfo );

        this->move_actuators_to_absolute_pos( 0, (*dataset_actuator)[ result.scan_pos[0] ][ result.scan_pos[1] ] );
    }
    else
    {
        this->move_actuators_to_scan_pos( result.scan_pos[1], result.scan_pos[0] );
    }
}


void AfterRunAction::center_of_mass( void )
{
    if (config_->actuators2.size() > 0)
    {
        SCAN_ERROR << "The 'CENTROID' after run action is not supported for a 2D scan" << ENDLOG;
        return;
    }

    SCAN_INFO << "Moving actuators to centroid of sensor "
              << config_->sensors[action_.sensor]
              << " versus actuator "
              << config_->actuators[action_.actuator]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo_sensor = this->get_sensor_dataset( action_.sensor );
    DataCollector::DataInfo& datainfo_actuator = this->get_actuator_dataset( action_.actuator );
    
    if( datainfo_sensor.src_dims != 0 && !config_->hw_continuous )
    {
        SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
        return;
    }

    DoubleConverter::dbl_imagep_t dataset_actuator = DoubleConverter::instance().convert( datainfo_actuator );
    DoubleConverter::dbl_imagep_t dataset_sensor = DoubleConverter::instance().convert( datainfo_sensor );
    
    double dot_product = 0;
    dot_product = std::inner_product( dataset_sensor->data(),
                                      dataset_sensor->data() + dataset_sensor->num_elements(),
                                      dataset_actuator->data(),
                                      dot_product );
    double sensor_sum = 0;
    sensor_sum = std::accumulate( dataset_sensor->data(),
                                  dataset_sensor->data() + dataset_sensor->num_elements(),
                                  sensor_sum );

    double centroid = 0;
    if ( ::fabs(sensor_sum) > DBL_EPSILON )
        centroid = dot_product / sensor_sum;

    SCAN_INFO << "Centroid : " << centroid << ENDLOG;

    this->move_actuators_to_absolute_pos( action_.actuator, centroid );
}

/*
  void AfterRunAction::fit( std::string fit_type )
  {
    if (config_->actuators2.size() > 0)
    {
      SCAN_ERROR << "The FIT after run action is not supported for a 2D scan" << ENDLOG;
      return;
    }

    SCAN_INFO << "Moving actuators to center of "
              << fit_type
              << " fit of sensor "
              << config_->sensors[action_.sensor]
              << " versus actuator "
              << config_->actuators[action_.actuator]
              << " ..."
              << ENDLOG;

    DataCollector::DataInfo& datainfo_sensor = this->get_sensor_dataset( action_.sensor );
    DataCollector::DataInfo& datainfo_actuator = this->get_actuator_dataset( action_.actuator );
    
    if( datainfo_sensor.src_dims != 0 && !config_->hw_continuous )
    {
      SCAN_WARN << config_->sensors[action_.sensor] << " is not a scalar : no post-scan behavior supported. Skipping..." << ENDLOG;
      return;
    }

    Tango::DeviceProxy datafitter( "test/scan/fitter" );

    DoubleConverter::dbl_imagep_t dbl_actuator_im = DoubleConverter::instance().convert( datainfo_actuator );
    DoubleConverter::dbl_imagep_t dbl_sensor_im = DoubleConverter::instance().convert( datainfo_sensor );
    
    {
      std::string x_attr = config_->properties.my_devicename + "/" + datainfo_actuator.dataset_name;
      Tango::DeviceAttribute da("deviceAttributeNameX", x_attr );
      datafitter.write_attribute( da );
    }
    {
      std::string y_attr = config_->properties.my_devicename + "/" + datainfo_sensor.dataset_name;
      Tango::DeviceAttribute da("deviceAttributeNameY", y_attr );
      datafitter.write_attribute( da );
    }

    {
      Tango::DeviceAttribute da("useSigma", false);
      datafitter.write_attribute( da );
    }
    {
      Tango::DeviceAttribute da("fitMode", true);
      datafitter.write_attribute( da );
    }
    {
      Tango::DeviceAttribute da("fittingFunctionType", fit_type);
      datafitter.write_attribute( da );
    }

    datafitter.command_inout("StartFit");

    yat::ThreadingUtilities::sleep( 1, 0 ); // wait 1 second
    
    std::cout << "DataFitter Status : " << std::endl << datafitter.status() << std::endl;


    std::vector< std::string > attr_to_read;
    attr_to_read.push_back("position");
    attr_to_read.push_back("width");
    attr_to_read.push_back("height");
    attr_to_read.push_back("background");
    attr_to_read.push_back("fwhm");
    attr_to_read.push_back("determinationQualityFactor");
    attr_to_read.push_back("fStatisticQualityFactor");
    attr_to_read.push_back("experimentalDataX");

    boost::scoped_ptr< std::vector<Tango::DeviceAttribute> > dev_attrs_p( datafitter.read_attributes( attr_to_read ) );
    std::vector<Tango::DeviceAttribute>& dev_attrs = *dev_attrs_p;
    double position, width, height, bg, fwhm, det_qf, fstat_qf;
    std::vector<double> expdata;
    dev_attrs[0] >> position;
    dev_attrs[1] >> width;
    dev_attrs[2] >> height;
    dev_attrs[3] >> bg;
    dev_attrs[4] >> fwhm;
    dev_attrs[5] >> det_qf;
    dev_attrs[6] >> fstat_qf;
    dev_attrs[7] >> expdata;

    std::cout << "Fit results : " << std::endl
              << "position : " << position << std::endl
              << "width : " << width << std::endl
              << "height : " << height << std::endl
              << "bg : " << bg << std::endl
              << "fwhm : " << fwhm << std::endl
              << "det_qf : " << det_qf << std::endl
              << "fstat_qf : " << fstat_qf << std::endl;


    // find the position in the trajectory
    // don't work if trajectory is not increasing
    double traj_point;
    size_t i;
    for (i = 0; i < expdata.size(); i++)
    {
      traj_point = expdata[i];
      if (traj_point > position)
        break;
    }


    if ( i != expdata.size() && i != 0 )
    {
      double before = expdata[i - 1];
      double after  = expdata[i];
      double lin_interp_factor = (position - before) / (after - before);
      std::vector<double> pos_to_go;

      for (size_t actu = 0; actu < config_->actuators.size(); actu++)
      {
        if (actu == size_t(action_.actuator))
        {
          pos_to_go.push_back( position );
        }
        else
        {
          pos_to_go.push_back( (1-lin_interp_factor) * config_->trajectories[actu][i-1]
                               + lin_interp_factor   * config_->trajectories[actu][i] );
        }
      }

      actuator_manager_->go_to( ActuatorDim_1, pos_to_go );

    }
    else
    {
      SCAN_WARN << "Fit result outside of trajectory. Skipping actuator movement" << ENDLOG;
    }

  }
*/


void AfterRunAction::goto_pos( void )
{

    // supported only for 1D scans (step by step, on-the-fly or HW)
    // no timescan, no 2d scans

    if (config_->actuators2.size() > 0)
    {
        SCAN_ERROR << "The 'GOTO POSITION' after run action is not supported for a 2D scan" << ENDLOG;
        return;
    }

    if (config_->actuators.empty())
    {
        SCAN_ERROR << "The 'GOTO POSITION' after run action is not supported for a timescan" << ENDLOG;
        return;
    }

    this->move_actuators_to_absolute_pos( action_.actuator, action_.actuator_value );

}



}

