/*!
 * \file
 * \brief    Definition of PowerPMACComposedAxisActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <scansl/Util.h>

namespace ScanUtils
{
class PowerPMACComposedAxisActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PowerPMACComposedAxisActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PowerPMACComposedAxis");

    }
};

class PowerPMACComposedAxisActuator : public WaitingStateChangeActuator
{
public:
    PowerPMACComposedAxisActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::ALARM);
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::INIT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";

    }
// ============================================================================
//   init method
// ============================================================================

      void init( std::string device_name )
    {
        std::string device_class = Util::instance().get_class( device_name );

        this->WaitingStateChangeActuator::init( device_name );
    }

// ============================================================================
//   check_initial_condition method
// ============================================================================
    void check_initial_condition( std::string /*attr_name*/ )
    {
        //- check the state
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
        {
            std::ostringstream oss;
            oss << "The device "
                << config_.dev_name
                << " must be STANDBY or ALARM to move";

            THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "PowerPMACComposedAxisActuatorInfo::check_initial_condition");
        }
    }
// ============================================================================
// PowerPMACComposedAxisActuator::go_to
// ============================================================================
    void set_enabled( std::string attr_name, bool enabled )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
         // unused
            proxy_->command_inout(enabled ? "On" : "Off");
    }

// ============================================================================
// PowerPMACComposedAxisActuator::go_to
// ============================================================================
//
    void go_to( const std::vector<PositionRequest>& positions )
    {
        Tango::DeviceAttribute freeze_attr("freeze",true);
        proxy_->write_attribute( freeze_attr);

        try
        {
       		this->WaitingStateChangeActuator::go_to(positions);
        }
        catch(...)
        {
            // silent catch to protect from abort action to ensure unfreeze
            // to be done
        }

       	freeze_attr << false;
       	proxy_->write_attribute( freeze_attr);

    }

    // ============================================================================
    // PowerPMACComposedAxisActuator::abort
    // ============================================================================
    //
        void abort( void )
        {
        	// First abort as usual
       		this->WaitingStateChangeActuator::abort();

            //- check the state is not MOVING anymore before telling that abort is completed : PROBLEM-1184

       		Tango::DevState state = Tango::MOVING;
             while( state == Tango::MOVING )
             {
                 // wait 10 msec
                 yat::ThreadingUtilities::sleep( 0, 10000000 );
                 state = proxy_->state();
             }

          }

private: //! private implementation

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PowerPMACComposedAxisActuator, \
                          ScanUtils::PowerPMACComposedAxisActuatorInfo);
