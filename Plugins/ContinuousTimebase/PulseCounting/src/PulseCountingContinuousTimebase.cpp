/*!
 * \file
 * \brief    Definition of PulseCountingContinuousTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <boost/scoped_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeContinuousTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/StringTokenizer.h>

namespace ScanUtils
{

class PulseCountingContinuousTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PulseCountingContinuousTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledContinuousTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PulseCounting");
    }
};

// Creation cf http://jira.synchrotron-soleil.fr/jira/browse/SCAN-606
class PulseCountingContinuousTimebase : public WaitingStateChangeContinuousTimebase
{
public: //! structors

    PulseCountingContinuousTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 500;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1.0;
        config_.nbpoint_attr          = "totalNbPoint";
        config_.start_cmd_name        = "Start";
    }

public: //! ITimebase implementation

    void init( std::string device_name )
    {
        this->WaitingStateChangeContinuousTimebase::init( device_name );

		// checking if acquisitionMode is equal to BUFFERED
		Tango::DeviceAttribute l_acquisitionMode;
		try
		{
			l_acquisitionMode = proxy_->read_attribute("acquisitionMode");
		}
		catch( Tango::DevFailed& df )
		{
			yat4tango::TangoYATException e(df);
			RETHROW_YAT_ERROR(	e,
								"TANGO_ERROR",
								"PulseCounting error : acquisitionMode cannot be read",
								"PulseCountingContinuousTimebase::init");
		}

		std::string mode;
		l_acquisitionMode >> mode;
		if (mode.compare("BUFFERED") != 0)
		{
			std::ostringstream oss;
			oss << "The device PulseCounting : "
				<< device_name
				<< " is not well configured. acquisitionMode attribute must be set to BUFFERED.";
			THROW_DEVFAILED( 	"CONTINUOUS_TIMEBASE_ERROR",
								oss.str().c_str(),
								"PulseCountingContinuousTimebase::init" );
		}

		// checking if 'IsMaster' exist
		Tango::DbData prop_data;
		prop_data.push_back( Tango::DbDatum("IsMaster") );
		proxy_->unsafe_proxy().get_property( prop_data );
		if ( prop_data[0].is_empty() )
		{
			std::ostringstream oss;
			oss << "The device PulseCounting : "
				<< device_name
				<< " has no property named 'IsMaster'. This is abnormal and the device is not usable as is";
			THROW_DEVFAILED( 	"CONTINUOUS_TIMEBASE_ERROR",
								oss.str().c_str(),
								"PulseCountingContinuousTimebase::init" );
		}

        // ensures that state is STANDBY
        Tango::DevState state = proxy_->state();
        if ( state != Tango::STANDBY )
        {
            std::ostringstream oss;
			oss << "The device "
				<< device_name
				<< " state is "
				<< Tango::DevStateName[state]
				<< "."
				<< std::endl
				<< "It should be STANDBY";

            THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "PulseCountingContinuousTimebase::init" );
        }


		// Read CounterXX properties to extract counter name, mode & proxy
		unsigned int total_counter = 0;
		for (unsigned int l_current_ctr_nb = 0; l_current_ctr_nb < 32; l_current_ctr_nb++)
		{
			// try to read properties for CounterXX
			char num_ctr[3];
			sprintf(num_ctr, "%02d", l_current_ctr_nb);
			std::string l_ct = std::string("Counter") + std::string(num_ctr);

			Tango::DbData prop_data;
			prop_data.push_back( Tango::DbDatum(l_ct) );
			proxy_->unsafe_proxy().get_property( prop_data );
			if (prop_data[0].is_empty())
			{
				// noop - this counter doesn't exist
			}
			else
			{
				total_counter++;

				// Property values reading 
				std::vector<std::string> l_def;
				prop_data[0] >> l_def;

				// Parse config vector and extract KEY:value fields
				std::map<std::string, std::string> acqKeyList;
				for (std::vector<std::string>::iterator it = l_def.begin() ; it != l_def.end(); ++it)
				{
					std::vector<std::string> tokenList;
					yat::StringTokenizer st(*it, std::string(":"));

					while (st.has_more_tokens()) // while there is a remaining token
					{
						std::string token_str = st.next_token();
					tokenList.push_back(token_str);
					}

					// insert <key,value> in map
					acqKeyList.insert(std::pair<const std::string, std::string>(tokenList[0], tokenList[1]));
				}

				// extract name
				std::map<std::string, std::string>::iterator itn = acqKeyList.find(std::string("Name"));
				if (itn == acqKeyList.end())
				{
					std::ostringstream oss;
					oss << "The device PulseCounting : "
						<< device_name
						<< " has bad property definition for "
						<< l_ct
						<< " : counter 'Name' not defined. This is abnormal and the device is not usable as is";
					THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
									 oss.str().c_str(),
									 "PulseCountingContinuousTimebase::init" );  
				}
				
				std::string name = itn->second;

				// extract mode
				itn = acqKeyList.find(std::string("Mode"));
				if (itn == acqKeyList.end())
				{
					std::ostringstream oss;
					oss << "The device PulseCounting : "
						<< device_name
						<< " has bad property definition for "
						<< l_ct
						<< " : counter 'Mode' not defined. This is abnormal and the device is not usable as is";
					THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
									 oss.str().c_str(),
									 "PulseCountingContinuousTimebase::init" ); 
				}
				
				std::string mode = itn->second;

				if (mode.compare(std::string("POS")) == 0)
				{
					// if mode = "POS" => actuator
					actuators_.push_back(name);
				}
				else
				{
					// if other => sensor
					sensors_.push_back(name);
				}
		
				// extract proxy only for POS counters
				if (mode.compare(std::string("POS")) == 0)
				{
					itn = acqKeyList.find(std::string("Proxy"));
					if (itn == acqKeyList.end())
					{
						std::ostringstream oss;
						oss << "The device PulseCounting : "
							<< device_name
							<< " has bad property definition for "
							<< l_ct
							<< " : counter 'Proxy' not defined. This is abnormal and the device is not usable as is";
						THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
										 oss.str().c_str(),
										 "PulseCountingContinuousTimebase::init" ); 
					}
					else
					{
						std::string prxy = itn->second;
						actuators_proxies_.push_back(prxy);
					}
				}
			}
		}
  
		// check if at least one counter defined
		if (total_counter == 0)
		{
			std::ostringstream oss;
			oss << "The device PulseCounting : "
				<< device_name
				<< " has bad property definition for 'CounterXX'. This is abnormal and the device is not usable as is";
			THROW_DEVFAILED( "CONTINUOUS_TIMEBASE_ERROR",
							 oss.str().c_str(),
							 "PulseCountingContinuousTimebase::init" );  
		}


        BOOST_FOREACH( std::string& s, sensors_ )
        {
            boost::to_lower( s );
        }
        BOOST_FOREACH( std::string& s, actuators_ )
        {
            boost::to_lower( s );
        }
        BOOST_FOREACH( std::string& s, actuators_proxies_ )
        {
            boost::to_lower( s );
        }
    }
    
    void check_initial_condition( )
    {
		WaitingStateChangeContinuousTimebase::check_file_generation_condition("nexusFileGeneration");
    }
    
	// overload start function
    void start( double integration_time, long nb_point )
    {
		long bd = 1;
		std::vector<Tango::DeviceAttribute> attrs;
		// checking if 'IsMaster' is true
		Tango::DbData prop_data;
		prop_data.push_back( Tango::DbDatum("IsMaster") );
		proxy_->unsafe_proxy().get_property( prop_data );
		bool is_master;
		prop_data[0] >> is_master;
		if(is_master)//attribute integrationTime does not exist in slave mode
		{
			attrs.push_back( Tango::DeviceAttribute(config_.integration_time_attr, integration_time * config_.integration_time_gain) );
		}
		
		attrs.push_back( Tango::DeviceAttribute(config_.nbpoint_attr, (Tango::DevLong) nb_point) );
		
		if ((integration_time * config_.integration_time_gain) < 1)
		{
			bd = (long)(1 / (integration_time * config_.integration_time_gain)); // to get intermediate buffer every second
		}
		
		attrs.push_back( Tango::DeviceAttribute("bufferDepth", (Tango::DevLong) bd) );
		proxy_->write_attributes( attrs );
		proxy_->command_inout( config_.start_cmd_name );
    }

    std::vector<std::string> get_sensors()
    {
        return sensors_;
    }

    std::vector<std::string> get_actuators()
    {
        return actuators_;
    }

    std::vector<std::string> get_actuators_proxies()
    {
        return actuators_proxies_;
    }

private: //! private implementation

    std::vector<std::string> sensors_;
    std::vector<std::string> actuators_;
    std::vector<std::string> actuators_proxies_;
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PulseCountingContinuousTimebase,
                          ScanUtils::PulseCountingContinuousTimebaseInfo);

