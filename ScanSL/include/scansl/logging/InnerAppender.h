#ifndef _SCANSERVER_INNER_APPENDER_H_
#define _SCANSERVER_INNER_APPENDER_H_

#include <deque>
#include <scansl/logging/ILogReceiver.h>
#include <boost/shared_ptr.hpp>
#include <yat/threading/Mutex.h>

namespace ScanUtils
{

class SCANSL_DECL InnerAppender : public log4tango::Appender
{
public:
    typedef std::deque<FormattedLogEventP> Logs;

public:
    /**
     *
     **/
    InnerAppender (const std::string& name,
                   bool open_connection=true);
    /**
     *
     **/
    virtual ~InnerAppender ();

    /**
     *
     **/
    virtual bool requires_layout (void) const;

    /**
     *
     **/
    virtual void set_layout(log4tango::Layout* layout);

    /**
     *
     **/
    virtual void close (void);

    /**
     *
     **/
    virtual bool reopen (void);

    /**
     *
     **/
    virtual bool is_valid (void) const;
    
    
    void get_logs( Logs& logs );

    void reinit_logs( void );
    
protected:
    /**
     *
     **/
    virtual int _append (const log4tango::LoggingEvent& event);

private:
    Logs logs;
    yat::Mutex mutex;
    //tm::ptime start_date;
};

} // namespace ScanServer_ns

#endif // _INNER_APPENDER_H_
//#endif // TANGO_HAS_LOG4TANGO
