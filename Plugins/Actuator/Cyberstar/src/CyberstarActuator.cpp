/*!
 * \file
 * \brief    Definition of CyberstarActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class CyberstarActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "CyberstarActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("CYBERSTARx1000");
    }
};

class CyberstarActuator : public WaitingStateChangeActuator
{
public:
    CyberstarActuator()
    {
        config_.move_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 200;
        config_.abort_cmd_name    = ""; //pas de stop
        config_.timeout_ms        = 5000;
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::CyberstarActuator, ScanUtils::CyberstarActuatorInfo);
