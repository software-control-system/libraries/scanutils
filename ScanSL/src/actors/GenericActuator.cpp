#include <limits>
#include <scansl/Util.h>
#include <scansl/actors/GenericActuator.h>
#include <boost/scoped_ptr.hpp>
#include <boost/algorithm/string.hpp>

namespace ScanUtils
{
GenericActuator::GenericActuator()
{
}

GenericActuator::~GenericActuator()
{
}

void GenericActuator::init( std::string device_name )
{
    device_name_ = device_name;
    proxy_ = DevProxies::instance().proxy(device_name);
    attr_info_.reset( this->proxy_->attribute_list_query() );
}

void GenericActuator::check_initial_condition( std::string ) {
    // no-op
}

void GenericActuator::ensure_supported( std::string attr_name )
{
    const Tango::AttributeInfoList& attr_info = *attr_info_;
    Tango::AttributeInfoList::const_iterator it(attr_info.begin());

    for (; it != attr_info.end(); ++it)
    {
        if ( boost::to_lower_copy( (*it).name ) == attr_name )
            break;
    }

    //- ensure attribute exists
    if ( it == attr_info.end() )
    {
        yat::OSStream oss;
        oss << attr_name
            << " is not a valid attribute name for the device "
            << device_name_;

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericActuator::is_allowed");
    }

    const Tango::AttributeInfo& attr_config = *it;

    //- ensure attribute is a SCALAR
    if ( attr_config.data_format != Tango::SCALAR )
    {
        yat::OSStream oss;
        oss << "The attribute "
            << attr_config.name
            << " is not SCALAR";

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericActuator::init");
    }

    //- ensure attribute type is supported
    if ( attr_config.data_type == Tango::DEV_STRING
         || attr_config.data_type == Tango::CONST_DEV_STRING
         || attr_config.data_type == Tango::DEV_STATE
         || attr_config.data_type == Tango::DEV_LONG64
         || attr_config.data_type == Tango::DEV_ULONG64 )
    {
        yat::OSStream oss;
        oss << "The type of the attribute "
            << attr_config.name
            << " is not suppported";

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericActuator::init");
    }

    //- ensure attribute is a writable
    if ( attr_config.writable != Tango::WRITE && attr_config.writable != Tango::READ_WRITE )
    {
        yat::OSStream oss;
        oss << "The attribute "
            << attr_config.name
            << " is not writable";

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericActuator::init");
    }
}

void GenericActuator::set_position( std::string, double )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "offset feature is not supported",
                     "GenericActuator::set_position" );
}

bool GenericActuator::is_enabled( std::string )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "on/off feature is not supported",
                     "GenericActuator::is_enabled" );
}

void GenericActuator::set_enabled( std::string, bool )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "on/off feature is not supported",
                     "GenericActuator::set_enabled" );
}

LimitsType GenericActuator::get_limits( std::string attr_name )
{
    Tango::DeviceProxy& px = proxy_->unsafe_proxy();
    Tango::AttributeInfoEx info = px.get_attribute_config(attr_name);
    return LimitsType(info.min_value, info.max_value);
}

void GenericActuator::set_limits( std::string attr_name, const LimitsType& limits )
{
    Tango::DeviceProxy& px = proxy_->unsafe_proxy();
    Tango::AttributeInfoEx info = px.get_attribute_config(attr_name);
    info.min_value = limits.first;
    info.max_value = limits.second;
    Tango::AttributeInfoListEx infos(1, info);
    px.set_attribute_config(infos);
}

double GenericActuator::get_speed( std::string )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "speed feature is not supported",
                     "GenericActuator::get_speed" );
}

void GenericActuator::set_speed( std::string, double )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "speed feature is not supported",
                     "GenericActuator::set_speed" );
}

void GenericActuator::go_to( const std::vector<PositionRequest>& positions )
{
    std::vector<Tango::DeviceAttribute> dev_attrs;
    for( std::size_t i = 0; i < positions.size(); ++i )
    {
        dev_attrs.push_back( Util::instance().create_device_attribute(*positions[i].attribute_info, positions[i].position) );
    }
    proxy_->write_attributes( dev_attrs );
}

bool GenericActuator::is_moving( void )
{
    return false;
}

void GenericActuator::error_check( void )
{
    // do nothing : no error on GenericActuator unless we get an exception
    // when reading or writing the value.
}

double GenericActuator::get_polling_period_ms( void )
{
    return 10.0;
}

void GenericActuator::abort( void )
{
    //- empty implementation
}

//debut SPYC-155
std::vector< std::string > GenericActuator::read_fixed_pos_list(void){
        /*lecture de la liste des positions possibles d un device*/
	
        THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read fixed position list is not supported",
                     "GenericActuator::read_fixed_pos_list" );
}//fin read_fixed_pos_list

std::string GenericActuator::read_current_value(void){
        /*lecture de la valeur courante d un moteur d un device*/

        THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read current attribute value is not supported",
                     "GenericActuator::read_current_value" );   
}//fin read_current_value
   
//fin SPYC-155

//debut SPYC-300
	//SpycStates
std::string GenericActuator::read_selected_position(void){
	/*lecture du nom de la destination finale ou l equipement doit etre deplace*/
       THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read selected position value is not supported",
                     "GenericActuator::read_selected_position" );  
}//fin read_selected_position
//fin SPYC-300


}

