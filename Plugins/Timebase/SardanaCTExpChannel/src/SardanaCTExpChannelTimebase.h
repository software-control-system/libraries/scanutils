/*!
 * \file
 * \brief    Declaration of SardanaCTExpChannelTimebase class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_SardanaCTExpChannel_H_
#define _SCANSERVER_SardanaCTExpChannel_H_


#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>

namespace ScanUtils
{
class SardanaCTExpChannelTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};

class SardanaCTExpChannelTimebase : public WaitingStateChangeTimebase
{
public:
    SardanaCTExpChannelTimebase();
};

}

#endif
