﻿/*!
* \file
* \brief    Definition of HexapodFixedPositionsActuator class
* \author   Ludmila KLENOV - Synchrotron SOLEIL
*/
//debut SPYC-155
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat/utils/String.h>
#include <yat/utils/Logging.h>
#include <yat/utils/StringTokenizer.h>



namespace ScanUtils
{
class HexapodFixedPositionsActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "HexapodFixedPositionsActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }


    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }



public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("FixedPositions");
    }
};

class HexapodFixedPositionsActuator : public WaitingStateChangeActuator
{
public:
    HexapodFixedPositionsActuator()
    {
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.move_state        = Tango::MOVING; // l etat du device en mouvement
        config_.abort_cmd_name    = "Stop"; //stop command
	//Debut SPYC-300
        config_.selected_attr_name= "selectedAttributeName"; //nom de la position selectionnee
        //Fin SPYC-300
    }

// ============================================================================
// init
// ============================================================================
void init( std::string device_name )
    {
        
        this->WaitingStateChangeActuator::init( device_name );

	final_position = -1; //index de la liste des positions possibles
       // Lire l attribute labelList du device 	
        label_list = read_fixed_pos_list();
   
    }//fin init



// ============================================================================
// read_fixed_pos_list
// ============================================================================
std::vector<std::string> read_fixed_pos_list() {
       /*lecture de la liste des positions possibles dans le device (en dure).
       */
	std::vector<std::string> label_list; //la liste des positions possibles
        std::ostringstream oss; //description d erreur possible
        Tango::DeviceProxy& the_inner_proxy = proxy_->unsafe_proxy(); //proxy vers le device
        //lecture de l attribute
        //extraction de la liste des positions possibles
        //si l attribute est vide, alors erreur
        Tango::DeviceAttribute label_list_attr = proxy_->read_attribute("labelList");
        if (! label_list_attr.is_empty())
            label_list_attr >> label_list; //extraction vers la liste
        else {
		oss << "The device " 
            		<< config_.dev_name
            		<< " : labelList attribute is empty";
		THROW_YAT_ERROR("PLUGIN_ERROR",
                               oss.str().c_str(),
                              "HexapodFixedPositionsActuator::read_fixed_pos_list");
        }
        return label_list;
}

// ============================================================================
// read_current_value
// ============================================================================
std::string read_current_value(){
  /*lecture de la valeur courant du motor.
  */
        
   return "";   
}//fin read_current_value


// ============================================================================
// go-to
// ============================================================================
void go_to(const std::vector<PositionRequest>& positions ){
     /*Envoyer le moteur a la position demandee.
       Parametre positions contient l index de la liste des positions possibles.
       Les valeurs de positions possibles : de 1 a la longeur de la liste.
     */
    	
     //recuperation de l index de la liste des positions possibles
     const PositionRequest& pos_req = positions[0];

     std::ostringstream oss; //description d erreur possible
     
     //l index de la liste des positions possibles.
	// index commence a 1, la liste commence a 0
     double pos = pos_req.position - 1; 
     final_position = pos;
     //si l index est plus petit que zero, erreur
     if(pos < 0){
	oss << "The device " 
            	<< config_.dev_name
            	<< " : Index of the labelList is smaller than zero";	
        THROW_YAT_ERROR("PLUGIN_ERROR",
            	oss.str().c_str(),
            	"HexapodFixedPositionsActuator::go_to");
     }


     //si l index est plus grand que la longeur de la liste, erreur
     if((size_t)pos >= label_list.size()){	
	
	oss << "The device " 
            	<< config_.dev_name
            	<< " : Index of the labelList is greater than its length";

        THROW_YAT_ERROR("PLUGIN_ERROR",
		oss.str().c_str(),
            	"HexapodFixedPositionsActuator::go_to");
     }
     
     
     //ecriture dans l attribut et envoie du moteur à la position demandee.
     try {
     Tango::DeviceAttribute enum_attr(label_list[(size_t)pos].c_str(), true);
     proxy_->write_attribute(enum_attr);
	}catch (Tango::DevFailed& df) {
		
        	oss << "The device " 
            		<< config_.dev_name
            		<< df.errors[0].desc;
                THROW_YAT_ERROR("PLUGIN_ERROR",
                                oss.str().c_str(),
                                "HexapodFixedPositionsActuator::go_to");
        }
   
}//fin go_to

// ============================================================================
// get_limits
// ============================================================================
LimitsType get_limits( std::string attr_name )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "HexapodFixedPositionsActuator::get_limits" );
}//fin get_limits

// ============================================================================
// set_limits
// ============================================================================
void set_limits( std::string attr_name, const LimitsType& limits )
{
    THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "limits feature is not supported",
                     "HexapodFixedPositionsActuator::set_limits" );
}//fin set_limits

protected:
	double final_position; //index de la liste des positions possibles
        std::vector<std::string> label_list;//list des positions possibles dans le device

};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::HexapodFixedPositionsActuator, \
                          ScanUtils::HexapodFixedPositionsActuatorInfo);
//fin SPYC-155
