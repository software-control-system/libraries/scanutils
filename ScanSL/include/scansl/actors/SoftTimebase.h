#ifndef _SCANSERVER_SOFTTIMEBASE_H
#define _SCANSERVER_SOFTTIMEBASE_H

#include <scansl/ScanSL.h>
#include <scansl/actors/ITimebase.h>
#include <boost/shared_ptr.hpp>
#include <yat/threading/Task.h>

namespace ScanUtils
{


class SCANSL_DECL SoftTimebase : public ITimebase,
        public yat::Task
{
public:
    SoftTimebase( );

    virtual void init( std::string device_name, bool sync );

    virtual void check_initial_condition( void );

    virtual void start( double integration_time );

    virtual bool is_counting( void );

    virtual void error_check( void );

    virtual void abort( void );

    virtual void before_run( void );

    virtual void after_run( void );

protected:
    virtual void handle_message (yat::Message& msg)
    throw (yat::Exception);

private:
    bool is_counting_;
    yat::Timer timer_;
    double int_time_;
};

}

#endif
