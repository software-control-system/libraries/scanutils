//*****************************************************************************
// Synchrotron SOLEIL
//
// NeXus Access API through DataRecorder device
//
// Cr�ation : 19/06/2008
// Auteur   : S. Poirier
//
//*****************************************************************************

#ifndef __NEXUS4TANGO_H__
#define __NEXUS4TANGO_H__

#include <string>

// For convenience
#define TheDataRecorderWriteLock DataRecorderWriteLock::Instance()

//=============================================================================
/// Singleton type class allowing write access to the current NeXus file in the DataRecorder device
//=============================================================================
class DataRecorderWriteLock
{
public:
	class Exception: public std::exception
	{
	private:
		const char *m_description;
	public:
		Exception(const char *description) { m_description = description; }

		virtual const char* what() const throw()
		{
          return m_description;
		}
	};

private:
	std::string m_datarecorder_name;
	long   m_lock_msg;
	std::string m_file_path;
	std::string m_nxentry_name;
	void  *m_private_data;
	bool m_is_recording_manager;
	bool PrivRequestAccess(int life_time=0);

	// Private constructor
	DataRecorderWriteLock();

public:
	/// Return reference to the unique object instance
	///
	static DataRecorderWriteLock &Instance();

	/// Request exclusive access to current NeXus file
	/// If allowed the access is granted for 3 sec by default
	///
	/// @param  life_time The granted access expire when this time is elapsed
	/// @param time_out if it isn't possible to get access util time_out then return
	/// @return 'true' if access is granted, otherwise 'false'
	///
	bool RequestAccess(int life_time=3, long time_out=0);

	/// Manually release access
	///
	void ReleaseAccess();

	/// Returns NeXus file path
	///
	const std::string &FilePath() { return m_file_path; }

	/// Returns currentNXentry name
	///
	const std::string &NXentryName() { return m_nxentry_name; }

	/// Set the DataRecorder device name
	///
	void SetDataRecorder(const std::string &datarecorder_name, bool is_recording_manager=false);

private:
	/// Not implemented private members
	DataRecorderWriteLock (const DataRecorderWriteLock&);
	DataRecorderWriteLock& operator=(const DataRecorderWriteLock&);
};

#endif // __NEXUS4TANGO_H__
