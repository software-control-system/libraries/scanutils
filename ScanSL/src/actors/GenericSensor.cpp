#include <scansl/actors/GenericSensor.h>
#include <boost/algorithm/string.hpp>
#include <yat4tango/CommonHeader.h>

namespace ScanUtils
{

GenericSensor::GenericSensor()
{
    // do nothing
}

GenericSensor::~GenericSensor()
{
    // do nothing
}

void GenericSensor::init(  std::string device_name, bool sync )
{
    device_name_ = device_name;
    proxy_ = DevProxies::instance().proxy(device_name);
    attr_info_.reset( this->proxy_->attribute_list_query() );
}

void GenericSensor::check_initial_condition( ) {
    // do nothing
}

void GenericSensor::ensure_supported(  std::string attr_name )
{
    const Tango::AttributeInfoList& attr_info = *attr_info_;
    Tango::AttributeInfoList::const_iterator it(attr_info.begin());
    for (; it != attr_info.end(); ++it)
    {
        if ( boost::to_lower_copy((*it).name) == attr_name )
            break;
    }

    //- ensure attribute exists
    if ( it == attr_info.end() )
    {
        yat::OSStream oss;
        oss << attr_name
            << " is not a valid attribute name for the device "
            << device_name_;

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericSensor::ensure_supported");
    }

    const Tango::AttributeInfo& attr_config = *it;

    //- ensure attribute is a SCALAR
    /*
    if ( attr_config.data_format != Tango::SCALAR )
    {
      yat::OSStream oss;
      oss << "The attribute "
          << attr_config.name
          << " is not SCALAR";

      THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                      oss.str().c_str(),
                      "GenericSensor::ensure_supported");
    }
    */

    //- ensure attribute type is supported
    if ( attr_config.data_type == Tango::DEV_STRING
         || attr_config.data_type == Tango::CONST_DEV_STRING
         || attr_config.data_type == Tango::DEV_STATE)
    {
        yat::OSStream oss;
        oss << "The type of attribute "
            << attr_config.name
            << " is not suppported";

        THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                        oss.str().c_str(),
                        "GenericSensor::ensure_supported");
    }

    //- ensure attribute is a readable
    /*if ( attr_config.writable != Tango::READ && attr_config.writable != Tango::READ_WRITE )
    {
      yat::OSStream oss;
      oss << "The attribute "
          << attr_config.name
          << " is not readable";

      THROW_YAT_ERROR("UNSUPPORTED_ATTR",
                      oss.str().c_str(),
                      "GenericSensor::ensure_supported");
    }*/
}

void GenericSensor::before_integration( void )
{
    // do nothing
}

void GenericSensor::after_integration( void )
{
    // do nothing
}

void GenericSensor::before_run( void )
{
    // do nothing
}

void GenericSensor::after_run( void )
{
    // do nothing
}

void GenericSensor::abort( void )
{
    // do nothing
}

void GenericSensor::set_parameter( const std::string&, const string&)
{
    // do nothing
}
}

