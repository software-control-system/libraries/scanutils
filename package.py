#!/usr/bin/env python

"""
Recursively collects all the shared libraries previously generated
by `mvn install` in the directory ./Plugins and copy them in a .zip archive.
Note that the archive is not compressed (equivalent to tar).
The whole archive is updated if it already exists.
"""

# TODO:
# * add options to black list plugins by name  ...
# * let plugin_path be configurable
# * let output paths be configurable


from __future__ import print_function, with_statement
import os
import os.path as osp
import sys
import glob
import argparse
import subprocess
import zipfile
import fnmatch
import re

__author__ = 'Julien Berthault'
__version__ = '1.0.0'


###########
# helpers #
###########

# @contextlib.contextmanager
# def change_dir(path):
#     """temporarily change current working directory"""
#     cwd = os.getcwd()
#     os.chdir(path)
#     try:
#         yield
#     finally:
#         os.chdir(cwd)


# def ensure_dir_exists(path):
#     try:
#         os.makedirs(path)
#     except OSError as err:
#         if err.errno != errno.EEXIST:
#             raise


def zip_transfer(in_zip, out_zip, names=None, path=None, pwd=None,
                 compress_type=None):
    """
    transfer files from one archive to another (thus reset files permissions)

    in_zip & out_zip
      ZipFiles objects considered opened, respectively 'r' & 'w'
    names
      list of filenames in the input zipfile (not ZipInfo)
      if None or empty, all its content will be copied
    path
      output directory in out_zip.
      if None, it keeps the same structure (creating intermediate folders)
      if empty, the archive root is assumed
    pwd
      password used to decompress input files
    compress_type
      output compression type, None standing for no compression
    """
    for name in names or in_zip.namelist():
        arcname = name if path is None else osp.join(path, osp.basename(name))
        out_zip.writestr(arcname, in_zip.read(name, pwd), compress_type)


def display(*args):
    """prints messages over standard output prefixing a header"""
    print('[PACKAGING]', *args)


#################
# configuration #
#################

# regex finding old snapshot version-yyyyMMdd.hhmmss-n-...
dup_pattern = re.compile(r'\d{8}\.\d{6}-\d+')
# directory of this script
scanutils_path = osp.abspath(osp.dirname(__file__))
# sibling directory Plugins
default_plugin_path = osp.join(scanutils_path, 'Plugins')
# try to detect windows platform
windows = 'win32' in sys.platform
# maven may be callable from console, so add .bat on windows
maven_command = 'mvn.bat' if windows else 'mvn'
# command that must be launched by option -i
install_command = "%s clean install -DenableCheckRelease=false" % maven_command
# globbing pattern of shared libraries
shared_pattern = '*.dll' if windows else '*.so'
# key -> dirname containing plugins ; value -> output directory
all_categories = {
    'Actuator': 'actuator',
    'ContinuousTimebase': 'continous_timebase',
    'Sensor': 'sensor',
    'Timebase': 'timebase',
}


###################
# program options #
###################

parser = argparse.ArgumentParser(description=__doc__)
parser_filters = parser.add_argument_group(
    'filtering options', 'options related to plugins selection'
)
parser.add_argument('-v', '--version', action='version', version=__version__)
parser_filters.add_argument(
    '--m2', metavar='VERSION',
    help="lookup modules in ~/.m2/repository instead of targets"
         " you must specify the version(s) to collect by a globbing format"
)
parser_filters.add_argument(
    '--mode', choices=('debug', 'release'), default='release',
    help="mode of plugins that must be collected (unused for local target)"
)
parser_filters.add_argument(
    '--aol', default='*',
    help='collect libraries that match the given globbing aol'
)
parser_filters.add_argument(
    '-x', '--exclude', metavar='CATEGORY', nargs='+', choices=all_categories,
    help="ignore the given categories (Actuator, Sensor, ...)"
)
parser.add_argument(
    '-i', '--install', action='store_true',
    help="run `%s` before collecting files" % install_command
)
parser.add_argument(
    '-o', '--output', required=True,
    help="name of the output archive (required)"
)


def main():
    # parse options
    ns = parser.parse_args()
    # filter out excluded categories
    exclude = ns.exclude or []
    categories = {k: v for k, v in all_categories.items() if k not in exclude}
    if not categories:
        raise Exception("can't exclude all categories")
    # seek plugins path (absolute path is required)
    plugins_path = default_plugin_path if ns.m2 is None else (
        osp.expanduser('~/.m2/repository/fr/soleil/lib/ScanUtils/Plugins')
    )
    # check plugin path (raises OSError with errno 2 if it does not exist)
    has_categories = set(all_categories).issubset(os.listdir(plugins_path))
    if not has_categories:
        raise Exception('wrong Plugins structure, expected Actuator, ...')
    # path components from plugin category to nar files
    nar_pattern = '*/target/*.nar' if ns.m2 is None else (
        '*-%s-shared-%s/%s/*.nar' % (ns.aol, ns.mode, ns.m2)
    )
    # run installation command (maven)
    if ns.install:
        display('running "%s"' % install_command)
        # call the subprocess changing its current working dir
        # raise CalledProcessError if failed
        subprocess.check_call(install_command.split(), cwd=scanutils_path)
    # store files extracted
    extracted = []
    # open and clear the output archive (if exists)
    with zipfile.ZipFile(ns.output, 'w') as archive:
        # iteratate over included categories
        for category, arcdir in categories.items():
           # get all .nar archives
            for nar in glob.glob(osp.join(plugins_path, category, nar_pattern)):
                # skip duplicate snapshots
                if dup_pattern.search(osp.basename(nar)):
                    continue
                # open archive
                with zipfile.ZipFile(nar) as arc:
                    # get all shared libs in the archive
                    filenames = fnmatch.filter(arc.namelist(), shared_pattern)
                    # copy these libs into the output archive
                    zip_transfer(arc, archive, filenames, arcdir)
                    # notify user
                    for filename in filenames:
                        display('extracted', osp.basename(filename))
                    # update extracted list
                    extracted += filenames
    # display a warning if no files were copied
    if not extracted:
        display("no shared libraries found in", plugins_path)


if __name__ == '__main__':
    main()
