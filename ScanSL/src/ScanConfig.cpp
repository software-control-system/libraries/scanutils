#include <scansl/ScanConfig.h>
#include <scansl/Util.h>
#include <boost/foreach.hpp>
#include <yat/utils/String.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

const std::string ErrorStrategyNames [] =  { std::string("IGNORE"),
                                             std::string("PAUSE"),
                                             std::string("ABORT") };

ErrorManagement::ErrorManagement()
    : timeout_s(5),
      retry_count(5),
      retry_timeout_s(0.2),
      strategy(ErrorStrategy_IGNORE)
{
}

std::string ErrorManagement::strategy_name() const
{
    return ErrorStrategyNames[ strategy ];
}


AfterRunActionDesc::AfterRunActionDesc()
    : type(AfterRunActionType_NO_ACTION),
      sensor(0),
      actuator(0),
      actuator_value(0)
{
}

ScanProperties::ScanProperties()
    : synchronize_data(true),
      datafile_activation(false),
      Ni6602_start_timebase_delay(0),
      read_attribute_async(false),
      no_throw_on_sensor_error(false),
      keep_sensor_on_failure(false),
      move_dim1_then_dim2_actuators(false),
      acquisitionname_sanitization(false),
      #ifdef SCANSL_ENABLE_NEXUS
      datafile_type(DataFileType_NEXUS),
      datarecorder_write_mode(DataRecorderWriteMode_DONE_BY_DATARECORDER),
      datarecorder_saving_period(30),
      #else
      datafile_type(DataFileType_ASCII),
      #endif
      device(0)
{
}


ScanConfig::ScanConfig()
    : run_name("unnamed"),
      on_the_fly(false),
      hw_continuous(false),
      manual_mode(false),
      enable_scan_speed(false),
      scan_number(1),
      hw_continuous_nbpt(0),
      ctxvalid_behaviour(CtxValidBehaviour_IGNORE),
      actuators_delay(0),
      sensors_delay(0),
      timebases_delay(0),
      automatic_direction(false),
      zig_zag(false),
      synchronous_dimension_change(true)
    #ifdef SCANSL_ENABLE_NEXUS
    ,datarecorder_only_write_scan_data(false)
    ,record_data(true)
    #endif
{
}

ScanConfig::ScanConfig( const ScanConfig& other )
{
    *this = other;
}


ScanConfig& ScanConfig::operator=( const ScanConfig& other )
{
    this->properties = other.properties;
    this->run_name = other.run_name;
    this->on_the_fly = other.on_the_fly;
    this->hw_continuous = other.hw_continuous;
    this->manual_mode = other.manual_mode;
    this->actuators = other.actuators;
    this->actuators2 = other.actuators2;
    this->timebases = other.timebases;
    this->sensors = other.sensors;
    this->integration_times.resize( boost::extents[other.integration_times.shape()[0]] );
    this->integration_times = other.integration_times;
    this->trajectories.resize( boost::extents[other.trajectories.shape()[0]][other.trajectories.shape()[1]] );
    this->trajectories = other.trajectories;
    this->trajectories2.resize( boost::extents[other.trajectories2.shape()[0]][other.trajectories2.shape()[1]] );
    this->trajectories2 = other.trajectories2;
    this->enable_scan_speed = other.enable_scan_speed;
    this->scan_speed.resize( boost::extents[other.scan_speed.shape()[0]] );
    this->scan_speed = other.scan_speed;
    this->scan_number = other.scan_number;
    this->hw_continuous_nbpt = other.hw_continuous_nbpt;
    this->prerun_hooks = other.prerun_hooks;
    this->prescan_hooks = other.prescan_hooks;
    this->prestep_hooks = other.prestep_hooks;
    this->postactuatormove_hooks = other.postactuatormove_hooks;
    this->postintegration_hooks = other.postintegration_hooks;
    this->poststep_hooks = other.poststep_hooks;
    this->postscan_hooks = other.postscan_hooks;
    this->postrun_hooks = other.postrun_hooks;
    this->actuators_mngt = other.actuators_mngt;
    this->timebases_mngt = other.timebases_mngt;
    this->sensors_mngt = other.sensors_mngt;
    this->hooks_mngt = other.hooks_mngt;
    this->context_validation = other.context_validation;
    this->ctxvalid_behaviour = other.ctxvalid_behaviour;
    this->actuators_delay = other.actuators_delay;
    this->sensors_delay = other.sensors_delay;
    this->timebases_delay = other.timebases_delay;
    this->automatic_direction = other.automatic_direction;
    this->zig_zag = other.zig_zag;
    this->synchronous_dimension_change = other.synchronous_dimension_change;
    this->after_run_action = other.after_run_action;

#ifdef SCANSL_ENABLE_NEXUS
    this->datarecorder_only_write_scan_data = other.datarecorder_only_write_scan_data;
    this->record_data = other.record_data;
    this->data_recorder_config = other.data_recorder_config;
    this->info = other.info;
#endif
    return *this;
}


void ScanConfig::clean()
throw( Tango::DevFailed )
{
    ScanProperties previous_properties = this->properties;
    *this = ScanConfig();
    this->properties = previous_properties;
}

void ScanConfig::validate()
throw( Tango::DevFailed )
{
#   define CHECK( condition, msg ) \
    if ( !(condition) ) \
    { \
    THROW_DEVFAILED( "CONFIG_ERROR", \
    msg, \
    "ScanConfig::validate" ); \
}

    const static std::string empty_string = std::string();
    CHECK( properties.plugin_root_path  != empty_string, "PluginRootPath invalid" );
    CHECK( properties.datafile_path     != empty_string, "DataFilePath invalid" );
    CHECK( properties.datafile_basename != empty_string, "DataFileBaseName invalid" );
    CHECK( properties.datafile_type  >= 0 && properties.datafile_type < DataFileType_MAX_, "DataFileType invalid" );

    // BEGIN PROBLEM-1831
    // Check multiple declarations of the same sensor
    bool multiple_decl = false;
    {
      std::set<yat::String> sensors_set;
      for( std::size_t i = 0; i < sensors.size(); ++i )
      {
        yat::String s = sensors[i];
        s.to_lower();
        s.trim();
        if( sensors_set.find(s) == sensors_set.end() )
          sensors_set.insert(s);
        else
        {
          multiple_decl = true;
          break;
        }
      }
    }
    CHECK( !multiple_decl, "Multiple declarations of sensors" );
    // END PROBLEM-1831

#ifdef SCANSL_ENABLE_NEXUS

    CHECK( properties.datarecorder_write_mode >= 0 && properties.datarecorder_write_mode < DataRecorderWriteMode_MAX_, "Invalid DataRecorderWriteMode" );

    //    CHECK( !(properties.datarecorder.empty() && this->record_data && !properties.datafile_activation), "Cannot record data" );
    //    CHECK( !(properties.datarecorder.empty() && this->record_data && !properties.datafile_activation), "Cannot record data" );
    CHECK( !(properties.datarecorder.empty() && !properties.datafile_activation), "Cannot record data" );

#  ifdef WIN32
    CHECK( properties.datarecorder_write_mode == 0, "On windows platform, DataRecorderWriteMode=1 is not supported" );
#  endif

#endif

    CHECK(!actuators.empty() || actuators2.empty(), "you can't declare actuators in the second dimension if no one is declared in the first one");

    if ( !on_the_fly && !hw_continuous )
    {
        CHECK( actuators.size() * integration_times.num_elements() == trajectories.num_elements(),
               "trajectories & integration times must have the same size" );
    }

    if (on_the_fly)
    {
        CHECK( actuators.size() > 0,
               "for a scan ON-THE-FLY, there must be at least one actuator of first dimension" );
    }

    //- check that there is no redondant actuator or sensor
    std::multiset<std::string> all_scanned_attr;
    all_scanned_attr.insert( this->actuators.begin(),  this->actuators.end() );
    all_scanned_attr.insert( this->actuators2.begin(), this->actuators2.end() );

    // SP 2012/05/04 Allow actuators to be used also as sensors
    // all_scanned_attr.insert( this->sensors.begin(),    this->sensors.end() );

    BOOST_FOREACH( const std::string& scanned_attr, all_scanned_attr )
    {
        if ( all_scanned_attr.count(scanned_attr) > 1 )
        {
            std::ostringstream oss;
            oss << "The attribute "
                << scanned_attr
                << " is used several times between ActuatorsX, ActuatorsY or Sensors";
            THROW_DEVFAILED( "CONFIG_ERROR",
                             oss.str().c_str(),
                             "ScanConfig::validate" );
        }
    }


    if (hw_continuous)
    {
        CHECK( hw_continuous_nbpt > 0, "hwContinuousNbPt not set" );

        if ( actuators.empty() )
        {
            CHECK( actuators2.empty(), "In 2D hardware continuous mode, you must have actuators in the first dimension. 2D timescans are not supported yet" );
        }
    }

    if ( (on_the_fly || hw_continuous) && enable_scan_speed )
    {
        CHECK( scan_speed.shape()[0] == actuators.size(), "'scanSpeed' attribute must have a size equal to the number of actuators of 1st dimension" );
    }

    CHECK( integration_times.shape()[0] > 0, "No integrationTimes defined" );
    CHECK( sensors.size() > 0,    "No sensors defined" );

    CHECK( actuators_delay >= 0,  "Actuators Delay must be >= 0" );
    CHECK( sensors_delay >= 0,    "Sensors Delay must be >= 0" );
    CHECK( timebases_delay >= 0,  "Timebases Delay must be >= 0" );

    CHECK( scan_number > 0,       "ScanNumber must be > 0" );
    CHECK( ctxvalid_behaviour >= 0 && ctxvalid_behaviour < CtxValidBehaviour_MAX_,
           "Context Validation Error Strategy invalid" );

    CHECK( after_run_action.type >= 0 && after_run_action.type < AfterRunActionType_MAX_, "Invalid afterRunActionType" );
    CHECK( after_run_action.sensor >= 0 && static_cast<size_t>(after_run_action.sensor) < sensors.size(), "Invalid afterRunActionSensor" );
    CHECK( actuators.size() == 0 || static_cast<size_t>(after_run_action.actuator) < actuators.size(), "Invalid afterRunActionActuator" );

    CHECK( !zig_zag || (!on_the_fly && !hw_continuous), "Zigzag is supported only for step-by-step scans" );

    CHECK( (!automatic_direction) || (automatic_direction && !hw_continuous), "Automatic direction is not yet supported for hardware scans" );

    // everything seems correct : reshape the trajectories to have coherent arrays
    boost::array<boost::multi_array_types::size_type, 2> new_shape;
    if (!actuators.empty())
    {
        new_shape[0] = actuators.size();
        new_shape[1] = trajectories.num_elements() / actuators.size();
        trajectories.reshape( new_shape );
    }
    if (!actuators2.empty())
    {
        new_shape[0] = actuators2.size();
        new_shape[1] = trajectories2.num_elements() / actuators2.size();
        trajectories2.reshape( new_shape );
    }

    // now handles the 'automatic direction' functionnality
    // if true, just inverse each line of the trajectory & the integration_times vector
    if ( automatic_direction && !actuators.empty() )
    {
        Tango::AttributeProxy first_actuator_proxy(actuators[0]);
        Tango::DeviceAttribute dev_attr = first_actuator_proxy.read();
        Tango::AttributeInfoEx attr_info = first_actuator_proxy.get_config();
        double current_position = Util::instance().extract_scalar_as_double( dev_attr, attr_info );

        size_t n = integration_times.num_elements();

        if ( std::fabs(current_position - trajectories[0][0])
             > std::fabs(current_position - trajectories[0][n - 1])  )
        {
            std::reverse( integration_times.data(), integration_times.data() + n );
            for ( size_t i = 0; i < actuators.size(); i++ )
            {
                std::reverse( trajectories.data() + i * n, trajectories.data() + (i + 1) * n );
            }
        }
    }

}

}

