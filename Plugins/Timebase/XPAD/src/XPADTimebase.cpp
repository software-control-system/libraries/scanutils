/*!
 * \file
 * \brief    Definition of XPADTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class XPADTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "XPADTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("XPAD");
    }
};

class XPADTimebase : public WaitingStateChangeTimebase
{

public:
    XPADTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 50;
        config_.start_cmd_name        = "Start";
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;

        config_.integration_time_attr = "exposure";
        config_.integration_time_gain = 1E3;
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XPADTimebase,
                          ScanUtils::XPADTimebaseInfo);
