/*!
 * \file
 * \brief    Declaration of MCA8715Sensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_MCA8715_H_
#define _SCANSERVER_MCA8715_H_

#include <scansl/interfaces/ISensor.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>

namespace ScanUtils
{

class MCA8715SensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};


class MCA8715Sensor : public Sensor
{
public: //! structors

    MCA8715Sensor();

    ~MCA8715Sensor();

public: //! ISensor implementation

    virtual void init( std::string device_name, bool sync);

    virtual void before_integration( void );

    virtual void after_integration( void );

private: //! private implementation

    //! configuration
    ProxyP proxy_;
    bool must_arm_;
};

}

#endif
