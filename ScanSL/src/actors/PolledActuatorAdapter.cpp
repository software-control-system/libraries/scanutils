#include <scansl/actors/PolledActuatorAdapter.h>
#include <scansl/plugin/MParameters.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
/*
  class TraceHelper
  {
  public:
    TraceHelper::TraceHelper(const char* _func_name )
      :  func_name(_func_name)
    {
      std::cout << func_name
                << " <-"
                << std::endl;
    };

    TraceHelper::~TraceHelper()
    {
      std::cout << func_name
                << " ->"
                << std::endl;
    };

  private:
    const char* func_name;
  };
  */


const size_t POSITIONMENT_REQUEST_MSG = yat::FIRST_USER_MSG;
const size_t INIT_PLUGIN_MSG          = yat::FIRST_USER_MSG + 1;
const size_t ABORT_REQUEST_MSG        = yat::FIRST_USER_MSG + 2;

PolledActuatorAdapter::PolledActuatorAdapter( IPolledActuatorPtr polled_actuator_obj )
    : polled_actuator_obj_(polled_actuator_obj),
      is_moving_(false),
      has_error_(false)
{
    //TraceHelper t( "PolledActuatorAdapter::PolledActuatorAdapter" );
    this->go();
}

void PolledActuatorAdapter::init( std::string device_name )
{
    //TraceHelper t( "PolledActuatorAdapter::init" );
    // send the message asynchronously
    yat::Message* msg = yat::Message::allocate(INIT_PLUGIN_MSG, DEFAULT_MSG_PRIORITY, true );
    msg->attach_data( device_name );
    this->wait_msg_handled( msg );
}

void PolledActuatorAdapter::check_initial_condition( std::string attr_name ) {
	polled_actuator_obj_->check_initial_condition(attr_name);
}

void PolledActuatorAdapter::set_parameter( const std::string &name, const string &value)
{
    // Insert parameter : Has it is used in init , it must be set before
    MParameters* parametrized_object_p = dynamic_cast<MParameters*>(polled_actuator_obj_.get());
    if( parametrized_object_p )
        parametrized_object_p->set_parameter(name, value);
    else
        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        "Cannot set parameter on actuator plugin object",
                        "PolledActuatorAdapter::set_parameter");
}

void   PolledActuatorAdapter::ensure_supported( std::string attr_name )
{
    //TraceHelper t( "PolledActuatorAdapter::ensure_supported" );
    polled_actuator_obj_->ensure_supported(attr_name);
}

void PolledActuatorAdapter::set_position( std::string attr_name, double position )
{
    polled_actuator_obj_->set_position(attr_name, position);
}

bool PolledActuatorAdapter::is_enabled( std::string attr_name )
{
    return polled_actuator_obj_->is_enabled(attr_name);
}

void PolledActuatorAdapter::set_enabled( std::string attr_name, bool enable )
{
	polled_actuator_obj_->set_enabled(attr_name, enable);
}

LimitsType PolledActuatorAdapter::get_limits( std::string attr_name )
{
    return polled_actuator_obj_->get_limits(attr_name);
}

void PolledActuatorAdapter::set_limits( std::string attr_name, const LimitsType& limits )
{
    polled_actuator_obj_->set_limits(attr_name, limits);
}

double PolledActuatorAdapter::get_speed( std::string attr_name )
{
    //TraceHelper t( "PolledActuatorAdapter::get_speed" );
    return polled_actuator_obj_->get_speed(attr_name);
}

void   PolledActuatorAdapter::set_speed( std::string attr_name, double speed )
{
    //TraceHelper t( "PolledActuatorAdapter::set_speed" );
    polled_actuator_obj_->set_speed(attr_name, speed);
}

void   PolledActuatorAdapter::go_to( const std::vector<PositionRequest>& positions )
{
    //TraceHelper t( "PolledActuatorAdapter::go_to" );
    // ensures that if is_moving() is called just after this method, it will return true
    {
        yat::MutexLock lock(mutex_);
        is_moving_ = true;
        has_error_ = false;
    }

    // send the message asynchronously
    yat::Message* msg = yat::Message::allocate(POSITIONMENT_REQUEST_MSG);
    msg->attach_data( positions );
    this->post( msg );
}

bool   PolledActuatorAdapter::is_moving( void )
{
    //TraceHelper t( "PolledActuatorAdapter::is_moving" );
    yat::MutexLock lock(mutex_);
    return is_moving_;
}

void PolledActuatorAdapter::error_check( void )
{
    yat::MutexLock lock(mutex_);

    if (has_error_)
        throw last_error_;

    //TraceHelper t( "PolledActuatorAdapter::error_check" );
    try
    {
        polled_actuator_obj_->error_check();
    }
    catch( Tango::DevFailed& df )
    {
        has_error_ = true;
        last_error_ = yat4tango::TangoYATException(df);
        throw last_error_;
    }
    catch( yat::Exception& ex )
    {
        has_error_ = true;
        last_error_ = ex;
        throw last_error_;
    }
}

void   PolledActuatorAdapter::abort( void )
{
    //TraceHelper t( "PolledActuatorAdapter::abort" );
    yat::Message* msg = yat::Message::allocate(ABORT_REQUEST_MSG);
    this->post( msg );
}

void PolledActuatorAdapter::handle_message (yat::Message& msg)
throw (yat::Exception)
{
    //TraceHelper t( "PolledActuatorAdapter::handle_message" );
    try
    {
        switch ( msg.type() )
        {
        case yat::TASK_TIMEOUT:
        {
            //TraceHelper t( "PolledActuatorAdapter::handle_message::TASK_TIMEOUT" );
            this->check_moving_state();
        }
            break;
        case yat::TASK_EXIT:
        {
            //TraceHelper t( "PolledActuatorAdapter::handle_message::TASK_EXIT" );
        }
            break;
        case INIT_PLUGIN_MSG:
        {
            //TraceHelper t( "PolledActuatorAdapter::handle_message::INIT_PLUGIN_MSG" );
            std::string* device_name_p = 0;
            msg.detach_data(device_name_p);
            boost::scoped_ptr< std::string > guard( device_name_p );
            polled_actuator_obj_->init( *device_name_p );
            polled_actuator_obj_->check_initial_condition("");
        }
            break;
        case POSITIONMENT_REQUEST_MSG:
        {
            //TraceHelper t( "PolledActuatorAdapter::handle_message::POSITIONMENT_REQUEST_MSG" );
            std::vector<PositionRequest>* positions;
            msg.detach_data(positions);
            boost::scoped_ptr< std::vector<PositionRequest> > guard( positions );

            {
                yat::MutexLock lock(mutex_);
                polled_actuator_obj_->go_to(*positions);
                double polling_period = polled_actuator_obj_->get_polling_period_ms();
                this->set_timeout_msg_period( static_cast<size_t>(polling_period) );
                this->enable_timeout_msg(true);

                // trigger an update immediately
                this->check_moving_state();
            }
        }
            break;
        case ABORT_REQUEST_MSG:
        {
            //TraceHelper t( "PolledActuatorAdapter::handle_message::ABORT_REQUEST_MSG" );
            this->enable_timeout_msg(false);
            {
                yat::MutexLock lock(mutex_);
                polled_actuator_obj_->abort();
            }
            is_moving_ = false;
        }
            break;
        }
    }
    catch( yat::Exception& ex )
    {
        if (msg.type() == INIT_PLUGIN_MSG)
            throw;
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = ex;
                is_moving_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
    catch( Tango::DevFailed& ex )
    {
        if (msg.type() == INIT_PLUGIN_MSG)
            throw;
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = yat4tango::TangoYATException(ex);
                is_moving_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
    catch( ... )
    {
        if (msg.type() == INIT_PLUGIN_MSG)
            throw;
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                std::ostringstream oss;
                oss << "Unknown error on actuator "
                    << device_name_;
                last_error_ = yat::Exception("UNKNWON",
                                             oss.str().c_str(),
                                             "PolledActuatorAdapter::handle_message");
                is_moving_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
}

void   PolledActuatorAdapter::check_moving_state( void )
{
    //TraceHelper t( "PolledActuatorAdapter::check_moving_state" );
    is_moving_ = polled_actuator_obj_->is_moving();

    if (!is_moving_)
    {
        this->enable_timeout_msg(false);
    }
}
}

