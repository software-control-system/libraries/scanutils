/*!
 * \file
 * \brief    Definition of ScientaAcquisitionTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <yat/plugin/PlugInSymbols.h>
#include <yat/threading/Thread.h>

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <scansl/Util.h>

namespace ScanUtils
{

class ScientaAcquisitionTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "ScientaAcquisitionTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("ScientaAcquisition");
        list.push_back("MBSAcquisition");
    }
};

class ScientaAcquisitionTimebase : public WaitingStateChangeTimebase
{

public:
    ScientaAcquisitionTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 20;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.start_cmd_name        = "Start";
    }

    void init( std::string device_name, bool sync )
    {
        WaitingStateChangeTimebase::init( device_name, sync );

        std::string device_class = Util::instance().get_class( device_name);

        is_mbs_ = false;
        if(device_class == "MBSAcquisition" )
            is_mbs_ = true;
    }

    void start( double integration_time )
    {
        WaitingStateChangeTimebase::start( integration_time );
        if( is_mbs_ )
            // Insert a delay because the device state is changed asynchronously
            yat::Thread::sleep( 50 );
    }
private:
    bool is_mbs_;
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::ScientaAcquisitionTimebase,
                          ScanUtils::ScientaAcquisitionTimebaseInfo);
