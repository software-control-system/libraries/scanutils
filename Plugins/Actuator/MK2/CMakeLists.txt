file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(MK2Actuator MODULE ${sources})
target_include_directories(MK2Actuator PRIVATE ${includedirs})
target_link_libraries(MK2Actuator PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(MK2Actuator PROPERTIES OUTPUT_NAME "MK2Actuator-${PROJECT_VERSION}")
endif()

install(TARGETS MK2Actuator LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
