#include "Scheduler_HWContinuous.h"

namespace ScanUtils
{

Scheduler_HWContinuous::Scheduler_HWContinuous( const ScanConfig& config )
    : Scheduler(config),
      last_move_(0)
{
}

bool Scheduler_HWContinuous::is_specific_action( int action )
{
    switch ( action )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    case ActionType_PRESCAN_HOOK:
    case SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE:
    case SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS:
    case ActionType_WAIT_ACTUATORS:
    case ActionType_POST_ACTUATOR_MOVE_HOOK:
    case SpecificAction_END_OF_ACTUATOR_MOVE:
    case ActionType_START_TIMEBASES:
    case ActionType_WAIT_TIMEBASES:
    case ActionType_INIT_STEP:
    case ActionType_PRESTEP_HOOK:
    case ActionType_READ_SENSORS:
    case SpecificAction_END_OF_LINE:
        return true;
    default:
        return false;
    }
}

SchedulerState Scheduler_HWContinuous::handle_specific_action( void )
{
    SchedulerState state;

    switch( next_action_ )
    {
    case SpecificAction_SCAN_FIRST_ACTION:
    {
        // this action is just a delegate implemented in all Scheduler derived classes

        // skip all actuator positioning if not relevant
        next_action_ = config_.actuators.empty() ? static_cast<int>(ActionType_INIT_STEP) : static_cast<int>(SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE);
        break;
    }
    case SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE:
    {
        this->move_actuators();
        last_move_ = SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE;
        next_action_ = ActionType_WAIT_ACTUATORS;
    }
        break;
    case SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS:
    {
        this->move_actuators();
        last_move_ = SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS;
        next_action_ = ActionType_WAIT_TIMEBASES;
    }
        break;
    case ActionType_WAIT_ACTUATORS:
    {
        state.must_pause = this->wait_actuators(SpecificAction_END_OF_ACTUATOR_MOVE,
                                                last_move_,
                                                ActionType_WAIT_ACTUATORS);
    }
        break;
    case SpecificAction_END_OF_ACTUATOR_MOVE:
    {
        if (last_move_ == SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE)
        {
            //- read only 2d-actuators : we must read all actuator and keep only those of interest
            if (!config_.actuators2.empty())
            {
                AttrValues values = manager_.actuator_manager->read( ActuatorDim_1 + ActuatorDim_2 );
                for( std::size_t i = 0; i < values.size(); ++i )
                {
                    for( std::size_t j = 0; j < config_.actuators2.size(); ++j )
                    {
                        if ( Util::instance().complete_attr_name( config_.actuators2[j] ) == values[i].name )
                        {
                            manager_.data_collector->check_for_error();
                            manager_.data_collector->save_actuator_data(values[i]);
                        }
                    }
                }
            }
            this->progress_manager_.line_start();

            this->set_scan_speed();
            if ( dim1_pt_ == 0 && dim2_pt_ == 0 )
            {
                next_action_ = ActionType_PRESCAN_HOOK;
            }
            else
            {
                next_action_ = ActionType_INIT_STEP;
            }
        }
        else
        {
            next_action_ = ActionType_READ_SENSORS;
        }
    }
        break;
    case ActionType_PRESCAN_HOOK:
    {
        int next_action_on_success = config_.actuators.empty() ? static_cast<int>(ActionType_INIT_STEP) : static_cast<int>(SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE);
        state.must_pause = this->handle_hook( HookType_PRE_SCAN,
                                              ActionType_PRESCAN_HOOK,
                                              ActionType_INIT_STEP );
    }
        break;
    case ActionType_INIT_STEP:
    {
        //- init the last sensor reading date
        last_sensor_reading_ = tm::microsec_clock::universal_time();
        this->init_step();
        integration_time_ = config_.integration_times[ 0 ];
        if ( !config_.actuators.empty() )
            this->compute_next_actuator_pos();
        next_action_ = ActionType_PRESTEP_HOOK;
    }
        break;
    case ActionType_PRESTEP_HOOK:
    {
        state.must_pause = this->handle_hook( HookType_PRE_STEP,
                                              ActionType_PRESTEP_HOOK,
                                              ActionType_SENSORS_PRE_INTEGRATION );
    }
        break;
    case ActionType_START_TIMEBASES:
    {
        int next_action_on_success = config_.actuators.empty() ? static_cast<int>(ActionType_WAIT_TIMEBASES) : static_cast<int>(SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS);
        this->start_timebases( next_action_on_success );
    }
        break;
    case ActionType_WAIT_TIMEBASES:
    {
        if ( this->continuous_timebases_idle() )
        {
            SCAN_INFO << "Timebase has finished counting" << ENDLOG;
            next_action_ = SpecificAction_END_OF_LINE;
        }
        else if ( !config_.actuators.empty()
                  && manager_.actuator_manager->move_request_completed()
                  && step_pos_.x != last_pos_.x )
        {
            SCAN_INFO << "Move request completed : going to next position" << ENDLOG;
            //- there is another 1D position to go
            this->compute_next_actuator_pos();
            next_action_ = SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS;
        }
        else
        {
            yat::ThreadingUtilities::sleep(0, 100000); //- sleep 100 microsec
            next_action_ = ActionType_READ_SENSORS;
        }
    }
        break;
    case ActionType_READ_SENSORS:
    {
        if ( this->continuous_timebases_idle() )
        {
            SCAN_INFO << "Timebase has finished counting" << ENDLOG;
            next_action_ = SpecificAction_END_OF_LINE;
        }
        else
        {
            // partial read
            tm::ptime now = tm::microsec_clock::universal_time();
            tm::time_duration from_last_time = now - last_sensor_reading_;

            // read every 5 seconds max
            if ( from_last_time.total_milliseconds() > 5000 )
            {
                SCAN_INFO << "Reading sensors" << ENDLOG;
                last_sensor_reading_ = now;
                this->read_sensors( ActionType_WAIT_TIMEBASES );
                manager_.data_collector->check_for_error();
                this->manager_.data_collector->set_point_nb( dim1_pt_, dim2_pt_ );
                manager_.data_collector->check_for_error();
                this->progress_manager_.step_end();
                this->manager_.data_collector->end_of_step( );
            }
            else
            {
                next_action_ = ActionType_WAIT_TIMEBASES;
            }
        }
    }
        break;
    case SpecificAction_END_OF_LINE:
    {
        SCAN_INFO << "Dimension completed" << ENDLOG;
        if ( (!config_.actuators.empty()) && (config_.enable_scan_speed ==false))  // SCAN-219 . Abort movement only if necessary
        {
            this->abort_actuators();
            //- do as if we did all steps of 1st dim
            step_pos_.x = last_pos_.x;
        }

        //- read the sensors one last time to be sure to have all values
        this->read_sensors( SpecificAction_END_OF_LINE );
        manager_.data_collector->check_for_error();
        this->manager_.data_collector->set_point_nb( dim1_pt_, dim2_pt_ );
        manager_.data_collector->check_for_error();
        this->manager_.data_collector->end_of_step( );
        manager_.data_collector->check_for_error();
        this->manager_.data_collector->end_of_dim();
        this->progress_manager_.line_end();
        if ( !config_.actuators.empty() && this->more_step() )
        {
            this->compute_next_actuator_pos();
            dim1_pt_ = 0;
            dim2_pt_ ++;
            manager_.data_collector->check_for_error();
            this->manager_.data_collector->set_point_nb( dim1_pt_, dim2_pt_ );
            state.end_of_step = true;
            next_action_ = SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE;
        }
        else
        {
            next_action_ = ActionType_POST_SCAN_HOOK;
        }
    }
        break;
    default:
    {
        //- we should never be here : end the scan immediately
        SCAN_ERROR << "Internal Error : Scan aborted" << ENDLOG;
        state.end_of_step = true;
        state.end_of_scan = true;
        state.end_of_run  = true;
    }
        break;
    }

    return state;
}
}

