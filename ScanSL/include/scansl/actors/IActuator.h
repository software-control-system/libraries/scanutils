#ifndef _SCANSERVER_I_ACTUATOR_H
#define _SCANSERVER_I_ACTUATOR_H

#include <vector>
#include <scansl/ScanSL.h>
#include <scansl/interfaces/PositionRequest.h>
#include <boost/noncopyable.hpp>
#include <boost/smart_ptr.hpp>

namespace ScanUtils
{
struct SCANSL_DECL IActuator : private boost::noncopyable
{
    virtual ~IActuator();

    virtual void init( std::string device_name ) = 0;

    virtual void check_initial_condition( std::string attr_name ) = 0;

    virtual void ensure_supported( std::string attr_name ) = 0;

    virtual void set_position( std::string attr_name, double position ) = 0;

    virtual bool is_enabled( std::string attr_name ) = 0;

    virtual void set_enabled( std::string attr_name, bool enable ) = 0;

    virtual LimitsType get_limits( std::string attr_name ) = 0;

    virtual void set_limits( std::string attr_name, const LimitsType& limits ) = 0;

    virtual double get_speed( std::string attr_name ) = 0;

    virtual void set_speed( std::string attr_name, double speed ) = 0;

    virtual void go_to( const std::vector<PositionRequest>& positions ) = 0;

    virtual bool is_moving( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual void abort( void ) = 0;
};

typedef boost::shared_ptr<IActuator> IActuatorPtr;
}

#endif
