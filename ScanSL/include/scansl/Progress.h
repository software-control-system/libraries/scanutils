#ifndef _SCANSERVER_PROGRESS_H
#define _SCANSERVER_PROGRESS_H

#include <scansl/ScanSL.h>
#include <yat/threading/Mutex.h>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <scansl/ScanConfig.h>

namespace ScanUtils
{

namespace dt = boost::gregorian;
namespace tm = boost::posix_time;

//==================================================================================================
// MeanDuration
//==================================================================================================
class SCANSL_DECL MeanDuration
{
public:
    MeanDuration()
        : n_(0),
          initialized_(false)

    {
    };

    void add( tm::time_duration new_val )
    {
        if ( !initialized_ )
        {
            mean_ = new_val;
            n_ = 1;
            initialized_ = true;
        }
        else
        {
            mean_ = (mean_ * n_ + new_val) / ( n_ + 1 );
            n_++;
        }
    };

    const tm::time_duration& get()
    {
        return mean_;
    };

    void reset()
    {
        initialized_ = false;
        mean_ = tm::time_duration();
    }

    std::size_t n()
    {
        return n_;
    }

private:
    std::size_t n_;
    bool initialized_;
    tm::time_duration mean_;
};


//==================================================================================================
// Progress
//==================================================================================================
class SCANSL_DECL Progress
{
public:
    Progress();

    Progress( const Progress& p );

    Progress& operator=( const Progress& p );

    //- we need char* l-values for Tango attributes...
    const char* run_startdate_c;
    const char* scan_startdate_c;
    const char* scan_enddate_c;
    const char* run_enddate_c;
    const char* scan_duration_c;
    const char* run_duration_c;
    const char* scan_remaining_time_c;
    const char* run_remaining_time_c;
    const char* scan_elapsed_time_c;
    const char* run_elapsed_time_c;

    double      scan_completion_percent;
    double      run_completion_percent;

    double      scan_duration_sec;
    double      run_duration_sec;
    double      dead_time_sec;
    double      dead_time_per_pt_ms;
    double      dead_time_percent;

    void set_run_start_date( const std::string& s );
    void set_scan_start_date( const std::string& s );
    void set_scan_end_date( const std::string& s );
    void set_run_end_date( const std::string& s );
    void set_scan_duration( const std::string& s );
    void set_run_duration( const std::string& s );
    void set_scan_remaining_time( const std::string& s );
    void set_run_remaining_time( const std::string& s );
    void set_scan_elapsed_time( const std::string& s );
    void set_run_elapsed_time( const std::string& s );

private:
    std::string run_startdate_;
    std::string scan_startdate_;
    std::string scan_enddate_;
    std::string run_enddate_;
    std::string scan_duration_;
    std::string run_duration_;
    std::string scan_remaining_time_;
    std::string run_remaining_time_;
    std::string scan_elapsed_time_;
    std::string run_elapsed_time_;
};

//==================================================================================================
// ConcreteProgressManager
//==================================================================================================
class SCANSL_DECL ConcreteProgressManager
{
public:
    ConcreteProgressManager( const ScanConfig& );

    virtual ~ConcreteProgressManager();

    Progress get_progress();

    virtual void run_start();

    virtual void scan_start();

    virtual void step_end();

    // Called after the actuators moved to a line's start position
    // (e.g. when SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE is completed)
    virtual void line_start();

    // Called when a line is completed
    virtual void line_end();

protected:
    const ScanConfig& config_;

    yat::Mutex progress_mutex_;
    Progress progress_;
};

//==================================================================================================
// StepScanProgressManager
//==================================================================================================
class SCANSL_DECL StepScanProgressManager : public ConcreteProgressManager
{
public:
    StepScanProgressManager( const ScanConfig& );

    virtual void run_start();

    virtual void scan_start();

    virtual void step_end();

    virtual void line_start();

    virtual void line_end();

protected:
    long nb_step_per_scan_;
    long nb_line_per_scan_;


    tm::ptime run_start_;
    tm::ptime scan_start_;

    long completed_step_;
    long completed_line_;
    long current_scan_; // 1-based


    tm::ptime last_step_end_;

    tm::time_duration run_first_step_duration_;
    MeanDuration inner_step_duration_;
    MeanDuration move_to_begin_of_line_;
    MeanDuration move_to_begin_of_scan_;
    MeanDuration dead_time_per_pt_;

    tm::time_duration first_scan_duration_;

    double integration_time_sum_;
};

//==================================================================================================
// ContinuousScanProgressManager
//==================================================================================================
class SCANSL_DECL ContinuousScanProgressManager : public ConcreteProgressManager
{
public:
    ContinuousScanProgressManager( const ScanConfig& );

    virtual void run_start();

    virtual void scan_start();

    virtual void step_end();

    virtual void line_start();

    virtual void line_end();

protected:
    long nb_line_per_scan_;

    tm::ptime run_start_;
    tm::ptime scan_start_;

    long completed_step_;
    long completed_line_;
    long current_scan_; // 1-based

    //--------------------------------------
    tm::time_duration theoric_line_duration_;
    tm::time_duration total_actuators_move_to_begin_of_lines_;
    tm::ptime last_line_start_;
    tm::ptime last_line_end_;

    MeanDuration move_to_begin_of_line_;
    MeanDuration mean_line_duration_;
    tm::time_duration scan_duration_;
    tm::time_duration run_duration_;
    tm::ptime expected_scan_end_;
    tm::ptime expected_run_end_;
    tm::time_duration int_time_;

    void compute_values();
    void set_values();
};


//==================================================================================================
// ProgressManager
//==================================================================================================
class SCANSL_DECL ProgressManager
{
public:
    ProgressManager( const ScanConfig& );

    Progress get_progress();

    void run_start();

    void scan_start();

    void step_end();

    void line_start();

    void line_end();

private:
    boost::shared_ptr<ConcreteProgressManager> pimpl;
};

}

#endif
