#ifndef _SCANSERVER_SCANMANAGER_H
#define _SCANSERVER_SCANMANAGER_H

#include <scansl/ScanSL.h>
#include <scansl/actors/ActuatorManager.h>
#include <scansl/actors/TimebaseManager.h>
#include <scansl/actors/ContinuousTimebaseManager.h>
#include <scansl/actors/SensorManager.h>
#include <scansl/actors/HooksManager.h>
#include <scansl/actors/DataCollector.h>
#include <scansl/actors/AfterRunAction.h>

namespace ScanUtils
{
struct SCANSL_DECL ScanManager
{
    ScanManager( const ScanConfig& );

    ActuatorManagerPtr             actuator_manager;
    TimebaseManagerPtr             timebase_manager;
    ContinuousTimebaseManagerPtr   continuous_timebase_manager;
    SensorManagerPtr               sensor_manager;
    HooksManagerPtr                hooks_manager;
    DataCollectorPtr               data_collector;
    AfterRunActionPtr              after_run_action;
};

}

#endif
