file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(NI6602Timebase MODULE ${sources})
target_include_directories(NI6602Timebase PRIVATE ${includedirs})
target_link_libraries(NI6602Timebase PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(NI6602Timebase PROPERTIES OUTPUT_NAME "NI6602Timebase-${PROJECT_VERSION}")
endif()

install(TARGETS NI6602Timebase LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
