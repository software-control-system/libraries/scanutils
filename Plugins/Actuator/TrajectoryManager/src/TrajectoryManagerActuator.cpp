/*!
 * \file
 * \brief    Definition of TrajectoryManagerActuator class
 * \author   Xavier Elattaoui - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>


namespace ScanUtils
{

class TrajectoryManagerActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

  virtual std::string get_plugin_id(void) const
  {
    return "TrajectoryManagerActuator";
  }

  virtual std::string get_interface_name(void) const
  {
    return IPolledActuatorInterfaceName;
  }

  virtual std::string get_version_number(void) const
  {
    return "0.0.1";
  }

public: //! IScanPlugInInfo implementation

  virtual void supported_classes ( std::vector<std::string>& list ) const
  {
    list.push_back("TrajectoryManager");
  }
};


class TrajectoryManagerActuator : public WaitingStateChangeActuator
{
private:
  bool _is_started;
public:
  TrajectoryManagerActuator()
  {
    config_.move_state        = Tango::MOVING;
    config_.error_states.push_back(Tango::FAULT);
    config_.error_states.push_back(Tango::ALARM);
    config_.polling_period_ms = 100;
    config_.abort_cmd_name    = "Abort";
    config_.timeout_ms        = 3000;
    _is_started = false;
  }
  // ************************************************************
  //
  //  init
  //
  // ************************************************************

  virtual void init( std::string device_name )
  {
    try
    {

      WaitingStateChangeActuator::init(device_name);

      Tango::DeviceAttribute da;
      // get nb points
      std::string it_str  = this->parameter_value("hw_continuous_nbpt");
      long hw_continuous_nbpt = std::atol(it_str.c_str());

      // Set total time ( = nb_point * integration_time )
      it_str  = this->parameter_value("IntegrationTime");
      double total_time = std::atof(it_str.c_str()) * hw_continuous_nbpt;


      da.set_name( "totalTime" );
      da << total_time;
      this->proxy_->write_attribute( da );

      // get "from" value (first point of Trajectory)
      std::string from_str  = this->parameter_value("from");
      double from = std::atof(from_str.c_str());
      da.set_name( "from" );
      da << from;
      this->proxy_->write_attribute( da );
      // get "to" value (second point of Trajectory)
      std::string to_str  = this->parameter_value("to");
      double to = std::atof(to_str.c_str());
      da.set_name( "to" );
      da << to;
      this->proxy_->write_attribute( da );

      //- now all parameters are sent, so do the PREPARE phase !!
      this->proxy_->command_inout("Prepare");
      _is_started = false;

    }
    catch (Tango::DevFailed& df)
    {
      throw yat4tango::TangoYATException(df);
    }
    catch (...)
    {
      throw ;
    }
  }
  // ************************************************************
  //
  //  init
  //
  // ************************************************************
  // ATTENTION : cette methode est appele 2 fois :
  // 1 fois pour le premier point
  // 1 fois pour le second point

  virtual void go_to( const std::vector<PositionRequest>& positions )
  {
    std::string from_str  = this->parameter_value("from");
    double from = std::atof(from_str.c_str());

    // get "to" value (second point of Trajectory)
    std::string to_str  = this->parameter_value("to");
    double to = std::atof(to_str.c_str());
    try
    {

      Tango::DevState state = Tango::UNKNOWN;
      Tango::DeviceData dd = this->proxy_->command_inout("State");
      dd >> state;

      if ( !_is_started )
      {
        // method is called for first point of trajectory
        // Then goto first point
        this->proxy_->command_inout("ScanSInitializeTrajectory");
        _is_started = true;
      }
      else
      {
        // method is called for second point of trajectory
        // Then run the trajectory itself
        this->proxy_->command_inout("ScanSExecuteTrajectory");
        _is_started = false;
      }
    }
    catch (Tango::DevFailed& df)
    {
      throw yat4tango::TangoYATException(df);
    }
    catch (...)
    {
      throw ;
    }
  }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::TrajectoryManagerActuator,
                          ScanUtils::TrajectoryManagerActuatorInfo);
