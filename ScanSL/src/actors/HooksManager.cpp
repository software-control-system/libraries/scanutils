#include <boost/scoped_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>

#ifdef SCANSL_ENABLE_PYTHON
#include <boost/python.hpp>
namespace py = boost::python;
#endif // ifdef SCANSL_ENABLE_PYTHON

#include <scansl/Util.h>
#include <scansl/actors/HooksManager.h>
#include <scansl/logging/ScanLogAdapter.h>

#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
struct IHook
{
    virtual void execute() = 0;

    virtual ~IHook()
    {
    }
};

class TangoHook : public IHook
{
public:
    TangoHook( const std::string& tango_command )
    {
        size_t last_slash = tango_command.find_last_of( '/' );
        this->device_name  = tango_command.substr(0, last_slash);
        this->command      = tango_command.substr(last_slash + 1, std::string::npos);

        //- do some check :
        try
        {
            //- check that we can instantiate a device proxy
            this->dev_proxy = DevProxies::instance().proxy(this->device_name);

            //- get info on command
            Tango::CommandInfo command_info = this->dev_proxy->command_query( this->command );

            if ( command_info.in_type != Tango::DEV_VOID )
            {
                yat::OSStream oss;
                oss << "The command "
                    << this->command
                    << " needs some input parameter and thus cannot be configured as a hook";

                THROW_YAT_ERROR("WRONG_HOOK", oss.str(), "TangoHook::TangoHook");
            }
        }
        catch( const Tango::DevFailed & df )
        {
            yat4tango::TangoYATException e(df);
            yat::OSStream oss;
            oss << "Error while configuring the hook " << tango_command;
            std::string error = oss.str();

            SCAN_ERROR << error << " ( " << df.errors[0].desc << " )" << ENDLOG;

            RETHROW_YAT_ERROR(e,
                              "TANGO_ERROR",
                              error,
                              "TangoHook::TangoHook");
        }
    };

    virtual ~TangoHook()
    {
    }

    virtual void execute()
    {
        try
        {
            SCAN_INFO << "Executing " << this->device_name << "/" << this->command << ENDLOG;
            this->dev_proxy->command_inout( const_cast<std::string&>(this->command) );
        }
        catch( Tango::DevFailed& df )
        {
            yat4tango::TangoYATException e(df);
            RETHROW_YAT_ERROR(e,
                              "TANGO_ERROR",
                              "Error while executing hook",
                              "TangoHook::execute");
        }
        catch( ... )
        {
            THROW_YAT_ERROR("TANGO_ERROR",
                            "Unknown error while executing hook",
                            "TangoHook::execute");
        }
    }

private:
    std::string device_name;
    std::string command;
    ProxyP dev_proxy;
};

#ifdef SCANSL_ENABLE_PYTHON
class Py
{
public:
    static Py& instance()
    {
        static Py unique_instance;
        if ( !unique_instance.initialized )
        {
            unique_instance.init();
        }
        return unique_instance;
    }

    py::object get_global()
    {
        return global;
    }

private:
    Py()
    {
        initialized = false;
    }

    void init()
    {
        Py_Initialize();
        main = py::import("__main__");
        global = main.attr("__dict__");
        py::exec("import sys\n",
                 global, global);
        /*
      py::exec("import signal\n"
               "signal.signal(signal.SIGINT, signal.SIG_IGN)",
               global, global);
               */
        initialized = true;
    }

    bool initialized;
    py::object main;
    py::object global;
};

class PythonHook : public IHook
{
public:

    PythonHook( const std::string& python_script_path )
    {
        file_name = python_script_path;
    }

    virtual ~PythonHook()
    {
    }

    virtual void execute()
    {
        py::object global = Py::instance().get_global();
        try
        {
            py::exec_file( file_name.c_str(), global, global );
        }
        catch( py::error_already_set const & )
        {
            if ( PyErr_ExceptionMatches(PyExc_KeyboardInterrupt) )
            {
                std::exit(1);
            }
            else
            {
                PyObject* py_exc_type = NULL;
                PyObject* py_exc_value = NULL;
                PyObject* py_exc_trace = NULL;

                // PyErr_Fetch clears the error indicator automatically
                PyErr_Fetch( &py_exc_type, &py_exc_value, &py_exc_trace );

                py::handle<> exc_type ( py_exc_type );
                py::handle<> exc_value( py_exc_value );
                py::handle<> exc_trace( py_exc_trace );
                py::handle<> value_str (PyObject_Str( exc_value.get() ));
                py::object value_type(py::handle<>(PyObject_Type(py_exc_value)));
                std::cout << ((PyTypeObject*)value_type.ptr())->tp_name << std::endl;
                std::cout << PyString_AsString( value_str.get() ) << std::endl;
            }

        }

    }

private:
    std::string file_name;
};
#endif


class HookFactory : public Singleton<HookFactory>
{
public:

    IHookPtr create_hook( const std::string& hook_str )
    {
#ifdef SCANSL_ENABLE_PYTHON
        const std::string python_descriptor = "python:";
        if ( boost::algorithm::starts_with(hook_str, python_descriptor) )
        {
            std::string script_path = boost::erase_head_copy( hook_str, python_descriptor.length() );
            boost::trim_copy(script_path);
            return IHookPtr( new PythonHook( script_path ) );
        }
        else
        {
            return IHookPtr( new TangoHook( boost::trim_copy(hook_str) ) );
        }
#else
        return IHookPtr( new TangoHook( boost::trim_copy(hook_str) ) );
#endif
    }
};

// ============================================================================
// HooksManager::HooksManager
// ============================================================================
HooksManager::HooksManager( const ScanConfig& config )
{
    for( std::size_t i = 0; i < config.prerun_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_PRE_RUN, HookFactory::instance().create_hook(config.prerun_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.prescan_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_PRE_SCAN, HookFactory::instance().create_hook(config.prescan_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.prestep_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_PRE_STEP, HookFactory::instance().create_hook(config.prestep_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.postactuatormove_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_POST_ACTUATOR_MOVE, HookFactory::instance().create_hook(config.postactuatormove_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.postintegration_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_POST_INTEGRATION, HookFactory::instance().create_hook(config.postintegration_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.poststep_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_POST_STEP, HookFactory::instance().create_hook(config.poststep_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.postscan_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_POST_SCAN, HookFactory::instance().create_hook(config.postscan_hooks[i]) ) );
    }

    for( std::size_t i = 0; i < config.postrun_hooks.size(); ++i )
    {
        hooks_map_.insert( std::make_pair( HookType_POST_RUN, HookFactory::instance().create_hook(config.postrun_hooks[i]) ) );
    }
}

// ============================================================================
// HooksManager::~HooksManager
// ============================================================================
HooksManager::~HooksManager()
{
}

// ============================================================================
// HooksManager::execute
// ============================================================================
void HooksManager::execute( HookType ht )
{
    HooksMap::const_iterator it = hooks_map_.lower_bound( ht );

    if ( it == hooks_map_.end() )
        return;

    SCAN_INFO << "Executing " << this->get_hook_type_name(ht) << " hooks" << ENDLOG;

    HooksMap::const_iterator up_it = hooks_map_.upper_bound( ht );

    while (it != up_it)
    {
        (*it).second->execute();
        it++;
    }
}

// ============================================================================
// [static] HooksManager::get_hook_name
// ============================================================================
std::string HooksManager::get_hook_type_name( HookType ht )
{
    switch( ht )
    {
    case HookType_PRE_RUN:            return "PRE RUN";            break;
    case HookType_PRE_SCAN:           return "PRE SCAN";           break;
    case HookType_PRE_STEP:           return "PRE STEP";           break;
    case HookType_POST_ACTUATOR_MOVE: return "POST ACTUATOR MOVE"; break;
    case HookType_POST_INTEGRATION:   return "POST INTEGRATION";   break;
    case HookType_POST_STEP:          return "POST STEP";          break;
    case HookType_POST_SCAN:          return "POST SCAN";          break;
    case HookType_POST_RUN:           return "POST RUN";           break;
    default:                          return "UNDEFINED";          break;
    }
}
}

