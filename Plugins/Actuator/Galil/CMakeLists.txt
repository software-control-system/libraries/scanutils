file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(GalilActuator MODULE ${sources})
target_include_directories(GalilActuator PRIVATE ${includedirs})
target_link_libraries(GalilActuator PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(GalilActuator PROPERTIES OUTPUT_NAME "GalilActuator-${PROJECT_VERSION}")
endif()

install(TARGETS GalilActuator LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
