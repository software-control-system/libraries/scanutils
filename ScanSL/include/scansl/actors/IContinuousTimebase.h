#ifndef _SCANSERVER_I_CONTINUOUS_TIMEBASE_H
#define _SCANSERVER_I_CONTINUOUS_TIMEBASE_H

#include <scansl/ScanSL.h>
#include <boost/noncopyable.hpp>
#include <boost/smart_ptr.hpp>

namespace ScanUtils
{
struct SCANSL_DECL IContinuousTimebase : private boost::noncopyable
{
    virtual ~IContinuousTimebase();

    virtual void init( std::string device_name ) = 0;

    virtual void check_initial_condition( ) = 0;

    virtual std::vector<std::string> get_sensors() = 0;

    virtual std::vector<std::string> get_actuators() = 0;

    virtual std::vector<std::string> get_actuators_proxies() = 0;

    virtual void start( double integration_time, long nb_points ) = 0;

    virtual bool is_counting( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual void abort( void ) = 0;
};

typedef boost::shared_ptr<IContinuousTimebase> IContinuousTimebasePtr;
}

#endif
