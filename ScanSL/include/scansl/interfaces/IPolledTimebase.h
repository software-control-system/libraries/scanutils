#ifndef _SCANSERVER_IPOLLEDTIMEBASE_H
#define _SCANSERVER_IPOLLEDTIMEBASE_H

#include <scansl/ScanConfig.h>
#include <boost/shared_ptr.hpp>
#include <yat/plugin/IPlugInObject.h>

namespace ScanUtils
{
const std::string IPolledTimebaseInterfaceName ("IPolledTimebase");

struct IPolledTimebase : public yat::IPlugInObject
{
    virtual void init( std::string device_name, bool sync ) = 0;

    virtual void check_initial_condition( ) = 0;

    virtual void start( double integration_time ) = 0;
    
    virtual double get_polling_period_ms( void ) = 0;

    virtual bool is_counting( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual void abort( void ) = 0;

    virtual void before_run( void ) = 0;

    virtual void after_run( void ) = 0;

};

typedef boost::shared_ptr<IPolledTimebase> IPolledTimebasePtr;

}

#endif
