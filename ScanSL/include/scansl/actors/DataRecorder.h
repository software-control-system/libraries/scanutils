#ifndef _SCANSERVER_DATA_RECORDER_H
#define _SCANSERVER_DATA_RECORDER_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/logging/ILogReceiver.h>
#include <boost/noncopyable.hpp>

#ifdef SCANSL_ENABLE_NEXUS
#  include <nexuscpp/nxfile.h>
#endif

namespace ScanUtils
{
class DataCollector;

class SCANSL_DECL DataRecorder : private boost::noncopyable
{
public: //! typedef
#ifdef SCANSL_ENABLE_NEXUS
    typedef boost::shared_ptr<nxcpp::NexusFile>     NexusFileP;
#endif

    typedef std::ofstream AsciiFile;
    typedef boost::shared_ptr<AsciiFile> AsciiFileP;

public: //! structors
    DataRecorder( const ScanConfig& config );

    ~DataRecorder();

public: //! modifiers

    void init_run( DataCollector* data_collector );

    void init_scan( std::size_t scan_idx );

    void save_scan( void );

    void save_step_data_ascii( void );

    void end_scan( void );

    void end_run( void );

private:
    void create_data_file( void );

#ifdef SCANSL_ENABLE_NEXUS
    void save_step_data_nexus( void );
    void save_on_the_fly_scan_nexus( void );
    void synchronize();

    void write_trajectories( int axis );
    void write_integration_times( void );
    void write_config( void );
    void write_cstring(const char *name_p, const std::string& content);
    void write_info_pair(const std::string &info_pair);
#endif

    const ScanConfig& config_;

    DataCollector* data_collector_; // yes, this is an ugly cross-reference !

#ifdef SCANSL_ENABLE_NEXUS
    ProxyP datarecorder_proxy_;
    int datarecorder_step_;
    NexusFileP nxs_file_;
    bool dataset_created_;
    tm::ptime last_data_recorder_saving_;
    bool scan_init_done_;
#endif

    AsciiFileP ascii_file_;
};

typedef boost::shared_ptr<DataRecorder> DataRecorderPtr;

}

#endif
