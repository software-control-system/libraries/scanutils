/*!
 * \file
 * \brief    Definition of BruckerBTPSActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class BruckerBTPSActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "BruckerBTPSActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("BruckerBTPS");
    }
};

class BruckerBTPSActuator : public WaitingStateChangeActuator
{
public:
    BruckerBTPSActuator()
    {
        config_.move_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 100;
        config_.abort_cmd_name    = "Stop";
        config_.timeout_ms        = 5000;
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::BruckerBTPSActuator, ScanUtils::BruckerBTPSActuatorInfo);
