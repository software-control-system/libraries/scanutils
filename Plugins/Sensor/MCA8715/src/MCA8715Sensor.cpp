/*!
 * \file
 * \brief    Definition of MCA8715Sensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "MCA8715Sensor.h"

#include <boost/scoped_ptr.hpp>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::MCA8715Sensor, ScanUtils::MCA8715SensorInfo);

namespace ScanUtils
{

const std::string PlugInID                ( "MCA8715Sensor" );
const std::string VersionNumber           ( "1.0.0" );

const std::string CLASS_NAME              ("MCA8715");

// ============================================================================
// MCA8715SensorInfo::get_plugin_id
// ============================================================================
std::string MCA8715SensorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// MCA8715SensorInfo::get_interface_name
// ============================================================================
std::string MCA8715SensorInfo::get_interface_name() const
{
    return ISensorInterfaceName;
}

// ============================================================================
// MCA8715SensorInfo::get_version_number
// ============================================================================
std::string MCA8715SensorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// MCA8715SensorInfo::supported_classes
// ============================================================================
void MCA8715SensorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back(CLASS_NAME);
}



// ============================================================================
// MCA8715Sensor::MCA8715Sensor
// ============================================================================
MCA8715Sensor::MCA8715Sensor()
{

}

// ============================================================================
// MCA8715Sensor::~MCA8715Sensor
// ============================================================================
MCA8715Sensor::~MCA8715Sensor()
{

}

// ============================================================================
// MCA8715Sensor::init
// ============================================================================
void MCA8715Sensor::init( std::string device_name ,bool sync)
{

    try
    {
        proxy_ = DevProxies::instance().proxy(device_name);

    }
    catch (Tango::DevFailed& df)
    {
        throw yat4tango::TangoYATException(df);
    }

}

// ============================================================================
// MCA8715Sensor::before_integration
// ============================================================================
void MCA8715Sensor::before_integration( void )
{
    proxy_->command_inout( "Clear" );
    proxy_->command_inout( "Start" );

}

// ============================================================================
// MCA8715Sensor::after_integration
// ============================================================================
void MCA8715Sensor::after_integration( void )
{
    proxy_->command_inout( "Stop" );
    proxy_->command_inout( "Read" );
}

}
