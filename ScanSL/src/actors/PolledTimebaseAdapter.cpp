#include <scansl/actors/PolledTimebaseAdapter.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

const size_t START_REQUEST_MSG = yat::FIRST_USER_MSG;
const size_t ABORT_REQUEST_MSG = yat::FIRST_USER_MSG + 1;

PolledTimebaseAdapter::PolledTimebaseAdapter( IPolledTimebasePtr polled_timebase_obj )
    : polled_timebase_obj_(polled_timebase_obj),
      is_counting_(false),
      has_error_(false)
{
    this->go();
}

void   PolledTimebaseAdapter::init( std::string device_name, bool sync )
{
    device_name_ = device_name;
    polled_timebase_obj_->init(device_name, sync);
}

void   PolledTimebaseAdapter::check_initial_condition( void )
{
    polled_timebase_obj_->check_initial_condition();
}

void   PolledTimebaseAdapter::start( double integration_time )
{
    // ensures that if is_counting() is called just after this method, it will return true
    {
        yat::MutexLock lock(mutex_);
        is_counting_ = true;
        has_error_ = false;
    }

    // send the message asynchronously
    yat::Message* msg = yat::Message::allocate(START_REQUEST_MSG);
    msg->attach_data( integration_time );
    this->post( msg );
}

bool   PolledTimebaseAdapter::is_counting( void )
{
    yat::MutexLock lock(mutex_);
    return is_counting_;
}

void   PolledTimebaseAdapter::error_check( void )
{
    yat::MutexLock lock(mutex_);

    if (has_error_)
        throw last_error_;

    try
    {
        polled_timebase_obj_->error_check();
    }
    catch( Tango::DevFailed& df )
    {
        has_error_ = true;
        last_error_ = yat4tango::TangoYATException(df);
        throw last_error_;
    }
    catch( yat::Exception& ex )
    {
        has_error_ = true;
        last_error_ = ex;
        throw last_error_;
    }
}

void   PolledTimebaseAdapter::abort( void )
{
    yat::Message* msg = yat::Message::allocate(ABORT_REQUEST_MSG);
    this->post( msg );
}

void PolledTimebaseAdapter::handle_message (yat::Message& msg)
throw (yat::Exception)
{
    try
    {
        switch ( msg.type() )
        {
        case yat::TASK_TIMEOUT:
        {
            this->check_counting_state();
            if (!is_counting_)
            {
                this->enable_timeout_msg(false);
            }
        }
            break;
        case START_REQUEST_MSG:
        {
            double* int_time;
            msg.detach_data(int_time);
            boost::scoped_ptr< double > guard( int_time );

            {
                yat::MutexLock lock(mutex_);
                polled_timebase_obj_->start(*int_time);
                double polling_period = polled_timebase_obj_->get_polling_period_ms();
                this->set_timeout_msg_period( static_cast<size_t>(polling_period) );
                this->enable_timeout_msg(true);

                // trigger an update immediately
                this->check_counting_state();
            }
        }
            break;
        case ABORT_REQUEST_MSG:
        {
            this->enable_timeout_msg(false);
            {
                yat::MutexLock lock(mutex_);
                polled_timebase_obj_->abort();
            }
            is_counting_ = false;
        }
            break;
        }
    }
    catch( yat::Exception& ex )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = ex;
                is_counting_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }
    catch( Tango::DevFailed& ex )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                last_error_ = yat4tango::TangoYATException(ex);
                is_counting_ = false;
            }
        }
        this->enable_timeout_msg(false);
    }

    catch( ... )
    {
        {
            yat::MutexLock lock(mutex_);
            if (! has_error_)
            {
                has_error_ = true;
                std::ostringstream oss;
                oss << "Unknown error on timebase "  << device_name_;
                last_error_ = yat::Exception("UNKNWON",oss.str().c_str(),"PolledTimebaseAdapter::handle_message");
                is_counting_ = false;
            }
        } //end MutexLock
        this->enable_timeout_msg(false);
    }
}

void   PolledTimebaseAdapter::check_counting_state( void )
{
    is_counting_ = polled_timebase_obj_->is_counting();
}

void PolledTimebaseAdapter::before_run( void )
{
    polled_timebase_obj_->before_run();
}

void PolledTimebaseAdapter::after_run( void )
{
    polled_timebase_obj_->after_run();
}
}

