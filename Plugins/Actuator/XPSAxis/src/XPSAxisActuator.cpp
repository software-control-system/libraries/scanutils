/*!
 * \file
 * \brief    Definition of XPSAxisActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <yat/plugin/PlugInSymbols.h>
#include <boost/lexical_cast.hpp>

namespace ScanUtils
{
  class XPSAxisActuatorInfo : public IScanPlugInInfo
  {
  public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
      return "XPSAxisActuator";
    }

    virtual std::string get_interface_name(void) const
    {
      return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
      return "1.0.0";
    }

  public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
      list.push_back("XPSAxis");
    }
  }; 
  
  class XPSAxisActuator : public WaitingStateChangeActuator
  {
  public:
    XPSAxisActuator()
    {
      config_.move_state        = Tango::MOVING;
      config_.error_states.push_back(Tango::FAULT);
	  config_.error_states.push_back(Tango::OFF);
      config_.error_states.push_back(Tango::UNKNOWN);
      config_.polling_period_ms = 50;
      config_.abort_cmd_name    = "Stop";
    }

    void check_initial_condition( std::string /*attr_name*/ )
    {
      Tango::DevState dev_state = proxy_->state();
      if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
      {
        std::ostringstream oss;
        oss << "The device " 
            << config_.dev_name
            << " must be STANDBY or ALARM to move";

        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        oss.str().c_str(),
                        "XPSAxisActuator::check_initial_condition");
      }
    }

    void set_position( std::string /*attr_name*/, double position )
    {
        Tango::DeviceData offset;
        offset << position;
        proxy_->command_inout("ComputeNewOffset", offset);
    }

    LimitsType get_limits( std::string attr_name )
    {
        // TODO factorize code with Galil
    	// cf jira SCAN-392
        if (attr_name != "position") {
            try {
                double lower, upper;
                Tango::DeviceAttribute lowerAttr = proxy_->read_attribute("softLimitMin");
                Tango::DeviceAttribute upperAttr = proxy_->read_attribute("softLimitMax");
                lowerAttr >> lower;
                upperAttr >> upper;
                return LimitsType(
                    boost::lexical_cast<std::string>(lower),
                    boost::lexical_cast<std::string>(upper)
                );
            } catch (const boost::bad_lexical_cast& err) {
                THROW_YAT_ERROR("BAD_CAST",
                                err.what(),
                                "XPSAxisActuator::get_limits");
            }
        } else {
            return WaitingStateChangeActuator::get_limits(attr_name);
        }
    }

    void set_limits( std::string attr_name, const LimitsType& limits )
    {
        // TODO factorize code with Galil
    	// cf jira SCAN-392
        if (attr_name != "position") {
            try {
                std::vector<Tango::DeviceAttribute> attrs;
                attrs.push_back(Util::instance().create_device_attribute(
                    "softLimitMin",
                    Tango::DEV_DOUBLE,
                    boost::lexical_cast<double>(limits.first)
                ));
                attrs.push_back(Util::instance().create_device_attribute(
                    "softLimitMax",
                    Tango::DEV_DOUBLE,
                    boost::lexical_cast<double>(limits.second)
                ));
                proxy_->write_attributes(attrs);
            } catch (const boost::bad_lexical_cast& err) {
                THROW_YAT_ERROR("BAD_CAST",
                                err.what(),
                                "XPSAxisActuator::set_limits");
            }
        } else {
            WaitingStateChangeActuator::set_limits(attr_name, limits);
        }
    }

    /// TODO implement is_enabled

    void set_enabled( std::string /*attr_name*/, bool enabled )
    {
        proxy_->command_inout(enabled ? "On" : "Off");
    }

    double get_speed( std::string attr_name )
    {
      if (attr_name == "position")
      {
        Tango::DeviceAttribute velocity_attr = proxy_->read_attribute( "velocity" );
        double velocity;
        velocity_attr >> velocity;
        return velocity;
      }
      else
      {
        return 0;
      }
    }

    void set_speed( std::string attr_name, double speed )
    {
      if (attr_name == "position")
      {
        Tango::DeviceAttribute velocity_attr("velocity", speed);
        proxy_->write_attribute( velocity_attr );
      }
    }
  };
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XPSAxisActuator, \
                          ScanUtils::XPSAxisActuatorInfo);
