/*!
 * \file
 * \brief    Declaration of DGG2Timebase class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_DGG2_H_
#define _SCANSERVER_DGG2_H_


#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>

namespace ScanUtils
{
class DGG2TimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};

class DGG2Timebase : public WaitingStateChangeTimebase
{
public:
    DGG2Timebase();
};

}

#endif
