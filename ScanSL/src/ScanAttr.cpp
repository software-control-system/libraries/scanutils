#include <scansl/ScanAttr.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
DynAttrManager::DynAttrManager( Tango::DeviceImpl* device )
    : Tango::LogAdapter(device),
      device_(device)
{
}

DynAttrManager::~DynAttrManager()
{
    for( std::size_t i = 0; i < attrs_.size(); ++i  )
    {
        try
        {
            device_->remove_attribute( attrs_[i].get(), false );
        }
        catch( Tango::DevFailed& df )
        {
            ERROR_STREAM << "Unable to remove attribute " << attrs_[i] << " (" << df.errors[0].desc << ")" << ENDLOG;
            //- don't throw !!! we are in a destructor
        }
    }
    device_ = 0;
}

void DynAttrManager::add( boost::shared_ptr<Tango::Attr> attr )
throw( Tango::DevFailed )
{
    device_->add_attribute( attr.get() );
    attrs_.push_back( attr );
}

void DynAttrManager::read_attr_hardware( void )
{
    for( std::size_t i = 0; i < attrs_.size(); ++i  )
    {
        boost::dynamic_pointer_cast< DataSetter >( attrs_[i] )->read_attr_hardware();
    }
}


}

