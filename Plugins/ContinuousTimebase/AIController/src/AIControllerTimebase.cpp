/*!
 * \file
 * \brief    Definition of AIControllerContinuousTimebase class
 * \author   Buteau Alain / Pierre-Joseph Zephir Sandra  - Synchrotron SOLEIL
 * Jira http://jira.synchrotron-soleil.fr/jira/browse/SCAN-310
 */

#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeContinuousTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class AIControllerContinuousTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "AIControllerContinuousTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledContinuousTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("AIController");
    }
};


class AIControllerContinuousTimebase : public WaitingStateChangeContinuousTimebase
{
public: //! structors

    AIControllerContinuousTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1E3;
        config_.nbpoint_attr          = "dataBufferNumber";
        config_.start_cmd_name        = "Start";
    }

public: //! ITimebase implementation

    void init( std::string device_name )
    {
        this->WaitingStateChangeContinuousTimebase::init( device_name );
        device_name_=device_name;

        Tango::DevState state = proxy_->state();
        if (state != Tango::STANDBY)
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " should be STANDBY";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "AIControllerContinuousTimebase::init" );
        }

        Tango::DbData prop_data;
        prop_data.push_back( Tango::DbDatum("HCSDeltaTimeSecond") );
        proxy_->unsafe_proxy().get_property( prop_data );

        // checking if 'HCSDeltaTimeSecond' is available
        if ( prop_data[0].is_empty() )
        {
            hcs_delta_time_ = 0.001;
            SCAN_DEBUG << "AIControllerContinuousTimebase HCSDeltaTimeSecond property doesn't exist, default value usage : " << hcs_delta_time_ << ENDLOG;
        }
        else
        {
            try
            {
                prop_data[0] >> hcs_delta_time_;
                SCAN_DEBUG << "AIControllerContinuousTimebase HCSDeltaTimeSecond is reading on the device : " << hcs_delta_time_ << ENDLOG;
            }
            catch( Tango::DevFailed& df )
            {
                yat4tango::TangoYATException e(df);
                RETHROW_YAT_ERROR(e,
                                  "TANGO_ERROR",
                                  "Invalid HCSDeltaTimeSecond property type ",
                                  "AIControllerContinuousTimebase::init");
            }
        }
     }

    void check_initial_condition( )
    {
		WaitingStateChangeContinuousTimebase::check_file_generation_condition("nexusFileGeneration");
    }
    
     void start( double integration_time, long nb_point )
     {
        if(integration_time - hcs_delta_time_ > 0)
        {
            std::vector<Tango::DeviceAttribute> attrs;
            attrs.push_back( Tango::DeviceAttribute(config_.integration_time_attr, (integration_time - hcs_delta_time_) * config_.integration_time_gain) );
            attrs.push_back( Tango::DeviceAttribute(config_.nbpoint_attr, (Tango::DevULong) nb_point) );
            attrs.push_back( Tango::DeviceAttribute("statHistoryBufferDepth", (Tango::DevULong) nb_point) );
            proxy_->write_attributes( attrs );
            proxy_->command_inout( config_.start_cmd_name );
        }
        else
        {
            std::ostringstream oss;
            oss << "Invalid HCSDeltaTimeSecond property type for the device "
                << device_name_
                << ". IntegrationTime - HCSDeltaTimeSecond property value is <= 0";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                            "AIControllerContinuousTimebase::start" );
        }
    }

    std::vector<std::string> get_sensors()
    {
        return std::vector<std::string>();
    }

    std::vector<std::string> get_actuators()
    {
        return std::vector<std::string>();
    }

    std::vector<std::string> get_actuators_proxies()
    {
        return std::vector<std::string>();
    }

private :
    double hcs_delta_time_;
    std::string device_name_;

};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::AIControllerContinuousTimebase,
                          ScanUtils::AIControllerContinuousTimebaseInfo);

