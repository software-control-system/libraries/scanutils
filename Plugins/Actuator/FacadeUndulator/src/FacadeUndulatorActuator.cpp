/*!
* \file
* \brief    Definition of FacadeProxyActuator class
* \author   Julien Malik - F. Langlois - Synchrotron SOLEIL
*/

#include <scansl/actors/GenericActuator.h>
#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat/plugin/PlugInSymbols.h>
#include "../../Galil/src/GalilActuator.h"

namespace ScanUtils
{

class FacadeProxyActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "FacadeProxyActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Proxy");
    }
};

//- Classe mere pour les onduleurs
class ElectroMagneticUndulatorActuator : public WaitingStateChangeActuator
{
public:
	ElectroMagneticUndulatorActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "";
        config_.timeout_ms        = 10000;
    }

    void check_initial_condition()
    {
        Tango::DevState state = proxy_->state();
        if (state != Tango::ON)
        {
            THROW_YAT_ERROR("UNAUTHORIZED",
                            "ElectroMagneticUndulator should be ON to move",
                            "ElectroMagneticUndulator::check_initial_condition");
        }
    }

};


//- Sous classe pour l'onduleur SMID (Apple2+InVacuum+Emphu)
class MIDActuator : public WaitingStateChangeActuator
{
public:

	MIDActuator(std::string remote_class_name)
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Abort";
        config_.timeout_ms        = 10000;
        if (remote_class_name == "EmphuBLI")
        {
        	config_.abort_cmd_name    = "";
        }
    }

    // ============================================================================
    // MIDActuator::check_initial_condition
    // ============================================================================
	void check_initial_condition()
    {
        Tango::DevState state = proxy_->state();
        if (state != Tango::ON)
        {
            THROW_YAT_ERROR("UNAUTHORIZED",
                            "Undulator should be ON to move",
                            "UndulatorActuator::check_initial_condition");
        }
    }


    // ============================================================================
    // MIDActuator::get_speed
    // ============================================================================
    double get_speed( std::string attr_name )
    {
        if (attr_name == "gap")
        {
            Tango::DeviceAttribute velocity_attr = proxy_->read_attribute( "gapVelocity" );
            double velocity;
            velocity_attr >> velocity;
            return velocity;
        }
        else
        {
            return 0;
        }
    }

    // ============================================================================
    // MIDActuator::set_speed
    // ============================================================================
    void set_speed( std::string attr_name, double speed )
    {
        if (attr_name == "gap")
        {
            SCAN_DEBUG << "Setting speed of " << config_.dev_name << " to " << speed << ENDLOG;
            Tango::DeviceAttribute velocity_attr("gapVelocity", speed);
            proxy_->write_attribute( velocity_attr );
        }
    }
};

//---------------------------------------------------------------------
//- Actuator
class FacadeProxyActuator : public IPolledActuator
{
public : //! IPolledActuator implementation

    virtual void init( std::string device_name )
    {
        try
        {
            std::string proxy_remote_class;
            Tango::DeviceAttribute dev_attr = DevProxies::instance().proxy(device_name)->read_attribute("remoteClass");
            dev_attr >> proxy_remote_class;

            //- If MID and EmphuBLI
            if ( (proxy_remote_class == "GenericMotorizedInsertionDeviceBLI") || (proxy_remote_class == "EmphuBLI"))
            {
                impl.reset( new MIDActuator(proxy_remote_class) );
            }
            //- If HU256
            else if ( (proxy_remote_class == "HU256SpiBli") || (proxy_remote_class == "HU640SpiV2Bli"))
            {
                impl.reset( new ElectroMagneticUndulatorActuator() );
            }
            //- if a GalilAxis or GalilPiezoAna or Diaphragme (as Diaphragme works like a GA)
            else if ( (proxy_remote_class == "GalilAxis") ||(proxy_remote_class == "GalilPiezoAna")||(proxy_remote_class == "Diaphragme") )
            {
                impl.reset( new GalilActuator() );
            }
            else
            {
                SCAN_DEBUG << "Unsupported remote device: " << proxy_remote_class << ", generic behavior is used" << ENDLOG;
                impl.reset( new GenericActuator() );
            }
            impl->init( device_name );
        }
        catch( Tango::DevFailed& df )
        {
            THROW_YAT_ERROR("PLUGIN_ERROR",df.errors[0].desc,"FacadeProxyActuator::init");
        }
    }

    virtual void check_initial_condition( std::string attr_name )
    {
        if (impl)
            impl->check_initial_condition(attr_name);
    }

    virtual void ensure_supported( std::string attr_name )
    {
        if (impl)
            impl->ensure_supported(attr_name);
    }

    void set_position( std::string attr_name, double position )
    {
        if (impl)
            impl->set_position(attr_name, position);
    }

    bool is_enabled( std::string attr_name )
    {
        return impl && impl->is_enabled(attr_name);
    }


    void set_enabled( std::string attr_name, bool enable )
    {
        if (impl)
            impl->set_enabled(attr_name, enable);
    }

    LimitsType get_limits( std::string attr_name )
    {
        return impl ? impl->get_limits(attr_name) : LimitsType("", "");
    }

    void set_limits( std::string attr_name, const LimitsType& limits )
    {
        if (impl)
            impl->set_limits(attr_name, limits);
    }


    virtual double get_speed( std::string attr_name )
    {
        if (impl)
            return impl->get_speed( attr_name );
        else
            return 0;
    }

    virtual void set_speed( std::string attr_name, double speed )
    {
        if (impl)
            impl->set_speed( attr_name, speed );
    }

    virtual void go_to( const std::vector<PositionRequest>& positions )
    {
        if (impl)
            impl->go_to( positions );
    }

    virtual bool is_moving( void )
    {
        if (impl)
            return impl->is_moving();
        else
            return false;
    }

    virtual void error_check( void )
    {
        if (impl)
            impl->error_check();
    }

    virtual double get_polling_period_ms( void )
    {
        if (impl)
            return impl->get_polling_period_ms();
        else
            return 100.0;
    }

    virtual void abort( void )
    {
        if (impl)
            impl->abort();
    }

//debut SPYC-155
std::vector< std::string > read_fixed_pos_list(void)
{     THROW_YAT_ERROR( "NOT IMPLEMENTED",
                      "read fixed position list is not supported",
                     "FacadeProxyActuator::read_fixed_pos_list" );
}//fin read_fixed_pos_list

std::string read_current_value(void)
{
      THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read current attribute value is not supported",
                     "FacadeProxyActuator::read_current_value" );
}//fin read_current_value
//fin SPYC-155

//debut SPYC-300
std::string read_selected_position(void)
{
      THROW_YAT_ERROR( "NOT IMPLEMENTED",
                     "read selected position attribute value is not supported",
                     "FacadeProxyActuator::read_selected_position" );
}//fin read_selected_position

//fin SPYC-300

private:
    IPolledActuatorPtr impl;
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::FacadeProxyActuator, ScanUtils::FacadeProxyActuatorInfo);

