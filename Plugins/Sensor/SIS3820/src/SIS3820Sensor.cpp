/*!
 * \file
 * \brief    Definition of SIS3820Sensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "SIS3820Sensor.h"

#include <boost/scoped_ptr.hpp>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::SIS3820Sensor, ScanUtils::SIS3820SensorInfo);

namespace ScanUtils
{
//  namespace Sensor
//  {
const std::string PlugInID                ( "SIS3820Sensor" );
const std::string VersionNumber           ( "1.0.0" );

const std::string CLASS_NAME              ("SIS3820");

// ============================================================================
// SIS3820SensorInfo::get_plugin_id
// ============================================================================
std::string SIS3820SensorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// SIS3820SensorInfo::get_interface_name
// ============================================================================
std::string SIS3820SensorInfo::get_interface_name() const
{
    return ISensorInterfaceName;
}

// ============================================================================
// SIS3820SensorInfo::get_version_number
// ============================================================================
std::string SIS3820SensorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// SIS3820SensorInfo::supported_classes
// ============================================================================
void SIS3820SensorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back(CLASS_NAME);
}



// ============================================================================
// SIS3820Sensor::SIS3820Sensor
// ============================================================================
SIS3820Sensor::SIS3820Sensor()
{

}

// ============================================================================
// SIS3820Sensor::~SIS3820Sensor
// ============================================================================
SIS3820Sensor::~SIS3820Sensor()
{

}

// ============================================================================
// SIS3820Sensor::init
// ============================================================================
void SIS3820Sensor::init( std::string device_name ,bool sync )
{

    try
    {
        proxy_ = DevProxies::instance().proxy(device_name);

    }
    catch (Tango::DevFailed& df)
    {
        throw yat4tango::TangoYATException(df);
    }

}

// ============================================================================
// SIS3820Sensor::before_integration
// ============================================================================
void SIS3820Sensor::before_integration( void )
{

    proxy_->command_inout( "Reset" );

}

}
