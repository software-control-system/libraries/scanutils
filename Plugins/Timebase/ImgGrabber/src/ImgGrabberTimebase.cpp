/*!
 * \file
 * \brief    Definition of ImgGrabberTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class ImgGrabberTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "ImgGrabberTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("ImgGrabber");
    }
};

class ImgGrabberTimebase : public WaitingStateChangeTimebase
{

public:
    ImgGrabberTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 20;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 11000; // Basler camera can integrate 10 seconds, and currently Snap is blocking
        config_.integration_time_attr = "ExposureTime";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Snap";
    }
/*  
 *  http://jira.synchrotron-soleil.fr/jira/browse/SPYC-83
 *  
 *
    void init( std::string device_name, bool sync )
    {
        // call the super class init
        WaitingStateChangeTimebase::init( device_name, sync );
        //if sync & RUNNING, call stop
        Tango::DevState current_state = proxy_->state();
        if(sync && current_state==Tango::RUNNING)
        {
            proxy_->command_inout("Stop");
        }
    }

    void after_run()
    {
        //always
        proxy_->command_inout("Start");
    }

    virtual void abort( void )
    {
        this->after_run();
    }
*/
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::ImgGrabberTimebase,
                          ScanUtils::ImgGrabberTimebaseInfo);
