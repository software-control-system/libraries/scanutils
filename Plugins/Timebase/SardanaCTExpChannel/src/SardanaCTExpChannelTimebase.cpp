/*!
 * \file
 * \brief    Declaration of SardanaCTExpChannelTimebase class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "SardanaCTExpChannelTimebase.h"
#include <yat/plugin/PlugInSymbols.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::SardanaCTExpChannelTimebase,
                          ScanUtils::SardanaCTExpChannelTimebaseInfo);

namespace ScanUtils
{
const std::string PlugInID                ( "SardanaCTExpChannelTimebase" );
const std::string VersionNumber           ( "1.0.0" );

// ============================================================================
// SardanaCTExpChannelTimebaseInfo::get_plugin_id
// ============================================================================
std::string SardanaCTExpChannelTimebaseInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// SardanaCTExpChannelTimebaseInfo::get_interface_name
// ============================================================================
std::string SardanaCTExpChannelTimebaseInfo::get_interface_name() const
{
    return IPolledTimebaseInterfaceName;
}

// ============================================================================
// SardanaCTExpChannelTimebaseInfo::get_version_number
// ============================================================================
std::string SardanaCTExpChannelTimebaseInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// SardanaCTExpChannelTimebaseInfo::supported_classes
// ============================================================================
void SardanaCTExpChannelTimebaseInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back("CTExpChannel");
}


// ============================================================================
// SardanaCTExpChannelTimebase::SardanaCTExpChannelTimebase
// ============================================================================
SardanaCTExpChannelTimebase::SardanaCTExpChannelTimebase()
{
    config_.counting_state        = Tango::MOVING;
    config_.error_states.push_back(Tango::FAULT);
    config_.error_states.push_back(Tango::UNKNOWN);
    config_.polling_period_ms     = 10;
    config_.abort_cmd_name        = "Abort";
    config_.timeout_ms            = 3000;
    config_.integration_time_attr = "Value";
    config_.integration_time_gain = 1.0;
    config_.start_cmd_name        = "Start";
}
}
