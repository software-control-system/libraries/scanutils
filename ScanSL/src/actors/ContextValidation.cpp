#include <scansl/actors/ContextValidation.h>
#include <scansl/ThreadExiter.h>
#include <boost/scoped_ptr.hpp>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{
const size_t POLLING_PERIOD_MS = 50;

const size_t CONTEXT_VALID_MSG_UPDATE = yat::FIRST_USER_MSG;

class ContextValidationTask : public yat::Task
{
public:
    ContextValidationTask()
        : validity_(false)
    {}

    bool is_valid( void )
    {
        return validity_;
    }

    void force_update( void )
    {
        this->wait_msg_handled( yat::Message::allocate( CONTEXT_VALID_MSG_UPDATE, 0, true ) );
    }

protected:
    virtual void handle_message (yat::Message& msg)
    throw (yat::Exception)
    {
        try
        {
            switch ( msg.type() )
            {
            case yat::TASK_INIT:
            {
                std::string* attr_name;
                msg.detach_data( attr_name );
                boost::scoped_ptr<std::string> guard(attr_name);
                proxy_.reset( new Tango::AttributeProxy(*attr_name) );
                this->on_update();
                this->set_periodic_msg_period( POLLING_PERIOD_MS );
            }
                break;
            case yat::TASK_EXIT:
            {
                proxy_.reset();
            }
                break;
            case yat::TASK_PERIODIC:
            {
                this->on_update();
            }
            case CONTEXT_VALID_MSG_UPDATE:
            {
                this->on_update();
            }
            }
        }
        catch( Tango::DevFailed& df )
        {
            yat4tango::TangoYATException ex(df);
            RETHROW_YAT_ERROR( ex,
                               "SOFTWARE_FAILURE",
                               "Error during Context Validation handling",
                               "ContextValidationTask::handle_message");
        }
    }

private:
    void on_update( void )
    {
        try
        {
            dev_attr_ = proxy_->read();
            dev_attr_ >> validity_;
        }
        catch( Tango::DevFailed& )
        {
            validity_ = false;
        }
    }

    bool validity_;
    boost::shared_ptr<Tango::AttributeProxy> proxy_;
    Tango::DeviceAttribute dev_attr_;
};


ContextValidation::ContextValidation( const ScanConfig& config )
{
    if ( !config.context_validation.empty() )
    {
        task_.reset( new ContextValidationTask(), ThreadExiter() );
        yat::Message* msg = yat::Message::allocate( yat::TASK_INIT, INIT_MSG_PRIORITY, true );
        msg->attach_data( config.context_validation );
        task_->go( msg );
    }
}

ContextValidation::~ContextValidation( )
{
}

void ContextValidation::start_monitoring( void )
{
    if (task_)
    {
        task_->enable_periodic_msg( true );
        task_->force_update();
    }
}

bool ContextValidation::is_valid( void )
{
    return task_ ? task_->is_valid() : true;
}

void ContextValidation::stop_monitoring( void )
{
    if (task_)
        task_->enable_periodic_msg( false );
}

}

