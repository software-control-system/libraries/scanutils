#ifndef FILE_MODIFIER_H
#define FILE_MODIFIER_H
#include <string>
#include <sstream>

/**
  This is a simple class to load a file, replace some key words and save it.
  It is used to transform the template files into compilable code.

  Author: Yves Acremann, 3.2.2012
**/
using namespace std;
class FileModifier{
public:
   FileModifier(string inputPath);
   ~FileModifier();

   void replace(string from, string to);
   void store(string path);
  
private:
   string* contents;
};

#endif

