/*!
* \file
* \brief    Definition of ImgBeamAnalyzerSensor class
* \author   Julien Malik - Synchrotron SOLEIL
*/

#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/plugin/Sensor.h>
#include <scansl/DevProxies.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class ImgBeamAnalyzerSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "ImgBeamAnalyzerSensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("ImgBeamAnalyzer");
    }
};



class ImgBeamAnalyzerSensor : public Sensor
{
public: //! structors

    ImgBeamAnalyzerSensor()
        : one_shot_( false ),
          synchronize_(false)
    {
    }

public: //! ISensor implementation

    virtual void init( std::string device_name, bool sync )
    {
        try
        { 
  	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
            synchronize_ = sync;

        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }

    virtual void after_integration( void )
    {
        //if synchronize (Mode forced to ONESHOT) OR Mode was ONESHOT => call Process command
        if(one_shot_ || synchronize_)
        {
            proxy_->command_inout( "Process" );
        }
    }
    virtual void before_run()
    {
        try {
            //- First save current settings (rois,...)
            proxy_->command_inout( "SaveCurrentSettings" );

            //- Then Check Mode (ONESHOT/CONTINUOUS)
            std::string prop_name( "Mode" );
            Tango::DbData prop_data;
            proxy_->unsafe_proxy().get_property( prop_name, prop_data );

            // checking if property 'Mode' exist
            if ( prop_data[0].is_empty() )
            {
                std::ostringstream oss;
                oss << "The device " << device_name_ << " has no property named 'Mode'."
                    <<"This is abnormal and the device is not usable as is";

                THROW_DEVFAILED( "SENSOR_ERROR",
                                 oss.str().c_str(),
                                 "ImgBeamAnalyserSensor::before_run" );
            }

            std::string prop_value;
            prop_data[0] >> prop_value;
            if (prop_value == "ONESHOT")
            {
                one_shot_ = true;
                // ensures that state is STANDBY in ONESHOT Mode, otherwise ERROR
                Tango::DevState state = proxy_->state();
                if ( state != Tango::STANDBY )
                {
                    std::ostringstream oss;
                    oss << "The device " << device_name_ << " state is " << Tango::DevStateName[state] << "." << std::endl
                        << "It should be STANDBY";

                    THROW_DEVFAILED( "SENSOR_ERROR",
                                     oss.str().c_str(),
                                     "ImgBeamAnalyserSensor::before_run" );
                }
            }
            else
            {
                one_shot_ = false;
                // ensures that state is RUNNING in CONTINUOUS Mode, otherwise Start it.
                Tango::DevState state = proxy_->state();
                if ( state != Tango::RUNNING )
                {
                    proxy_->command_inout( "Start" );
                }
            }

            //if synchronize AND Mode was CONTINUOUS => force Mode to ONESHOT
            if(synchronize_ && !one_shot_ )
            {
                // Force ONESHOT Mode
                Tango::DbData properties;
                Tango::DbDatum mode("Mode");
                mode << "ONESHOT";
                properties.push_back( mode );
                proxy_->unsafe_proxy().put_property(properties);
                proxy_->command_inout("Init");
            }
        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }
    virtual void after_run( void )
    {

        //if synchronize (Mode forced to ONESHOT) AND Mode was CONTINUOUS => restore Mode to CONTINIUOUS
        if(synchronize_ && !one_shot_)
        {
            // restore CONTINUOUS Mode
            Tango::DbData properties;
            Tango::DbDatum mode("Mode");
            mode << "CONTINUOUS";
            properties.push_back( mode );
            proxy_->unsafe_proxy().put_property(properties);
            proxy_->command_inout("Init");
        }
    }

    virtual void abort( void )
    {
        this->after_run();
    }

private: //! private implementation
    //! configuration
    bool one_shot_;
    bool synchronize_;
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::ImgBeamAnalyzerSensor,
                          ScanUtils::ImgBeamAnalyzerSensorInfo);
