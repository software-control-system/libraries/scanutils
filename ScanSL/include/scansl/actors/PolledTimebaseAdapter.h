#ifndef _SCANSERVER_POLLED_TIMEBASE_ADAPTER_H
#define _SCANSERVER_POLLED_TIMEBASE_ADAPTER_H

#include <scansl/interfaces/IPolledTimebase.h>
#include <scansl/actors/ITimebase.h>
#include <yat/threading/Task.h>

namespace ScanUtils
{

class SCANSL_DECL PolledTimebaseAdapter : public ITimebase,
        public yat::Task
{
public:
    PolledTimebaseAdapter( IPolledTimebasePtr polled_timebase_obj );

    virtual void init( std::string device_name, bool sync );

    virtual void check_initial_condition( );

    virtual void start( double integration_time );

    virtual bool is_counting( void );

    virtual void error_check( void );

    virtual void abort( void );

    virtual void before_run( void );

    virtual void after_run( void );

protected:
    virtual void handle_message (yat::Message& msg)
    throw (yat::Exception);

private:
    void check_counting_state( void );

    std::string device_name_;

    IPolledTimebasePtr polled_timebase_obj_;

    yat::Mutex mutex_;
    bool is_counting_;

    bool has_error_;
    yat::Exception last_error_;

};

}

#endif
