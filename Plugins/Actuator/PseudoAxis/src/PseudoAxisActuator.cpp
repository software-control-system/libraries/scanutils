/*!
 * \file
 * \brief    Definition of PseudoAxisActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>
#include <scansl/Util.h>

namespace ScanUtils
{
class PseudoAxisActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PseudoAxisActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PseudoAxis");
        list.push_back("HklGroup");
    	// be careful the following class exists in Diffracto V1 and V2
        // but for the Scan or Spyc it is used only in V2
        list.push_back("PseudoAxes");

    }
};

class PseudoAxisActuator : public WaitingStateChangeActuator
{
public:
    PseudoAxisActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";

		// cf http://jira/jira/browse/SCAN-510
        is_diff_V1 = true;
    }

    void init( std::string device_name )
    {
    	this->WaitingStateChangeActuator::init( device_name );

    	std::string device_class = Util::instance().get_class( device_name);
    	if(device_class == "PseudoAxes" )
    	{
    		// we must know if it is a Diffracto V1 ou V2
    		Tango::DeviceAttribute freeze_attr;
    		bool value;
    		try
    		{
    			freeze_attr = proxy_->read_attribute("freeze");
    			// An exception is raised if it doesn't exist
    			freeze_attr >> value;
    			is_diff_V1 = false;
            }
    	    catch( Tango::DevFailed& df )
            {
    	    	is_diff_V1 = true;
            }
    	}

     }

    void check_initial_condition( std::string /*attr_name*/ )
    {
        //- check the state
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
        {
            std::ostringstream oss;
            oss << "The device "
                << config_.dev_name
                << " must be STANDBY or ALARM to move";

            THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "PseudoAxisActuatorInfo::check_initial_condition");
        }
    }

    void go_to( const std::vector<PositionRequest>& positions )
    {
       	if(is_diff_V1)
       	{
       		this->WaitingStateChangeActuator::go_to(positions);
       	}
       	else
       	{
       		Tango::DeviceAttribute freeze_attr("freeze",true);
       		proxy_->write_attribute( freeze_attr);

       		this->WaitingStateChangeActuator::go_to(positions);

       		freeze_attr << false ;
       		proxy_->write_attribute( freeze_attr);
       	}
    }

    void set_position( std::string attr_name, double position )
    {
        if(!is_diff_V1)
	{
        Tango::DeviceData data_compute_cmd;
	 	std::vector<double> list_pos ;
		std::vector<std::string> list_att_name;

			list_pos.push_back(position);
			list_att_name.push_back(attr_name + "_offset");

	    	data_compute_cmd.insert(list_pos,list_att_name);
            proxy_->command_inout("ComputeNewOffset", data_compute_cmd);
        }
	else {
            WaitingStateChangeActuator::set_position(attr_name, position);
        }
    }



private: //! private implementation
	// Value true if the device is a Diffractometer V1 release
	bool is_diff_V1;

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PseudoAxisActuator, \
                          ScanUtils::PseudoAxisActuatorInfo);
