#include <scansl/ScanTask.h>
#include <scansl/State.h>
#include <scansl/actors/ContextValidation.h>
#include <scansl/scheduler/SchedulerFactory.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/scoped_ptr.hpp>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

const size_t COMMAND_TIMEOUT_MS = 5000;

enum
{
    ScanTaskMsgID_START = yat::FIRST_USER_MSG,
    ScanTaskMsgID_PAUSE,
    ScanTaskMsgID_RESUME,
    ScanTaskMsgID_STOP,
    ScanTaskMsgID_ABORT,
    ScanTaskMsgID_CHECK_CONTEXT,
    ScanTaskMsgID_NEXT_ACTION,
    ScanTaskMsgID_EXECUTE_AFTERRUN_ACTION
} ScanTaskMsgID;


ScanTask::ScanTask( void )
    : state_( &State_ON::instance() ), acquisition_started_(false)
{
}

ScanTask::~ScanTask( void )
{
}


//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > ScanTask::get_actuators_attributes()
{
    return scheduler_->get_actuators_attributes();
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > ScanTask::get_actuators2_attributes()
{
    return scheduler_->get_actuators2_attributes();
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > ScanTask::get_sensors_attributes()
{
    return scheduler_->get_sensors_attributes();
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > ScanTask::get_timestamps_attributes()
{
    return scheduler_->get_timestamps_attributes();
}


//! start the current run
void ScanTask::start( const ScanConfig& c )
{
    //- post a START msg, and wait for it to be handled
    post_msg( *this, ScanTaskMsgID_START, COMMAND_TIMEOUT_MS, true, c );
}

//! pause the current run
void ScanTask::pause( void )
{
    post_msg( *this, ScanTaskMsgID_PAUSE, COMMAND_TIMEOUT_MS, false);
}

//! resume the current run
void ScanTask::resume( void )
{
    post_msg( *this, ScanTaskMsgID_RESUME, COMMAND_TIMEOUT_MS, false );
}

//! abort the run immediately
void ScanTask::abort( void )
{
    post_msg( *this, ScanTaskMsgID_ABORT, COMMAND_TIMEOUT_MS, true );
}

//! abort the run immediately
void ScanTask::execute_afterrun_action( AfterRunActionDesc& action )
{
    post_msg( *this, ScanTaskMsgID_EXECUTE_AFTERRUN_ACTION, COMMAND_TIMEOUT_MS, true, action );
}


void ScanTask::handle_message (yat::Message& msg)
{
    try
    {
        switch( msg.type() )
        {
        case yat::TASK_INIT:
            this->on_init();
            break;
        case yat::TASK_EXIT:
            this->on_exit();
            break;
        case ScanTaskMsgID_START:
        {
            ScanConfig* config;
            msg.detach_data( config );
            boost::scoped_ptr<ScanConfig> config_ptr( config );
            this->on_start( *config_ptr );
        }
            break;
        case ScanTaskMsgID_PAUSE:
        {
            this->on_pause( false );
        }
            break;
        case ScanTaskMsgID_RESUME:
            this->on_resume();
            break;
        case ScanTaskMsgID_ABORT:
            this->on_abort_by_user();
            break;
        case ScanTaskMsgID_CHECK_CONTEXT:
            this->on_check_context();
            break;
        case ScanTaskMsgID_NEXT_ACTION:
            this->on_next_action();
            break;
        case ScanTaskMsgID_EXECUTE_AFTERRUN_ACTION:
        {
            AfterRunActionDesc* desc;
            msg.detach_data( desc );
            boost::scoped_ptr<AfterRunActionDesc> desc_ptr( desc );
            this->on_execute_afterrun_action( *desc );
        }
            break;
        }
    }
    catch( yat::Exception& ex )
    {
        this->on_abort_on_error(ex);
        if (msg.type() == ScanTaskMsgID_START)
            state_ = &State_ON::instance();
        throw ex;
    }
    catch( Tango::DevFailed& df )
    {
        yat4tango::TangoYATException ex(df);
        this->on_abort_on_error(ex);
        if (msg.type() == ScanTaskMsgID_START)
            state_ = &State_ON::instance();
        throw ex;
    }
    catch( ... )
    {
        yat::Exception ex("UNKNWON_ERROR","Unknown error caught","ScanTask::handle_message");
        this->on_abort_on_error(ex);
        if (msg.type() == ScanTaskMsgID_START)
            state_ = &State_ON::instance();
        throw ex;
    }
}

void ScanTask::on_init ( void )
{
}

void ScanTask::on_exit ( void )
{
    if( scheduler_ && acquisition_started_ )
    {
        scheduler_->hard_abort_datacollector();
    }

    scheduler_.reset();
    context_validation_.reset();

    // Stopper ici le datarecorder !!!
}

void ScanTask::on_start ( const ScanConfig& config )
{
    SCAN_INFO << "Starting a new run..." << ENDLOG;
    context_validation_.reset();
    scheduler_.reset();
    config_ = config;
    scheduler_ = SchedulerFactory::instance().create( config_ );

    if( config_.ctxvalid_behaviour != CtxValidBehaviour_IGNORE )
    {
      context_validation_.reset( new ContextValidation( config_ ) );
      context_validation_->start_monitoring();
    }

    state_ = &state_->start();
    post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
    acquisition_started_ = true;
}

void ScanTask::on_pause ( bool on_context_error )
{
    SCAN_INFO << "Pausing run at user request..." << ENDLOG;
    state_ = &state_->pause(on_context_error);
}

void ScanTask::on_resume ( void )
{
    SCAN_INFO << "Resuming run at user request..." << ENDLOG;
    state_ = &state_->resume();
    post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
}

void ScanTask::on_abort_on_error ( yat::Exception& ex )
{
    LOG_EXCEPTION_FATAL( ex );
    acquisition_started_ = false;
    if ( scheduler_ )
    {
        scheduler_->abort_actuators();
        scheduler_->abort_sensors();
        scheduler_->abort_scan_on_error();
    }

    if ( context_validation_ )
    {
        context_validation_->stop_monitoring();
    }
    // paused_on_context_error_ = false;
    state_ = &state_->abort_on_error(ex);

    if ( scheduler_ && config_.properties.execute_after_run_action_on_abort )
    {
        if( config_.after_run_action.type == AfterRunActionType_FIRST ||
                config_.after_run_action.type == AfterRunActionType_PRIOR )
            scheduler_->execute_afterrun_action( config_.after_run_action );
    }

}

void ScanTask::on_abort_by_user ( void )
{
    acquisition_started_ = false;
    if ( scheduler_ )
    {
        scheduler_->abort_actuators();
        scheduler_->abort_sensors();
        scheduler_->abort_scan_on_error();
    }

    if ( context_validation_ )
    {
        context_validation_->stop_monitoring();
    }

    // paused_on_context_error_ = false;
    state_ = &state_->abort_by_user();

    if ( scheduler_ )
    {
        if( config_.after_run_action.type == AfterRunActionType_FIRST ||
                config_.after_run_action.type == AfterRunActionType_PRIOR )
            scheduler_->execute_afterrun_action( config_.after_run_action );
    }

}

void ScanTask::on_complete_scan ( void )
{
    acquisition_started_ = false;
    if ( context_validation_ )
    {
        context_validation_->stop_monitoring();
    }
    
    // Keep state if the scan was aborted by the user
    // in this case the user has executed an authorized AfterRun action
    if( state_ != &State_ABORTED_BY_USER::instance() )
        state_ = &state_->abort_on_successfullscan();
}

void ScanTask::on_execute_afterrun_action ( const AfterRunActionDesc& action )
{
    acquisition_started_ = false;
    // allowed only on a complete scan or if the action concerns only the actuators
    if ( state_ == &State_ABORTED_ON_ERROR::instance() ||
         (state_ == &State_ABORTED_BY_USER::instance() &&
          !( action.type == AfterRunActionType_FIRST ||
             action.type == AfterRunActionType_PRIOR) ) )
    {
        THROW_YAT_ERROR( "WRONG_STATE",
                         "This scan has been aborted. Executing a post scan behavior is not (yet) supported in this case",
                         "ScanTask::on_execute_afterrun_action" );
    }


    if ( scheduler_ )
    {
        // Keep state if user abort
        if( state_ != &State_ABORTED_BY_USER::instance() )
            state_ = &state_->start();
        
        scheduler_->execute_afterrun_action( action );
        post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
    }
}

void ScanTask::on_check_context ( void )
{
    if ( state_ == &State_PAUSED_ON_CONTEXT::instance() )
    {
        // check if we can resume the scan
        if ( context_validation_ && !context_validation_->is_valid() )
        {
            SCAN_WARN << "Context is still invalid. Waiting..." << ENDLOG;
            //- sleep one second
            yat::ThreadingUtilities::sleep( 1, 0 );

            // check back later
            post_msg( *this, ScanTaskMsgID_CHECK_CONTEXT, COMMAND_TIMEOUT_MS, false );
        }
        else
        {
            SCAN_INFO << "Resuming run because context is back to a valid state..." << ENDLOG;
            state_ = &state_->resume();
            post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
        }
    }
    else
    {
        // check context and apply strategy if invalid
        if ( context_validation_ && !context_validation_->is_valid() )
        {
            SCAN_WARN << "Invalid context detected" << ENDLOG;
            switch (config_.ctxvalid_behaviour)
            {
            case CtxValidBehaviour_IGNORE:
            {
                SCAN_WARN << "Ignoring context error at user request..." << ENDLOG;
                post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
                break;
            }
            case CtxValidBehaviour_PAUSE:
            {
                SCAN_WARN << "Pausing run because of an invalid context..." << ENDLOG;
                this->on_pause( true );
                post_msg( *this, ScanTaskMsgID_CHECK_CONTEXT, COMMAND_TIMEOUT_MS, false );
            }
                break;
            case CtxValidBehaviour_ABORT:
            default:
            {
                SCAN_WARN << "Aborting run because of an invalid context..." << ENDLOG;
                yat::Exception ex( "INVALID_CONTEXT", "Context invalid", "ScanTask::on_next_action" );
                this->on_abort_on_error( ex );
            }
                break;
            }
        }
        else
        {
            post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
        }
    }
}


void ScanTask::on_next_action ( void )
{
    try
    {
        if ( ! state_->is_next_action_allowed() )
            return;

        SchedulerState result = scheduler_->execute_action();

        if ( result.end_of_run )
        {
            SCAN_INFO << "Run ended successfully" << ENDLOG;
            this->on_complete_scan( );
        }
        else if ( result.must_pause || result.end_of_step && config_.manual_mode )
        {
            SCAN_INFO << "Pausing run" << ENDLOG;
            this->on_pause( false );
        }
        else
        {
            if( config_.ctxvalid_behaviour == CtxValidBehaviour_IGNORE )
                post_msg( *this, ScanTaskMsgID_NEXT_ACTION, COMMAND_TIMEOUT_MS, false );
            else
                post_msg( *this, ScanTaskMsgID_CHECK_CONTEXT, COMMAND_TIMEOUT_MS, false );
        }
    }
    catch( Tango::DevFailed& df )
    {
        // Turn Tango::DevFailed exception into yat::Exception
        yat4tango::TangoYATException e(df);
        throw e;
    }
    catch( yat::Exception& e )
    {
        throw e;
    }
    catch( std::exception& e )
    {
        throw yat::Exception("SYSTEM_ERROR", e.what(), "ScanTask::on_next_action");
    }
    catch( ... )
    {
        throw yat::Exception("UNKNOWN_ERROR", "Unknown error occured when executing action on actor", "ScanTask::on_next_action");
    }
}


void ScanTask::get_state_status( Tango::DevState& state, std::string& status )
{
    state = state_->get_tango_state();
    status = state_->get_tango_status();
}

ScanData ScanTask::get_data( void )
{
    if (scheduler_)
        return scheduler_->get_data();
    else
        return ScanData();
}

ScanBuffers ScanTask::get_buffers( void )
{
    if (scheduler_)
        return scheduler_->get_buffers();
    else
        return ScanBuffers();
}

}

