file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(XiaDxpContinuousTimebase MODULE ${sources})
target_include_directories(XiaDxpContinuousTimebase PRIVATE ${includedirs})
target_link_libraries(XiaDxpContinuousTimebase PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(XiaDxpContinuousTimebase PROPERTIES OUTPUT_NAME "XiaDxpContinuousTimebase-${PROJECT_VERSION}")
endif()

install(TARGETS XiaDxpContinuousTimebase LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
