/*!
 * \file
 * \brief    Definition of HALTimebase class
 * \author   Sandra PIERRE-JOSEPH - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class HALTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "HALTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("HAL");
    }
};

class TDCTimebase : public WaitingStateChangeTimebase
{
public:
    TDCTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Start";
    }

    void check_initial_condition( )
    {
		WaitingStateChangeTimebase::check_file_generation_condition("nexusFileGeneration");
    }
    
    void before_run()
    {

        try
        {
            Tango::DevLong read_att;
            Tango::DeviceAttribute dev_attr = proxy_->read_attribute("integrationType");
            dev_attr >> read_att;

            //- only soft finite integration type is supported by step by step scan
            if ( read_att != 1)
            {
                std::ostringstream oss;
                oss << "Invalid integrationType type value - only 1 is supported (software finite integration).";
                THROW_DEVFAILED( "TIMEBASE_ERROR",
                                 oss.str().c_str(),
                                 "TDCTimebase::before_run" );
            }

            dev_attr = proxy_->read_attribute("acquisitionMode");
            dev_attr >> read_att;

            //- 2 modes are available for step by step scan: 
            //- * final mode (whole data available at the end of acquisition)
            //- * continuous mode (raw data continuously updated, user data available at the end of acquisition)
            if ( (read_att != 1) && (read_att != 0))
            {
                std::ostringstream oss;
                oss << "Invalid acquisitionMode type value - only 0 (continous mode) or 1 (final mode) is supported.";
                THROW_DEVFAILED( "TIMEBASE_ERROR",
                                 oss.str().c_str(),
                                 "TDCTimebase::before_run" );
            }
        }
        catch( Tango::DevFailed& df )
        {
            THROW_YAT_ERROR("PLUGIN_ERROR",df.errors[0].desc,"TDCTimebase::before_run");
        }
    }

};

class HALTimebase : public WaitingStateChangeTimebase
{

public:

    void init( std::string device_name, bool sync )
    {
        WaitingStateChangeTimebase::init( device_name, sync );

        // retrieve the kind of object
        Tango::DbData prop_data;
        std::string remote_object;
        prop_data.push_back( Tango::DbDatum("ObjectType") );
        proxy_->unsafe_proxy().get_property( prop_data );

        // checking if 'Buffered' is false
        if ( prop_data[0].is_empty() )
        {
            std::ostringstream oss;
            oss << "Object Type must be specified as a device property for device " << device_name;
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "HALTimebase::init()" );
        }

        prop_data[0] >> remote_object;
        if ( remote_object == "TDC")
        {
            impl.reset( new TDCTimebase() );
        }
        else
        {
            std::ostringstream oss;
            oss << "Unknown ObjectType value in the device property of " << device_name;
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "HALTimebase::init()" );
        }

        if (impl)
            impl->init( device_name, sync);
    }

    void  start( double integration_time )
    {
        if (impl)
            impl->start(integration_time );
    }

    bool  is_counting( void )
    {
        return bool(impl) && impl->is_counting();
    }

    void   error_check( void )
    {
        if (impl)
            impl->error_check();
    }

    double get_polling_period_ms( void )
    {
        if (impl)
            return impl->get_polling_period_ms();
        return 0. ;
    }

    void   abort( void )
    {
        if (impl)
            impl->abort();
    }

    void before_run( void )
    {

        if (impl)
            impl->before_run();
    }

    void after_run( void )
    {
        if (impl)
            impl->after_run();
    }
    
    void check_initial_condition( void )
    {
        if (impl)
            impl->check_initial_condition();
    }

private:
    IPolledTimebasePtr impl;
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::HALTimebase,
                          ScanUtils::HALTimebaseInfo);
