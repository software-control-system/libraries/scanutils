/*!
 * \file
 * \brief    Definition of WaitingStateChangeTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <limits>
#include <boost/scoped_ptr.hpp>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <scansl/ThreadExiter.h>
#include <scansl/Util.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

WaitingStateChangeTimebaseConfig::WaitingStateChangeTimebaseConfig()
    : counting_state(Tango::RUNNING),
      polling_period_ms(100),
      timeout_ms(3000),
      integration_time_gain(1.0)
{
}

// ============================================================================
// WaitingStateChangeTimebase::WaitingStateChangeTimebase
// ============================================================================
WaitingStateChangeTimebase::WaitingStateChangeTimebase()
{

}

// ============================================================================
// WaitingStateChangeTimebase::~WaitingStateChangeTimebase
// ============================================================================
WaitingStateChangeTimebase::~WaitingStateChangeTimebase()
{
}

// ============================================================================
// WaitingStateChangeTimebase::init
// ============================================================================
void WaitingStateChangeTimebase::init( std::string device_name, bool sync )
{
    config_.dev_name = device_name;
    
    proxy_ = DevProxies::instance().proxy(device_name);
    proxy_->set_timeout_millis( config_.timeout_ms );
}

// ============================================================================
// WaitingStateChangeTimebase::check_initial_condition
// ============================================================================
void WaitingStateChangeTimebase::check_initial_condition( ) {
    // no-op
}


// ============================================================================
// WaitingStateChangeTimebase::go_to
// ============================================================================
void WaitingStateChangeTimebase::start( double integration_time )
{
    if ( !config_.integration_time_attr.empty() )
    {
        Tango::DeviceAttribute dev_attr( config_.integration_time_attr, integration_time * config_.integration_time_gain );
        proxy_->write_attribute( dev_attr );
    }

    if ( !config_.start_cmd_name.empty() )
    {
        proxy_->command_inout( config_.start_cmd_name );
    }
}

// ============================================================================
// WaitingStateChangeTimebase::is_moving
// ============================================================================
bool WaitingStateChangeTimebase::is_counting( void )
{
    Tango::DevState current_state = proxy_->state();
    return (current_state == config_.counting_state);
}

// ============================================================================
// WaitingStateChangeTimebase::error_check
// ============================================================================
void WaitingStateChangeTimebase::error_check( void )
{
    Tango::DevState current_state= Tango::UNKNOWN;
    try
    {
        current_state = proxy_->state();
    }
    catch( Tango::DevFailed& df )
    {
        throw yat4tango::TangoYATException( df );
    }

    if( std::find(config_.error_states.begin(), config_.error_states.end(), current_state) != config_.error_states.end() )
    {
        std::ostringstream oss;
        oss << proxy_->dev_name()
            << " has an invalid state : "
            << Tango::DevStateName[current_state];
        THROW_YAT_ERROR( "INVALID_STATE",
                         oss.str().c_str(),
                         "WaitingStateChangeTimebase::error_check" );
    }
}

// ============================================================================
// WaitingStateChangeTimebase::get_polling_period_ms
// ============================================================================
double WaitingStateChangeTimebase::get_polling_period_ms( void )
{
    return config_.polling_period_ms;
}

// ============================================================================
// WaitingStateChangeTimebase::abort
// ============================================================================
void WaitingStateChangeTimebase::abort( void )
{
    if ( ! config_.abort_cmd_name.empty() )
        proxy_->command_inout( config_.abort_cmd_name );
}

// ============================================================================
// WaitingStateChangeTimebase::after_run
// ============================================================================
void WaitingStateChangeTimebase::after_run( void )
{
    //TODO restore old mode
}

// ============================================================================
// WaitingStateChangeTimebase::before_run
// ============================================================================
void WaitingStateChangeTimebase::before_run( void )
{
    // NOOP by default
}

// ============================================================================
// WaitingStateChangeTimebase::check_file_generation_condition
// ============================================================================
void WaitingStateChangeTimebase::check_file_generation_condition( std::string file_generation_att_name )
{

    	bool isFileGeneration = false;
	Tango::DeviceAttribute fileGeneration_attr;
	    try
  	    {
    		fileGeneration_attr = proxy_->read_attribute(file_generation_att_name);
    		fileGeneration_attr >> isFileGeneration;
            }
    	    catch( Tango::DevFailed& df )
            {
	    	// if attribute doesn't exist nothing to do 
            }
	    
	    if(isFileGeneration)
	    {
	    	std::ostringstream oss;
        	oss 	<< "The " << file_generation_att_name 
			<< " attribute must be False for the device "
                	<< config_.dev_name;


            	THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "check_file_generation_condition");
	    }

}

}
