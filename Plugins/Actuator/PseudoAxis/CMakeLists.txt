file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(PseudoAxisActuator MODULE ${sources})
target_include_directories(PseudoAxisActuator PRIVATE ${includedirs})
target_link_libraries(PseudoAxisActuator PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(PseudoAxisActuator PROPERTIES OUTPUT_NAME "PseudoAxisActuator-${PROJECT_VERSION}")
endif()

install(TARGETS PseudoAxisActuator LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
