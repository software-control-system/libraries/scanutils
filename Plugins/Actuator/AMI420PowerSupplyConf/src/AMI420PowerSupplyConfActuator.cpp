/*!
 * \file
 * \brief    Definition of AMI420PowerSupplyConfActuator class
 * \author   FL - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>


namespace ScanUtils
{

class AMI420PowerSupplyConfActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "AMI420PowerSupplyConfActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("AMI420PowerSupplyConf");
    }
};

class AMI420PowerSupplyConfActuator : public WaitingStateChangeActuator
{
public:
    AMI420PowerSupplyConfActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::ALARM);
        config_.polling_period_ms = 200;
        config_.abort_cmd_name    = "RampPause";
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::AMI420PowerSupplyConfActuator,
                          ScanUtils::AMI420PowerSupplyConfActuatorInfo);

