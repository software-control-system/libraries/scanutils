/*!
* \file
* \brief    Definition of Mar345Sensor class
* \author   Julien Malik - Synchrotron SOLEIL
*/

#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class Mar345SensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "Mar345Sensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Mar345");
    }
};


class Mar345Sensor : public Sensor
{
public: //! structors

    Mar345Sensor()
    {
        proxy_open_device_ =0;
        proxy_close_device_ =0;
    }

public: //! ISensor implementation

    virtual void init( std::string device_name,bool sync )
    {
        try
        {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
	    
            //- Then Check if Command exist for Open Shutter : MANTIS 23916
            std::string prop_name( "OpenShutterCommand" );
            std::string open_shutter_command_fqn;

            Tango::DbData prop_data;
            proxy_->unsafe_proxy().get_property( prop_name, prop_data );

            // checking if property 'OpenShutterCommand' exist
            if ( prop_data[0].is_empty() )
            {
                std::ostringstream oss;
                oss << "The device " << device_name << " has no property named 'OpenShutterCommand'."
                    <<"This is abnormal and the device is not usable as is";

                THROW_DEVFAILED( "SENSOR_ERROR",
                                 oss.str().c_str(),
                                 "Mar345Sensor::init" );
            }
            prop_data[0] >> open_shutter_command_fqn;

            size_t pos = open_shutter_command_fqn.find_last_of('/');
            //- extract device name
            std::string open_shutter_device_name = open_shutter_command_fqn.substr(0, pos);
            // get DeviceProxy
            proxy_open_device_ = new Tango::DeviceProxy(open_shutter_device_name);

            //- extract command name
            open_shutter_command_name_ = open_shutter_command_fqn.substr(pos+1,open_shutter_command_fqn.size() );

            //- Then Check if Command exist for Close Shutter : MANTIS 23916
            prop_name= "CloseShutterCommand" ;
            std::string close_shutter_command_fqn;

            proxy_->unsafe_proxy().get_property( prop_name, prop_data );

            // checking if property 'CloseShutterCommand' exist
            if ( prop_data[0].is_empty() )
            {
                std::ostringstream oss;
                oss << "The device " << device_name << " has no property named 'CloseShutterCommand'."
                    <<"This is abnormal and the device is not usable as is";

                THROW_DEVFAILED( "SENSOR_ERROR",
                                 oss.str().c_str(),
                                 "Mar345Sensor::init" );
            }
            prop_data[0] >> close_shutter_command_fqn;

            pos = close_shutter_command_fqn.find_last_of('/');
            //- extract device name
            string close_shutter_device_name = close_shutter_command_fqn.substr(0, pos);
            // get DeviceProxy
            proxy_close_device_ = new Tango::DeviceProxy(close_shutter_device_name);

            //- extract command name
            close_shutter_command_name_ = close_shutter_command_fqn.substr(pos+1,close_shutter_command_fqn.size() );

        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }

    }

    virtual void ensure_supported( std::string)
    {
        //- nothing to do here
    }

    virtual void before_integration( void )
    {
        //   We must send the Open Shutter command
        proxy_open_device_ -> command_inout(open_shutter_command_name_ );
    }

    virtual void after_integration( void )
    {

        //   We must send the close Shutter command
        proxy_close_device_ -> command_inout(close_shutter_command_name_ );

        proxy_->command_inout( "Scan" );
        //- Wait end of running State
        Tango::DevState state = Tango::RUNNING;
        while( state == Tango::RUNNING )
        {
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
            state = proxy_->state();
        }
    }

    virtual void after_run( void )
    {
        //TODO
        //....
    }

    virtual void abort( void )
    {
        //  NO OP
    }

private: //! private implementation
    std::string open_shutter_command_name_;
    std::string close_shutter_command_name_;

    //! configuration
    Tango::DeviceProxy* proxy_open_device_;
    Tango::DeviceProxy* proxy_close_device_;
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::Mar345Sensor,
                          ScanUtils::Mar345SensorInfo);
