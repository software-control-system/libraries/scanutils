#include "FileModifier.h"
#include <iostream>
#include <fstream>

/**
  This is a simple class to load a file, replace some key words and save it.
  It is used to transform the template files into compilable code.

  Author: Yves Acremann, 3.2.2012
**/

// read the whole file into a string
FileModifier::FileModifier(string inputPath){
  contents = new string();
  ifstream infile;
  infile.open (inputPath.c_str(), ifstream::in);
  char c;
  while (infile.good()){
    c = infile.get();       // get character from file
    if (infile.good())
      (*contents) += c;
  }
  infile.close();
}

// delete the string
FileModifier::~FileModifier(){
  delete contents;
}



// replace the occurance of "from" by "to"
void FileModifier::replace(string from, string to){
  string* newData = new string();
  int currentPos = 0;
  while (currentPos < contents->length()){
    int pos = contents->find(from, currentPos);
    if (pos == contents->npos) break;
    contents->replace(pos, from.length(), to);
    currentPos = pos + to.length();
  }
}

// and store it
void FileModifier::store(string path){
  ofstream outfile;
  outfile.open(path.c_str());
  outfile.write(contents->c_str(), contents->length());
  outfile.close();
}

