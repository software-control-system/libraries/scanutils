/*!
 * \file
 * \brief    Definition of GalilActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/lexical_cast.hpp>

namespace ScanUtils
{

class PowerPMACAxisActuator : public WaitingStateChangeActuator
{
public:
    PowerPMACAxisActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::ALARM); //- Alarms mean limit switch (from F Picca)
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::INIT ); //- means motor position is not known TANGODEVIC-1341
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";
    }

    void init(std::string device_name)
    {
        WaitingStateChangeActuator::init( device_name );
    }

    // ============================================================================
    // PowerPMACAxisActuator::check_initial_condition
    // ============================================================================
    void check_initial_condition( std::string attr_name )
    {
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
        {
            std::ostringstream oss;
            oss << "The device "
                << config_.dev_name
                << " must be STANDBY or ALARM to move";

            THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "GalilActuator::check_initial_condition");
        }
    }

    // ============================================================================
    // PowerPMACAxisActuator::set_position
    // ============================================================================
    void set_position( std::string attr_name, double position )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        Tango::DeviceData offset;
        offset << position;
        proxy_->command_inout("ComputeNewOffset", offset);
    }

    /// TODO implement is_enabled ?

    // ============================================================================
    // PowerPMACAxisActuator::set_enabled
    // ============================================================================
    void set_enabled( std::string /*attr_name*/, bool enabled )
    {
        proxy_->command_inout(enabled ? "On" : "Off");
    }

    // ============================================================================
    // PowerPMACAxisActuator::get_limits
    // ============================================================================
    LimitsType get_limits( std::string attr_name )
    {
        return WaitingStateChangeActuator::get_limits(attr_name);
    }

    // ============================================================================
    // PowerPMACAxisActuator::set_limits
    // ============================================================================
    void set_limits( std::string attr_name, const LimitsType& limits )
    {
        WaitingStateChangeActuator::set_limits(attr_name, limits);
    }

    // ============================================================================
    // PowerPMACAxisActuator::get_speed
    // ============================================================================
    double get_speed( std::string attr_name )
    {
        if( (attr_name == "position" || attr_name == "masterposition") )
        {
            Tango::DeviceAttribute velocity_attr = proxy_->read_attribute("velocity");
            double velocity;
            velocity_attr >> velocity;
            return velocity;
        }
        else
        {
            return 0;
        }
    }

    // ============================================================================
    // GalilActuator::set_speed
    // ============================================================================
    void set_speed( std::string attr_name, double speed )
    {
        if( (attr_name == "position" || attr_name == "masterposition") )
        {
            SCAN_DEBUG << "Setting speed of " << config_.dev_name << " to " << speed << ENDLOG;
            Tango::DeviceAttribute velocity_attr("velocity", speed);
            proxy_->write_attribute( velocity_attr );
        }
    }
};

}
