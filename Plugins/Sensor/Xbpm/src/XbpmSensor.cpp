/*!
* \file
* \brief    Definition of XbpmSensor class
* \author   FL - Synchrotron SOLEIL
*/


#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class XbpmSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "XbpmSensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Xbpm");
    }
};


//----------------------------------------------------------------------
//-
//----------------------------------------------------------------------
class XbpmSensor : public Sensor
{
public: //! structors

    XbpmSensor()
    {
    }

public: //! ISensor implementation

    virtual void init( std::string device_name,bool sync )
    {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );

    }

    virtual void before_run()
    {
        // Lire la propri�t� ScanMode du device Xbpm
        bool scan_mode;

        Tango::DbData dev_prop1;
        dev_prop1.push_back( Tango::DbDatum("ScanMode") );

        proxy_->unsafe_proxy().get_property(dev_prop1);
        if ( !dev_prop1[0].is_empty() )
        {
            dev_prop1[0] >> scan_mode;
        }
        else
        {
             std::ostringstream oss;
             oss << "The device XBPM " << device_name_ << " has no property named 'ScanMode'."
                 << "This is abnormal and the device is not usable as is";

             THROW_DEVFAILED( "SENSOR_ERROR",
                              oss.str().c_str(),
                              "XbpmSensor::before_run" );
        }

        if(scan_mode != true )
        {
            std::ostringstream oss;
            oss << "The device XBPM " << device_name_ << " must be in ScanMode. So, ScanMode property must be set to TRUE.";
            THROW_DEVFAILED( "SENSOR_ERROR",
                               oss.str().c_str(),
                               "XbpmSensor::before_run" );
        }

    }


    // JIRA SPYC-163 et TANGODEVIC-651
    /*
    //- nothing to do in before_integration
    //- Xbpm has the same state as the AICtrl

    virtual void after_integration( void )
    {
        //- Wait end of running State
        Tango::DevState state = Tango::RUNNING;
        while( state == Tango::RUNNING )
        {
            state = proxy_->state();
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
        }
    }*/

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XbpmSensor,
                          ScanUtils::XbpmSensorInfo);
