/**
  This program generates templates for ScanServer plugins.

  Author: Yves Acremann, 3.2.2012
**/

#include <iostream>
#include "FileModifier.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>

#define T_ACTUATOR 1
#define T_SENSOR 2
#define T_TIMEBASE 3
#define T_CONTINUOUS_TIMEBASE 4

/**
  Print the help message to the console
**/
void help(){
  std::cout << "\nUsage: \n";
  std::cout << "PluginGenerator [options] -t <type> <Plugin name> \n\n";
  std::cout << "Possible plugin types are: \n";
  std::cout << "               a  : actuator\n";
  std::cout << "               s  : sensor\n";
  std::cout << "               t  : time base\n";
  std::cout << "               ct : continuous time base\n\n";
  std::cout << "\nOptions:\n";
  std::cout << "-d <templatedir> : select an alternative directory containing templates \n    (default: " << DATADIR << "/templates)\n";
  std::cout << "-m : generate the makefile only, don't touch the cpp file\n";
  std::cout << "-i <plugin_root>  : select an alternative install root directory for the plugins \n    (default: " << DATADIR << "/plugins)\n\n";
  
}




/**
  The main procedure
**/
int main(int argc, char **argv){
  
  // the default parameters:  
  string templateDir = DATADIR;
  templateDir += "/templates";

  string pluginRoot = DATADIR;
  pluginRoot += "/plugins";

  string pluginName;
  
  bool makefileOnly = false;
  int type = 0;

  string typeString = "";

  // now parse the options and parameters:
  int c;
  while ((c = getopt (argc, argv, "d:mi:t:?")) != -1)
     switch (c)
           {
           case 'd':
               templateDir = optarg;
             break;
           case 'm':
             makefileOnly = true;
             break;
           case 'i':
               pluginRoot = optarg;
             break;
           case 't':
               typeString = optarg;
	       if (typeString.compare("a") == 0) type = T_ACTUATOR;
	       if (typeString.compare("s") == 0) type = T_SENSOR;
	       if (typeString.compare("t") == 0) type = T_TIMEBASE;
	       if (typeString.compare("ct") == 0) type = T_CONTINUOUS_TIMEBASE;
               if (type == 0) {std::cout << "\n\nInvalid type selection\n\n"; return -1;}
             break;
           case '?':
             help();
             return 0;
           default:
             abort ();
           }
  // check that the number of arguments is correct and get the plugin name
  if (optind != argc -1 ) {std::cout << "No plugin name specified!\n\n"; return -1;}
  pluginName = argv[optind];

  // check if a type has been set, otherwise give an error message!
  if (type == 0) {std::cout << "No type set!\n\n";}



  // make an Actuator:
  if (type == T_ACTUATOR){
     FileModifier makeFileModifier(templateDir + "/Makefile_template");
     makeFileModifier.replace("PLUGINTYPE", "actuator");
     string shlibName = "libPlugins.Actuator." + pluginName;
     makeFileModifier.replace("scan_plugin_shlib_name", shlibName);
     makeFileModifier.replace("scan_plugin_cpp_name", pluginName);
     makeFileModifier.store("Makefile");
     if (!makefileOnly) {
     FileModifier cppModifier(templateDir + "/actuator_template.cpp");
     cppModifier.replace("ActuatorTemplate", pluginName);
     cppModifier.store(pluginName + ".cpp");}
  }

  // make a Timebase
  if (type == T_TIMEBASE){
     FileModifier makeFileModifier(templateDir + "/Makefile_template");
     makeFileModifier.replace("PLUGINTYPE", "timebase");
     string shlibName = "libPlugins.Timebase." + pluginName;
     makeFileModifier.replace("scan_plugin_shlib_name", shlibName);
     makeFileModifier.replace("scan_plugin_cpp_name", pluginName);
     makeFileModifier.store("Makefile");
     if (!makefileOnly) {
     FileModifier cppModifier(templateDir + "/timebase_template.cpp");
     cppModifier.replace("TimebaseTemplate", pluginName);
     cppModifier.store(pluginName + ".cpp");}
  }
  
  // make a Sensor
  if (type == T_SENSOR){
     FileModifier makeFileModifier(templateDir + "/Makefile_template");
     makeFileModifier.replace("PLUGINTYPE", "sensor");
     string shlibName = "libPlugins.Sensor." + pluginName;
     makeFileModifier.replace("scan_plugin_shlib_name", shlibName);
     makeFileModifier.replace("scan_plugin_cpp_name", pluginName);
     makeFileModifier.store("Makefile");
     if (!makefileOnly) {
     FileModifier cppModifier(templateDir + "/sensor_template.cpp");
     cppModifier.replace("SensorTemplate", pluginName);
     cppModifier.store(pluginName + ".cpp");}
  }

  // make a continuous time base
  if (type == T_CONTINUOUS_TIMEBASE){
     FileModifier makeFileModifier(templateDir + "/Makefile_template");
     makeFileModifier.replace("PLUGINTYPE", "continuous_timebase");
     string shlibName = "libPlugins.ContinuousTimebase." + pluginName;
     makeFileModifier.replace("scan_plugin_shlib_name", shlibName);
     makeFileModifier.replace("scan_plugin_cpp_name", pluginName);
     makeFileModifier.store("Makefile");
     if (!makefileOnly) {
     FileModifier cppModifier(templateDir + "/continuousTimebase_template.cpp");
     cppModifier.replace("ContTimebaseTemplate", pluginName);
     cppModifier.store(pluginName + ".cpp");}
  }

  return 0;
}


