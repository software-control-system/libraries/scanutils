#include <yat/utils/String.h>
#include <scansl/Util.h>
#include <scansl/actors/ContinuousTimebaseManager.h>
#include <scansl/actors/PolledContinuousTimebaseAdapter.h>
#include <scansl/ThreadExiter.h>
#include <scansl/ScanConfig.h>
#include <boost/bind.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

namespace ScanUtils
{
IContinuousTimebase::~IContinuousTimebase()
{
}

// ============================================================================
// ContTBFactory::ContTBFactory
// ============================================================================
ContTBFactory::ContTBFactory()
    : Factory( IPolledContinuousTimebaseInterfaceName )
{
}

// ============================================================================
// ContTBFactory::~ContTBFactory
// ============================================================================
ContTBFactory::~ContTBFactory()
{
}

// ============================================================================
// ContTBFactory::create_hard_timebase
// ============================================================================
IContinuousTimebasePtr ContTBFactory::create_hard_timebase( std::string device_name ) const
{
    std::string device_class = Util::instance().get_class( device_name );
    std::map< std::string,  Entry >::const_iterator it;
    it = factory_rep_.find( device_class );

    IContinuousTimebasePtr obj_ptr;
    if ( it == factory_rep_.end() )
    {
        THROW_YAT_ERROR("TIMEBASE_ERROR",
                        "Timebase not supported",
                        "Timebase::Factory::create_hard_timebase");
    }
    else
    {
        SCAN_DEBUG << "Creating " << interface_name_ << " for " << device_name << " : "
                   << "using plugin " << (*it).second.info->get_plugin_id() << ENDLOG;
        yat::IPlugInFactory* factory = (*it).second.factory;

        yat::IPlugInObject* obj;
        factory->create(obj);

        IPolledContinuousTimebase* typed_obj;
        try
        {
            typed_obj = static_cast<IPolledContinuousTimebase*>( obj );
            if (typed_obj == 0)
                throw std::bad_cast();
        }
        catch( std::bad_cast& )
        {
            THROW_YAT_ERROR("TIMEBASE_ERROR",
                            "Timebase not supported",
                            "TFactory::create_hard_timebase");
        }
        IPolledContinuousTimebasePtr typed_obj_ptr( typed_obj );
        obj_ptr.reset( new PolledContinuousTimebaseAdapter(typed_obj_ptr), ThreadExiter() );
    }
    return obj_ptr;
}

// ============================================================================
// ContinuousTimebaseManager::ContinuousTimebaseManager
// ============================================================================
ContinuousTimebaseManager::ContinuousTimebaseManager( const ScanConfig& config )
    : config_(config)
{
    SCAN_DEBUG << "Initializing continuous timebase factory..." << ENDLOG;
    boost::filesystem::path plugin_path( config.properties.plugin_root_path );
    plugin_path /= "continuous_timebase";
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
    std::string path_str = plugin_path.string();
#else
    std::string path_str = plugin_path.file_string();
#endif
    factory_.init( path_str );
    SCAN_DEBUG << "Timebase factory initialized" << ENDLOG;

    if ( config.timebases.empty() )
        THROW_YAT_ERROR("BAD_CONFIG",
                        "In HW Continuous mode, a timebase must be defined",
                        "Scheduler::Scheduler");
    this->register_hard_timebases( config.timebases );

}

// ============================================================================
// ContinuousTimebaseManager::~ContinuousTimebaseManager
// ============================================================================
ContinuousTimebaseManager::~ContinuousTimebaseManager()
{
}

// ============================================================================
// ContinuousTimebaseManager::register_hard_timebases
// ============================================================================
void ContinuousTimebaseManager::register_hard_timebases ( const std::vector<std::string>& device_names )
{
    for( std::size_t k = 0; k < device_names.size(); ++k )
    {
        try
        {
            std::string resolved_name = Util::instance().resolve_device_name( device_names[k] );
            IContinuousTimebasePtr timebase = factory_.create_hard_timebase( resolved_name );
            timebase->init( resolved_name );
            timebase->check_initial_condition();

            std::vector< std::string > actu = timebase->get_actuators();
            std::vector< std::string > actu_proxies = timebase->get_actuators_proxies();

            size_t i;
            for ( i = 0; i < actu.size(); i++)
            {
                actuators_values_[ Util::instance().complete_attr_name( actu_proxies[i] ) ] = \
                        Util::instance().make_attr_name( resolved_name, actu[i] );
            }

            timebases_.push_back(timebase);
            timebases_names_.push_back(resolved_name);

            { // SCAN-420: introduce a delay before start Ni6602 timebase
                Tango::Database db;
                // Get the device class name
                std::string class_name = db.get_class_for_device( resolved_name );
                SCAN_DEBUG << "hard_timebases device class: " << class_name << ENDLOG;


                if( class_name.find("6602") != std::string::npos ||
                    yat::StringUtil::is_equal_no_case(class_name, "pulsegeneration") ||
                    yat::StringUtil::is_equal_no_case(class_name, "pulsecounting") )
                {
                    SCAN_DEBUG << "It's a Ni6602(-like) device" << ENDLOG;
                    is_timebases_ni6602_.push_back(true);
                }
                else
                    is_timebases_ni6602_.push_back(false);
            }
        }
        catch( yat::Exception& )
        {
            //- TODO : catch it or not ?
            throw;
        }
    }
}

// ============================================================================
// ContinuousTimebaseManager::get_actuator_value_attr
// ============================================================================
std::string ContinuousTimebaseManager::get_actuator_value_attr ( std::string actuator_attr )
{
    std::map<std::string, std::string>::iterator it;
    it = actuators_values_.find( boost::to_lower_copy(actuator_attr) );
    if ( it == actuators_values_.end() )
    {
        yat::OSStream oss;
        oss << " The actuator "
            << actuator_attr
            << " is not registered in ContinuousTimebaseManager";

        THROW_YAT_ERROR("ACTUATOR_ERROR",
                        oss.str(),
                        "Manager::get_actuator_value_attr");
    }

    return (*it).second;
}

// ============================================================================
// ContinuousTimebaseManager::start
// ============================================================================
void ContinuousTimebaseManager::start( double integration_time, long nb_points )
throw( yat::Exception )
{
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
      SCAN_DEBUG << "Starting continous timebase: " << timebases_names_[i] << ENDLOG;

      {  // SCAN-420 & PROBLEM-1328
        if( is_timebases_ni6602_[i] )
        {
          if( timebases_.size() > 1 )
          {
            SCAN_DEBUG << "Inserting delay because it's a Ni6602 device: " << config_.properties.Ni6602_start_timebase_delay << "ms" << ENDLOG;
            if( config_.properties.Ni6602_start_timebase_delay > 0 )
              yat::Thread::sleep(config_.properties.Ni6602_start_timebase_delay);
          }
          else
          {
            SCAN_DEBUG << "Only one timebase (Ni6602), don't apply start delay" << ENDLOG;
          }
        }
      }

      timebases_[i]->start( integration_time, nb_points );
    }

}

// ============================================================================
// ContinuousTimebaseManager::finished_counting
// ============================================================================
bool ContinuousTimebaseManager::finished_counting( void )
{
    // counting is completed unless one of the timebase is still counting
    bool still_counting = false;

    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        still_counting = ( still_counting || timebases_[i]->is_counting() );
    }

    return !still_counting;
}

// ============================================================================
// ContinuousTimebaseManager::abort
// ============================================================================
void ContinuousTimebaseManager::abort( )
throw( yat::Exception )
{
    for( std::size_t i = 0; i < timebases_.size(); ++i )
    {
        SCAN_INFO << "Aborting count on ContinuousTimebase " << timebases_names_[i] << "..." << ENDLOG;
        timebases_[i]->abort( );
    }
}

}

