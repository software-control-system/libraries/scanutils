/*!
 * \file
 * \brief    Definition of SardanaExpChannelSensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "SardanaExpChannelSensor.h"

#include <boost/scoped_ptr.hpp>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::SardanaExpChannelSensor, ScanUtils::SardanaExpChannelSensorInfo);

namespace ScanUtils
{

const std::string PlugInID                ( "SardanaExpChannelSensor" );
const std::string VersionNumber           ( "1.0.0" );

// ============================================================================
// SardanaExpChannelSensorInfo::get_plugin_id
// ============================================================================
std::string SardanaExpChannelSensorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// SardanaExpChannelSensorInfo::get_interface_name
// ============================================================================
std::string SardanaExpChannelSensorInfo::get_interface_name() const
{
    return ISensorInterfaceName;
}

// ============================================================================
// SardanaExpChannelSensorInfo::get_version_number
// ============================================================================
std::string SardanaExpChannelSensorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// SardanaExpChannelSensorInfo::supported_classes
// ============================================================================
void SardanaExpChannelSensorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back("CTExpChannel");
    list.push_back("ZeroDExpChannel");
    list.push_back("OneDExpChannel");
    list.push_back("TwoDExpChannel");
}



// ============================================================================
// SardanaExpChannelSensor::SardanaExpChannelSensor
// ============================================================================
SardanaExpChannelSensor::SardanaExpChannelSensor()
{

}

// ============================================================================
// SardanaExpChannelSensor::~SardanaExpChannelSensor
// ============================================================================
SardanaExpChannelSensor::~SardanaExpChannelSensor()
{

}

// ============================================================================
// SardanaExpChannelSensor::init
// ============================================================================
void SardanaExpChannelSensor::init( std::string device_name , bool sync)
{

    try
    {
        proxy_ = DevProxies::instance().proxy(device_name);

    }
    catch (Tango::DevFailed& df)
    {
        throw yat4tango::TangoYATException(df);
    }

}

// ============================================================================
// SardanaExpChannelSensor::before_integration
// ============================================================================
void SardanaExpChannelSensor::before_integration( void )
{

    proxy_->command_inout( "Start" );

}

// ============================================================================
// SardanaExpChannelSensor::after_integration
// ============================================================================
void SardanaExpChannelSensor::after_integration( void )
{

    proxy_->command_inout( "Abort" );
    //- nothing to do here
}

}
