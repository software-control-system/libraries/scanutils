/*!
 * \file
 * \brief    Definition of PulseGenerationTimebase class
 * \author   Falilou THIAM - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class PulseGenerationTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PulseGenerationTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PulseGeneration");
    }
};

class PulseGenerationTimebase : public WaitingStateChangeTimebase
{

  public:
  
    PulseGenerationTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 50;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 100000;
        config_.integration_time_gain = 1.0;
        config_.start_cmd_name        = "Start";
    }
      
    void before_run()
    {
      // check that generationType is in Finite mode 
      Tango::DeviceAttribute l_generationType;
      try{
        l_generationType = proxy_->read_attribute("generationType");
      }
      catch( Tango::DevFailed& df )
      {
        yat4tango::TangoYATException e(df);
        RETHROW_YAT_ERROR(e,
          "TANGO_ERROR",
          "PulseGeneration error : generationType cannot be read",
          "PulseGenerationTimebase::before_run");
      }

      std::string mode;
      l_generationType >> mode;
      if (mode.compare("FINITE") != 0)
      {
        std::ostringstream oss;
        oss << "The device PulseGeneration : "
        << config_.dev_name
        << " is not well configured. generationType attribute must be set to FINITE.";
        THROW_DEVFAILED( "TIMEBASE_ERROR",
        oss.str().c_str(),
        "PulseGenerationTimebase::before_run" );
      }
    }
  
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PulseGenerationTimebase,
                          ScanUtils::PulseGenerationTimebaseInfo);
