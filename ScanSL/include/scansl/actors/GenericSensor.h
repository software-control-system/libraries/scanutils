#ifndef _SCANSERVER_GENERICSENSOR_H
#define _SCANSERVER_GENERICSENSOR_H

#include <scansl/ScanSL.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <boost/shared_ptr.hpp>

namespace ScanUtils
{
class SCANSL_DECL GenericSensor : public ISensor
{
public : //! structors
    GenericSensor();

    ~GenericSensor();

public : //! ISensor implementation

    virtual void init( std::string device_name,bool sync );

    virtual void check_initial_condition( );

    virtual void ensure_supported( std::string attr_name );

    virtual void set_parameter( const std::string &param_name, const string &param_value);

    virtual void before_integration( void );

    virtual void after_integration( void );

    virtual void before_run( void );

    virtual void after_run( void );

    virtual void abort( void );

private:

    std::string device_name_;

    //! helpers
    ProxyP proxy_;
    boost::shared_ptr<Tango::AttributeInfoList>  attr_info_;
};
}

#endif
