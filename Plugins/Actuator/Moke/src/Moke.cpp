/*!
 * \file
 * \brief    Definition of MokeActuator class
 * \author   FL - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>


namespace ScanUtils
{

class MokeActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "MokeActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Moke");
    }
};

class MokeActuator : public WaitingStateChangeActuator
{
public:
    MokeActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::ALARM);
        config_.polling_period_ms = 200;
        config_.abort_cmd_name    = "";
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::MokeActuator,
                          ScanUtils::MokeActuatorInfo);

