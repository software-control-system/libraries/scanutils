/*!
 * \file
 * \brief    Definition of XPSGroupActuator class
 * \author   FL - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <yat/plugin/PlugInSymbols.h>
#include <boost/lexical_cast.hpp>

namespace ScanUtils
{
  class XPSGroupActuatorInfo : public IScanPlugInInfo
  {
  public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
      return "XPSGroupActuator";
    }

    virtual std::string get_interface_name(void) const
    {
      return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
      return "1.0.0";
    }

  public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
	  list.push_back("XPSGroup");
    }
  }; 
  
  class XPSGroupActuator : public WaitingStateChangeActuator
  {
  public:
    XPSGroupActuator()
    {
      config_.move_state        = Tango::MOVING;
      config_.error_states.push_back(Tango::FAULT);
 	  config_.error_states.push_back(Tango::OFF);
      config_.error_states.push_back(Tango::UNKNOWN);
      config_.polling_period_ms = 50;
      config_.abort_cmd_name    = "Stop";
    }

    void check_initial_condition( std::string /*attr_name*/ )
    {
      //- check the state
      Tango::DevState dev_state = proxy_->state();
      if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
      {
        std::ostringstream oss;
        oss << "The device "
            << config_.dev_name
            << " must be STANDBY or ALARM to move";

        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        oss.str().c_str(),
                        "XPSGroupActuator::check_initial_condition");
      }
    }

    LimitsType get_limits( std::string attr_name )
    {
    	/// TODO factorize code with Galil
        try {
            double lower, upper;
            Tango::DeviceAttribute lowerAttr = proxy_->read_attribute(attr_name + "SoftLimitMin");
            Tango::DeviceAttribute upperAttr = proxy_->read_attribute(attr_name + "SoftLimitMax");
            lowerAttr >> lower;
            upperAttr >> upper;
            return LimitsType(
                boost::lexical_cast<std::string>(lower),
                boost::lexical_cast<std::string>(upper)
            );
        } catch (const boost::bad_lexical_cast& err) {
            THROW_YAT_ERROR("BAD_CAST",
                            err.what(),
                            "GalilActuator::get_limits");
        }
    }

    void set_limits( std::string attr_name, const LimitsType& limits )
    {
        /// TODO factorize code with Galil
        try {
            std::vector<Tango::DeviceAttribute> attrs;
            attrs.push_back(Util::instance().create_device_attribute(
                attr_name + "SoftLimitMin",
                Tango::DEV_DOUBLE,
                boost::lexical_cast<double>(limits.first)
            ));
            attrs.push_back(Util::instance().create_device_attribute(
                attr_name + "SoftLimitMax",
                Tango::DEV_DOUBLE,
                boost::lexical_cast<double>(limits.second)
            ));
            proxy_->write_attributes(attrs);
        } catch (const boost::bad_lexical_cast& err) {
            THROW_YAT_ERROR("BAD_CAST",
                            err.what(),
                            "XPSGroupActuator::set_limits");
        }
    }

    /// TODO implement is_enabled

    void set_enabled( std::string /*attr_name*/, bool enabled )
    {
        proxy_->command_inout(enabled ? "On" : "Off");
    }

    void set_position( std::string attr_name, double position )
    {
        Tango::DeviceData offset;
        offset << position;
        proxy_->command_inout(attr_name + "ComputeNewOffset", offset);
    }

    double get_speed( std::string attr_name )
    {
		string velocity_full_name = attr_name + "Velocity";
        Tango::DeviceAttribute velocity_attr = proxy_->read_attribute( velocity_full_name );
        double velocity;
        velocity_attr >> velocity;
        return velocity;
      
    }

    void set_speed( std::string attr_name, double speed )
    { 
		string velocity_full_name = attr_name + "Velocity";
        Tango::DeviceAttribute velocity_attr(velocity_full_name, speed);
        proxy_->write_attribute( velocity_attr );
    }
  };
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XPSGroupActuator, \
                          ScanUtils::XPSGroupActuatorInfo);
