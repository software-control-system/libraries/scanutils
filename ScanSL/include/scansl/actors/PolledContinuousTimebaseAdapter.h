#ifndef _SCANSERVER_POLLED_CONTINUOUS_TIMEBASE_ADAPTER_H
#define _SCANSERVER_POLLED_CONTINUOUS_TIMEBASE_ADAPTER_H

#include <scansl/interfaces/IPolledContinuousTimebase.h>
#include <scansl/actors/IContinuousTimebase.h>
#include <yat/threading/Task.h>

namespace ScanUtils
{

class SCANSL_DECL PolledContinuousTimebaseAdapter : public IContinuousTimebase,
        public yat::Task
{
public:
    PolledContinuousTimebaseAdapter( IPolledContinuousTimebasePtr polled_timebase_obj );

    virtual void   init( std::string device_name );

    virtual void   check_initial_condition( );

    virtual std::vector<std::string> get_sensors();

    virtual std::vector<std::string> get_actuators();

    virtual std::vector<std::string> get_actuators_proxies();

    virtual void   start( double integration_time, long nb_points );

    virtual bool   is_counting( void );

    virtual void   error_check( void );

    virtual void   abort( void );

protected:
    virtual void handle_message (yat::Message& msg)
    throw (yat::Exception);

private:
    void check_counting_state( void );

    std::string device_name_;

    IPolledContinuousTimebasePtr polled_timebase_obj_;

    yat::Mutex mutex_;
    bool is_counting_;

    bool has_error_;
    yat::Exception last_error_;
};

}

#endif
