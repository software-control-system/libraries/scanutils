#ifndef _SCANSERVER_SCAN_ATTR_H
#define _SCANSERVER_SCAN_ATTR_H

#include <tango.h>
#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <boost/any.hpp>
#include <yat/threading/Mutex.h>

namespace ScanUtils
{
class SCANSL_DECL DynAttrManager : public Tango::LogAdapter
{
public:
    DynAttrManager( Tango::DeviceImpl* device );

    ~DynAttrManager();

    void add( boost::shared_ptr<Tango::Attr> attr )
    throw( Tango::DevFailed );

    void read_attr_hardware( void );

private:
    Tango::DeviceImpl* device_;
    std::vector< boost::shared_ptr<Tango::Attr> > attrs_;
};

typedef boost::shared_ptr< DynAttrManager > DynAttrManagerPtr;

struct DataSetter
{
    virtual void read_attr_hardware( void ) = 0;

    virtual void set( boost::any d ) = 0;
    
    virtual void set_width( size_t width ) = 0;
    
    virtual boost::any get( void ) = 0;
};

template <typename T>
struct TangoTraits
{
};

# define DEFINE_TANGO_TRAITS( TypeID, Type ) \
    template<> struct TangoTraits<Type> \
{ \
    BOOST_STATIC_CONSTANT( int, type_id = TypeID ); \
}
DEFINE_TANGO_TRAITS( Tango::DEV_SHORT,   Tango::DevShort   );
DEFINE_TANGO_TRAITS( Tango::DEV_LONG,    Tango::DevLong    );
DEFINE_TANGO_TRAITS( Tango::DEV_DOUBLE,  Tango::DevDouble  );
DEFINE_TANGO_TRAITS( Tango::DEV_FLOAT,   Tango::DevFloat   );
DEFINE_TANGO_TRAITS( Tango::DEV_BOOLEAN, Tango::DevBoolean );
DEFINE_TANGO_TRAITS( Tango::DEV_USHORT,  Tango::DevUShort  );
DEFINE_TANGO_TRAITS( Tango::DEV_ULONG,   Tango::DevULong   );
DEFINE_TANGO_TRAITS( Tango::DEV_UCHAR,   Tango::DevUChar   );
DEFINE_TANGO_TRAITS( Tango::DEV_LONG64,  Tango::DevLong64  );
DEFINE_TANGO_TRAITS( Tango::DEV_ULONG64, Tango::DevULong64 );

template <typename T>
class SCANSL_DECL SpectrumAttr : public Tango::SpectrumAttr,
        public DataSetter
{
public:
    SpectrumAttr( std::string name, Tango::DispLevel disp, Tango::AttrWriteType type )
        : Tango::SpectrumAttr( name.c_str(), TangoTraits<T>::type_id, type, LONG_MAX, disp ),
          width(0)
    {
        no_value_for_spectrum = new T[1];
        ::memset(no_value_for_spectrum, 0, sizeof(T));
    }

    SpectrumAttr( std::string name, Tango::DispLevel disp, Tango::AttrWriteType type, const Tango::UserDefaultAttrProp &prop )
        : Tango::SpectrumAttr( name.c_str(), TangoTraits<T>::type_id, type, LONG_MAX, disp ),
          width(0)
    {
        this->set_default_properties( const_cast<Tango::UserDefaultAttrProp &>(prop) );
        no_value_for_spectrum = new T[1];
        ::memset(no_value_for_spectrum, 0, sizeof(T));
    }

    ~SpectrumAttr()
    {
        delete [] no_value_for_spectrum;
    }
    
public: //- [Tango::Attr impl]

    virtual void read(Tango::DeviceImpl *, Tango::Attribute &att)
    {
        if ( this->data_for_read_attr )
            att.set_value( this->data_for_read_attr->data(), this->width, 0 );
        else
            att.set_value( this->no_value_for_spectrum, 1, 0);
    }

    virtual void write(Tango::DeviceImpl *, Tango::WAttribute &)
    {
    }

    virtual bool is_allowed (Tango::DeviceImpl *, Tango::AttReqType )
    {
        return true;
    }

public: //- [DataSetter impl]
    virtual void read_attr_hardware( void )
    {
        yat::MutexLock guard( mutex );
        data_for_read_attr = data;
    }

    virtual void set( boost::any d )
    {
        yat::MutexLock guard( mutex );
        data = boost::any_cast< typename ScanArray<T>::ImgP >(d);
    }

    virtual void set_width( size_t width )
    {
        this->width = width;
    }

    virtual boost::any get( )
    {
        return data;
    }

private:
    typedef typename ScanArray<T>::ImgP ImgP;
    yat::Mutex mutex;
    ImgP data_for_read_attr;
    ImgP data;
    size_t width;
    T *no_value_for_spectrum;
};


template <typename T>
class SCANSL_DECL ImageAttr : public Tango::ImageAttr,
        public DataSetter
{
public:
    ImageAttr( std::string name, Tango::DispLevel disp, Tango::AttrWriteType type )
        : Tango::ImageAttr( name.c_str(), TangoTraits<T>::type_id, type, LONG_MAX, LONG_MAX, disp )
    {
        no_value_for_image = new T[1];
        ::memset(no_value_for_image, 0, sizeof(T));
    }

    ImageAttr( std::string name, Tango::DispLevel disp, Tango::AttrWriteType type, const Tango::UserDefaultAttrProp &prop  )
        : Tango::ImageAttr( name.c_str(), TangoTraits<T>::type_id, type, LONG_MAX, LONG_MAX, disp )
    {
        this->set_default_properties( const_cast<Tango::UserDefaultAttrProp &>(prop) );
        no_value_for_image = new T[1];
        ::memset(no_value_for_image, 0, sizeof(T));
    }

    ~ImageAttr()
    {
        delete [] no_value_for_image;
    }
    
public: //- [Tango::Attr impl]

    virtual void read(Tango::DeviceImpl *, Tango::Attribute &att)
    {
        if ( this->data_for_read_attr )
            att.set_value( this->data_for_read_attr->data(),
                           this->data_for_read_attr->shape()[1],
                    this->data_for_read_attr->shape()[0] );
        else
            att.set_value( this->no_value_for_image, 1, 1);
    }

    virtual void write(Tango::DeviceImpl *, Tango::WAttribute &)
    {
    }

    virtual bool is_allowed (Tango::DeviceImpl *, Tango::AttReqType )
    {
        return true;
    }

public: //- [DataSetter impl]
    virtual void read_attr_hardware( void )
    {
        yat::MutexLock guard( mutex );
        data_for_read_attr = data;
    }

    virtual void set( boost::any d )
    {
        yat::MutexLock guard( mutex );
        data = boost::any_cast< typename ScanArray<T>::ImgP >(d);
    }

    virtual void set_width( size_t )
    {
    }

    virtual boost::any get( )
    {
        return data;
    }

private:
    typedef typename ScanArray<T>::ImgP ImgP;
    yat::Mutex mutex;
    ImgP data_for_read_attr;
    ImgP data;
    T *no_value_for_image;
};

}

#endif
