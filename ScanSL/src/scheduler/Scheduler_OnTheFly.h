#ifndef _SCANSERVER_SCHEDULER_ONTHEFLY_H
#define _SCANSERVER_SCHEDULER_ONTHEFLY_H

#include <scansl/scheduler/Scheduler.h>

namespace ScanUtils
{

class Scheduler_OnTheFly : public Scheduler
{
public: //! structors
    Scheduler_OnTheFly( const ScanConfig& config );

protected:

    virtual bool is_specific_action( int action );

    virtual SchedulerState handle_specific_action( void );

private:
    void read_actuators( int group_id );
    void save_actuator_values_1d( void );
    void read_actuators_1d_after_integration( void );

    enum
    {
        SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE = Scheduler::ActionType_MAX_,
        SpecificAction_MOVE_ACTUATOR_END_OF_LINE,
        SpecificAction_END_OF_ACTUATOR_MOVE,
        SpecificAction_READ_ACTUATORS_BEFORE_INTEGRATION,
        SpecificAction_READ_ACTUATORS_AFTER_INTEGRATION,
        SpecificAction_END_OF_DIM
    };

    int last_move_;
    AttrValues actuators_values_before_integration_;

};

}

#endif
