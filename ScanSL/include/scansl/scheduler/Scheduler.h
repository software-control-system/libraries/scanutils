#ifndef _SCANSERVER_SCHEDULER_H
#define _SCANSERVER_SCHEDULER_H

#include <scansl/ScanSL.h>
#include <scansl/ScanConfig.h>
#include <scansl/ScanData.h>
#include <scansl/ScanManager.h>
#include <scansl/logging/ILogReceiver.h>
#include <boost/noncopyable.hpp>

namespace ScanUtils
{

struct SCANSL_DECL SchedulerState
{
    SchedulerState() : must_pause(false), end_of_step(false), end_of_scan(false), end_of_run(false) {};
    bool must_pause;
    bool end_of_step;
    bool end_of_scan;
    bool end_of_run;
};

struct SCANSL_DECL ScanPosition
{
    ScanPosition(int _x = -1, int _y = -1) : x(_x), y(_y) {};
    int x;
    int y;
};

inline bool operator == (const ScanPosition& lhs, const ScanPosition& rhs)
{
    return lhs.x == rhs.x && lhs.y == rhs.y;
}

inline bool operator != (const ScanPosition& lhs, const ScanPosition& rhs)
{
    return lhs.x != rhs.x || lhs.y != rhs.y;
}

inline std::ostream& operator << (std::ostream& os, const ScanPosition& p)
{
    return os << " [" << p.x << " , " << p.y << " ]";
}



class SCANSL_DECL Scheduler : private boost::noncopyable
{
protected: //! protected structors [this is a base class]
    Scheduler( const ScanConfig& );

    virtual ~Scheduler();

public: //! modifiers

    SchedulerState execute_action( void );

    //- can be called from the ScanTask when aborting a scan
    void abort_actuators( void );

    //- can be called from the ScanTask when aborting a scan
    void abort_sensors( void );

    //- can be called from the ScanTask when aborting a scan
    void abort_scan_on_error( void );

    void execute_afterrun_action( const AfterRunActionDesc& action_desc );

public: //! queries

    //! get attributes list
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_actuators2_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_sensors_attributes();
    std::vector< boost::shared_ptr<Tango::Attr> > get_timestamps_attributes();

    ScanBuffers get_buffers();

    ScanData get_data();

    void hard_abort_datacollector( void );

protected:

    virtual bool is_specific_action( int action ) = 0;

    virtual SchedulerState handle_specific_action( void ) = 0;

    SchedulerState handle_standard_action( void );

    typedef enum
    {
        ActionType_NONE,
        ActionType_ABORT_ACTUATORS,
        ActionType_SAVE_ACTUATORS_PRIOR_POS,
        ActionType_INIT_RUN,
        ActionType_INIT_RUN_SYNCDATA,
        ActionType_PRERUN_HOOK,
        ActionType_INIT_SCAN,
        ActionType_INIT_SCAN_SYNCDATA,
        SpecificAction_SCAN_FIRST_ACTION,
        ActionType_PRESCAN_HOOK,
        ActionType_INIT_STEP,
        ActionType_PRESTEP_HOOK,
        ActionType_MOVE_ACTUATORS,
        ActionType_MOVE_ACTUATORS1,
        ActionType_MOVE_ACTUATORS2,
        ActionType_WAIT_ACTUATORS,
        ActionType_WAIT_ACTUATORS1,
        ActionType_WAIT_ACTUATORS2,
        ActionType_WAIT_ACTUATORS_DELAY,
        ActionType_POST_ACTUATOR_MOVE_HOOK,
        ActionType_SENSORS_PRE_INTEGRATION,
        ActionType_START_TIMEBASES,
        ActionType_WAIT_TIMEBASES,
        ActionType_WAIT_TIMEBASES_DELAY,
        ActionType_SENSORS_POST_INTEGRATION,
        ActionType_POST_INTEGRATION_HOOK,
        ActionType_READ_SENSORS,
        ActionType_POST_STEP_HOOK,
        ActionType_END_OF_STEP,
        ActionType_POST_SCAN_HOOK,
        ActionType_END_SCAN_RECORDING,
        ActionType_END_SCAN_RECORDING_SYNC,
        ActionType_END_OF_SCAN,
        ActionType_POST_RUN_HOOK,
        ActionType_END_RUN_RECORDING,
        ActionType_END_RUN_RECORDING_SYNC,
        ActionType_AFTER_RUN_ACTION,
        ActionType_WAIT_ACTUATORS_AFTER_ACTION,
        ActionType_END_OF_RUN,
        ActionType_MAX_
    } ActionType;

    void save_actuator_prior_position( void );

    void init_run( void );

    void init_scan( void );

    void init_step( void );

    void move_actuators( void );
    void move_actuators1( void );
    void move_actuators2( void );

    bool wait_actuators( int next_action_on_success,
                         int next_action_on_failure,
                         int waiting_action);

    bool continuous_timebases_idle( void );

    void get_after_move_pos( bool save_values );

    void wait_actuators_delay( int next_action_when_delay_elapsed );

    void sensor_pre_integration( void );

    void start_timebases( int next_action_on_success );

    bool wait_timebases( int next_action_on_success,
                         int next_action_on_failure );

    void wait_timebases_delay( void );

    void timebase_after_run( void );

    void sensor_post_integration( void );

    void sensor_after_run( void );

    void post_step_hook( void );

    void end_of_step( void );

    void after_run_action( const AfterRunActionDesc& action_desc );

    bool read_sensors( int next_action_on_success );

    bool handle_hook( HookType hook_type,
                      int hook_action,
                      int next_action_on_success );

    void sync_with_datacollector( int next_action_on_success );

    bool more_step( void );

    void compute_next_actuator_pos( void );

    void set_scan_speed( void );

    void restore_speed( void );

protected:
    const ScanConfig& config_;
    ScanManager manager_;
    int next_action_;
    int prev_action_;
    size_t scan_to_do_;

    yat::Mutex data_mutex_;
    ScanData data_;

    //- actuator position
    ScanPosition prestep_pos_;
    ScanPosition step_pos_;
    ScanPosition last_pos_;

    //- current point number (0-based)
    size_t dim1_pt_;
    size_t dim2_pt_;

    double integration_time_;

    ProgressManager progress_manager_;

    bool afterrun_action_ondemand_;




    struct ErrorManager
    {
        ErrorManager( const ErrorManagement& e )
            : err_mngt(e),
              retry_left( e.retry_count )
        {}

        void reinit()
        {
            retry_left = err_mngt.retry_count;
        }

        void consume()
        {
            retry_left--;
        }

        bool more_try()
        {
            return retry_left > 0;
        }

        const ErrorManagement& err_mngt;
        long retry_left;
    };

    ErrorManager actuators_retries;
    ErrorManager timebases_retries;
    ErrorManager sensors_retries;
    ErrorManager hooks_retries;

private:
    void move_all_actuators( ScanPosition pos );
    void move_dim1_actuators( ScanPosition pos );
    void move_dim2_actuators( ScanPosition pos );
};

typedef boost::shared_ptr<Scheduler> SchedulerPtr;

}

#endif
