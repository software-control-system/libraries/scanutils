/*!
 * \file
 * \brief    Definition of FastAttenuationSensor class
 * \author   Boris de Laage de Meux - Synchrotron SOLEIL
 */

#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/plugin/Sensor.h>
#include <scansl/DevProxies.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class FastAttenuationSensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "FastAttenuationSensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("FastAttenuation");
    }
};

class FastAttenuatorControlSensor : public Sensor
{

public: //! ISensor implementation

    virtual void init( std::string device_name, bool sync )
    {
        try
        {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
	    
            running_mode_=0;
            Tango::DeviceAttribute running_mode_attr = proxy_->read_attribute("runningMode");
            running_mode_attr >> running_mode_;

        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }

    virtual void before_integration(void)
    {
        // In step by step mode, just turn the attenuator off.
        try
        {
            proxy_->command_inout("Off");
        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }


    virtual void after_integration( void )
    {
        if( running_mode_ == 2 )
            // Step by step mode.
        {
            try
            {
                proxy_->command_inout("On");
            }
            catch (Tango::DevFailed& df)
            {
                throw yat4tango::TangoYATException(df);
            }
        }
    }

private: //! private implementation
    //! configuration
    Tango::DevLong running_mode_;
};


class FastAttenuatorAcquisitionSensor : public Sensor
{

public: //! ISensor implementation

    FastAttenuatorAcquisitionSensor(MParameters *parameter):Sensor(),parent_parameter_ (parameter)
    {
    }

    virtual void init( std::string device_name, bool sync )
    {
        try
        {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
        }
        catch (Tango::DevFailed& df)
        {
            throw yat4tango::TangoYATException(df);
        }
    }

    virtual void before_integration(void)
    {

        try
        {
            Tango::DeviceAttribute da;

            stopAndResetAcquisition();

            // Set IntegrationTime on FastAtt
	        std::string it_str  = parent_parameter_->parameter_value("IntegrationTime");
	        double integration_time = std::atof(it_str.c_str());

            da.set_name( "integrationTime" );
            da << static_cast<Tango::DevFloat>(integration_time);
            proxy_->write_attribute( da );

            // Set IntegrationTime on FastAtt
            it_str  = parent_parameter_->parameter_value("hw_continuous_nbpt");
            long hw_continuous_nbpt = std::atol(it_str.c_str());

            da.set_name( "totalNbPoint" );
            da << static_cast<Tango::DevLong>(hw_continuous_nbpt);
            proxy_->write_attribute( da );

            // Set sampling clock as external (NI6602 is clocking)
            da.set_name( "acqSamplingSource" );
            da << static_cast<Tango::DevLong>( 1 );
            proxy_->write_attribute( da );

            // Start acquisition
            da.set_name ( "acqCmdSampling" );
            da << static_cast<Tango::DevLong>( 1 );
            proxy_->write_attribute( da );

            //- check the state
            Tango::DevState dev_state = proxy_->state();
            if ( dev_state != Tango::RUNNING )
            {
                std::ostringstream oss;
                oss << "The device " << device_name_
                    << " did not start acquisition during Scan startup";

                THROW_YAT_ERROR("SOFTWARE_FAILURE",
                                oss.str().c_str(),
                                "FastAttenuatorAcquisition::before_integration");
            }

        }
        catch ( yat::Exception& e )
        {
            throw e;
        }
        catch ( Tango::DevFailed& df )
        {
            throw yat4tango::TangoYATException(df);
        }
    }


    virtual void after_integration( void )
    {
        stopAndResetAcquisition();
    }

    virtual void abort( void )
    {
        // Restore FastAttenuator state
        after_integration();
    }

    // ----------------------------------
    //
    // For HCS only
    //
    // ----------------------------------

    virtual void stopAndResetAcquisition(void)
    {
        try
        {
            Tango::DeviceAttribute da;

            // Force acquisition to be stopped
            da.set_name( "acqCmdSampling" );
            da << static_cast<Tango::DevLong>( 0 );
            proxy_->write_attribute( da );

            // Clear acquisition buffers
            da.set_name( "resetError" );
            da << static_cast<Tango::DevLong>( 1 );
            proxy_->write_attribute( da );

            yat::ThreadingUtilities::sleep( 0, 200000000 );
        }
        catch ( yat::Exception& e )
        {
            throw e;
        }
        catch ( Tango::DevFailed& df )
        {
            throw yat4tango::TangoYATException(df);
        }
    }

private: //! private implementation
    //! configuration
    MParameters *parent_parameter_;
};


class FastAttenuationSensor : public Sensor
{
public:
    virtual void init( std::string device_name, bool sync )
    {
        Tango::DbData prop_data;
        std::string remote_object;

 	//--------------------------------------------------
        //- Get the device proxy and device_name storage
        Sensor::init( device_name, sync );
	
        prop_data.push_back( Tango::DbDatum("ObjectType") );
        proxy_->unsafe_proxy().get_property( prop_data );

        // checking if 'Buffered' is false
        if ( prop_data[0].is_empty() )
        {
            std::ostringstream oss;
            oss << "Object Type must be specified as a device property for device " << device_name;
            THROW_DEVFAILED( "SENSOR_ERROR",
                             oss.str().c_str(),
                             "FastAttenuationSensor::init()" );
        }

        prop_data[0] >> remote_object;
        if ( remote_object == "FastAttenuatorAcquisition")
        {
            impl.reset( new FastAttenuatorAcquisitionSensor(this) );
        }
        else if ( remote_object == "FastAttenuatorControl")
        {
            impl.reset( new FastAttenuatorControlSensor() );
        }
        else
        {
            std::ostringstream oss;
            oss << "Unknown ObjectType value in the device property of " << device_name;
            THROW_DEVFAILED( "SENSOR_ERROR",
                             oss.str().c_str(),
                             "FastAttenuationSensor::init()" );
        }

        if (impl)
            impl->init( device_name, sync);
    }

    virtual void ensure_supported( std::string attr_name)
    {
        if(impl)
            impl->ensure_supported(attr_name);
    }

    virtual void before_integration( void )
    {
        if(impl) 
            impl->before_integration();
    }

    virtual void after_integration( void )
    {
        if(impl)
            impl->after_integration();
    }

    virtual void before_run ( void )
    {
        if(impl)
            impl->before_run();
    }

    virtual void after_run( void )
    {
        if(impl)
            impl->after_run();
    }

    virtual void abort( void )
    {
        if(impl)
            impl->abort();
    }

private:
    ISensorPtr impl;

};


}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::FastAttenuationSensor,
                          ScanUtils::FastAttenuationSensorInfo);
