#ifndef _SCANSERVER_SCHEDULER_HWCONTINUOUS_H
#define _SCANSERVER_SCHEDULER_HWCONTINUOUS_H

#include <scansl/scheduler/Scheduler.h>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace ScanUtils
{
namespace tm = boost::posix_time;

class Scheduler_HWContinuous : public Scheduler
{
public: //! structors
    Scheduler_HWContinuous( const ScanConfig& config );

protected:

    virtual bool is_specific_action( int action );

    virtual SchedulerState handle_specific_action( void );

private:
    enum
    {
        SpecificAction_MOVE_ACTUATOR_BEGIN_OF_LINE = Scheduler::ActionType_MAX_,
        SpecificAction_MOVE_ACTUATOR_NEXT_1D_POS,
        SpecificAction_END_OF_LINE,
        SpecificAction_END_OF_ACTUATOR_MOVE
    };

    int last_move_;
    tm::ptime last_sensor_reading_;


};

}

#endif
