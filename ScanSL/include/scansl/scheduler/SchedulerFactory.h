#ifndef _SCANSERVER_SCHEDULER_FACTORY_H
#define _SCANSERVER_SCHEDULER_FACTORY_H

#include <scansl/ScanSL.h>
#include <scansl/Singleton.h>
#include <scansl/scheduler/Scheduler.h>

namespace ScanUtils
{

class SCANSL_DECL SchedulerFactory : public Singleton<SchedulerFactory>
{
public:
    SchedulerPtr create( const ScanConfig& );
};

}

#endif
