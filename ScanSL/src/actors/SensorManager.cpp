﻿#include <sstream>
#include <scansl/Util.h>
#include <scansl/actors/SensorManager.h>
#include <scansl/plugin/MParameters.h>
#include <boost/scoped_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <yat4tango/ExceptionHelper.h>
#include <yat/utils/String.h>

namespace ScanUtils
{
// ============================================================================
// SensorManager::SensorManager
// ============================================================================
SensorManager::SensorManager( const ScanConfig& config, ContinuousTimebaseManagerPtr cont_tb_manager )
    : config_( config ),
      factory_( ISensorInterfaceName )
{
    SCAN_DEBUG << "Initializing sensor factory..." << ENDLOG;
    SCAN_DEBUG << "SensorManager::SensorManager config.properties.plugin_root_path: " << config.properties.plugin_root_path << ENDLOG;
    boost::filesystem::path plugin_path( config.properties.plugin_root_path );
    plugin_path /= "sensor";
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
    std::string path_str = plugin_path.string();
#else
    std::string path_str = plugin_path.file_string();
#endif
    factory_.init( path_str );
    SCAN_DEBUG << "Sensor factory initialized" << ENDLOG;

    state_.error_flag_ = false;

    std::vector< std::string > sensors_to_register = config.sensors;
    //## Mettre en commentaire ce bloc
    if (config_.hw_continuous)
    {
        //- add the actuators values as sensors so as they get read
        //- with the same asynchronous call as the 'real' sensors

        for( std::size_t i = 0; i < config_.actuators.size(); ++i )
        {
            std::string resolved_actuator1d = Util::instance().complete_attr_name( config_.actuators[i] );
            boost::to_lower( resolved_actuator1d );
            std::string actuator1d_value = cont_tb_manager->get_actuator_value_attr( resolved_actuator1d );
            sensors_to_register.push_back( actuator1d_value );
        }
    }
    this->register_sensors( sensors_to_register );
}

// ============================================================================
// SensorManager::~SensorManager
// ============================================================================
SensorManager::~SensorManager()
{
}

// ============================================================================
// SensorManager::register_sensors
// ============================================================================
void SensorManager::register_sensors ( const std::vector<std::string>& attr_names )
{
    //- a multimap containing attr names (w/o device name), indexed by the device names
    typedef std::multimap< std::string, std::string > StrMMap;
    typedef StrMMap::const_iterator StrMMapCIt;
    StrMMap organized_attr;
    std::vector<std::pair<std::string, std::string> > resolved_attr_names;
    std::vector<std::string> full_attr_names;

    std::map<std::string, ISensorPtr> sensors_objects;

    {
        //- What is done here:
        //-
        //- 1. resolve the attribute names (get the device name and and the attr names)
        //-    this phase is necessary to resolve devices and attributes aliases
        //-
        //- 2. build 'resolved_attr_names' and 'full_attr_names' so that they contain
        //-    attr names sorted in the same order than the 'attr_names' input argument
        //-
        //- 3. if the sensor object corresponding to a device name is not instantiated,
        //-    instantiate it and put it in the 'sensors_' list
        //-
        //- 4. for each attribute, call the corresponding ISensor::ensure_supported method
        //-
        for( std::size_t i = 0; i < attr_names.size(); ++i )
        {
            std::pair<std::string, std::string> attr_resolution = Util::instance().resolve_attr_name( attr_names[i] );
            resolved_attr_names.push_back( attr_resolution );
            full_attr_names.push_back( Util::instance().make_attr_name(attr_resolution.first,attr_resolution.second) );

            organized_attr.insert( attr_resolution );
            if ( sensors_objects.find( attr_resolution.first ) == sensors_objects.end() )
            {
                ISensorPtr sensor_ptr( factory_.create( attr_resolution.first ) );

                if( config_.hw_continuous )
                {
                    std::ostringstream oss;
                    oss << config_.hw_continuous_nbpt;
                    // Insert parameter : Has it is used in init , it must be set before
                    MParameters* sensor_param_p = dynamic_cast<MParameters*>(sensor_ptr.get());
                    if( sensor_param_p )
                        sensor_param_p->set_parameter("hw_continuous_nbpt", oss.str());
                }
                else
                {
                    int total_nb_points = 1;
                    if( config_.actuators2.empty() )
                    {   //- 1d or timescan
                        total_nb_points = config_.integration_times.num_elements();
                    }
                    else
                    {   //- 2d scan
                        total_nb_points = config_.integration_times.num_elements() * config_.trajectories2.shape()[1];
                    }
                    std::ostringstream oss;
                    oss << total_nb_points;
                    // Insert parameter : Has it is used in init , it must be set before
                    MParameters* sensor_param_p = dynamic_cast<MParameters*>(sensor_ptr.get());
                    if( sensor_param_p )
                        sensor_param_p->set_parameter("sbs_nbpt", oss.str());
                }
                sensor_ptr->init( attr_resolution.first, config_.properties.synchronize_data );
                sensor_ptr->check_initial_condition();
                sensor_ptr->before_run();
                sensors_objects.insert( std::make_pair(attr_resolution.first, sensor_ptr) );
            }
            sensors_objects[attr_resolution.first]->ensure_supported( attr_resolution.second );
        }
    }

    {
        //- iterate on all elements of the multimap to build the group
        //- they are sorted by device name
        StrMMapCIt it  = organized_attr.begin();
        StrMMapCIt end = organized_attr.end();

        while ( it != end )
        {
            std::string device_name = (*it).first;
            //- find the element corresponding to the next device
            //- -> all element between 'it' and 'next_device' correspond to the same device
            //- they will be put in the same subgroup
            StrMMapCIt next_device = organized_attr.upper_bound( device_name );

            SubGroup sub_group;
            sub_group.device_name = device_name;
            sub_group.enable = true;
            sub_group.sensor_object = sensors_objects[device_name];
            sub_group.proxy = DevProxies::instance().proxy(device_name);
            //## PROBLEM-1961 <
            sub_group.proxy->set_timeout_millis(config_.sensors_mngt.timeout_s * 1000);
            //## PROBLEM-1961 >
            for (; it != next_device; ++it)
            {
                sub_group.attributes.push_back( (*it).second );
                sub_group.attributes_info.push_back( sub_group.proxy->attribute_query((*it).second) );
            }
            sensors_group_.push_back( sub_group );
        }
    }
}

// ============================================================================
// SensorManager::get_sensors_names
// ============================================================================
std::set<std::string> SensorManager::get_sensors_names()
{
    std::set<std::string> names;
    for(std::vector<SubGroup>::const_iterator it1 = sensors_group_.begin(), end1 = sensors_group_.end(); it1 != end1 ; ++it1)
        for(std::vector<std::string>::const_iterator it2 = it1->attributes.begin(), end2 = it1->attributes.end() ; it2 != end2 ; ++it2)
            names.insert(Util::instance().make_attr_name(it1->device_name, *it2));
    return names;
}

// ============================================================================
// SensorManager::get_sensors_nb
// ============================================================================
size_t SensorManager::get_sensors_nb( void )
{
    size_t sensors_nb = 0;
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        sensors_nb += sensors_group_[i].attributes.size();
    }
    return sensors_nb;
}

// ============================================================================
// SensorManager::before_integration
// ============================================================================
void SensorManager::before_integration( double integration_time )
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        std::ostringstream oss;
        oss << integration_time;
        MParameters* sensor_param_p = dynamic_cast<MParameters*>(sensors_group_[i].sensor_object.get());
        if( sensor_param_p )
            sensor_param_p->set_parameter("IntegrationTime", oss.str());
        sensors_group_[i].sensor_object->before_integration();
    }
}

// ============================================================================
// SensorManager::after_integration
// ============================================================================
void SensorManager::after_integration( void )
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        sensors_group_[i].sensor_object->after_integration();
    }
}

// ============================================================================
// SensorManager::after_run
// ============================================================================
void SensorManager::after_run( void )
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        sensors_group_[i].sensor_object->after_run();
    }

    if( state_.all_errors_.size() > 0 )
    {
        for( std::size_t i = 0; i < state_.all_errors_.size(); ++i )
        {
            SCAN_ERROR << state_.all_errors_[i].desc << ENDLOG;
        }
        if( !config_.properties.no_throw_on_sensor_error )
            throw yat::Exception("ERROR", "Sensors errors occured during the scan", "SensorManager::after_run");
        else
            SCAN_ERROR << "!!!Sensors errors occured during the scan!!!" << ENDLOG;
    }
}

// ============================================================================
// SensorManager::abort
// ============================================================================
void SensorManager::abort( void )
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        SCAN_INFO << "Aborting acquisition on Sensor " << sensors_group_[i].device_name << "..." << ENDLOG;
        try
        {
            sensors_group_[i].sensor_object->abort();
        }
        catch( Tango::DevFailed& df )
        {
            _TANGO_TO_YAT_EXCEPTION(df, ex);
            for( std::size_t j = 0; j < ex.errors.size(); ++j )
            {
                SCAN_ERROR << ex.errors[j].desc << ENDLOG;
            }

            SCAN_ERROR << "An error occured while aborting acquisition on sensor " << sensors_group_[i].device_name << "..." << ENDLOG;
        }
        catch( yat::Exception& ex )
        {
            for( std::size_t j = 0; j < ex.errors.size(); ++j )
            {
                SCAN_ERROR << ex.errors[j].desc << ENDLOG;
            }
            SCAN_ERROR << "An error occured while aborting acquisition on sensor " << sensors_group_[i].device_name << "..." << ENDLOG;
        }
        catch( ... )
        {
            SCAN_ERROR << "An unknown error occured while aborting acquisition on sensor " << sensors_group_[i].device_name << "..." << ENDLOG;
        }
    }
}

// ============================================================================
// SensorManager::read
// ============================================================================
void SensorManager::read()
{
    if( config_.properties.read_attribute_async )
    {
        SCAN_DEBUG << "read_attribute_async is ON" << ENDLOG;
        read_by_attr();
        return;
    }

    if ( state_.pending_request_ == true )
    {
        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        "The last read request has not finished yet",
                        "Sensor::SensorManager::read");
    }

    AttrValues empty_values;
    state_.values_.swap( empty_values );
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        if( (state_.error_flag_ && !sensors_group_[i].data_read && sensors_group_[i].enable) ||
            (!state_.error_flag_ && sensors_group_[i].enable) )
        {
            SCAN_DEBUG << "Sensors to read: " << sensors_group_[i].device_name << ENDLOG;
            state_.sensors_to_read_.insert( sensors_group_[i].device_name );
            sensors_group_[i].data_read = false;
        }
    }

    state_.errors_.clear();
    state_.error_flag_ = false;
    state_.pending_request_ = true;

    yat::Exception ex;
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        if( !sensors_group_[i].enable || sensors_group_[i].data_read )
            // ignore disable device
            continue;

        SCAN_DEBUG << "Reading sensor device " << sensors_group_[i].device_name << ENDLOG;

        try
        {
            sensors_group_[i].proxy->read_attributes_asynch( sensors_group_[i].attributes, *this );
        }
        catch( Tango::DevFailed& df )
        {
            SCAN_ERROR << "read_attributes failed" << ENDLOG;
            state_.pending_request_ = false;
            _TANGO_TO_YAT_EXCEPTION( df, e );
            // Concat errors
            ex.errors.insert( ex.errors.begin(), e.errors.begin(), e.errors.end());
            // disable this sensor device until the end of the scan
            // PROBLEM-1312
            // {
            // sensors_group_[i].enable = false;
            // }
            state_.error_flag_ = true;
            state_.sensors_to_read_.erase( sensors_group_[i].device_name );

            // Do not throw until all asynchronous read command are invoked
        }
    }

    // Finaly throw if errors occured
    if( ex.errors.size() > 0 )
        throw ex;
}

// ============================================================================
// SensorManager::read_by_attr
// ============================================================================
void SensorManager::read_by_attr()
{
    SCAN_DEBUG << "entering read_by_attr" << ENDLOG;
    if ( state_.pending_request_ == true )
    {
        THROW_YAT_ERROR("SOFTWARE_FAILURE",
                        "The last read request has not finished yet",
                        "Sensor::SensorManager::read_by_attr");
    }

    AttrValues empty_values;
    state_.values_.swap( empty_values );
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        SubGroup& group = sensors_group_[i];
        std::string device_name = group.device_name;
        yat::StringUtil::to_lower( &device_name );

        for( std::size_t j = 0; j < group.attributes.size(); ++j )
        {
            std::string attr_name = group.attributes[j];
            yat::StringUtil::to_lower(&attr_name);
            std::string full_sensor_name = Util::instance().make_attr_name(device_name, attr_name);
            SCAN_DEBUG << "Sensor: " << full_sensor_name << ENDLOG;

            if( group.attributes_read.find(attr_name) == group.attributes_read.end() )
                group.attributes_read[attr_name] = false;

            if( group.attributes_enable.find(attr_name) == group.attributes_enable.end() )
                group.attributes_enable[attr_name] = true;

            if(  ( state_.error_flag_ && !group.attributes_read[attr_name] && group.attributes_enable[attr_name] )
              || ( !state_.error_flag_ && group.attributes_enable[attr_name] ) )
            {
                    SCAN_DEBUG << "Sensors to read: " << full_sensor_name << ENDLOG;

                    state_.sensors_to_read_.insert( full_sensor_name );
                    group.attributes_read[attr_name] = false;
            }
        }
    }

    state_.errors_.clear();
    state_.error_flag_ = false;
    state_.pending_request_ = true;

    yat::Exception ex;
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        SubGroup& group = sensors_group_[i];
        std::string device_name = group.device_name;
        yat::StringUtil::to_lower( &device_name );

        for( std::size_t j = 0; j < group.attributes.size(); ++j )
        {
            std::string attr_name = group.attributes[j];
            yat::StringUtil::to_lower(&attr_name);

            std::string full_sensor_name = Util::instance().make_attr_name(device_name, attr_name);

            if( !group.attributes_enable[attr_name] || group.attributes_read[attr_name] )
                // ignore disable or already read attribute
                continue;

            try
            {
                SCAN_DEBUG << "Reading sensor " << full_sensor_name << ENDLOG;
                group.proxy->read_attribute_asynch( attr_name.c_str(), *this );
            }
            catch( Tango::DevFailed& df )
            {
                SCAN_ERROR << "read_attribute_async failed" << ENDLOG;
                state_.pending_request_ = false;
                _TANGO_TO_YAT_EXCEPTION( df, e );
                // Concat errors
                ex.errors.insert( ex.errors.begin(), e.errors.begin(), e.errors.end());
                // disable this sensor device until the end of the scan

                // PROBLEM-1312
                // {
                // group.attributes_enable[attr_name] = false;
                // }
                state_.error_flag_ = true;
                state_.sensors_to_read_.erase( full_sensor_name );

                // Do not throw until all asynchronous read command are invoked
            }
        }
    }

    // Finaly throw if errors occured
    if( ex.errors.size() > 0 )
        throw ex;
}

// ============================================================================
// SensorManager::mark_as_read
// ============================================================================
void SensorManager::mark_as_read(const std::string& device_name)
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        if( sensors_group_[i].device_name == device_name )
        {
            SCAN_INFO << "Data read for device " << sensors_group_[i].device_name << ENDLOG;
            sensors_group_[i].data_read = true;
            break;
        }
    }
}

// ============================================================================
// SensorManager::mark_attr_as_read
// ============================================================================
void SensorManager::mark_attr_as_read(const std::string& device_name, const std::string& attr_name)
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        SubGroup& group = sensors_group_[i];
        if( yat::StringUtil::is_equal_no_case(group.device_name, device_name) )
        {
            for( std::size_t j = 0; j < group.attributes.size(); ++j )
            {
                if( yat::StringUtil::is_equal_no_case(attr_name, group.attributes[j]) )
                {
                    std::string full_sensor_name = Util::instance().make_attr_name(device_name, attr_name);
                    SCAN_INFO << "Data read for sensor " << full_sensor_name << ENDLOG;
                    group.attributes_read[attr_name] = true;
                    break;
                }
            }
        }
    }
}

// ============================================================================
// SensorManager::disable_failed_sensors
// ============================================================================
void SensorManager::disable_failed_sensors(int x_pos, int y_pos)
{
    if( !config_.properties.keep_sensor_on_failure )
    {
        if( config_.properties.read_attribute_async )
        {
            for( std::size_t i = 0; i < sensors_group_.size(); ++i )
            {
                SubGroup& group = sensors_group_[i];
                std::string device_name = group.device_name;
                yat::StringUtil::to_lower( &device_name );

                for( std::size_t j = 0; j < group.attributes.size(); ++j )
                {
                    if( !group.attributes_read[group.attributes[j]] )
                    {
                        std::string attr_name = group.attributes[j];
                        yat::StringUtil::to_lower(&attr_name);

                        if( group.attributes_enable[attr_name] )
                        {
                            std::string full_sensor_name = Util::instance().make_attr_name(device_name, attr_name);
                            SCAN_INFO << "Sensor " << full_sensor_name << " ignored up to the end of this scan because of fatal access errors" << ENDLOG;
                            std::ostringstream oss;
                            oss << "Sensor " << full_sensor_name << " was disabled during scan (pos: " << x_pos << ", " << y_pos << ") because of non recoverable access errors";
                            state_.all_errors_.push_back( yat::Error("ERROR", oss.str(), "SensorManager::disable_failed_sensors") );
                            group.attributes_enable[attr_name] = false;
                        }
                    }
                }
            }

        }
        else
        {
            for( std::size_t i = 0; i < sensors_group_.size(); ++i )
            {
                if( !sensors_group_[i].data_read && sensors_group_[i].enable )
                {
                    SCAN_INFO << "Sensor device " << sensors_group_[i].device_name << " ignored up to the end of this scan because of fatal access errors" << ENDLOG;
                    std::ostringstream oss;
                    oss << "Sensor device " << sensors_group_[i].device_name << " was disabled during scan (pos: " << x_pos << ", " << y_pos << ") because of non recoverable access errors";
                    state_.all_errors_.push_back( yat::Error("ERROR", oss.str(), "SensorManager::disable_failed_sensors") );
                    sensors_group_[i].enable = false;
                }
            }
        }
    }
}

// ============================================================================
// SensorManager::clear_errors
// ============================================================================
void SensorManager::clear_errors()
{
    // keep information about errors
    if( state_.errors_.size() > 0 )
        state_.all_errors_.insert( state_.all_errors_.end(), state_.errors_.begin(), state_.errors_.end() );

    state_.errors_.clear();
}

// ============================================================================
// SensorManager::attr_read
// ============================================================================
void SensorManager::attr_read( Tango::AttrReadEvent* attr_read_data )
{
    if( config_.properties.read_attribute_async )
    {
        single_attr_read( attr_read_data );
        return;
    }

    yat::MutexLock lock(state_.mutex_);

    std::string device_name;
    device_name = attr_read_data->device->dev_name();
    yat::StringUtil::to_lower( &device_name );
    try
    {
        if ( state_.pending_request_ == false )
            return;

        state_.sensors_to_read_.erase( device_name );

        SCAN_DEBUG << "Read values from " << device_name << std::endl;
        SCAN_DEBUG << state_.sensors_to_read_.size() << " sensor(s) left" << ENDLOG;

        if( attr_read_data->err )
        {
            SCAN_ERROR << "Sensor error on " << device_name << " (see the following messages)" << ENDLOG;
            for( std::size_t i = 0; i < attr_read_data->errors.length(); ++i )
            {
                SCAN_ERROR << attr_read_data->errors[i].desc << ENDLOG;
            }
            throw yat::Exception("READ_ERROR", std::string("Read error on sensor device ") + device_name + std::string(": ") + std::string(attr_read_data->errors[0].desc), "SensorManager::attr_read");
        }
        else
        {
            //- store the data
            boost::scoped_ptr< std::vector<Tango::DeviceAttribute> > argout(attr_read_data->argout);
            for( std::size_t i = 0; i < argout->size(); ++i )
            {
                Tango::DeviceAttribute& da = (*argout)[i];

                if( config_.properties.check_attribute_quality && Tango::ATTR_INVALID == da.quality )
                {
                    // pas bon
                    std::ostringstream oss;
                    oss << "Invalid quality on " << device_name << " (see the following messages)";
                    SCAN_ERROR << oss.str() << ENDLOG;
                    throw yat::Exception("READ_ERROR", oss.str(), "SensorManager::attr_read");
                }

                std::string attr_name = boost::to_lower_copy( da.get_name() );
                std::string full_attr_name = Util::instance().make_attr_name(device_name, attr_name);
                const Tango::AttributeInfoEx& info = this->get_attr_info( device_name, attr_name );

                state_.values_.push_back( AttrValue(full_attr_name, Util::instance().extract_data(da, info)) );
                SCAN_DEBUG << "Sensor read : " << full_attr_name << ENDLOG;
            }
            mark_as_read(device_name);
        }
    }
    catch( Tango::DevFailed& df )
    {
        SCAN_ERROR << "Catched Tango::DevFailed exception while reading sensor" << ENDLOG;
        yat4tango::TangoYATException e( df );
        e.dump();
        state_.errors_.insert(state_.errors_.end(), e.errors.begin(), e.errors.end());
        state_.error_flag_ = true;
    }
    catch( yat::Exception& e )
    {
        SCAN_ERROR << "Catched yat::Exception exception while reading sensor" << ENDLOG;
        e.dump();
        state_.errors_.insert(state_.errors_.end(), e.errors.begin(), e.errors.end());
        state_.error_flag_ = true;
    }
    catch( std::exception& e )
    {
        SCAN_ERROR << "Catched std::exception exception while reading sensor" << ENDLOG;
        SCAN_ERROR << e.what() << ENDLOG;
        state_.errors_.push_back( yat::Error("READ_ERROR", e.what(), "SensorManager::attr_read"));
        state_.error_flag_ = true;
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown error while receiving read_attributes_async response" << ENDLOG;
        state_.errors_.push_back( yat::Error("READ_ERROR", "Unknown error while receiving read_attributes_async response", "SensorManager::attr_read"));
        state_.error_flag_ = true;
    }
}

// ============================================================================
// SensorManager::single_attr_read
// ============================================================================
void SensorManager::single_attr_read( Tango::AttrReadEvent* attr_read_data )
{
    yat::MutexLock lock(state_.mutex_);

    std::string device_name;
    device_name = attr_read_data->device->dev_name();
    std::string attr_name = attr_read_data->attr_names[0];

    yat::StringUtil::to_lower( &device_name );
    yat::StringUtil::to_lower( &attr_name );
    std::string full_sensor_name = Util::instance().make_attr_name(device_name, attr_name);

    try
    {
        if ( state_.pending_request_ == false )
            return;

        state_.sensors_to_read_.erase( full_sensor_name );

        SCAN_DEBUG << "Read values from " << full_sensor_name << std::endl;
        SCAN_DEBUG << state_.sensors_to_read_.size() << " sensor(s) left" << ENDLOG;

        if( attr_read_data->err )
        {
            SCAN_ERROR << "Sensor error on " << full_sensor_name << " (see the following messages)" << ENDLOG;
            for( std::size_t i = 0; i < attr_read_data->errors.length(); ++i )
            {
                SCAN_ERROR << attr_read_data->errors[i].desc << ENDLOG;
            }
            throw yat::Exception("READ_ERROR", std::string("Read error on sensor ") + full_sensor_name + std::string(": ") + std::string(attr_read_data->errors[0].desc), "SensorManager::attr_read");
        }
        else
        {

            //- store the data
            boost::scoped_ptr< std::vector<Tango::DeviceAttribute> > argout(attr_read_data->argout);
            Tango::DeviceAttribute& da = (*argout)[0];

            if( config_.properties.check_attribute_quality && Tango::ATTR_INVALID == da.quality )
            {
                // pas bon
                std::ostringstream oss;
                oss << "Invalid quality on " << full_sensor_name << " (see the following messages)";
                SCAN_ERROR << oss.str() << ENDLOG;
                throw yat::Exception("READ_ERROR", oss.str(), "SensorManager::attr_read");
            }

            const Tango::AttributeInfoEx& info = this->get_attr_info( device_name, attr_name );
            state_.values_.push_back( AttrValue(full_sensor_name, Util::instance().extract_data(da,info)) );
            SCAN_DEBUG << "Sensor read : " << full_sensor_name << ENDLOG;
            mark_attr_as_read(device_name, attr_name);
        }
    }
    catch( Tango::DevFailed& df )
    {
        SCAN_ERROR << "Catched Tango::DevFailed exception while reading sensor" << ENDLOG;
        yat4tango::TangoYATException e( df );
        e.dump();
        state_.errors_.insert(state_.errors_.end(), e.errors.begin(), e.errors.end());
        state_.error_flag_ = true;
    }
    catch( yat::Exception& e )
    {
        SCAN_ERROR << "Catched yat::Exception exception while reading sensor" << ENDLOG;
        e.dump();
        state_.errors_.insert(state_.errors_.end(), e.errors.begin(), e.errors.end());
        state_.error_flag_ = true;
    }
    catch( std::exception& e )
    {
        SCAN_ERROR << "Catched std::exception exception while reading sensor" << ENDLOG;
        SCAN_ERROR << e.what() << ENDLOG;
        state_.errors_.push_back( yat::Error("READ_ERROR", e.what(), "SensorManager::attr_read"));
        state_.error_flag_ = true;
    }
    catch( ... )
    {
        SCAN_ERROR << "Unknown error while receiving read_attributes_async response" << ENDLOG;
        state_.errors_.push_back( yat::Error("READ_ERROR", "Unknown error while receiving read_attributes_async response", "SensorManager::attr_read"));
        state_.error_flag_ = true;
    }
}

// ============================================================================
// SensorManager::wait_end
// ============================================================================
AttrValues SensorManager::wait_end( size_t timeout_ms )
{
    if ( state_.pending_request_ == false )
        return AttrValues();

    yat::Timestamp start_time;
    _GET_TIME(start_time);

    const unsigned long sleep_ns = 500 * 1000; // = 500 µs
    const double timeout_ms_d = static_cast<double>(timeout_ms);

    while( ! this->read_request_completed() )
    {
        yat::ThreadingUtilities::sleep(0, sleep_ns);

        //- check timeout
        yat::Timestamp current_time;
        _GET_TIME(current_time);
        if ( _ELAPSED_MSEC(start_time, current_time) > timeout_ms_d )
        {
            state_.errors_.push_back( yat::Error("ERROR", "Timeout expired while reading sensors", "SensorManager::wait_end") );
            break;
        }
    }

    state_.pending_request_ = false;
    return state_.values_;
}

// ============================================================================
// SensorManager::read_request_completed
// ============================================================================
bool SensorManager::read_request_completed( void )
{
    yat::MutexLock lock(state_.mutex_);
    if (state_.pending_request_ == false)
        return true;
    else
        return state_.sensors_to_read_.empty();
}

// ============================================================================
// SensorManager::get_attr_info
// ============================================================================
const Tango::AttributeInfoEx& SensorManager::get_attr_info( std::string device_name, std::string attr_name ) const
{
    for( std::size_t i = 0; i < sensors_group_.size(); ++i )
    {
        if (sensors_group_[i].device_name == device_name)
        {
            for (size_t j = 0; j < sensors_group_[i].attributes.size(); j++)
            {
                if (sensors_group_[i].attributes[j] == attr_name)
                {
                    return sensors_group_[i].attributes_info[j];
                }
            }
        }
    }

    std::ostringstream oss;
    oss << "Attribute infos for attribute "
        << device_name
        << "/"
        << attr_name
        << " have not been initialized properly";

    THROW_YAT_ERROR( "INTERNAL_ERROR",
                     oss.str().c_str(),
                     "SensorManager::get_attr_info" );
}
}

