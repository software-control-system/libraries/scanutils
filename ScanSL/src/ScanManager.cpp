#include <scansl/ScanSL.h>
#include <scansl/ThreadExiter.h>
#include <scansl/ScanManager.h>

namespace ScanUtils
{
ScanManager::ScanManager( const ScanConfig& config )
    : actuator_manager( new ActuatorManager(config) ),
      timebase_manager( config.hw_continuous ? 0 : new TimebaseManager(config) ),
      continuous_timebase_manager( !config.hw_continuous ? 0 : new ContinuousTimebaseManager(config) ),
      sensor_manager  ( new SensorManager(config, continuous_timebase_manager) ),
      hooks_manager   ( new HooksManager(config) ),
      data_collector  ( new DataCollector(config, continuous_timebase_manager), ThreadExiter() ),
      after_run_action( new AfterRunAction(config) )
{
}

}

