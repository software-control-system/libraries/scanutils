/*!
 * \file
 * \brief    Declaration of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */
#ifndef _SCANSERVER_MPARAMETERS_H_
#define _SCANSERVER_MPARAMETERS_H_

#include <scansl/interfaces/ISensor.h>

namespace ScanUtils
{

//==============================================================================
// class MParameters
// Mixin that adds parameters access methods to plugins
//==============================================================================
class SCANSL_DECL MParameters
{
public: //! structors

    virtual ~MParameters() {}

    //! \brief return true if the parameter exists
    bool has_parameter(const std::string &param_name);

    //! \brief returns the parameter's value
    //! \throw yat::Exception if the parameter is not found
    std::string parameter_value(const std::string &param_name);

    //! \brief set a parameter value
    void set_parameter( const std::string &name, const string &value);

protected:

    //! protected c-tor
    MParameters() {}

private:
    std::map<std::string, std::string> parameter_map_;
    std::string name_marshalling( const std::string &_name);
    yat::Mutex  mutex_;
};

}

#endif
