/*!
 * \file
 * \brief    Definition of OpticsActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <yat/plugin/PlugInSymbols.h>


namespace ScanUtils
{

class OpticsActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "OpticsActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("GenericMirror");
        list.push_back("MechanicalGenericBender");
        list.push_back("PiezoGenericBender");
        list.push_back("TraitPointPlan");

        list.push_back("MonochromatorCassiopee");
        list.push_back("MonochromatorCristal");
        list.push_back("MonochromatorDesir");
        list.push_back("MonochromatorDiffabs");
        list.push_back("MonochromatorDisco");
        list.push_back("MonochromatorOde");
        list.push_back("MonochromatorProxima");
        list.push_back("MonochromatorSamba");
        list.push_back("MonochromatorDCMSamba");
        list.push_back("MonochromatorSwing");
        list.push_back("MonochromatorTempo");
        list.push_back("MonochromatorMars1");
        list.push_back("LuciaMonochromator");
        list.push_back("MonochromatorMetrologie");
        list.push_back("MonochromatorDCMMetrologie");
        list.push_back("MonochromatorDCMSixs");
        list.push_back("MonochromatorDeimos");
        list.push_back("MonochromatorMicroFocus");
        list.push_back("MonochromatorDCMNanoscopium");
        list.push_back("MonochromatorDCMPsiche");
        list.push_back("MonochromatorDCMSirius");
        list.push_back("MonochromatorHermes");
        list.push_back("MonochromatorDCM");
        list.push_back("MonochromatorGrating");
        // For GALAXIES : The RIXS is similar to a Monochromator
        list.push_back("RIXS"); //for GALAXIES BL


        list.push_back("BeamEnergy"); //for Lucia Beam Line
        list.push_back("BeamLineEnergyTempo1");
        list.push_back("BeamLineEnergyProxima1");
        list.push_back("BeamLineEnergySwing");
        list.push_back("BeamLineEnergyCristal");
        list.push_back("BeamLineEnergyMars");
        list.push_back("BeamLineEnergyPleiades");
        list.push_back("BeamLineEnergyAntares");
        list.push_back("BeamLineEnergyDeimos");
        list.push_back("BeamLineEnergyCassiopee");
        list.push_back("BeamLineEnergyGeneric");
        list.push_back("BeamLineEnergyHermes");

        list.push_back("XUVOrderSorter"); //for Metrologie BL

        list.push_back("HexapodNewport");
        list.push_back("HexapodFMBOExpert");
        list.push_back("HexapodFMBOUser");
        list.push_back("HexapodPI");		// SAMBA

        // Controleurs d'axes "exotiques"
        //	  list.push_back("UHRAxis");			// ANTARES -> Mantis 22663
        list.push_back("PIE712Axis");			// GALAXIES Mono HRM
		list.push_back("PI861");			// NANOPROBE
        list.push_back("EnsembleAxis");		// SEXTANTS

        list.push_back("MotorsKBDiffabs");

        list.push_back("SPITest"); //- For test on SPI controller (For YMA)
    }
};

class OpticsActuator : public WaitingStateChangeActuator
{
public:
    OpticsActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";
    }
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::OpticsActuator,
                          ScanUtils::OpticsActuatorInfo);

