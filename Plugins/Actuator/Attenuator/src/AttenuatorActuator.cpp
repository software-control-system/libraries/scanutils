/*!
 * \file
 * \brief    Definition of AttenuatorActuator class
 * \author   Xavier Elattaoui - Synchrotron SOLEIL
 */

#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class AttenuatorActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "AttenuatorActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Attenuator");
    }
};

class AttenuatorActuator : public WaitingStateChangeActuator
{
public:
    AttenuatorActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::ALARM);
        config_.polling_period_ms = 100;
        config_.abort_cmd_name    = ""; //pas de stop
        config_.timeout_ms        = 3000;
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::AttenuatorActuator, ScanUtils::AttenuatorActuatorInfo);
