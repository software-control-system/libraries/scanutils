#include <scansl/Util.h>
#include <scansl/DevProxies.h>
#include <scansl/ScanAttr.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/algorithm/string.hpp>
#include <yat4tango/ExceptionHelper.h>


namespace ScanUtils
{
void post_msg( yat::Task& t, size_t msg_id, size_t timeout_ms, bool wait )
{
    yat::Message* msg = 0;
    try
    {
        msg = yat::Message::allocate(msg_id, DEFAULT_MSG_PRIORITY, wait);
    }
    catch( yat::Exception& ex )
    {
        RETHROW_YAT_ERROR(ex,
                          "OUT_OF_MEMORY",
                          "Out of memory",
                          "ScanTask::post_msg");
    }

    try
    {
        if (wait)
            t.wait_msg_handled( msg, timeout_ms );
        else
            t.post( msg, timeout_ms );
    }
    catch( yat::Exception& ex )
    {
        ex.dump();
        SCAN_ERROR << "Exception while posting a message (id: " << msg_id << ")" << ENDLOG;
        t.msgq_statistics().dump();
        RETHROW_YAT_ERROR(ex,
                          "SOFTWARE_FAILURE",
                          "Unable to post a msg",
                          "ScanTask::post_msg");
    }
}

// ============================================================================
// Util::resolve_device_name
// ============================================================================
std::string Util::resolve_device_name( std::string device_name )
{
    try
    {
        return boost::to_lower_copy( DevProxies::instance().proxy(device_name)->dev_name() );
    }
    catch ( const Tango::DevFailed & df )
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to resolve the device name : " << device_name;
        std::string error = oss.str();

        SCAN_ERROR << error << " ( " << df.errors[0].desc << " )" << ENDLOG;

        RETHROW_YAT_ERROR(e,
                          "TANGO_ERROR",
                          error,
                          "Util::resolve_device_name");
    }
}

// ============================================================================
// Util::resolve_attr_name
// ============================================================================
std::pair<std::string, std::string> Util::resolve_attr_name( std::string attr_name )
{
    try
    {
        Tango::AttributeProxy attr_proxy(attr_name);
        std::pair<std::string, std::string> ret( attr_proxy.get_device_proxy()->dev_name(),
                                                 attr_proxy.name() );
        boost::to_lower( ret.first );
        boost::to_lower( ret.second );
        return ret;
    }
    catch ( const Tango::DevFailed & df )
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to resolve the attribute name : " << attr_name;
        std::string error = oss.str();
        SCAN_ERROR << error << " ( " << df.errors[0].desc << " )" << ENDLOG;

        RETHROW_YAT_ERROR(e,
                          "TANGO_ERROR",
                          error,
                          "Util::resolve_attr_name");
    }
}

// ============================================================================
// Util::complete_attr_name
// ============================================================================
std::string Util::complete_attr_name( std::string attr_name )
{
    std::pair<std::string, std::string> resolved = Util::resolve_attr_name(attr_name);
    return Util::make_attr_name( resolved.first, resolved.second );
}

// ============================================================================
// Util::make_attr_name
// ============================================================================
std::string Util::make_attr_name( std::string device_name, std::string attr_name )
{
    return device_name + DEVICE_SEP + attr_name;
}

// ============================================================================
// Util::get_class
// ============================================================================
std::string Util::get_class( std::string device_name )
{
    try
    {
        return DevProxies::instance().proxy(device_name)->unsafe_proxy().info().dev_class;
    }
    catch ( const Tango::DevFailed & df )
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to get the class name of " << device_name;
        std::string error = oss.str();

        SCAN_ERROR << error << " ( " << df.errors[0].desc << " )" << ENDLOG;
        RETHROW_YAT_ERROR(e,
                          "TANGO_ERROR",
                          error,
                          "Util::get_class");
    }

}

template <typename T>
struct internal_buffer
{
    typedef typename ScanArray<T>::DevVarArray_var DevVarArray_var;
    DevVarArray_var& operator() ( Tango::DeviceAttribute& dev_attr )
    {}
};

#define DEFINE_INTERNAL_BUFFER_FUNC( T, BUFFER ) \
    template <> \
    struct internal_buffer<T> \
{ \
    typedef ScanArray<T>::DevVarArray_var DevVarArray_var; \
    DevVarArray_var& operator() ( Tango::DeviceAttribute& dev_attr ) \
{ return dev_attr.BUFFER; } \
};

DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevShort, ShortSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevLong, LongSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevDouble, DoubleSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevFloat, FloatSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevBoolean, BooleanSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevUShort, UShortSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevUChar, UCharSeq )
#if (TANGO_VERSION_MAJOR >= 9)
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong, ULongSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevLong64, Long64Seq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong64, ULong64Seq )
#elif (TANGO_VERSION_MAJOR == 8)
// Tango8
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong, get_ULong_data() )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevLong64, get_Long64_data() )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong64, get_ULong64_data() )
#else
//Tango7
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong, ext->ULongSeq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevLong64, ext->Long64Seq )
DEFINE_INTERNAL_BUFFER_FUNC( Tango::DevULong64, ext->ULong64Seq )
#endif

template <typename T>
struct ScalarExtractor
{
    bool operator()( Tango::DeviceAttribute& dev_attr, int data_type, boost::any& result )
    {
        if ( data_type == TangoTraits<T>::type_id )
        {
            T value;
            dev_attr >> value;
            result = value;
            return true;
        }
        else
            return false;
    }
};

template <typename T>
struct SpectrumExtractor
{
    bool operator()( Tango::DeviceAttribute& dev_attr, int data_type, boost::any& result )
    {
        if( 0 == dev_attr.dim_x )
            throw yat::Exception("ERROR", "Null size detected (dim_x) for sensor image!!!", "ImageExtractor::()");
        
        if ( data_type == TangoTraits<T>::type_id )
        {
            typename ScanArray<T>::ImgP buf( new typename ScanArray<T>::Img(boost::extents[1][dev_attr.dim_x]) );
            std::copy( internal_buffer<T>()(dev_attr)->get_buffer(),
                       internal_buffer<T>()(dev_attr)->get_buffer() + dev_attr.dim_x,
                       buf->data() );
            result = buf;
            return true;
        }
        else
            return false;
    }
};

template <typename T>
struct ImageExtractor
{
    bool operator()( Tango::DeviceAttribute& dev_attr, int data_type, boost::any& result )
    {
        if( 0 == dev_attr.dim_y )
            throw yat::Exception("ERROR", "Null size detected (dim_y) for sensor image!!!", "ImageExtractor::()");
        
        if( 0 == dev_attr.dim_x )
            throw yat::Exception("ERROR", "Null size detected (dim_x) for sensor image!!!", "ImageExtractor::()");
        
        if ( data_type == TangoTraits<T>::type_id )
        {
            typename ScanArray<T>::ImgP buf( new typename ScanArray<T>::Img(boost::extents[dev_attr.dim_y][dev_attr.dim_x]) );
            std::copy( internal_buffer<T>()(dev_attr)->get_buffer(),
                       internal_buffer<T>()(dev_attr)->get_buffer() + dev_attr.dim_x * dev_attr.dim_y,
                       buf->data() );
            result = buf;
            return true;
        }
        else
            return false;
    }
};

template <typename T>
struct DevAttrExtractor
{
    bool operator()( Tango::DeviceAttribute& dev_attr, int data_type, int data_format, boost::any& result )
    {
        if ( data_format == Tango::SCALAR )
        {
            return ScalarExtractor<T>()( dev_attr, data_type, result );
        }
        else if ( data_format == Tango::SPECTRUM )
        {
            return SpectrumExtractor<T>()( dev_attr, data_type, result );
        }
        else
        {
            return ImageExtractor<T>()( dev_attr, data_type, result );
        }
    }
};

template <typename T>
struct DevAttrDblExtractor
{
    bool operator()( Tango::DeviceAttribute& dev_attr, int data_type, double& result )
    {
        if ( data_type == TangoTraits<T>::type_id )
        {
            T value;
            dev_attr >> value;
            result = static_cast<double>(value);
            return true;
        }
        else
            return false;
    }
};


// ============================================================================
// Util::extract_data
// ============================================================================
boost::any Util::extract_data( const Tango::DeviceAttribute& dev_attr_in, const Tango::AttributeInfoEx& info  )
{
    Tango::DeviceAttribute& dev_attr = const_cast<Tango::DeviceAttribute&>( dev_attr_in );
    boost::any result;
    try
    {
        int data_type = dev_attr.get_type();
        int data_format = info.data_format;

        if      ( DevAttrExtractor<Tango::DevShort  >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevLong   >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevDouble >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevFloat  >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevBoolean>()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevUShort >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevULong  >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevUChar  >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevLong64 >()( dev_attr, data_type, data_format, result ) ) {}
        else if ( DevAttrExtractor<Tango::DevULong64>()( dev_attr, data_type, data_format, result ) ) {}
    }
    catch(Tango::DevFailed& df)
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to extract the value from the DeviceAttribute " << dev_attr.name;
        RETHROW_YAT_ERROR(e, "TANGO_ERROR", oss.str(), "Util::extract_data");
    }
    catch(yat::Exception& e)
    {
        yat::OSStream oss;
        oss << "Unable to extract the value from the DeviceAttribute " << dev_attr.name;
        RETHROW_YAT_ERROR(e, "ERROR", oss.str(), "Util::extract_data");
    }
    catch(const std::exception& e)
    {
        yat::OSStream oss;
        oss << "Catched system exception while extracting the value from the DeviceAttribute: " << dev_attr.name;
        oss << " : " << e.what();
        throw yat::Exception("UNKNOWN_ERROR", oss.str(), "Util::extract_data");
    }
    catch(...)
    {
        yat::OSStream oss;
        oss << "Catched unknown exception while extracting the value from the DeviceAttribute " << dev_attr.name;
        throw yat::Exception("UNKNOWN_ERROR", oss.str(), "Util::extract_data");
    }
    return result;
}


// ============================================================================
// Util::extract_scalar_as_double
// ============================================================================
double Util::extract_scalar_as_double( const Tango::DeviceAttribute& dev_attr_in, const Tango::AttributeInfoEx& info  )
{
    Tango::DeviceAttribute& dev_attr = const_cast<Tango::DeviceAttribute&>( dev_attr_in );
    double result;
    try
    {
        int data_type = info.data_type;

        if      ( DevAttrDblExtractor<Tango::DevShort  >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevLong   >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevDouble >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevFloat  >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevBoolean>()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevUShort >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevULong  >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevUChar  >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevLong64 >()( dev_attr, data_type, result ) ) {}
        else if ( DevAttrDblExtractor<Tango::DevULong64>()( dev_attr, data_type, result ) ) {}
    }
    catch(Tango::DevFailed& df)
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to extract the value from the DeviceAttribute " << dev_attr.name;
        RETHROW_YAT_ERROR(e, "TANGO_ERROR", oss.str(), "Util::extract_data");
    }
    catch(yat::Exception& e)
    {
        yat::OSStream oss;
        oss << "Unable to extract the value from the DeviceAttribute " << dev_attr.name;
        RETHROW_YAT_ERROR(e, "ERROR", oss.str(), "Util::extract_data");
    }
    catch(const std::exception& e)
    {
        yat::OSStream oss;
        oss << "Catched system exception while extracting the value from the DeviceAttribute: " << dev_attr.name;
        oss << " : " << e.what();
        throw yat::Exception("UNKNOWN_ERROR", oss.str(), "Util::extract_data");
    }
    catch(...)
    {
        yat::OSStream oss;
        oss << "Catched unknown exception while extracting the value from the DeviceAttribute " << dev_attr.name;
        throw yat::Exception("UNKNOWN_ERROR", oss.str(), "Util::extract_data");
    }
    return result;
}



Tango::DeviceAttribute Util::create_device_attribute(const Tango::AttributeInfo& attr_info, const double& value)
{
    return Util::instance().create_device_attribute(attr_info.name, attr_info.data_type, value);
};

Tango::DeviceAttribute Util::create_device_attribute(std::string name, int data_type, const double& value)
{
    Tango::DeviceAttribute dev_attr;
    dev_attr.name = name;
    try
    {
        switch (data_type)
        {
        case Tango::DEV_BOOLEAN:
            dev_attr << (value > 0);
            break;
        case Tango::DEV_UCHAR:
            dev_attr << static_cast<Tango::DevUChar>(value);
            break;
        case Tango::DEV_SHORT:
            dev_attr << static_cast<Tango::DevShort>(value);
            break;
        case Tango::DEV_USHORT:
            dev_attr << static_cast<Tango::DevUShort>(value);
            break;
        case Tango::DEV_LONG:
            dev_attr << static_cast<Tango::DevLong>(value);
            break;
        case Tango::DEV_ULONG:
            dev_attr << static_cast<Tango::DevULong>(value);
            break;
        case Tango::DEV_LONG64:
            dev_attr << static_cast<Tango::DevLong64>(value);
            break;
        case Tango::DEV_ULONG64:
            dev_attr << static_cast<Tango::DevULong64>(value);
            break;
        case Tango::DEV_FLOAT:
            dev_attr << static_cast<Tango::DevFloat>(value);
            break;
        case Tango::DEV_DOUBLE:
            dev_attr << static_cast<Tango::DevDouble>(value);
            break;
        default:
            break;
        }
    }
    catch( Tango::DevFailed& df)
    {
        yat4tango::TangoYATException e(df);
        yat::OSStream oss;
        oss << "Unable to create a DeviceAttribute object for " << name << " with value " << value;
        std::string error = oss.str();
        SCAN_ERROR << error << " ( " << df.errors[0].desc << " )" << ENDLOG;

        RETHROW_YAT_ERROR(e,
                          "TANGO_ERROR",
                          oss.str(),
                          "Util::create_device_attribute");
    }

    return dev_attr;
}

}

