/*!
 * \file
 * \brief    Definition of XiaDxpContinuousTimebase class
 * \author   Arafat NOUREDDINE - Synchrotron SOLEIL
 * Jira 
 */

#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeContinuousTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class XiaDxpContinuousTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "XiaDxpContinuousTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledContinuousTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("XiaDxp");
    }
};


class XiaDxpContinuousTimebase : public WaitingStateChangeContinuousTimebase
{
public: //! structors

    XiaDxpContinuousTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        //config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1E3;
        config_.nbpoint_attr          = "nbPixels";
        config_.start_cmd_name        = "Snap";
    }

public: //! ITimebase implementation

    void init( std::string device_name )
    {
        this->WaitingStateChangeContinuousTimebase::init( device_name );
        device_name_=device_name;

        Tango::DevState state = proxy_->state();
        if (state != Tango::STANDBY)
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " should be STANDBY";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "XiaDxpContinuousTimebase::init" );
        }        
     }

    void check_initial_condition( )
    {
    	WaitingStateChangeContinuousTimebase::check_file_generation_condition("fileGeneration");
    }

     void start( double integration_time, long nb_point )
     {

            std::vector<Tango::DeviceAttribute> attrs;
            attrs.push_back( Tango::DeviceAttribute(config_.nbpoint_attr, (Tango::DevLong) nb_point) );
            proxy_->write_attributes( attrs );
            proxy_->command_inout( config_.start_cmd_name );
    }

    std::vector<std::string> get_sensors()
    {
        return std::vector<std::string>();
    }

    std::vector<std::string> get_actuators()
    {
        return std::vector<std::string>();
    }

    std::vector<std::string> get_actuators_proxies()
    {
        return std::vector<std::string>();
    }

private :
    std::string device_name_;

};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XiaDxpContinuousTimebase,
                          ScanUtils::XiaDxpContinuousTimebaseInfo);

