#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/actors/DataCollector.h>
#include <yat/threading/Utilities.h>
#include <yat/utils/XString.h>
#include <yat/file/FileName.h>
#include <yat4tango/ExceptionHelper.h>

#ifdef SCANSL_ENABLE_NEXUS
#  include <scansl/actors/nexus4tango.h>
#endif

namespace ScanUtils
{

#define HANDLE_NEXUSCPP_ERROR( Statement )                              \
    try                                                                   \
{                                                                     \
    Statement ;                                                         \
}                                                                     \
    catch( nxcpp::NexusException& ex )                                           \
{                                                                     \
    throw ex;                                \
}                                                                     \
    catch( ... )                                                          \
{                                                                     \
    std::ostringstream oss;                                             \
    oss << __FILE__ << "[" << __LINE__ << "]";                          \
    throw yat::Exception( "NEXUS_ERROR",                                \
    "Unknown error during Nexus recording",       \
    oss.str().c_str() );                          \
}

#define HANDLE_NEXUS4TANGO_ERROR( Statement )                           \
    try                                                                   \
{                                                                     \
    Statement ;                                                         \
}                                                                     \
    catch( DataRecorderWriteLock::Exception& ex )                         \
{                                                                     \
    std::ostringstream oss;                                             \
    oss << __FILE__ << "[" << __LINE__ << "]";                          \
    throw yat::Exception( "NEXUS4TANGO_ERROR",                          \
    ex.what(),                                    \
    oss.str().c_str() );                          \
}                                                                     \
    catch( Tango::DevFailed& ex )                                         \
{                                                                     \
    throw yat4tango::TangoYATException( ex );                           \
}                                                                     \
    catch( ... )                                                          \
{                                                                     \
    std::ostringstream oss;                                             \
    oss << __FILE__ << "[" << __LINE__ << "]";                          \
    throw yat::Exception( "NEXUS4TANGO_ERROR",                          \
    "Unknown error during Nexus recording",       \
    oss.str().c_str() );                          \
}


#define IGNORE_ERROR( Statement, Message ) \
    try                                      \
{                                        \
    Statement ;                          \
}                                        \
    catch( ... )                             \
{                                        \
    SCAN_ERROR << Message << ENDLOG;       \
}





#ifdef SCANSL_ENABLE_NEXUS
//- Nexus to Tango type
template <typename T>
struct NexusTraits
{
};


# define DEFINE_NEXUS_TRAITS( Type, TypeID ) \
    template<> struct NexusTraits<Type> \
{ \
    BOOST_STATIC_CONSTANT( nxcpp::NexusDataType, type_id = TypeID ); \
}
DEFINE_NEXUS_TRAITS( Tango::DevShort  , nxcpp::NX_INT16 );
DEFINE_NEXUS_TRAITS( Tango::DevLong   , nxcpp::NX_INT32 );
DEFINE_NEXUS_TRAITS( Tango::DevLong64 , nxcpp::NX_INT64 );
DEFINE_NEXUS_TRAITS( Tango::DevDouble , nxcpp::NX_FLOAT64 );
DEFINE_NEXUS_TRAITS( Tango::DevFloat  , nxcpp::NX_FLOAT32 );
DEFINE_NEXUS_TRAITS( Tango::DevBoolean, nxcpp::NX_UINT8 );
DEFINE_NEXUS_TRAITS( Tango::DevUChar  , nxcpp::NX_UINT8 );
DEFINE_NEXUS_TRAITS( Tango::DevUShort , nxcpp::NX_UINT16 );
DEFINE_NEXUS_TRAITS( Tango::DevULong  , nxcpp::NX_UINT32 );
DEFINE_NEXUS_TRAITS( Tango::DevULong64, nxcpp::NX_UINT64 );

template <typename T>
struct NexusDataSetCreator
{
    typedef typename ScanArray<T>::ImgP imagep_t;

    bool operator()( DataCollector::DataInfo& data_info,
                     DataRecorder::NexusFileP file,
                     const ScanConfig& config )
    {
        if (data_info.attr_info.data_type == TangoTraits<T>::type_id)
        {
            std::vector<size_t> shape(2);
            boost::any buffer_any = data_info.datasetter->get();
            imagep_t& buffer = boost::any_cast<imagep_t&>( buffer_any );
            GetShape<T>()( data_info.attr_info.data_type, buffer_any, shape );

            std::vector<int> dimensions(data_info.storage_dims);
            std::vector<int> chunksize(data_info.storage_dims);

            /* bug #15342
#define CREATE_DATASET \
          HANDLE_NEXUSCPP_ERROR ( file->CreateCompressedDataSet( data_info.dataset_name.c_str(), \
                                                                 NexusTraits<T>::type_id,        \
                                                                 data_info.storage_dims,         \
                                                                 &dimensions.front(),            \
                                                                 &chunksize.front(),             \
                                                                 true ) )
*/
#define CREATE_DATASET \
    HANDLE_NEXUSCPP_ERROR ( file->CreateDataSet( data_info.dataset_name.c_str(), \
    NexusTraits<T>::type_id,        \
    data_info.storage_dims,         \
    &dimensions.front(),            \
    true ) )

            if (config.hw_continuous)
            {
                dimensions[0] = config.hw_continuous_nbpt;
                chunksize[0] = dimensions[0];

                CREATE_DATASET;

                // i'm obliged to allocate a temp buffer to init the dataset
                boost::scoped_array<T> tmp_buf( new T[config.hw_continuous_nbpt] );
                int value = 0;
                ::memset( tmp_buf.get(), value, config.hw_continuous_nbpt * sizeof(T) );
                HANDLE_NEXUSCPP_ERROR( file->PutData( tmp_buf.get() ) );

            }
            else if (data_info.src_dims == data_info.display_dims)
            {
                //- this is a spectrum or an image
                if (config.actuators2.empty())
                {
                    //- 1d or timescan
                    dimensions[0] = config.integration_times.num_elements();
                    chunksize[0] = 1;
                    int chunk_nb_elem = 1;

                    if (data_info.src_dims == 1)
                    {
                        // 1d scan of spectrum
                        dimensions[1] = shape[1];
                        chunksize[1] = shape[1];
                        chunk_nb_elem = chunksize[1];
                    }
                    else
                    {
                        // 1d scan of images
                        dimensions[1] = shape[0];
                        dimensions[2] = shape[1];
                        chunksize[1] = shape[0];
                        chunksize[2] = shape[1];
                        chunk_nb_elem = chunksize[1] * chunksize[2];
                    }

                    CREATE_DATASET;

                    // i'm obliged to allocate a temp buffer to init the dataset
                    boost::scoped_array<T> tmp_buf( new T[chunk_nb_elem] );
                    int value = 0;
                    ::memset( tmp_buf.get(), value, chunk_nb_elem * sizeof(T) );

                    std::vector<int> start_index(3);
                    for (int i = 0; i < dimensions[0]; i++)
                    {
                        start_index[0] = i;
                        start_index[1] = 0;
                        start_index[2] = 0; // used if 1d scan of images

                        // put a chunk of 0s
                        HANDLE_NEXUSCPP_ERROR( file->PutDataSubSet( tmp_buf.get(), &start_index.front(), &chunksize.front(), 0 ) );
                    }
                }
                else
                {
                    //- 2d scan
                    dimensions[0] = config.trajectories2.shape()[1];
                    dimensions[1] = config.integration_times.num_elements();
                    chunksize[0] = 1;
                    chunksize[1] = 1;
                    int chunk_nb_elem = 1;

                    if (data_info.src_dims == 1)
                    {
                        dimensions[2] = shape[1];
                        chunksize[2] = shape[1];
                        chunk_nb_elem = chunksize[2];
                    }
                    else
                    {
                        dimensions[2] = shape[0];
                        dimensions[3] = shape[1];
                        chunksize[2] = shape[0];
                        chunksize[3] = shape[1];
                        chunk_nb_elem = chunksize[2] * chunksize[3];
                    }

                    CREATE_DATASET;
                    // i'm obliged to allocate a temp buffer to init the dataset
                    boost::scoped_array<T> tmp_buf( new T[chunk_nb_elem] );
                    int value = 0;
                    ::memset( tmp_buf.get(), value, chunk_nb_elem * sizeof(T) );

                    std::vector<int> start_index(4);
                    for (int i = 0; i < dimensions[0]; i++)
                    {
                        for (int j = 0; j < dimensions[1]; j++)
                        {
                            start_index[0] = i;
                            start_index[1] = j;
                            start_index[2] = 0;
                            start_index[3] = 0; // used if 2d scan of images

                            // put a chunk of 0s
                            HANDLE_NEXUSCPP_ERROR( file->PutDataSubSet( buffer->data(), &start_index.front(), &chunksize.front(), 0 ) );
                        }
                    }
                }
            }
            else
            {
                //- this is a scalar
                if (config.actuators2.empty())
                {
                    //- 1d or timescan
                    dimensions[0] = shape[1];
                    chunksize[0] = dimensions[0];
                }
                else if (!config.actuators2.empty() && data_info.scan_dim == 2)
                {
                    //- actuators2 in a 2d scan -> represented as a spectrum
                    dimensions[0] = shape[1];
                    chunksize[0] = dimensions[0];
                }
                else
                {
                    //- 2d
                    dimensions[0] = shape[0];
                    dimensions[1] = shape[1];
                    chunksize[0] = dimensions[0];
                    chunksize[1] = dimensions[1];
                }

                // the buffer has the size of the dataset, so it is simple (no need of temp buffers)
                CREATE_DATASET;
                HANDLE_NEXUSCPP_ERROR( file->PutData(buffer->data()) );
            }

            HANDLE_NEXUSCPP_ERROR( file->PutAttr( "long_name", data_info.attr_name.c_str() ) );
            HANDLE_NEXUSCPP_ERROR( file->PutAttr( "description", data_info.attr_info.description.c_str() ) );
            HANDLE_NEXUSCPP_ERROR( file->PutAttr( "units", data_info.attr_info.unit.c_str() ) );
            HANDLE_NEXUSCPP_ERROR( file->PutAttr( "format", data_info.attr_info.format.c_str() ) );

            // JIRA EXPDATA-114: Ajouter l'alias d'attribut dans les informations
            if( !data_info.attr_alias.empty() )
            {
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "alias", data_info.attr_alias.c_str() ) );
            }

            if ( data_info.type == DataCollector::ACTUATOR )
            {
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "axis", long(data_info.scan_dim) ) );
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "primary", long(data_info.index + 1) ) );

                std::string trajectory_dataset_name = boost::replace_first_copy( data_info.dataset_name, "actuator", "trajectory" );
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "trajectory", trajectory_dataset_name.c_str() ) );
            }

            if (data_info.type == DataCollector::SENSOR)
            {
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "signal", long(1) ) );

                if( data_info.attr_info.data_format == Tango::SCALAR )
                    HANDLE_NEXUSCPP_ERROR( file->PutAttr( "interpretation", "scalar" ) );
                if( data_info.attr_info.data_format == Tango::SPECTRUM )
                    HANDLE_NEXUSCPP_ERROR( file->PutAttr( "interpretation", "spectrum" ) );
                if( data_info.attr_info.data_format == Tango::IMAGE )
                    HANDLE_NEXUSCPP_ERROR( file->PutAttr( "interpretation", "image" ) );
            }

            if (data_info.type == DataCollector::TIMESTAMP)
            {
                HANDLE_NEXUSCPP_ERROR( file->PutAttr( "axis", long(0) ) );
            }

            std::string now = tm::to_iso_extended_string( tm::microsec_clock::local_time() );
            boost::algorithm::erase_tail( now, 3 ); // we want only millisec, not microsec
            HANDLE_NEXUSCPP_ERROR( file->PutAttr( "timestamp", now.c_str() ) );

            return true;
        }
        else
            return false;
    }
};

//#### C'est ici qu'il faut pousser les donnees dans le DataStreamer
template <typename T>
struct NexusDataSetWriter
{
    typedef typename ScanArray<T>::ImgP imagep_t;

    bool operator()( DataCollector::DataInfo& data_info,
                     DataRecorder::NexusFileP file,
                     std::vector<int>& scan_pos,
                     const ScanConfig& config )
    {
        if (data_info.attr_info.data_type == TangoTraits<T>::type_id)
        {
            boost::any buffer_any = data_info.datasetter->get();

            // BEGIN PROBLEM-1312
            if( buffer_any.empty() )
                return true;
            // END PROBLEM-1312

            imagep_t& buffer = boost::any_cast<imagep_t&>( buffer_any );

            std::vector<int> start_index(data_info.storage_dims);
            std::vector<int> size_in_dims(data_info.storage_dims);
            void* data = 0;

            if (config.hw_continuous)
            {
                if (!buffer)
                {
                    SCAN_INFO << data_info.attr_name << " : empty buffer" << ENDLOG;
                    return true;
                }
                start_index [0] = 0;
                size_in_dims[0] = buffer->num_elements();
                data = buffer->data();
                SCAN_DEBUG << "Writing " << size_in_dims[0] << " values for " << data_info.attr_name << ENDLOG;
            }
            else if (data_info.src_dims == data_info.display_dims)
            {
                //- this is a spectrum or an image
                if (data_info.src_dims == 1)
                {
                    //- spectrum
                    if (config.actuators2.empty())
                    {
                        //- scan 1D or timescan -> 2D data
                        start_index[0] = scan_pos[1];
                        start_index[1] = 0;

                        size_in_dims[0] = 1;
                        size_in_dims[1] = buffer->num_elements();
                    }
                    else
                    {
                        //- scan 2D -> 3D data
                        start_index[0] = scan_pos[0];
                        start_index[1] = scan_pos[1];
                        start_index[2] = 0;

                        size_in_dims[0] = 1;
                        size_in_dims[1] = 1;
                        size_in_dims[2] = buffer->num_elements();
                    }
                }
                else
                {
                    //- image
                    if (config.actuators2.empty())
                    {
                        //- scan 1D or timescan -> 3D data
                        start_index[0] = scan_pos[1];
                        start_index[1] = 0;
                        start_index[2] = 0;

                        size_in_dims[0] = 1;
                        if( buffer->num_dimensions() >= 2 )
                        {
                            size_in_dims[1] = buffer->shape()[0];
                            size_in_dims[2] = buffer->shape()[1];
                        }
                        else
                        {
                            throw yat::Exception("DATA_ERROR",
                                                 yat::Format("Invalid number of dimensions ({}) for image buffer")
                                                            .arg(buffer->num_dimensions()),
                                                 "NexusDataSetWriter::operator()");
                        }

                    }
                    else
                    {
                        //- scan 2D -> 4D data
                        start_index[0] = scan_pos[0];
                        start_index[1] = scan_pos[1];
                        start_index[2] = 0;
                        start_index[3] = 0;

                        size_in_dims[0] = 1;
                        size_in_dims[1] = 1;
                        if( buffer->num_dimensions() >= 2 )
                        {
                            size_in_dims[2] = buffer->shape()[0];
                            size_in_dims[3] = buffer->shape()[1];
                        }
                        else
                        {
                            throw yat::Exception("DATA_ERROR",
                                                 yat::Format("Invalid number of dimensions ({}) for image buffer")
                                                            .arg(buffer->num_dimensions()),
                                                 "NexusDataSetWriter::operator()");
                        }
                    }
                }
                data = buffer->data();
            }
            else
            {
                //- scalar
                if (config.actuators2.empty())
                {
                    //- scan 1D or timescan -> 1D data
                    start_index [0] = scan_pos[1];
                    size_in_dims[0] = 1;
                    data = buffer->data() + scan_pos[1];
                }
                else if (!config.actuators2.empty() && data_info.scan_dim == 2)
                {
                    //- actuators2 in a 2d scan -> represented as a spectrum
                    start_index [0] = scan_pos[0];
                    size_in_dims[0] = 1;
                    data = buffer->data() + scan_pos[0];
                }
                else
                {
                    //- scan 2D -> 2D data
                    start_index [0] = scan_pos[0];
                    start_index [1] = scan_pos[1];
                    size_in_dims[0] = 1;
                    size_in_dims[1] = 1;
                    data = &(*buffer)(scan_pos);
                }
            }

            try
            {
                HANDLE_NEXUSCPP_ERROR( file->PutDataSubSet( data, &start_index.front(), &size_in_dims.front(), 0) );
            }
            catch( nxcpp::NexusException& ex )
            {
                SCAN_ERROR << "Write error!!! can't write data into " << file->CurrentGroupPath() << '/' << file->CurrentDataset() << ENDLOG;
                SCAN_ERROR << "Start Index: ";
                for( std::size_t i = 0; i < start_index.size(); ++i ) SCAN_ERROR << start_index[i] << "; ";
                SCAN_ERROR << ENDLOG;
                SCAN_ERROR << "Dims: ";
                for( std::size_t i = 0; i < size_in_dims.size(); ++i ) SCAN_ERROR << size_in_dims[i] << "; ";
                SCAN_ERROR << ENDLOG;
                throw ex;
            }
            catch( const yat::Exception& ex )
            {
                SCAN_ERROR << "Write error!!! can't write data into " << file->CurrentGroupPath() << '/' << file->CurrentDataset() << ENDLOG;
                SCAN_ERROR << "Start Index: ";
                for( std::size_t i = 0; i < start_index.size(); ++i ) SCAN_ERROR << start_index[i] << "; ";
                SCAN_ERROR << ENDLOG;
                SCAN_ERROR << "Dims: ";
                for( std::size_t i = 0; i < size_in_dims.size(); ++i ) SCAN_ERROR << size_in_dims[i] << "; ";
                SCAN_ERROR << ENDLOG;
                throw ex;
            }

            return true;
        }
        else
            return false;
    }
};


template <typename T>
struct NexusDataSetCreatorAndWriter
{
    typedef typename ScanArray<T>::ImgP imagep_t;

    bool operator()( DataCollector::DataInfo& data_info,
                     DataRecorder::NexusFileP file,
                     const ScanConfig& config )
    {
        if (data_info.attr_info.data_type == TangoTraits<T>::type_id)
        {
            NexusDataSetCreator<T>()( data_info, file, config );

            HANDLE_NEXUSCPP_ERROR( file->OpenDataSet( data_info.dataset_name.c_str() ) );

            boost::any buffer_any = data_info.datasetter->get();
            imagep_t& buffer = boost::any_cast<imagep_t&>( buffer_any );
            HANDLE_NEXUSCPP_ERROR( file->PutData( buffer->data() ) );

            return true;
        }
        else
            return false;
    }
};

struct NexusFileCloser
{
    void operator() ( nxcpp::NexusFile* file )
    {
        if (file)
        {
            IGNORE_ERROR( file->Flush(),          "Error while flushing Nexus file" );
            IGNORE_ERROR( file->CloseAllGroups(), "Error while closing all Nexus groups"  );
            IGNORE_ERROR( file->Close(),          "Error while closing Nexus file"  );
            delete file;
        }
    }
};

class DataRecorderAutoLock
{
public:
    DataRecorderAutoLock(bool use_system_lock)
        : locked_(false)
    {
        SCAN_DEBUG << "Requesting access to NeXus/HDF5 data file..." << ENDLOG;
        use_lock_ = use_system_lock;
        HANDLE_NEXUS4TANGO_ERROR( locked_ = DataRecorderWriteLock::Instance().RequestAccess() );
        if (locked_)
        {
            SCAN_DEBUG << "Access granted to NeXus/HDF5 data file..." << ENDLOG;
        }
        else
        {
            throw yat::Exception( "NEXUS4TANGO_ERROR", "Unable to get access to DataRecorder file. Check data recording device state", "DataRecorderAutoLock::DataRecorderAutoLock" );
        }
    }

    ~DataRecorderAutoLock()
    {
        if (locked_)
        {
            IGNORE_ERROR( DataRecorderWriteLock::Instance().ReleaseAccess(), "Unknown error while ReleaseAccess to NeXus/HDF5 data file" );
            SCAN_DEBUG << "Access to NeXus/HDF5 data file has been released..." << ENDLOG;
        }
    }

    bool locked() const
    {
        return locked_;
    }

    DataRecorder::NexusFileP get_opened_file() const
    {
        DataRecorder::NexusFileP file;
        if (this->locked())
        {
            yat::FileName fn(DataRecorderWriteLock::Instance().FilePath());
            if( !fn.file_exist() )
            {
                throw yat::Exception("DATAFILE_ERROR",
                                     yat::Format("Can't record data. The file {} don't exists. "
                                                 "Probably the client application didn't ask "
                                                 "the recording service to write metadata!")
                                                 .arg(fn.full_name()),
                                            "DataRecorderAutoLock::get_opened_file");
            }

            SCAN_DEBUG << "Opening file " << DataRecorderWriteLock::Instance().FilePath() << ENDLOG;

            // Use the file system locking
            if( use_lock_ )
            {
                HANDLE_NEXUSCPP_ERROR( file.reset( new nxcpp::NexusFile(DataRecorderWriteLock::Instance().FilePath().c_str(), nxcpp::NexusFile::NONE, use_lock_), NexusFileCloser() ) );
            }
            else
            {
                HANDLE_NEXUSCPP_ERROR( file.reset( new nxcpp::NexusFile(), NexusFileCloser() ) );
            }

            SCAN_DEBUG << "Opening File " << DataRecorderWriteLock::Instance().FilePath() << ENDLOG;
            HANDLE_NEXUSCPP_ERROR( file->OpenReadWrite( DataRecorderWriteLock::Instance().FilePath().c_str() ) );
            SCAN_DEBUG << "Opening NXentry " << DataRecorderWriteLock::Instance().NXentryName() << ENDLOG;
            HANDLE_NEXUSCPP_ERROR( file->OpenGroup( DataRecorderWriteLock::Instance().NXentryName().c_str(), "NXentry", true ) );
        }
        return file;
    }

private:
    bool locked_;
    bool use_lock_;
};

#endif


template <typename T>
struct AsciiDataSetWriter
{
    typedef typename ScanArray<T>::Img  image_t;
    typedef typename ScanArray<T>::ImgP imagep_t;

    bool operator()( DataCollector::DataInfo& data_info,
                     DataRecorder::AsciiFile& file,
                     std::vector<int>& scan_pos,
                     const ScanConfig& config )
    {
        if (data_info.attr_info.data_type == TangoTraits<T>::type_id)
        {
            if (data_info.src_dims > 0 || config.hw_continuous)
            {
                // no ASCII saving support for spectrum and images sensors
                // neither for hardware continuous scans
                return true;
            }

            boost::any buffer_any = data_info.datasetter->get();
            imagep_t& bufferp = boost::any_cast<imagep_t&>( buffer_any );
            image_t& buffer = *bufferp;

            if ( data_info.scan_dim == 2 )
            {
                //- actuators2 in a 2d scan -> represented as a spectrum
                file << buffer[0][scan_pos[0]];
            }
            else
            {
                // either scan1D or scan2D
                file << buffer(scan_pos);
            }
            return true;
        }
        else
            return false;
    }
};


struct ASCIIFileCloser
{
    void operator() ( DataRecorder::AsciiFile* file )
    {
        try
        {
            if (file)
            {
                file->flush();
                file->close();
                delete file;
                SCAN_DEBUG << "ASCII file closed successfully" << ENDLOG;
            }
        }
        catch( ... )
        {
            SCAN_ERROR << "Error when closing file" << ENDLOG;
        }
    }
};


DataRecorder::DataRecorder( const ScanConfig& config )
    : config_( config )
    #ifdef SCANSL_ENABLE_NEXUS
    , datarecorder_step_( 0 )
    , dataset_created_( false )
    , scan_init_done_( false )
    #endif
{
    try
    {
#ifdef SCANSL_ENABLE_NEXUS
        if ( !config_.properties.datarecorder.empty() )
        {
            datarecorder_proxy_ = DevProxies::instance().proxy(config_.properties.datarecorder);
            datarecorder_proxy_->ping();

            //- set timeout to 30 seconds
            datarecorder_proxy_->set_timeout_millis( 30000 );

            Tango::DevState state = datarecorder_proxy_->state();
            if( config_.properties.is_recording_manager )
            {
                if( Tango::FAULT == state )
                {
                    THROW_YAT_ERROR("DATA_RECORDER",
                                    yat::Format("The RecordingManager device ({}) must not be in FAULT state. Please fix it then retry")
                                               .arg(config_.properties.datarecorder),
                                    "DataRecorder::DataRecorder");
                }

                bool is_running;
                Tango::DeviceAttribute da = datarecorder_proxy_->read_attribute("recordingSession");
                da >> is_running;
                if( is_running && !config_.datarecorder_only_write_scan_data )
                {
                    THROW_YAT_ERROR("DATA_RECORDER",
                                    "A recording session is already in progress. Please end it before trying to start a new scan session.",
                                    "DataRecorder::DataRecorder");
                }
                else if( !is_running && config_.datarecorder_only_write_scan_data )
                {
                    THROW_YAT_ERROR("DATA_RECORDER",
                                    "A recording session should be already started. Please start it before trying to start a new scan.",
                                    "DataRecorder::DataRecorder");
                }
            }
            else // using Using deprecated DataRecorder device
            {
                if (config_.datarecorder_only_write_scan_data)
                {
                    if (state != Tango::RUNNING && state != Tango::ALARM)
                    {
                        THROW_YAT_ERROR("DATA_RECORDER",
                                        "The DataRecorder must be RUNNING. Call StartRecording first",
                                        "DataRecorder::DataRecorder");
                    }
                }
                else
                {
                    if (state != Tango::ON)
                    {
                        THROW_YAT_ERROR("DATA_RECORDER",
                                        "The DataRecorder must be ON. Check DataRecorder state (end the current session if necessary)",
                                        "DataRecorder::DataRecorder");
                    }
                }
            }
            if ( config_.properties.datarecorder_write_mode == DataRecorderWriteMode_DONE_BY_SCANSERVER )
            {
                HANDLE_NEXUS4TANGO_ERROR( DataRecorderWriteLock::Instance().SetDataRecorder(config_.properties.datarecorder, config_.properties.is_recording_manager); )
            }

            SCAN_INFO << "Data recording handled by ScanServer" << ENDLOG;
        }
        else
#endif
        {
            SCAN_INFO << "DataRecorder deactivated" << ENDLOG;

            if (config_.properties.datafile_activation == true)
            {
                SCAN_INFO << "Data will be saved by the ScanServer itself in the directory " << config_.properties.datafile_path << ENDLOG;
            }
            else
            {
                SCAN_WARN << "Data will NOT be saved on disk" << ENDLOG;
            }
        }
    }
    catch( Tango::DevFailed& df )
    {
        yat4tango::TangoYATException ex(df);
        throw ex;
    }
}

DataRecorder::~DataRecorder()
{
}

void DataRecorder::init_run( DataCollector* data_collector )
{
    this->data_collector_ = data_collector;

#ifdef SCANSL_ENABLE_NEXUS
    if ( datarecorder_proxy_ && datarecorder_step_ == 0 )
    {
        if ( !config_.datarecorder_only_write_scan_data )
        {
            if( config_.properties.is_recording_manager )
            {
                std::string acq_name = config_.run_name;

                // Sanitize acquisition name
                yat::StringUtil::replace(&acq_name, '.', '_');
                yat::StringUtil::replace(&acq_name, ' ', '_');

                SCAN_INFO << "DataRecorder : acquisitionName=" << acq_name << ENDLOG;
                try
                {
                    Tango::DeviceAttribute devattr2("acquisitionName",
                                                   const_cast<std::string&>(acq_name) );
                    datarecorder_proxy_->write_attribute(devattr2);
                }
                catch(Tango::DevFailed& df)
                {   // Retry with the alternative attribute name
                    Tango::DeviceAttribute devattr2("acqName",
                                                   const_cast<std::string&>(acq_name) );
                    datarecorder_proxy_->write_attribute(devattr2);
                }
                SCAN_INFO << "Data recording: start [" << config_.data_recorder_config << "]" << ENDLOG;
                datarecorder_proxy_->command_inout("StartRecording");
            }
            else // Using deprecated DataRecorder device
            {
                SCAN_INFO << "DataRecorder : StartRecording [" << config_.data_recorder_config << "]" << ENDLOG;

                Tango::DeviceAttribute devattr( "asynchronousWrite", true );
                datarecorder_proxy_->write_attribute( devattr );

                if( !config_.data_recorder_config.empty() )
                {
                    Tango::DeviceData arg;
                    std::string str = config_.data_recorder_config;
                    arg << str;
                    datarecorder_proxy_->command_inout( "LoadPostRecording", arg );
                }
                // BEGIN PROBLEM-1304
                std::string acq_name = config_.run_name;

                yat::StringUtil::replace(&acq_name, ' ', '_');
                if( config_.properties.acquisitionname_sanitization )
                {
                    // Sanitize acquisition name
                    yat::StringUtil::replace(&acq_name, '.', '_');
                }

                SCAN_INFO << "DataRecorder : acquisitionName=" << acq_name << ENDLOG;
                Tango::DeviceAttribute devattr2("acquisitionName",
                                               const_cast<std::string&>(acq_name) );
                datarecorder_proxy_->write_attribute(devattr2);
                // END PROBLEM-1304

                datarecorder_proxy_->command_inout("StartRecording");
                this->synchronize();
            }
        }
        datarecorder_step_ = 1;
    }
    else if ( config_.properties.datafile_activation
              && config_.properties.datafile_type == DataFileType_NEXUS )
    {
        // in Nexus mode, we have 1 file per run
        this->create_data_file();
    }
#endif

}

void DataRecorder::init_scan(std::size_t scan_idx)
{
#ifdef SCANSL_ENABLE_NEXUS
    scan_init_done_ = false;
    dataset_created_ = false;

    if ( datarecorder_proxy_ && (datarecorder_step_ == 1 || datarecorder_step_ == 4) )
    {
        if ( !config_.datarecorder_only_write_scan_data )
        {
            if( config_.properties.is_recording_manager )
            {
                if( scan_idx > 1 )
                {
                    SCAN_INFO << "New file/entry" << ENDLOG;
                    datarecorder_proxy_->command_inout("Newfile");
                }

                SCAN_INFO << "Data recording : write metadata" << ENDLOG;
                Tango::DeviceData dd;
                std::string str("default");
                dd << str;
                datarecorder_proxy_->command_inout("RecordDeviceList", dd);
                synchronize();
            }
            else // Using deprecated DataRecorder device
            {
                if( !config_.properties.no_inc_indexes_at_first_scan || datarecorder_step_ != 1 )
                {
                    SCAN_INFO << "DataRecorder : IncAcquisitionIndex & IncExperimentIndex" << ENDLOG;
                    datarecorder_proxy_->command_inout("IncAcquisitionIndex");
                    this->synchronize();
                    datarecorder_proxy_->command_inout("IncExperimentIndex");
                }
    // BEGIN PROBLEM-1304
    //            SCAN_INFO << "DataRecorder : acquisitionName=" << config_.run_name << ENDLOG;
    //            Tango::DeviceAttribute devattr("acquisitionName",
    //                                           const_cast<std::string&>(config_.run_name) );
    //            datarecorder_proxy_->write_attribute(devattr);
    // END PROBLEM-1304
                SCAN_INFO << "Data recording: call WriteUserData & WritePreTechnicalData" << ENDLOG;
                datarecorder_proxy_->command_inout("WriteUserData");
                this->synchronize();
                datarecorder_proxy_->command_inout("WritePreTechnicalData");
                this->synchronize();
            }
        }
        last_data_recorder_saving_ = data_collector_->init_scan_timestamp_;
        datarecorder_step_ = 2;
        scan_init_done_ = true;
    }
    else if ( config_.properties.datafile_activation
              && config_.properties.datafile_type == DataFileType_NEXUS )
    {
        if ( nxs_file_ )
        {
            //- create the NXentry for the scan
            std::ostringstream nx_entry_name;
            nx_entry_name << "exp_";
            nx_entry_name << config_.run_name.c_str();
            nx_entry_name << "_";
            nx_entry_name << std::setw(5) << std::setfill('0') << yat::XString<size_t>::to_string( data_collector_->scan_index_ );

            HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseAllGroups() ); // if ever sthg was still opened
            HANDLE_NEXUSCPP_ERROR( nxs_file_->CreateGroup( nx_entry_name.str().c_str(), "NXentry", true ) );
            SCAN_DEBUG << "NXentry '" << nx_entry_name.str().c_str() << "' created" << ENDLOG;
        }
        scan_init_done_ = true;
    }
    else
#endif
        if ( config_.properties.datafile_activation
             && config_.properties.datafile_type == DataFileType_ASCII )
        {
            // in ASCII mode, we have 1 file per scan
            this->create_data_file();
        }
}


void DataRecorder::create_data_file( void )
{
    fs::path data_path(config_.properties.datafile_path);

    //- get the local time now (date + offset in the day)
    tm::ptime current_ptime = tm::microsec_clock::local_time();

    //- make a directory with the date (in the format 20071112 for November 12th, 2007)
    data_path /= dt::to_iso_string( current_ptime.date() );

    //- create the directory hierarchy
    if ( !fs::exists( data_path ) )
    {
        try
        {
            fs::create_directories( data_path );
        }
        catch( fs::filesystem_error& exc )
        {
            yat::OSStream oss;
            oss << "Unable to create the directory "
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
                << data_path.string()
#else
                << data_path.directory_string()
#endif
                << " (Reason : "
                << exc.what()
                << ")";

            THROW_YAT_ERROR("FILESYSTEM_ERROR",
                            oss.str(),
                            "DataCollector::prepare_data_file");
        }
    }

    //- extract the offset in the day (hour/min/sec/millisec)
    tm::time_duration current_tod = current_ptime.time_of_day();

    //- build the hour string as for example : 01h23m45.678
    yat::OSStream oss;
    oss << std::setw(2) << std::setfill('0') << current_tod.hours()
        << 'h'
        << std::setw(2) << std::setfill('0') << current_tod.minutes()
        << 'm'
        << std::setw(2) << std::setfill('0') << current_tod.seconds()
        << '.'
        << std::setw(3) << std::setfill('0') << current_tod.total_milliseconds() - 1000 * current_tod.total_seconds();

    std::string current_tod_str = oss.str();

    //- build the file name : basename_{time in the day}.dat
    std::string filename = config_.properties.datafile_basename;
    filename += "_";
    filename += current_tod_str;
    if ( config_.properties.datafile_type == DataFileType_ASCII )
    {
        filename += config_.properties.datafile_extension[0] == '.' ? "" : ".";
        filename += config_.properties.datafile_extension;
    }
#ifdef SCANSL_ENABLE_NEXUS
    else
    {
        // for Nexus file, force the extension to be 'nxs'
        filename += ".nxs";
    }
#endif

    //- create file on the filesystem
    data_path /= filename;
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
    std::string full_filename = data_path.string();
#else
    std::string full_filename = data_path.file_string();
#endif

    SCAN_INFO << "Creating file " << full_filename << ENDLOG;

    data_collector_->scan_data_p_->generated_file_name_ = full_filename;

    if ( config_.properties.datafile_type == DataFileType_ASCII )
    {
        ascii_file_.reset( new AsciiFile(full_filename.c_str()), ASCIIFileCloser() );

        if ( !ascii_file_->good() )
        {
            std::ostringstream oss;
            oss << "Unable to create file " << full_filename;
            THROW_YAT_ERROR("FILESYSTEM_ERROR",
                            oss.str().c_str(),
                            "DataCollector::prepare_data_file");
        }

        //- write first line containing the date
        *ascii_file_ << "# "
                     << dt::to_simple_string( current_ptime.date() )
                     << " "
                     << current_tod_str
                     << std::endl;

        //- write second line : actuators / actuators2 / sensors / "integration time"
        if ( config_.actuators.size() > 0 )
            *ascii_file_ << "# actuators_timestamp" << "\t";

        *ascii_file_ << "sensors_timestamp" << "\t";

        *ascii_file_ << "integration_time" << "\t";

        for( std::size_t i = 0; i < config_.actuators.size(); ++i )
        {
            *ascii_file_ << config_.actuators[i] << "\t";
        }
        for( std::size_t i = 0; i < config_.actuators2.size(); ++i )
        {
            *ascii_file_ << config_.actuators2[i] << "\t";
        }
        for( std::size_t i = 0; i < config_.sensors.size(); ++i )
        {
            *ascii_file_ << config_.sensors[i] << "\t";
        }
        *ascii_file_ << std::endl;
        SCAN_DEBUG << "File " << full_filename << " created successfully" << ENDLOG;
    }
#ifdef SCANSL_ENABLE_NEXUS
    else
    {
        HANDLE_NEXUSCPP_ERROR( nxs_file_.reset( new nxcpp::NexusFile(), NexusFileCloser() ) );
        HANDLE_NEXUSCPP_ERROR( nxs_file_->Create( full_filename.c_str(), nxcpp::NX_HDF5 ) );
        HANDLE_NEXUSCPP_ERROR( nxs_file_->OpenReadWrite( full_filename.c_str() ) );
        SCAN_DEBUG << "File " << full_filename << " created successfully" << ENDLOG;
    }
#endif
}

void DataRecorder::save_scan( void )
{
#ifdef SCANSL_ENABLE_NEXUS
    if ( datarecorder_proxy_
         && (datarecorder_step_ == 2 || datarecorder_step_ == 3) )
    {
        //- disable continuous saving for 2d-scans && on-the-fly scans
        if ( config_.properties.datarecorder_write_mode == DataRecorderWriteMode_DONE_BY_DATARECORDER )
        {
            if( config_.properties.is_recording_manager )
            {
                // do nothing
            }
            else
            {
                if ( config_.actuators2.empty() || !config_.on_the_fly )
                {
                    tm::ptime now = tm::microsec_clock::local_time();
                    tm::time_duration from_last_time = now - last_data_recorder_saving_;
                    if ( double(from_last_time.total_milliseconds()) / 1000 > config_.properties.datarecorder_saving_period )
                    {
                        std::vector< std::string > devices;
                        devices.push_back( config_.properties.my_devicename );

                        Tango::DeviceData dev_data;
                        dev_data << devices;

                        SCAN_INFO << "DataRecorder : WriteScanData" << ENDLOG;
                        datarecorder_proxy_->command_inout( "WriteScanData", dev_data );
                        SCAN_DEBUG << "DataRecorder : WriteScanData OK" << ENDLOG;
                        this->synchronize();
                        SCAN_DEBUG << "DataRecorder : RUNNING" << ENDLOG;

                        datarecorder_step_ = 3;
                        last_data_recorder_saving_ = now;
                    }
                }
            }
        }
        else if ( config_.properties.datarecorder_write_mode == DataRecorderWriteMode_DONE_BY_SCANSERVER )
        {
            DataRecorderAutoLock auto_lock(config_.properties.use_recorder_proxy);
            // Get current full file path, which should be localised in a spool (temporary path)
            std::string file_path = DataRecorderWriteLock::Instance().FilePath();
            if( config_.properties.is_recording_manager )
            {
                data_collector_->scan_data_p_->generated_file_name_ = file_path;
            }
            else // Using deprecated DataRecorder device
            {
                // Extract file name
                std::string file_name = file_path.substr(file_path.rfind('/') + 1);
                // Get the final target directory for the NeXus file
                Tango::DeviceAttribute devattr = datarecorder_proxy_->read_attribute( "targetDirectory" );
                std::string target_dir;
                devattr >> target_dir;

                // Build real full file path to be returned
                data_collector_->scan_data_p_->generated_file_name_ = target_dir + "/" + file_name;
            }

            // we always have auto_lock.locked() == true (or an exception is raised)
            if( !config_.on_the_fly )
            {
                nxs_file_ = auto_lock.get_opened_file();
                this->save_step_data_nexus();
                nxs_file_.reset();
            }
        }
    }
    else if ( config_.properties.datafile_activation
              && config_.properties.datafile_type == DataFileType_NEXUS )
    {
        if ( nxs_file_ && !config_.on_the_fly)
        {
            this->save_step_data_nexus();
        }
    }
    else
#endif
        if ( config_.properties.datafile_activation
             && config_.properties.datafile_type == DataFileType_ASCII )
        {
            if ( ascii_file_ )
            {
                this->save_step_data_ascii();
            }
        }
}

void DataRecorder::save_step_data_ascii( void )
{
    SCAN_INFO << "Writing step data to ASCII file..." << ENDLOG;

    AsciiFile& file = *ascii_file_;

    std::vector<int> scan_pos(2);
    scan_pos[0] = data_collector_->dim2_pt_;
    scan_pos[1] = ( !config_.zig_zag || data_collector_->dim2_pt_ % 2 == 0 )
            ? data_collector_->dim1_pt_
            : data_collector_->capacity_ - data_collector_->dim1_pt_ - 1;
    SCAN_DEBUG << "[ " << scan_pos[1] + 1 << " , " << scan_pos[0] + 1 << " ] Writing step values to ASCII file" << ENDLOG;

    // actuators timestamps
    if ( config_.actuators.size() > 0 )
    {
        DataCollector::DataInfo& datainfo = data_collector_->actuator_data_[ "actuatorstimestamps" ];
        AsciiDataSetWriter<Tango::DevDouble >()( datainfo, file, scan_pos, config_ );
        file << "\t";
    }

    // sensor timestamps
    if ( config_.actuators.size() > 0 )
    {
        DataCollector::DataInfo& datainfo = data_collector_->actuator_data_[ "sensorstimestamps" ];
        AsciiDataSetWriter<Tango::DevDouble >()( datainfo, file, scan_pos, config_ );
        file << "\t";
    }

    double integration_time = config_.on_the_fly ? config_.integration_times[0] : config_.integration_times[ scan_pos[1] ];
    file << integration_time << "\t";

    // actuator of dimension 1
    for (size_t i = 0; i < config_.actuators.size(); i++)
    {
        DataCollector::DataInfo& datainfo = data_collector_->actuator_data_[ boost::to_lower_copy(config_.actuators[i]) ];
        if      (AsciiDataSetWriter<Tango::DevShort  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevLong   >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevDouble >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevFloat  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevBoolean>()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUShort >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevULong  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUChar  >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevLong64 >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevULong64>()( datainfo, file, scan_pos, config_ )) {}
        file << "\t";
    }

    // actuator of dimension 2
    for (size_t i = 0; i < config_.actuators2.size(); i++)
    {
        DataCollector::DataInfo& datainfo = data_collector_->actuator_data_[ boost::to_lower_copy(config_.actuators2[i]) ];
        if      (AsciiDataSetWriter<Tango::DevShort  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevLong   >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevDouble >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevFloat  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevBoolean>()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUShort >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevULong  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUChar  >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevLong64 >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevULong64>()( datainfo, file, scan_pos, config_ )) {}
        file << "\t";
    }

    // sensors
    for (size_t i = 0; i < config_.sensors.size(); i++)
    {
        DataCollector::DataInfo& datainfo = data_collector_->sensor_data_[ boost::to_lower_copy(config_.sensors[i]) ];
        if      (AsciiDataSetWriter<Tango::DevShort  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevLong   >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevDouble >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevFloat  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevBoolean>()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUShort >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevULong  >()( datainfo, file, scan_pos, config_ )) {}
        else if (AsciiDataSetWriter<Tango::DevUChar  >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevLong64 >()( datainfo, file, scan_pos, config_ )) {}
        //else if (AsciiDataSetWriter<Tango::DevULong64>()( datainfo, file, scan_pos, config_ )) {}
        if (i != config_.sensors.size() - 1)
            file << "\t";
        else
            file << std::endl;
    }

    file.flush();
}

#ifdef SCANSL_ENABLE_NEXUS

void DataRecorder::write_trajectories( int axis )
{
    if ( !config_.on_the_fly && !config_.hw_continuous )
    {
        const std::vector<std::string>& actulist     = ( axis == 1 ? config_.actuators : config_.actuators2 );
        const ScanArray<double>::Img&   trajectories = ( axis == 1 ? config_.trajectories : config_.trajectories2 );

        // save all the trajectories as 1D spectrum (usefull for axis)
        // because for a 2D scan, actuator data is an image
        for ( size_t i = 0; i < actulist.size(); i++ )
        {
            std::ostringstream trajectory_name;
            trajectory_name << "trajectory_" << axis << "_" << i+1;
            std::ostringstream measured_values;
            measured_values << "actuator_" << axis << "_" << i+1;

            void* data = const_cast<void*>(static_cast<const void*>(&trajectories[i][0]));
            int n = trajectories.shape()[1];
            HANDLE_NEXUSCPP_ERROR(  nxs_file_->WriteData( trajectory_name.str().c_str(),
                                                          data,
                                                          NexusTraits<double>::type_id,
                                                          1,
                                                          &n ) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->OpenDataSet( measured_values.str().c_str() ) );

            // retrieve the attributes from the actuator_x_y dataset
            // and copy them back to trajectory_x_y
            std::string long_name;
            std::string description;
            std::string units;
            std::string format;
            HANDLE_NEXUSCPP_ERROR( nxs_file_->GetAttribute("long_name",   &long_name) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->GetAttribute("description", &description) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->GetAttribute("units",       &units) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->GetAttribute("format",      &format) );

            HANDLE_NEXUSCPP_ERROR( nxs_file_->OpenDataSet( trajectory_name.str().c_str() ) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "long_name", long_name.c_str()) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "description", description.c_str()) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "units", units.c_str()) );
            HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "format", format.c_str()) );
            /*HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "axis",    long(axis) ) );
        HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "primary", long(i+1) ) );
        HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "measured_values", measured_values.str().c_str() ) );*/
        }
    }
}

void DataRecorder::write_cstring(const char *name_p, const std::string& content)
{
    if( name_p && !content.empty() )
    {
        int n = content.size() + 1;
        HANDLE_NEXUSCPP_ERROR( nxs_file_->WriteData( name_p, (void*)(content.c_str()), nxcpp::NX_CHAR, 1, &n ) );
    }
}

void DataRecorder::write_info_pair(const std::string &info_pair)
{
    SCAN_INFO << "Write info_pair " << info_pair << ENDLOG;
    std::string line = info_pair;
    std::size_t sep_group_pos = info_pair.find('/');
    std::size_t sep_key_pos = info_pair.find(':');

    if( sep_key_pos == std::string::npos )
    { // sperarator between key and value not found: skip line
        return;
    }

    if( sep_group_pos != std::string::npos && sep_group_pos < sep_key_pos)
    { // line is in form: group/../group/key:value
        std::string group = line.substr(0, sep_group_pos);
        if( group.empty() )
        { // Skip unnamed group
            write_info_pair(line.substr(sep_group_pos + 1));
        }

        // Entering group
        if( !nxs_file_->OpenGroup( group.c_str(), "NXconfig", false ) )
        {
            HANDLE_NEXUSCPP_ERROR( nxs_file_->CreateGroup( group.c_str(), "NXconfig", true ) );
        }
        // Recursion
        write_info_pair(line.substr(sep_group_pos + 1));
        HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseGroup() );
    }
    else
    {
        std::string key = line.substr(0, sep_key_pos);
        std::string value = line.substr(sep_key_pos + 1);
        write_cstring(key.c_str(), value.c_str());
    }
    SCAN_INFO << "Write info_pair done" << ENDLOG;
}

void DataRecorder::write_config()
{
    SCAN_INFO << "Write config" << ENDLOG;
    // Get the last part of the scan configuration identification
    std::string scan_name, full_name = config_.run_name;
    std::size_t pos = full_name.rfind('.');
    if( pos != std::string::npos )
        scan_name = full_name.substr(pos + 1);
    else
        scan_name = full_name;

    HANDLE_NEXUSCPP_ERROR( nxs_file_->CreateGroup( "scan_config", "NXconfig", true ) );
    write_cstring("full name", full_name.c_str());
    write_cstring("name", scan_name.c_str());

    bool write_type = true;
    for( std::size_t i = 0; i < config_.info.size(); ++i )
    {
        std::string key, value;
        yat::StringUtil::split(config_.info[i], ':', &key, &value);
        if( yat::StringUtil::is_equal_no_case(key, "type") )
            write_type = false;
    }
    if( write_type )
    {
        // Scan type
        if( !config_.hw_continuous && !config_.on_the_fly )
            write_cstring("type", "step by step");
        else if( config_.hw_continuous )
            write_cstring("type", "hardware continuous");
        else if( config_.on_the_fly )
            write_cstring("type", "on the fly");
    }

    write_cstring("zig_zag", config_.zig_zag ? "true" : "false");
    write_cstring("actuators_speed_control", config_.enable_scan_speed ? "true" : "false");
    write_cstring("context_validation", config_.context_validation);

    for( std::size_t i = 0; i < config_.info.size(); ++i )
        write_info_pair(config_.info[i]);

    SCAN_INFO << "config_.info.size(): " << config_.info.size() << ENDLOG;
    HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseGroup() );
    SCAN_INFO << "Write config done" << ENDLOG;
}

void DataRecorder::write_integration_times()
{
    const ScanArray<double>::Buf&   integration_times = config_.integration_times;
    void* data = const_cast<void*>(static_cast<const void*>(&integration_times[0]));
    int n = integration_times.shape()[0];

    if ( config_.on_the_fly || config_.hw_continuous )
        n = 1;

    HANDLE_NEXUSCPP_ERROR(  nxs_file_->WriteData( "integration_times",
                                                  data,
                                                  NexusTraits<double>::type_id,
                                                  1,
                                                  &n ) );

    HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "long_name", "integration_time" ) );
    HANDLE_NEXUSCPP_ERROR( nxs_file_->PutAttr( "units", "seconds" ) );
}

void DataRecorder::save_step_data_nexus( void )
{
    //- TODO : don't write to the file at each step if there is no image or spectrum...

    //- Create the DataSet if not already done
    if ( !dataset_created_ )
    {
        // we should be here after the first step of the scan, once sensors have been collected 1 time
        // (to know their size in case of spectrum or images)

        // Write the configuration information
        write_config();

        if( !nxs_file_->OpenGroup( "scan_data", "NXdata", false ) )
        {
            HANDLE_NEXUSCPP_ERROR( nxs_file_->CreateGroup( "scan_data", "NXdata", true ) );
            SCAN_DEBUG << "NXdata 'scan_data' created" << ENDLOG;
        }
    }

    {
        for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->actuator_data_.begin();
             it != data_collector_->actuator_data_.end(); ++it )
        {
            DataCollector::DataInfo& datainfo = (*it).second;
            if( datainfo.dataset_name != "actuators_timestamps"
              /* BEGIN PROBLEM-1454 */ && !datainfo.dataset_created /* END PROBLEM-1454 */ )
            {
                if      (NexusDataSetCreator<Tango::DevShort  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevLong   >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevDouble >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevFloat  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevBoolean>()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevUShort >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevULong  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevUChar  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevLong64 >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevULong64>()( datainfo, nxs_file_, config_ )) {}
                //# BEGIN PROBLEM-1454
                datainfo.dataset_created = true;
                //# END PROBLEM-1454
            }
        }
        for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->sensor_data_.begin();
             it != data_collector_->sensor_data_.end(); ++it )
        {
            DataCollector::DataInfo& datainfo = (*it).second;
            if( datainfo.dataset_name != "actuators_timestamps"
              /* BEGIN PROBLEM-1454 */ && !datainfo.no_data && !datainfo.dataset_created /* END PROBLEM-1454 */ )
            {
                if      (NexusDataSetCreator<Tango::DevShort  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevLong   >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevDouble >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevFloat  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevBoolean>()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevUShort >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevULong  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevUChar  >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevLong64 >()( datainfo, nxs_file_, config_ )) {}
                else if (NexusDataSetCreator<Tango::DevULong64>()( datainfo, nxs_file_, config_ )) {}
                //# BEGIN PROBLEM-1454
                datainfo.dataset_created = true;
                //# END PROBLEM-1454
            }
        }
    }

    if( !dataset_created_ )
    {
        this->write_integration_times();
        this->write_trajectories( 1 );
        this->write_trajectories( 2 );
        dataset_created_ = true;
    }
    else
    {
        HANDLE_NEXUSCPP_ERROR( nxs_file_->OpenGroup( "scan_data", "NXdata", true ) );
        SCAN_DEBUG << "NXdata 'scan_data' opened" << ENDLOG;
    }

    // 'scan_data' group is opened
    std::vector<int> scan_pos(2);
    scan_pos[0] = data_collector_->dim2_pt_;
    scan_pos[1] = ( !config_.zig_zag || data_collector_->dim2_pt_ % 2 == 0 )
            ? data_collector_->dim1_pt_
            : data_collector_->capacity_ - data_collector_->dim1_pt_ - 1;
    SCAN_DEBUG << "[ " << scan_pos[1] + 1 << " , " << scan_pos[0] + 1 << " ] Writing step values to Nexus file" << ENDLOG;

    //- save actuator data
    for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->actuator_data_.begin();
         it != data_collector_->actuator_data_.end(); ++it )
    {
        DataCollector::DataInfo& datainfo = (*it).second;
        if( datainfo.dataset_name != "actuators_timestamps"
            /* BEGIN PROBLEM-1454 */ && datainfo.dataset_created /* END PROBLEM-1454 */ )
        {
            bool dataset_opened;
            HANDLE_NEXUSCPP_ERROR( dataset_opened = nxs_file_->OpenDataSet( datainfo.dataset_name.c_str() ));
            if ( dataset_opened )
            {
                //- now put the data
                if      (NexusDataSetWriter<Tango::DevShort  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevLong   >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevDouble >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevFloat  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevBoolean>()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevUShort >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevULong  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevUChar  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevLong64 >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevULong64>()( datainfo, nxs_file_, scan_pos, config_ )) {}

                HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseDataSet() );
            }
            else
            {
                //- could not open data set -> DataRecorder error strategy ?
                SCAN_ERROR << "Unable to open dataset " << datainfo.dataset_name << ENDLOG;
            }
        }
    }

    //- save sensor data
    for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->sensor_data_.begin();
         it != data_collector_->sensor_data_.end(); ++it )
    {
        DataCollector::DataInfo& datainfo = (*it).second;
        if( datainfo.dataset_name != "actuators_timestamps"
            /* BEGIN PROBLEM-1454 */ && !datainfo.no_data && datainfo.dataset_created /* END PROBLEM-1454 */ )
        {
            bool dataset_opened;
            HANDLE_NEXUSCPP_ERROR( dataset_opened = nxs_file_->OpenDataSet( datainfo.dataset_name.c_str() ));
            if ( dataset_opened )
            {
                //- now put the data
                if      (NexusDataSetWriter<Tango::DevShort  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevLong   >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevDouble >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevFloat  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevBoolean>()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevUShort >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevULong  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevUChar  >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevLong64 >()( datainfo, nxs_file_, scan_pos, config_ )) {}
                else if (NexusDataSetWriter<Tango::DevULong64>()( datainfo, nxs_file_, scan_pos, config_ )) {}

                HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseDataSet() );
            }
            else
            {
                //- could not open data set -> DataRecorder error strategy ?
                SCAN_ERROR << "Unable to open dataset " << datainfo.dataset_name << ENDLOG;
            }
        }
    }

    HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseGroup() ); // close 'scan_data' group
}


void DataRecorder::save_on_the_fly_scan_nexus( void )
{
    // Write the configuration information first
    write_config();

    HANDLE_NEXUSCPP_ERROR( nxs_file_->CreateGroup( "scan_data", "NXdata", true ) );
    SCAN_DEBUG << "NXdata 'scan_data' created and opened" << ENDLOG;

    for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->actuator_data_.begin();
         it != data_collector_->actuator_data_.end(); ++it )
    {
        DataCollector::DataInfo& datainfo = (*it).second;

        if (datainfo.dataset_name != "time_2")
        {
            SCAN_DEBUG << "Saving on-the-fly data for dataset " << datainfo.dataset_name << ENDLOG;

            if      (NexusDataSetCreatorAndWriter<Tango::DevShort  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevLong   >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevDouble >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevFloat  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevBoolean>()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevUShort >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevULong  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevUChar  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevLong64 >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevULong64>()( datainfo, nxs_file_, config_ )) {}
        }
    }
    for( std::map<std::string, DataCollector::DataInfo>::iterator it = data_collector_->sensor_data_.begin();
         it != data_collector_->sensor_data_.end(); ++it )
    {
        DataCollector::DataInfo& datainfo = (*it).second;

        if (datainfo.dataset_name != "time_2")
        {
            SCAN_DEBUG << "Saving on-the-fly data for dataset " << datainfo.dataset_name << ENDLOG;

            if      (NexusDataSetCreatorAndWriter<Tango::DevShort  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevLong   >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevDouble >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevFloat  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevBoolean>()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevUShort >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevULong  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevUChar  >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevLong64 >()( datainfo, nxs_file_, config_ )) {}
            else if (NexusDataSetCreatorAndWriter<Tango::DevULong64>()( datainfo, nxs_file_, config_ )) {}
        }
    }
    HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseGroup() );
}
#endif


void DataRecorder::end_scan( void )
{
#ifdef SCANSL_ENABLE_NEXUS
    if( !scan_init_done_ )
        return;

    bool is_recording_session = false;
    if( datarecorder_proxy_ && config_.properties.is_recording_manager )
    {
        Tango::DeviceAttribute da = datarecorder_proxy_->read_attribute("recordingSession");
        da >> is_recording_session;
    }

    if( datarecorder_proxy_ && is_recording_session && datarecorder_proxy_->state() != Tango::FAULT
         && datarecorder_step_ >= 1
         && datarecorder_step_ != 4 )
    {
        if( config_.on_the_fly )
        {
            DataRecorderAutoLock auto_lock(config_.properties.use_recorder_proxy);
            // we always have auto_lock.locked() == true (or an exception is raised)
            nxs_file_ = auto_lock.get_opened_file();
            SCAN_INFO << "Saving data for on-the-fly scan" << ENDLOG;
            save_on_the_fly_scan_nexus();
            nxs_file_.reset();
        }

        if ( !config_.datarecorder_only_write_scan_data )
        {
            SCAN_INFO << "Data recording : write post-acquisition metadata" << ENDLOG;
            Tango::DeviceData dd;
            std::string str("post_acq");
            dd << str;
            datarecorder_proxy_->command_inout("RecordDeviceList", dd);
            synchronize();
        }
        datarecorder_step_ = 4;
    }
    else if ( datarecorder_proxy_
         && (datarecorder_proxy_->state() == Tango::RUNNING || datarecorder_proxy_->state() == Tango::ALARM)
         && datarecorder_step_ >= 1
         && datarecorder_step_ != 4 )  // Using deprecated DataRecorder device
    {
        if ( config_.properties.datarecorder_write_mode == DataRecorderWriteMode_DONE_BY_DATARECORDER )
        {
            //- save data one last time to be sure everything is saved
            //- in case of 2d onthe-fly scan, it is the only place where data are saved
            if ( data_collector_->buffers_.pt_nb != 0 && data_collector_->buffers_.pt_nb2 != 0 )
            {
                std::vector< std::string > devices;
                devices.push_back( config_.properties.my_devicename );

                Tango::DeviceData dev_data;
                dev_data << devices;

                SCAN_INFO << "DataRecorder : WriteScanData" << ENDLOG;
                datarecorder_proxy_->command_inout( "WriteScanData", dev_data );
                SCAN_DEBUG << "DataRecorder : WriteScanData OK" << ENDLOG;
                this->synchronize();
                SCAN_DEBUG << "DataRecorder : RUNNING" << ENDLOG;
            }
        }
        else if ( config_.properties.datarecorder_write_mode == DataRecorderWriteMode_DONE_BY_SCANSERVER )
        {
            DataRecorderAutoLock auto_lock(config_.properties.use_recorder_proxy);
            // we always have auto_lock.locked() == true (or an exception is raised)
            nxs_file_ = auto_lock.get_opened_file();
            if (config_.on_the_fly)
            {
                SCAN_INFO << "Saving data for on-the-fly scan" << ENDLOG;
                this->save_on_the_fly_scan_nexus();
            }
            nxs_file_.reset();
        }

        if ( !config_.datarecorder_only_write_scan_data )
        {
            SCAN_INFO << "DataRecorder : WritePostTechnicalData" << ENDLOG;
            datarecorder_proxy_->command_inout( "WritePostTechnicalData" );
            this->synchronize();
            SCAN_INFO << "DataRecorder : WriteMonitoredData" << ENDLOG;
            datarecorder_proxy_->command_inout( "WriteMonitoredData" );
            this->synchronize();
            SCAN_INFO << "DataRecorder : EndNXentry" << ENDLOG;
            datarecorder_proxy_->command_inout( "EndNXentry" );
            this->synchronize();
        }
        datarecorder_step_ = 4;
    }
    else if ( config_.properties.datafile_activation && nxs_file_ )
    {
        if (config_.on_the_fly)
        {
            this->save_on_the_fly_scan_nexus();
            SCAN_DEBUG << "On the fly scan saved successfully" << ENDLOG;
        }
        HANDLE_NEXUSCPP_ERROR( nxs_file_->CloseAllGroups() );
    }
    else
#endif
        if ( config_.properties.datafile_activation && ascii_file_ )
        {
            tm::ptime current_ptime = tm::microsec_clock::local_time();

            //- extract the offset in the day (hour/min/sec/millisec)
            tm::time_duration current_tod = current_ptime.time_of_day();

            //- build the hour string as for example : 01h23m45.678
            std::ostringstream oss;
            oss << std::setw(2) << std::setfill('0') << current_tod.hours()
                << 'h'
                << std::setw(2) << std::setfill('0') << current_tod.minutes()
                << 'm'
                << std::setw(2) << std::setfill('0') << current_tod.seconds()
                << '.'
                << std::setw(3) << std::setfill('0') << current_tod.total_milliseconds() - 1000 * current_tod.total_seconds();

            //- write last line containing the date of the end of scan
            *ascii_file_ << "# "
                         << dt::to_simple_string( current_ptime.date() )
                         << " "
                         << oss.str()
                         << std::endl;


            ascii_file_.reset(); // will Flush & Close the ASCII file
        }
}

void DataRecorder::end_run( void )
{
#ifdef SCANSL_ENABLE_NEXUS

    bool is_recording_session = false;
    if( datarecorder_proxy_ && config_.properties.is_recording_manager )
    {
        if ( !config_.datarecorder_only_write_scan_data )
        {
            SCAN_INFO << "Data recording: EndRecording" << ENDLOG;
            datarecorder_proxy_->command_inout("EndRecording");

            Tango::DeviceAttribute da = datarecorder_proxy_->read_attribute("recordingSession");
            da >> is_recording_session;
        }
        else
            datarecorder_step_ = 5;
    }

    if ( datarecorder_proxy_ && is_recording_session
         && datarecorder_proxy_->state() != Tango::FAULT
         && datarecorder_step_ >= 1
         && datarecorder_step_ != 5 )
    {
        while( is_recording_session )
        {
            Tango::DeviceAttribute da = datarecorder_proxy_->read_attribute("recordingSession");
            da >> is_recording_session;
            if( is_recording_session )
                // sleep 50 ms
                yat::ThreadingUtilities::sleep(0,50000000);
            else
                break;
        }
    }
    // Using deprecated DataRecorder device
    else if ( datarecorder_proxy_
         && (datarecorder_proxy_->state() == Tango::RUNNING || datarecorder_proxy_->state() == Tango::ALARM)
         && datarecorder_step_ >= 1
         && datarecorder_step_ != 5 )
    {
        if ( !config_.datarecorder_only_write_scan_data )
        {
            SCAN_INFO << "Data recording: EndRecording" << ENDLOG;
            datarecorder_proxy_->command_inout( "EndRecording" );

            while( !(datarecorder_proxy_->state() == Tango::ON || datarecorder_proxy_->state() == Tango::ALARM) )
            {
                // sleep 50 ms
                yat::ThreadingUtilities::sleep(0,50000000);
            }
        }
        datarecorder_step_ = 5;
    }
    else if ( config_.properties.datafile_activation && nxs_file_ )
    {
        nxs_file_.reset(); // will Flush & Close the NexusFile
        SCAN_DEBUG << "Nexus file closed successfully" << ENDLOG;
    }
#endif
}

#ifdef SCANSL_ENABLE_NEXUS

void DataRecorder::synchronize()
{
    if (datarecorder_proxy_)
    {
        if( config_.properties.is_recording_manager )
        {
            bool recording_in_progress = true;
            while( recording_in_progress && datarecorder_proxy_->state() != Tango::FAULT )
            {
                Tango::DeviceAttribute da = datarecorder_proxy_->read_attribute("recording");
                da >> recording_in_progress;
                if( recording_in_progress )
                    // sleep 50 ms
                    yat::ThreadingUtilities::sleep(0,50000000);
                else
                    break;
            }

        }
        else // Using deprecated DataRecorder device
        {
            while( datarecorder_proxy_->state() == Tango::MOVING )
            {
                // sleep 50 ms
                yat::ThreadingUtilities::sleep(0,50000000);
            }
        }
    }
}
#endif

}

