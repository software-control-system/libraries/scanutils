#ifndef _SCANSERVER_SCANCONFIG_H
#define _SCANSERVER_SCANCONFIG_H

#include <string>
#include <vector>
#include <map>
#include <tango.h>
#include <scansl/ScanSL.h>
#include <scansl/DevProxies.h>
#include <boost/smart_ptr.hpp>
#include <boost/multi_array.hpp>


namespace ScanUtils
{

template <typename T>
struct ScanArray_Base
{
    typedef boost::multi_array<T, 1> Buf;
    typedef boost::multi_array<T, 2> Img;

    typedef boost::shared_ptr<Buf> BufP;
    typedef boost::shared_ptr<Img> ImgP;
};

template <typename T>
struct ScanArray : public ScanArray_Base<T>
{
};

template <>
struct ScanArray<Tango::DevShort> : public ScanArray_Base<Tango::DevShort>
{
    typedef Tango::DevVarShortArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevLong> : public ScanArray_Base<Tango::DevLong>
{
    typedef Tango::DevVarLongArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevDouble> : public ScanArray_Base<Tango::DevDouble>
{
    typedef Tango::DevVarDoubleArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevFloat> : public ScanArray_Base<Tango::DevFloat>
{
    typedef Tango::DevVarFloatArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevBoolean> : public ScanArray_Base<Tango::DevBoolean>
{
    typedef Tango::DevVarBooleanArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevUShort> : public ScanArray_Base<Tango::DevUShort>
{
    typedef Tango::DevVarUShortArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevULong> : public ScanArray_Base<Tango::DevULong>
{
    typedef Tango::DevVarULongArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevUChar> : public ScanArray_Base<Tango::DevUChar>
{
    typedef Tango::DevVarCharArray_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevLong64> : public ScanArray_Base<Tango::DevLong64>
{
    typedef Tango::DevVarLong64Array_var DevVarArray_var;
};

template <>
struct ScanArray<Tango::DevULong64> : public ScanArray_Base<Tango::DevULong64>
{
    typedef Tango::DevVarULong64Array_var DevVarArray_var;
};

typedef boost::multi_array_types::index_range range;

typedef enum
{
    ActuatorDim_1,
    ActuatorDim_2,
    ActuatorDim_MAX_
} ActuatorDim;

typedef enum {
    HookType_PRE_RUN,
    HookType_PRE_SCAN,
    HookType_PRE_STEP,
    HookType_POST_ACTUATOR_MOVE,
    HookType_POST_INTEGRATION,
    HookType_POST_STEP,
    HookType_POST_SCAN,
    HookType_POST_RUN,
    HookType_MAX_
} HookType;

typedef enum {
    ErrorStrategy_IGNORE,
    ErrorStrategy_PAUSE,
    ErrorStrategy_ABORT,
    ErrorStrategy_MAX_
} ErrorStrategy;

typedef enum {
    CtxValidBehaviour_IGNORE,
    CtxValidBehaviour_PAUSE,
    CtxValidBehaviour_ABORT,
    CtxValidBehaviour_MAX_
} CtxValidBehaviour;

typedef enum {
    AfterRunActionType_NO_ACTION,
    AfterRunActionType_FIRST,
    AfterRunActionType_PRIOR,
    AfterRunActionType_PEAK,
    AfterRunActionType_VALLEY,
    AfterRunActionType_MAX_OF_DERIVATIVE,
    AfterRunActionType_MIN_OF_DERIVATIVE,
    AfterRunActionType_CENTER_OF_MASS,
    //    AfterRunActi5onType_CENTER_OF_GAUSSIAN_FIT,
    //    AfterRunActionType_CENTER_OF_GAUSSIAN_FIT_BG,
    AfterRunActionType_GOTO_POS,
    AfterRunActionType_MAX_
} AfterRunActionType;

typedef enum {
    DataFileType_ASCII,
#ifdef SCANSL_ENABLE_NEXUS
    DataFileType_NEXUS,
#endif
    DataFileType_MAX_
} DataFileType;

#ifdef SCANSL_ENABLE_NEXUS
typedef enum {
    DataRecorderWriteMode_DONE_BY_DATARECORDER, // call WriteScanData
    DataRecorderWriteMode_DONE_BY_SCANSERVER,   // use NexusCpp & Nexus4Tango
    DataRecorderWriteMode_MAX_
} DataRecorderWriteMode;
#endif

struct ErrorManagement
{
    ErrorManagement();

    std::string strategy_name( void ) const;

    double        timeout_s;
    long          retry_count;
    double        retry_timeout_s;
    ErrorStrategy strategy;
};

struct SCANSL_DECL AfterRunActionDesc
{
    AfterRunActionDesc();

    AfterRunActionType type;
    long sensor;
    long actuator;
    double actuator_value;
};

struct SCANSL_DECL ScanProperties
{
    ScanProperties();

    //- Plugins directory
    std::string   plugin_root_path;
    //-Enable/Disable synchronize data between Timebase & Sensor
    bool          synchronize_data;
    //- data file created by scanserver
    bool          datafile_activation;
    DataFileType  datafile_type;
    std::string   datafile_path;
    std::string   datafile_basename;
    std::string   datafile_extension;
    bool          timescan_actuators_delay;
    std::size_t   Ni6602_start_timebase_delay;

    // if false (default value) one command is called for each attribute of each device
    // if true the scanserver will call one single command to read the all attributes for a single device
    bool read_attribute_async;

    // If some sensor read failures occure during the scan the scanserver will set its final state
    // to FAULT if an exception is thrown
    bool no_throw_on_sensor_error;

    // If true the failed sensor is not disabled until the end of the scan
    bool keep_sensor_on_failure;

    // If true the quality of attributes value is checked
    bool check_attribute_quality;

    // Execute the AfterRun action in case of an error occure during the scan and then abort the scan process
    bool execute_after_run_action_on_abort;

    // Use the file system lock with Nexus files
    bool use_recorder_proxy;

    // If true then moves the 2nd dimension's actuators after the 1st dimension's actuators
    bool move_dim1_then_dim2_actuators;

    // If true than dont't call IncAcquisitionIndex & IncExperimentIndex on the DataRecorder device at first scan
    bool no_inc_indexes_at_first_scan;

    // Apply (or no) acquisitionName sanitization
    bool acquisitionname_sanitization;

#ifdef SCANSL_ENABLE_NEXUS
    //- data file created by DataRecorder device
    DataRecorderWriteMode datarecorder_write_mode;
    double                datarecorder_saving_period; // period at which to call DataRecorder->WriteScanData
    std::string           datarecorder;  // DataRecorder device name
    std::string           my_devicename; // ScanServer device name
    bool                  is_recording_manager;
#endif

    std::string           datafitter;  // DataFitter device name

    Tango::DeviceImpl* device;
};

struct SCANSL_DECL ScanConfig
{
    ScanConfig();

    ScanConfig( const ScanConfig& c );

    ScanConfig& operator= ( const ScanConfig& c );

    void clean()
    throw (Tango::DevFailed);

    //- check that all parameters fit well together
    void validate()
    throw (Tango::DevFailed);

    ScanProperties properties;

    std::string run_name;

    bool on_the_fly;
    bool hw_continuous;
    bool manual_mode;

    std::vector< std::string > actuators;
    std::vector< std::string > actuators2;
    std::vector< std::string > timebases;
    std::vector< std::string > sensors;

    // Additionnal parameters information to be written into the NeXus file
    std::vector< std::string > info;

    ScanArray<double>::Buf integration_times;
    ScanArray<double>::Img trajectories;
    ScanArray<double>::Img trajectories2;

    bool   enable_scan_speed;
    ScanArray<double>::Buf scan_speed;

    long scan_number;

    long hw_continuous_nbpt;

    std::vector< std::string > prerun_hooks;
    std::vector< std::string > prescan_hooks;
    std::vector< std::string > prestep_hooks;
    std::vector< std::string > postactuatormove_hooks;
    std::vector< std::string > postintegration_hooks;
    std::vector< std::string > poststep_hooks;
    std::vector< std::string > postscan_hooks;
    std::vector< std::string > postrun_hooks;

    ErrorManagement actuators_mngt;
    ErrorManagement timebases_mngt;
    ErrorManagement sensors_mngt;
    ErrorManagement hooks_mngt;

    std::string context_validation;
    CtxValidBehaviour ctxvalid_behaviour;

    double actuators_delay;
    double sensors_delay;
    double timebases_delay;

    bool automatic_direction;
    bool zig_zag;
    bool synchronous_dimension_change;

    AfterRunActionDesc after_run_action;

#ifdef SCANSL_ENABLE_NEXUS
    bool datarecorder_only_write_scan_data;
    bool record_data;
    std::string data_recorder_config;
#endif
};


}

#endif
