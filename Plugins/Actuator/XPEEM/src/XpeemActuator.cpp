/*!
 * \file
 * \brief    Definition of XpeemActuator class
 * \author   Sandra PIERRE-JOSEPH ZEPHIR - Synchrotron SOLEIL
 */

#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class XpeemActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "XpeemActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
    list.push_back("XpeemMicroscope");
	list.push_back("XpeemSamplePosition");
    }
};

class XpeemActuator : public WaitingStateChangeActuator
{
public:
    XpeemActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::ALARM);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop"; //pas de stop 
    }
	
    void init( std::string device_name )
    {
        std::string device_class = Util::instance().get_class( device_name );
        if (device_class == "XpeemMicroscope")
        {
            // no Stop command for "XpeemMicroscope"
            config_.abort_cmd_name = "";
        }

        this->WaitingStateChangeActuator::init( device_name );
    }

    // ============================================================================
    // XpeemActuator::check_initial_condition
    // ============================================================================
    void check_initial_condition( std::string /*attr_name*/ )
    {
        //- check the state
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
        {
            std::ostringstream oss;
            oss << "The device "
                << config_.dev_name
                << " must be STANDBY or ALARM to move";

            THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "XpeemActuator::check_initial_condition");
        }
    }

    // ============================================================================
    // XpeemActuator::set_position
    // ============================================================================
   void set_position( std::string attr_name, double position )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
	if (device_class == "XpeemMicroscope") {
		Tango::DeviceData offset;
        	offset << position;
        	proxy_->command_inout("ComputeNewOffset", offset);
	}else {
            WaitingStateChangeActuator::set_position(attr_name, position);
        }

    }

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::XpeemActuator, ScanUtils::XpeemActuatorInfo);
