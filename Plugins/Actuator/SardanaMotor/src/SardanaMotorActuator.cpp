/*!
 * \file
 * \brief    Definition of SardanaMotorActuator class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "SardanaMotorActuator.h"
#include <yat/plugin/PlugInSymbols.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::SardanaMotorActuator, \
                          ScanUtils::SardanaMotorActuatorInfo);

namespace ScanUtils
{
const std::string PlugInID                ( "SardanaMotorActuator" );
const std::string VersionNumber           ( "1.0.0" );


// ============================================================================
// SardanaMotorActuatorInfo::get_plugin_id
// ============================================================================
std::string SardanaMotorActuatorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// SardanaMotorActuatorInfo::get_interface_name
// ============================================================================
std::string SardanaMotorActuatorInfo::get_interface_name() const
{
    return IPolledActuatorInterfaceName;
}

// ============================================================================
// SardanaMotorActuatorInfo::get_version_number
// ============================================================================
std::string SardanaMotorActuatorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// SardanaMotorActuatorInfo::supported_classes
// ============================================================================
void SardanaMotorActuatorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back("Motor");
}



// ============================================================================
// SardanaMotorActuator::SardanaMotorActuator
// ============================================================================
SardanaMotorActuator::SardanaMotorActuator()
{
    config_.move_state        = Tango::MOVING;
    config_.error_states.push_back(Tango::FAULT);
    config_.polling_period_ms = 50;
    config_.abort_cmd_name    = "Abort";
}

}
