/*!
 * \file
 * \brief    Definition of GalilActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <scansl/Util.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <boost/lexical_cast.hpp>

namespace ScanUtils
{

class GalilActuator : public WaitingStateChangeActuator
{
public:
    GalilActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::ALARM); //- Alarms mean limit switch (from F Picca)
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::INIT ); //- means motor position is not known TANGODEVIC-1341
        config_.error_states.push_back(Tango::DISABLE);  // For InVacuum motors
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Stop";
    }

    void init( std::string device_name )
    {
        std::string device_class = Util::instance().get_class( device_name );
        if (device_class == "GalilPiezoAna")
        {
            // no Stop command for "GalilPiezoAna"
            config_.abort_cmd_name = "";
        }

        this->WaitingStateChangeActuator::init( device_name );
    }

    // ============================================================================
    // GalilActuator::check_initial_condition
    // ============================================================================
    void check_initial_condition( std::string /*attr_name*/ )
    {
        //- check the state
        Tango::DevState dev_state = proxy_->state();
        if ( dev_state != Tango::STANDBY && dev_state != Tango::ALARM )
        {
            std::ostringstream oss;
            oss << "The device "
                << config_.dev_name
                << " must be STANDBY or ALARM to move";

            THROW_YAT_ERROR("SOFTWARE_FAILURE",
                            oss.str().c_str(),
                            "GalilActuator::check_initial_condition");
        }
    }

    /*void error_check( void )
  {
      SCAN_INFO << "---------> FL: Plugin: error_check " << ENDLOG;
  }*/

    // ============================================================================
    // GalilActuator::set_position
    // ============================================================================
    void set_position( std::string attr_name, double position )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        Tango::DeviceData offset;
        offset << position;
		// Note PMACAxis is for SIRIUS monochromator
        if (device_class == "GalilAxis" || device_class == "PMACAxis") {
            proxy_->command_inout("ComputeNewOffset", offset);
        } else if (device_class == "GalilSlit" && attr_name == "gap") {
            proxy_->command_inout("ComputeNewGapOffset", offset);
        } else if (device_class == "GalilSlit" && attr_name == "position") {
            proxy_->command_inout("ComputeNewPositionOffset", offset);
        } else {
            WaitingStateChangeActuator::set_position(attr_name, position);
        }
    }

    /// TODO implement is_enabled

    // ============================================================================
    // GalilActuator::set_enabled
    // ============================================================================
    void set_enabled( std::string attr_name, bool enabled )

    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        if (device_class == "GalilAxis" || device_class == "GalilSlit"||device_class == "PMACAxis") {
            // atr_name unused
            proxy_->command_inout(enabled ? "On" : "Off");
        } else {
            WaitingStateChangeActuator::set_enabled(attr_name, enabled);
        }
    }

    // ============================================================================
    // GalilActuator::get_limits
    // ============================================================================
    LimitsType get_limits( std::string attr_name )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        if (device_class == "MCSAxis") {
        	/// TODO test
            try {
                double lower, upper;
                Tango::DeviceAttribute lowerAttr = proxy_->read_attribute("softLimitMin");
                Tango::DeviceAttribute upperAttr = proxy_->read_attribute("softLimitMax");
                lowerAttr >> lower;
                upperAttr >> upper;
                return LimitsType(
                    boost::lexical_cast<std::string>(lower),
                    boost::lexical_cast<std::string>(upper)
                );
            } catch (const boost::bad_lexical_cast& err) {
                THROW_YAT_ERROR("BAD_CAST",
                                err.what(),
                                "GalilActuator::get_limits");
            }
        } else {
            return WaitingStateChangeActuator::get_limits(attr_name);
        }
    }

    // ============================================================================
    // GalilActuator::set_limits
    // ============================================================================
    void set_limits( std::string attr_name, const LimitsType& limits )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );
        if (device_class == "MCSAxis") {
            /// TODO test
            try {
                std::vector<Tango::DeviceAttribute> attrs;
                attrs.push_back(Util::instance().create_device_attribute(
                    "softLimitMin",
                    Tango::DEV_DOUBLE,
                    boost::lexical_cast<double>(limits.first)
                ));
                attrs.push_back(Util::instance().create_device_attribute(
                    "softLimitMax",
                    Tango::DEV_DOUBLE,
                    boost::lexical_cast<double>(limits.second)
                ));
                proxy_->write_attributes(attrs);
            } catch (const boost::bad_lexical_cast& err) {
                THROW_YAT_ERROR("BAD_CAST",
                                err.what(),
                                "GalilActuator::set_limits");
            }
        } else {
            WaitingStateChangeActuator::set_limits(attr_name, limits);
        }
    }

    // ============================================================================
    // GalilActuator::get_speed
    // ============================================================================
    double get_speed( std::string attr_name )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );

        if ( (attr_name == "position" || attr_name == "masterposition" )  && is_speed_possible(device_class))
        {
        	// Special case for GalilGearedAxes :SCAN-743
       	std::string velocity_attribute_name;
       	if (device_class == "GalilGearedAxes")
        		velocity_attribute_name="masterVelocity";
        	else
        		velocity_attribute_name="velocity";

            Tango::DeviceAttribute velocity_attr = proxy_->read_attribute(velocity_attribute_name );
            double velocity;
            velocity_attr >> velocity;
            return velocity;
        }
        else
        {
            return 0;
        }
    }

    // ============================================================================
    // GalilActuator::set_speed
    // ============================================================================
    void set_speed( std::string attr_name, double speed )
    {
        std::string device_class = Util::instance().get_class( config_.dev_name );


        if ( (attr_name == "position" ||attr_name == "masterposition" )  && is_speed_possible(device_class))
        {
        	// Special case for GalilGearedAxes :SCAN-743
       	std::string velocity_attribute_name;
           	if (device_class == "GalilGearedAxes")
            		velocity_attribute_name="masterVelocity";
            	else
            		velocity_attribute_name="velocity";

            SCAN_DEBUG << "Setting speed of " << config_.dev_name << " to " << speed << ENDLOG;
            Tango::DeviceAttribute velocity_attr(velocity_attribute_name, speed);
            proxy_->write_attribute( velocity_attr );
        }
    }

private :
    bool is_speed_possible(std::string device_class)
    {
    	return (device_class == "GalilAxis" || device_class == "SimulatedMotor" ||
                device_class == "PMACAxis"|| device_class == "GalilGearedAxes");
    }
};

}
