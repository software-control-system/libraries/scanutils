file(GLOB_RECURSE sources src/*.cpp)
set(includedirs src)

add_library(OpticsActuator MODULE ${sources})
target_include_directories(OpticsActuator PRIVATE ${includedirs})
target_link_libraries(OpticsActuator PRIVATE
    scansl
    yat4tango::yat4tango
    boostlibraries::boostlibraries
)

if(PROJECT_VERSION)
    set_target_properties(OpticsActuator PROPERTIES OUTPUT_NAME "OpticsActuator-${PROJECT_VERSION}")
endif()

install(TARGETS OpticsActuator LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
