/*!
* \file
* \brief    Definition of SmartWLISensor class
* \author   Florent Langlois - Synchrotron SOLEIL
*/


#include <boost/scoped_ptr.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class SmartWLISensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "SmartWLISensor";
    }

    virtual std::string get_interface_name(void) const
    {
        return ISensorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("SmartWLI");
    }
};



class SmartWLISensor : public Sensor
{
public: //! structors

    SmartWLISensor()
    {
    }

public: //! ISensor implementation

    virtual void init( std::string device_name,bool sync )
    {
 	    //--------------------------------------------------
            //- Get the device proxy and device_name storage
            Sensor::init( device_name, sync );
    }

    virtual void before_integration( void )
    {
        //- Refresh State
        proxy_->state();
        //- Start
        proxy_->command_inout("StartMeasureScan");
        //- Wait state Moving
        Tango::DevState state = Tango::STANDBY;
        while( state != Tango::MOVING )
        {
            state = proxy_->state();
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
        }
    }

    virtual void after_integration( void )
    {
        
        //- Wait end of Moving State
        Tango::DevState state = Tango::MOVING;
        while( state == Tango::MOVING )
        {
            state = proxy_->state();
            // wait 10 msec
            yat::ThreadingUtilities::sleep( 0, 10000000 );
        }
    }

    virtual void abort( void )
    {
        // TODO
    }

};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::SmartWLISensor,
                          ScanUtils::SmartWLISensorInfo);
