#include <scansl/Progress.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat/CommonHeader.h>

namespace ScanUtils
{
Progress::Progress()
    : scan_completion_percent(0),
      run_completion_percent(0),
      scan_duration_sec(0),
      run_duration_sec(0),
      dead_time_sec(0),
      dead_time_per_pt_ms(0),
      dead_time_percent(0)
{
    const std::string unknown( "unknown" );
    this->set_run_start_date( unknown );
    this->set_scan_start_date( unknown );
    this->set_scan_end_date( unknown );
    this->set_run_end_date( unknown );
    this->set_scan_duration( unknown );
    this->set_run_duration( unknown );
    this->set_scan_remaining_time( unknown );
    this->set_run_remaining_time( unknown );
    this->set_scan_elapsed_time( unknown );
    this->set_run_elapsed_time( unknown );
}

Progress::Progress( const Progress& p )
{
    *this = p;
}

Progress& Progress::operator=( const Progress& p )
{
    this->scan_completion_percent = p.scan_completion_percent;
    this->run_completion_percent = p.run_completion_percent;
    this->scan_duration_sec = p.scan_duration_sec;
    this->run_duration_sec = p.run_duration_sec;
    this->dead_time_sec = p.dead_time_sec;
    this->dead_time_per_pt_ms = p.dead_time_per_pt_ms;
    this->dead_time_percent = p.dead_time_percent;

    this->set_run_start_date( p.run_startdate_ );
    this->set_scan_start_date( p.scan_startdate_ );
    this->set_scan_end_date( p.scan_enddate_ );
    this->set_run_end_date( p.run_enddate_ );
    this->set_scan_duration( p.scan_duration_ );
    this->set_run_duration( p.run_duration_ );
    this->set_scan_remaining_time( p.scan_remaining_time_ );
    this->set_run_remaining_time( p.run_remaining_time_ );
    this->set_scan_elapsed_time( p.scan_elapsed_time_ );
    this->set_run_elapsed_time( p.run_elapsed_time_ );

    return *this;
}


void Progress::set_run_start_date( const std::string& s )
{
    run_startdate_ = s;
    run_startdate_c = run_startdate_.c_str();
}

void Progress::set_scan_start_date( const std::string& s )
{
    scan_startdate_ = s;
    scan_startdate_c = scan_startdate_.c_str();
}

void Progress::set_scan_end_date( const std::string& s )
{
    scan_enddate_ = s;
    scan_enddate_c = scan_enddate_.c_str();
}

void Progress::set_run_end_date( const std::string& s )
{
    run_enddate_ = s;
    run_enddate_c = run_enddate_.c_str();
}

void Progress::set_scan_duration( const std::string& s )
{
    scan_duration_ = s;
    scan_duration_c = scan_duration_.c_str();
}

void Progress::set_run_duration( const std::string& s )
{
    run_duration_ = s;
    run_duration_c = run_duration_.c_str();
}

void Progress::set_scan_remaining_time( const std::string& s )
{
    scan_remaining_time_ = s;
    scan_remaining_time_c = scan_remaining_time_.c_str();
}

void Progress::set_run_remaining_time( const std::string& s )
{
    run_remaining_time_ = s;
    run_remaining_time_c = run_remaining_time_.c_str();
}

void Progress::set_scan_elapsed_time( const std::string& s )
{
    scan_elapsed_time_ = s;
    scan_elapsed_time_c = scan_elapsed_time_.c_str();
}

void Progress::set_run_elapsed_time( const std::string& s )
{
    run_elapsed_time_ = s;
    run_elapsed_time_c = run_elapsed_time_.c_str();
}

ProgressManager::ProgressManager( const ScanConfig& c )
{
    if ( !c.on_the_fly && !c.hw_continuous )
    {
        SCAN_DEBUG << "step_by_step: new StepScanProgressManager" << ENDLOG;
        pimpl.reset( new StepScanProgressManager(c) );
    }
    else if( c.hw_continuous )
    {
        SCAN_DEBUG << "hw_continuous: new ContinuousScanProgressManager" << ENDLOG;
        pimpl.reset( new ContinuousScanProgressManager(c) );
    }
    else if( c.on_the_fly )
    {
        SCAN_DEBUG << "on_the_fly: new StepScanProgressManager" << ENDLOG;
        //- Normally should be new ContinuousScanProgressManager
        //- but for simplicity we use the same progress measures as Step
        pimpl.reset( new StepScanProgressManager(c) );
    }
}

Progress ProgressManager::get_progress()
{
    return pimpl->get_progress();
}


void ProgressManager::run_start()
{
    pimpl->run_start();
}

void ProgressManager::scan_start()
{
    pimpl->scan_start();
}

void ProgressManager::step_end()
{
    pimpl->step_end();
}

void ProgressManager::line_start()
{
    pimpl->line_start();
}

void ProgressManager::line_end()
{
    pimpl->line_end();
}


ConcreteProgressManager::ConcreteProgressManager( const ScanConfig& c )
    : config_(c)
{

}

ConcreteProgressManager::~ConcreteProgressManager()
{
}

Progress ConcreteProgressManager::get_progress()
{
    yat::MutexLock guard(progress_mutex_);
    return progress_;
}

void ConcreteProgressManager::run_start()
{
}

void ConcreteProgressManager::scan_start()
{
}

void ConcreteProgressManager::step_end()
{
}

void ConcreteProgressManager::line_start()
{
}

void ConcreteProgressManager::line_end()
{
}








StepScanProgressManager::StepScanProgressManager( const ScanConfig& c )
    : ConcreteProgressManager(c),
      nb_line_per_scan_(0),
      completed_step_(0),
      completed_line_(0),
      current_scan_(0),
      integration_time_sum_(0)
{

    size_t nb_int_time = config_.integration_times.shape()[0];
    nb_line_per_scan_ = config_.actuators2.size() ? config_.trajectories2.shape()[1] : 1;
    nb_step_per_scan_ = nb_int_time * nb_line_per_scan_;
}


void StepScanProgressManager::run_start()
{
    yat::MutexLock guard(progress_mutex_);
    run_start_ = tm::microsec_clock::local_time();

    //- for display, remove fractional seconds
    tm::ptime run_start_display = tm::ptime( run_start_.date(),
                                             tm::seconds(run_start_.time_of_day().total_seconds()) );
    progress_.set_run_start_date( tm::to_simple_string( run_start_display ) );

    last_step_end_ = tm::microsec_clock::local_time();
}

void StepScanProgressManager::scan_start()
{
    yat::MutexLock guard(progress_mutex_);
    scan_start_ = tm::microsec_clock::local_time();

    //- for display, remove fractional seconds
    tm::ptime scan_start_display = tm::ptime( scan_start_.date(),
                                              tm::seconds(scan_start_.time_of_day().total_seconds()) );
    progress_.set_scan_start_date( tm::to_simple_string( scan_start_display ) );

    completed_step_ = 0;
    completed_line_ = 0;
    current_scan_++;
    integration_time_sum_ = 0;

    if ( current_scan_ == 2 )
        first_scan_duration_ = scan_start_ - run_start_;
}

//#define DEBUG_PROGRESS

#ifdef DEBUG_PROGRESS
#  define LOG(  p ) SCAN_DEBUG << p << ENDLOG
#  define LOGG( p ) SCAN_DEBUG << #p << " : " << p << ENDLOG
#else
#define LOG( p )
#define LOGG( p )
#endif

void StepScanProgressManager::step_end()
{
    yat::MutexLock guard(progress_mutex_);
    tm::ptime now = tm::microsec_clock::local_time();
    tm::time_duration step_duration = now - last_step_end_;

    tm::time_duration scan_duration,
            run_duration;
    tm::ptime         scan_end,
            run_end;

    double integration_time = config_.integration_times[ completed_step_ % config_.integration_times.shape()[0] ];
    integration_time_sum_ += integration_time;

    completed_step_++;
    LOGG( completed_step_ );
    LOGG( config_.integration_times.num_elements() );

    //- store step_duration at the right place
    if (completed_step_ == 1)
    {
        if (current_scan_ == 1)
        {
            run_first_step_duration_ = step_duration;

            LOGG(run_first_step_duration_);
        }
        else
        {
            move_to_begin_of_scan_.add(step_duration);

            scan_duration = move_to_begin_of_scan_.get()
                    + move_to_begin_of_line_.get() * (nb_line_per_scan_ - 1)
                    + inner_step_duration_.get() * (nb_step_per_scan_ - nb_line_per_scan_);

            run_duration = first_scan_duration_ + scan_duration * (config_.scan_number - 1);
            LOGG(move_to_begin_of_scan_.get());
            LOGG(inner_step_duration_.get());
            LOGG(scan_duration);
            LOGG(run_duration);
        }
    }
    else if ( (completed_step_ - 1) % config_.integration_times.shape()[0] == 0 )
    {
        completed_line_++;
        LOGG( completed_line_ );
        move_to_begin_of_line_.add( step_duration );

        tm::time_duration first_step_of_scan;
        first_step_of_scan = current_scan_ == 1 ? run_first_step_duration_ : move_to_begin_of_scan_.get();
        scan_duration = first_step_of_scan
                + move_to_begin_of_line_.get() * (nb_line_per_scan_ - 1)
                + inner_step_duration_.get() * (nb_step_per_scan_ - nb_line_per_scan_);

        if (current_scan_ > 1)
            run_duration = first_scan_duration_ + scan_duration * (config_.scan_number - 1);
        else
            run_duration = scan_duration * config_.scan_number;

        LOGG(move_to_begin_of_line_.get());
        LOGG(first_step_of_scan);
        LOGG(inner_step_duration_.get());
        LOGG(scan_duration);
        LOGG(run_duration);

    }
    else
    {
        //- this is an inner-scan step
        inner_step_duration_.add( step_duration );

        tm::time_duration int_time = tm::microseconds( static_cast<long>(integration_time * 1E6) );

        LOG( step_duration - int_time );
        dead_time_per_pt_.add( step_duration - int_time );
        //      LOGG( dead_time_per_pt_.n );

        tm::time_duration first_step_of_scan;
        first_step_of_scan = current_scan_ == 1 ? run_first_step_duration_ : move_to_begin_of_scan_.get();
        tm::time_duration return_to_begin_of_line_step;
        return_to_begin_of_line_step = completed_line_ == 0 ? inner_step_duration_.get() : move_to_begin_of_line_.get();

        scan_duration = first_step_of_scan
                + return_to_begin_of_line_step * (nb_line_per_scan_ - 1)
                + inner_step_duration_.get() * (nb_step_per_scan_ - nb_line_per_scan_);

        if (current_scan_ > 1)
            run_duration = first_scan_duration_ + scan_duration * (config_.scan_number - 1);
        else
            run_duration = scan_duration * config_.scan_number;

        LOGG(move_to_begin_of_line_.get());
        LOGG(first_step_of_scan);
        LOGG(inner_step_duration_.get());
        LOGG(scan_duration);
        LOGG(run_duration);
    }


    if ( completed_step_ != 1 || current_scan_ != 1 )
    {

        tm::time_duration scan_elapsed = now - scan_start_;
        tm::time_duration run_elapsed = now - run_start_;

        if (scan_elapsed > scan_duration || completed_step_ == nb_step_per_scan_)
            scan_duration = scan_elapsed;
        if ( (run_elapsed > run_duration)
             || ((completed_step_ == nb_step_per_scan_) && (current_scan_ == config_.scan_number)) )
        {
            run_duration = run_elapsed;
        }

        //- update scan_duration/run_duration/scan_end/run_end
        scan_end = scan_start_ + scan_duration;
        run_end = run_start_ + run_duration;

        //- for display, remove fractional seconds
        tm::ptime scan_end_display = tm::ptime( scan_end.date(),
                                                tm::seconds(scan_end.time_of_day().total_seconds()) );
        tm::ptime run_end_display = tm::ptime( run_end.date(),
                                               tm::seconds(run_end.time_of_day().total_seconds()) );

        progress_.dead_time_per_pt_ms = dead_time_per_pt_.get().total_microseconds() * 1E-3;
        progress_.dead_time_percent   = 100 * progress_.dead_time_per_pt_ms / (inner_step_duration_.get().total_microseconds() * 1E-3);

        progress_.set_scan_end_date( tm::to_simple_string( scan_end_display ) );
        progress_.set_run_end_date( tm::to_simple_string( run_end_display ) );

        progress_.set_scan_duration( tm::to_simple_string( tm::seconds(scan_duration.total_seconds()) ) );
        progress_.set_run_duration( tm::to_simple_string( tm::seconds(run_duration.total_seconds()) ) );

        tm::time_duration scan_remaining = scan_duration - scan_elapsed;
        tm::time_duration run_remaining = run_duration - run_elapsed;
        progress_.set_scan_remaining_time( tm::to_simple_string(tm::seconds(scan_remaining.total_seconds())) );
        progress_.set_run_remaining_time( tm::to_simple_string(tm::seconds(run_remaining.total_seconds())) );
        progress_.set_scan_elapsed_time( tm::to_simple_string(tm::seconds(scan_elapsed.total_seconds())) );
        progress_.set_run_elapsed_time( tm::to_simple_string(tm::seconds(run_elapsed.total_seconds())) );

        progress_.scan_duration_sec = scan_duration.total_seconds() + scan_duration.total_milliseconds() * 1E-3;
        progress_.run_duration_sec  = run_duration.total_seconds() + run_duration.total_milliseconds() * 1E-3;

        // Progress indicators shouldn't be double values, so we calculate integer values
        progress_.scan_completion_percent = int((100.0 * scan_elapsed.total_microseconds()) / scan_duration.total_microseconds() + 0.5);
        progress_.run_completion_percent  = int((100.0 * run_elapsed.total_microseconds()) / run_duration.total_microseconds() + 0.5);
    }


    last_step_end_ = now;
}

void StepScanProgressManager::line_start()
{
}

void StepScanProgressManager::line_end()
{
}


//==================================================================================================
// ContinuousScanProgressManager
//==================================================================================================

//------------------------------------------------------------------------
// ContinuousScanProgressManager::ContinuousScanProgressManager
//------------------------------------------------------------------------
ContinuousScanProgressManager::ContinuousScanProgressManager( const ScanConfig& config )
    : ConcreteProgressManager(config),
      nb_line_per_scan_(0),
      completed_step_(0),
      completed_line_(0),
      current_scan_(0)
{
    nb_line_per_scan_ = config_.actuators2.size() ? config_.trajectories2.shape()[1] : 1;
    int_time_ = tm::microseconds( static_cast<long>(config_.integration_times[ 0 ] * 1E6) );
    scan_duration_  = int_time_ * config.hw_continuous_nbpt * nb_line_per_scan_;
    theoric_line_duration_  = int_time_ * config.hw_continuous_nbpt;
    run_duration_ = scan_duration_ * config_.scan_number;
}


//------------------------------------------------------------------------
// ContinuousScanProgressManager::run_start
//------------------------------------------------------------------------
void ContinuousScanProgressManager::run_start()
{
    yat::MutexLock guard(progress_mutex_);
    run_start_ = tm::microsec_clock::local_time();

    //- for display, remove fractional seconds
    tm::ptime run_start_display = tm::ptime( run_start_.date(),
                                             tm::seconds(run_start_.time_of_day().total_seconds()) );
    progress_.set_run_start_date( tm::to_simple_string( run_start_display ) );
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::scan_start
//------------------------------------------------------------------------
void ContinuousScanProgressManager::scan_start()
{
    yat::MutexLock guard(progress_mutex_);
    scan_start_ = tm::microsec_clock::local_time();

    //- for display, remove fractional seconds
    tm::ptime scan_start_display = tm::ptime( scan_start_.date(),
                                              tm::seconds(scan_start_.time_of_day().total_seconds()) );
    progress_.set_scan_start_date( tm::to_simple_string( scan_start_display ) );

    completed_line_ = 0;
    current_scan_++;
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::step_end
//------------------------------------------------------------------------
void ContinuousScanProgressManager::step_end()
{
    set_values();
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::line_start
//------------------------------------------------------------------------
void ContinuousScanProgressManager::line_start()
{
    tm::ptime now = tm::microsec_clock::local_time();
    last_line_start_ = now;

    // Mean time duration to move actuators to begin of line
    if( completed_line_ > 0 )
        move_to_begin_of_line_.add(now - last_line_end_);
    else
        move_to_begin_of_line_.add(now - scan_start_);

    compute_values();
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::line_end
//------------------------------------------------------------------------
void ContinuousScanProgressManager::line_end()
{
    tm::ptime now = tm::microsec_clock::local_time();

    last_line_end_ = tm::microsec_clock::local_time();
    mean_line_duration_.add(last_line_end_ - last_line_start_);

    completed_line_++;

    compute_values();

    // To avoid rounding errors
    if( nb_line_per_scan_ == completed_line_ )
    {
        progress_.scan_completion_percent = 100;

        if( config_.scan_number == current_scan_ )
            progress_.run_completion_percent = 100;
    }
    set_values();
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::compute_values
//------------------------------------------------------------------------
void ContinuousScanProgressManager::compute_values()
{
    tm::ptime now = tm::microsec_clock::local_time();

    tm::time_duration line_duration;
    if( mean_line_duration_.n() == 0 )
        line_duration = theoric_line_duration_;
    else
        line_duration = mean_line_duration_.get();

    scan_duration_ = (move_to_begin_of_line_.get() + line_duration) * nb_line_per_scan_;
    run_duration_ = scan_duration_ * config_.scan_number;

    //- update scan_duration/run_duration/scan_end/run_end
    expected_scan_end_ = now + (move_to_begin_of_line_.get() + line_duration)
            * (nb_line_per_scan_ - completed_line_);

    expected_run_end_ = now + (move_to_begin_of_line_.get() + line_duration)
            * (nb_line_per_scan_ - completed_line_);
    expected_run_end_ += scan_duration_ * (config_.scan_number - current_scan_);
}

//------------------------------------------------------------------------
// ContinuousScanProgressManager::set_values
//------------------------------------------------------------------------
void ContinuousScanProgressManager::set_values()
{
    yat::MutexLock guard(progress_mutex_);
    tm::ptime now = tm::microsec_clock::local_time();

    tm::time_duration scan_elapsed = now - scan_start_;
    tm::time_duration run_elapsed = now - run_start_;

    //- for display, remove fractional seconds
    tm::ptime scan_end_display = tm::ptime( expected_scan_end_.date(),
                                            tm::seconds(expected_scan_end_.time_of_day().total_seconds()) );
    tm::ptime run_end_display = tm::ptime( expected_run_end_.date(),
                                           tm::seconds(expected_run_end_.time_of_day().total_seconds()) );
    progress_.dead_time_per_pt_ms = 0;
    progress_.dead_time_percent   = 0;

    progress_.set_scan_end_date( tm::to_simple_string( scan_end_display ) );
    progress_.set_run_end_date( tm::to_simple_string( run_end_display ) );

    progress_.set_scan_duration( tm::to_simple_string( tm::seconds(scan_duration_.total_seconds()) ) );
    progress_.set_run_duration( tm::to_simple_string( tm::seconds(run_duration_.total_seconds()) ) );

    tm::time_duration scan_remaining = scan_duration_ - scan_elapsed;
    tm::time_duration run_remaining = run_duration_ - run_elapsed;

    if( scan_remaining.total_seconds() < 0 )
        scan_remaining += -scan_remaining;
    if( run_remaining.total_seconds() < 0 )
        run_remaining += -run_remaining;

    progress_.set_scan_remaining_time( tm::to_simple_string(tm::seconds(scan_remaining.total_seconds())) );
    progress_.set_run_remaining_time( tm::to_simple_string(tm::seconds(run_remaining.total_seconds())) );
    progress_.set_scan_elapsed_time( tm::to_simple_string(tm::seconds(scan_elapsed.total_seconds())) );
    progress_.set_run_elapsed_time( tm::to_simple_string(tm::seconds(run_elapsed.total_seconds())) );

    progress_.scan_duration_sec = scan_duration_.total_seconds() + scan_duration_.total_milliseconds() * 1E-3;
    progress_.run_duration_sec  = run_duration_.total_seconds() + run_duration_.total_milliseconds() * 1E-3;

    // Progress indicators shouldn't be double values, so we calculate integer values
    progress_.scan_completion_percent = int((100.0 * scan_elapsed.total_microseconds()) / scan_duration_.total_microseconds() + 0.5);
    progress_.run_completion_percent  = int((100.0 * run_elapsed.total_microseconds()) / run_duration_.total_microseconds() + 0.5);

    if( progress_.scan_completion_percent > 100 )
        progress_.scan_completion_percent = 100;
    if( progress_.run_completion_percent > 100 )
        progress_.run_completion_percent = 100;
}

} // namespace

