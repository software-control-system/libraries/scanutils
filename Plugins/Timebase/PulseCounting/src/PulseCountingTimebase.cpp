/*!
 * \file
 * \brief    Definition of PulseCountingTimebase class
 * \author   Sandra Pierre-Joseph Z�phir - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class PulseCountingTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "PulseCountingTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("PulseCounting");
    }
};

class PulseCountingTimebase : public WaitingStateChangeTimebase
{

public:
    PulseCountingTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 10;
        config_.abort_cmd_name        = "Stop";
        config_.integration_time_attr = "integrationTime";
        config_.start_cmd_name        = "Start";
    }

    void check_initial_condition( )
    {
		WaitingStateChangeTimebase::check_file_generation_condition("nexusFileGeneration");
    }
    
    void before_run()
    {
	       // check that we are in scalar counting mode

		// checking if acquisitionMode is equal to SCALAR
		Tango::DeviceAttribute l_acquisitionMode;
		try{
			l_acquisitionMode = proxy_->read_attribute("acquisitionMode");
	        }
	        catch( Tango::DevFailed& df )
	        {
	            yat4tango::TangoYATException e(df);
	            RETHROW_YAT_ERROR(e,
	                    "TANGO_ERROR",
	                    "PulseCounting error : acquisitionMode cannot be read",
	                    "PulseCountingTimebase::before_run");
	        }

		std::string mode;
		l_acquisitionMode >> mode;
	     	if (mode.compare("SCALAR") != 0)
	      	{
	        std::ostringstream oss;
	        oss << "The device PulseCounting : "
	            << config_.dev_name
	            << " is not well configured. acquisitionMode attribute must be set to SCALAR.";
	        THROW_DEVFAILED( "TIMEBASE_ERROR",
	                         oss.str().c_str(),
	                         "PulseCountingTimebase::before_run" );
	       }

	      // checking if 'IsMaster' is true
	      Tango::DbData prop_data;
	      prop_data.push_back( Tango::DbDatum("IsMaster") );
	      proxy_->unsafe_proxy().get_property( prop_data );
	      if ( prop_data[0].is_empty() )
	      {
	        std::ostringstream oss;
	        oss << "The device PulseCounting : "
	            << config_.dev_name
	            << " has no property named 'IsMaster'. This is abnormal and the device is not usable as is";
	        THROW_DEVFAILED( "TIMEBASE_ERROR",
	                         oss.str().c_str(),
	                         "PulseCountingTimebase::before_run" );
	      }

	      bool is_master;
	      prop_data[0] >> is_master;
	      if ( !is_master )
	      {
	        std::ostringstream oss;
	        oss << "The device PulseCounting : "
	            << config_.dev_name
	            << " handles a SLAVE board. You should configure only the master board in the 'Timebase' parameter";
	        THROW_DEVFAILED( "TIMEBASE_ERROR",
	                         oss.str().c_str(),
	                         "PulseCountingTimebase::before_run" );
	      }

	      // ensures that state is STANDBY
	      Tango::DevState state = proxy_->state();
	      if ( state != Tango::STANDBY )
	      {
	        std::ostringstream oss;
	        oss << "The device PulseCounting : "
	            << config_.dev_name
	            << " state is "
	            << Tango::DevStateName[state]
	            << "."
	            << std::endl
	            << "It should be STANDBY";

	        THROW_DEVFAILED( "TIMEBASE_ERROR",
	                         oss.str().c_str(),
	                         "PulseCountingTimebase::before_run" );
	      }

	      // Checking ratemeter value
	      Tango::DeviceAttribute ratemeter_attr = proxy_->read_attribute( "continuous" );
	      bool ratemeter_;
	      ratemeter_attr >> ratemeter_;

	      if ( ratemeter_ )
	      {
	        std::ostringstream oss;
	        oss << "The device PulseCounting : "
	            << config_.dev_name
	            << " is configured in continuous mode. You must configure it at false";
	        THROW_DEVFAILED( "TIMEBASE_ERROR",
	                         oss.str().c_str(),
	                         "PulseCountingTimebase::before_run" );
	      }
    }


};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::PulseCountingTimebase,
                          ScanUtils::PulseCountingTimebaseInfo);
