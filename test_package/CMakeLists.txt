cmake_minimum_required(VERSION 3.15)
project(PackageTest CXX)

find_package(scanutils CONFIG REQUIRED)
find_package(yat4tango CONFIG REQUIRED)
find_package(nexuscpp CONFIG REQUIRED)
find_package(boostlibraries CONFIG REQUIRED)

add_executable(test_package src/test_package.cpp)
target_link_libraries(test_package
    scanutils::scansl
    yat4tango::yat4tango
    nexuscpp::nexuscpp
    boostlibraries::boostlibraries
)