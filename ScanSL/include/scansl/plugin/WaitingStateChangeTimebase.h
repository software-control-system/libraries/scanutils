/*!
 * \file
 * \brief    Declaration of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */
#ifndef _SCANSERVER_WAITING_STATE_CHANGE_TIMEBASE_H_
#define _SCANSERVER_WAITING_STATE_CHANGE_TIMEBASE_H_

#include <scansl/interfaces/IPolledTimebase.h>
#include <scansl/DevProxies.h>
#include <scansl/ScanConfig.h>

namespace ScanUtils
{
struct WaitingStateChangeTimebaseConfig
{
    WaitingStateChangeTimebaseConfig();

    std::string     dev_name;

    Tango::DevState counting_state;
    std::vector<Tango::DevState> error_states;
    size_t          polling_period_ms;
    int             timeout_ms;

    std::string     integration_time_attr;
    double          integration_time_gain;
    std::string     start_cmd_name;
    std::string     abort_cmd_name;


};

class SCANSL_DECL WaitingStateChangeTimebase : public IPolledTimebase
{
public: //! structors
    WaitingStateChangeTimebase();

    ~WaitingStateChangeTimebase();

public: //! IPolledActuator implementation

    void   init( std::string device_name, bool sync );

    void   check_initial_condition( );

    void   start( double integration_time );

    bool   is_counting( void );

    void   error_check( void );

    double get_polling_period_ms( void );

    void   abort( void );

    void before_run( void );

    void after_run( void );

protected:

    void check_file_generation_condition( std::string );

    WaitingStateChangeTimebaseConfig config_;
    ProxyP proxy_;

};
}

#endif
