/*!
 * \file
 * \brief    Declaration of WaitingStateChangeActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */
#ifndef _SCANSERVER_WAITING_STATE_CHANGE_ACTUATOR_H_
#define _SCANSERVER_WAITING_STATE_CHANGE_ACTUATOR_H_

#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/plugin/MParameters.h>
#include <scansl/DevProxies.h>


namespace ScanUtils
{
struct WaitingStateChangeActuatorConfig
{
    WaitingStateChangeActuatorConfig();

    std::string     dev_name;

    Tango::DevState move_state;
    std::vector<Tango::DevState> error_states;
    size_t          polling_period_ms;
    int             timeout_ms;

    std::string     abort_cmd_name;
    //Debut SPYC-300
    std::string     selected_attr_name;
    //Fin SPYC-300

};

class SCANSL_DECL WaitingStateChangeActuator : public IPolledActuator, public MParameters
{
public: //! structors
    WaitingStateChangeActuator();

    ~WaitingStateChangeActuator();

public: //! IPolledActuator implementation

    void init( std::string device_name );

    void check_initial_condition( std::string attr_name );

    void ensure_supported( std::string attr_name );

    void set_position( std::string attr_name, double position );

    bool is_enabled( std::string attr_name );

    void set_enabled( std::string attr_name, bool enable );

    LimitsType get_limits( std::string attr_name );

    void set_limits( std::string attr_name, const LimitsType& limits );

    double get_speed( std::string attr_name );

    void   set_speed( std::string attr_name, double speed );

    void   go_to( const std::vector<PositionRequest>& positions );

    bool   is_moving( void );

    void   error_check( void );

    double get_polling_period_ms( void );

    void   abort( void );
//debut SPYC-294 SCAN-643
    bool check_cmd_exists(std::string command_name);
//fin SPYC-294 SCAN-643
//debut SPYC-155
    //SpycStates
    std::vector< std::string > read_fixed_pos_list(void);
    std::string read_current_value(void);
    
//fin SPYC-155

//debut SPYC-300
	//SpycStates
    std::string read_selected_position(void);
//fin SPYC-300

protected:

    WaitingStateChangeActuatorConfig config_;
    ProxyP proxy_;

};
}

#endif
