#ifndef _SCANSERVER_IPOLLEDACTUATOR_H
#define _SCANSERVER_IPOLLEDACTUATOR_H

#include <scansl/interfaces/PositionRequest.h>
#include <scansl/ScanConfig.h>
#include <boost/shared_ptr.hpp>
#include <yat/plugin/IPlugInObject.h>

namespace ScanUtils
{
const std::string IPolledActuatorInterfaceName ("IPolledActuator");

struct IPolledActuator : public yat::IPlugInObject
{
    virtual void init( std::string device_name ) = 0;

    virtual void check_initial_condition( std::string attr_name ) = 0;

    virtual void ensure_supported( std::string attr_name ) = 0;

    virtual void set_position( std::string attr_name, double position ) = 0;

    virtual bool is_enabled( std::string attr_name ) = 0;

    virtual void set_enabled( std::string attr_name, bool enable ) = 0;

    virtual LimitsType get_limits( std::string attr_name ) = 0;

    virtual void set_limits( std::string attr_name, const LimitsType& limits ) = 0;

    virtual double get_speed( std::string attr_name ) = 0;

    virtual void set_speed( std::string attr_name, double speed ) = 0;

    virtual void go_to( const std::vector<PositionRequest>& positions ) = 0;

    virtual bool is_moving( void ) = 0;

    virtual void error_check( void ) = 0;

    virtual double get_polling_period_ms( void ) = 0;

    virtual void abort( void ) = 0;
//debut SPYC-155
    //SpycStates
    virtual std::vector< std::string > read_fixed_pos_list(void) = 0; /*lecture de la liste des positions possibles d un device*/
    virtual std::string read_current_value(void) = 0; /*lecture de la valeur courante d un moteur d un device*/
//debut SPYC-155

//debut SPYC-300
	//SpycStates
    virtual std::string read_selected_position(void) = 0; /*lecture du nom de la destination finale ou l equipement doit etre deplace*/
//fin SPYC-300

};

typedef boost::shared_ptr<IPolledActuator> IPolledActuatorPtr;

}

#endif
