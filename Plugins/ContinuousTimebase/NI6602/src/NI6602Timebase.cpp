/*!
 * \file
 * \brief    Definition of NI6602ContinuousTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <boost/scoped_ptr.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeContinuousTimebase.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{

class NI6602ContinuousTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "NI6602ContinuousTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledContinuousTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("NI6602");
    }
};


class NI6602ContinuousTimebase : public WaitingStateChangeContinuousTimebase
{
public: //! structors

    NI6602ContinuousTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 500;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "integrationTime";
        config_.integration_time_gain = 1.0;
        config_.nbpoint_attr          = "totalNbPoint";
        config_.start_cmd_name        = "Start";
    }

public: //! ITimebase implementation

    void init( std::string device_name )
    {
        this->WaitingStateChangeContinuousTimebase::init( device_name );

        Tango::DbData prop_data;
        prop_data.push_back( Tango::DbDatum("Buffered") );
        prop_data.push_back( Tango::DbDatum("IsMaster") );
        proxy_->unsafe_proxy().get_property( prop_data );

        // checking if 'Buffered' is true
        if ( prop_data[0].is_empty() )
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " has no property named 'Buffered'. This is abnormal and the device is not usable as is";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "NI6602Timebase::init" );
        }

        bool buffered;
        prop_data[0] >> buffered;

        if (!buffered)
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " is configured for scalar counting. The 'Buffered' property should be set to 'true' and the device should be reinitialized";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "NI6602Timebase::init" );
        }


        // checking if 'IsMaster' is true
        if ( prop_data[1].is_empty() )
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " has no property named 'IsMaster'. This is abnormal and the device is not usable as is";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "NI6602Timebase::init" );
        }

        bool is_master;
        prop_data[1] >> is_master;

        if ( !is_master )
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " handles a SLAVE board. You should configure only the master board in the 'Timebase' parameter";
            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "NI6602Timebase::init" );
        }

        // ensures that state is STANDBY
        Tango::DevState state = proxy_->state();
        if ( state != Tango::STANDBY )
        {
            std::ostringstream oss;
            oss << "The device "
                << device_name
                << " state is "
                << Tango::DevStateName[state]
                   << "."
                   << std::endl
                   << "It should be STANDBY";

            THROW_DEVFAILED( "TIMEBASE_ERROR",
                             oss.str().c_str(),
                             "NI6602Timebase::init" );
        }

        std::vector<std::string> attr_to_read;
        attr_to_read.push_back( "sensors" );
        attr_to_read.push_back( "actuators" );
        attr_to_read.push_back( "actuatorsProxies" );

        boost::scoped_ptr< std::vector<Tango::DeviceAttribute> > dev_attrs_guard;
        dev_attrs_guard.reset( proxy_->read_attributes( attr_to_read ) );

        std::vector<Tango::DeviceAttribute>& dev_attrs = *dev_attrs_guard;
        dev_attrs[ 0 ] >> sensors_;
        dev_attrs[ 1 ] >> actuators_;
        dev_attrs[ 2 ] >> actuators_proxies_;

        BOOST_FOREACH( std::string& s, sensors_ )
        {
            boost::to_lower( s );
        }
        BOOST_FOREACH( std::string& s, actuators_ )
        {
            boost::to_lower( s );
        }
        BOOST_FOREACH( std::string& s, actuators_proxies_ )
        {
            boost::to_lower( s );
        }
    }

    std::vector<std::string> get_sensors()
    {
        return sensors_;
    }

    std::vector<std::string> get_actuators()
    {
        return actuators_;
    }

    std::vector<std::string> get_actuators_proxies()
    {
        return actuators_proxies_;
    }

private: //! private implementation

    std::vector<std::string> sensors_;
    std::vector<std::string> actuators_;
    std::vector<std::string> actuators_proxies_;
};

}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::NI6602ContinuousTimebase,
                          ScanUtils::NI6602ContinuousTimebaseInfo);

