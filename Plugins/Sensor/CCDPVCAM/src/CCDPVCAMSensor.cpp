/*!
 * \file
 * \brief    Definition of CCDPVCAMSensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */

#include "CCDPVCAMSensor.h"

#include <boost/scoped_ptr.hpp>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::CCDPVCAMSensor, ScanUtils::CCDPVCAMSensorInfo);

namespace ScanUtils
{

const std::string PlugInID                ( "CCDPVCAMSensor" );
const std::string VersionNumber           ( "1.0.0" );

const std::string CLASS_NAME              ("CCDPVCAM");

// ============================================================================
// CCDPVCAMSensorInfo::get_plugin_id
// ============================================================================
std::string CCDPVCAMSensorInfo::get_plugin_id() const
{
    return PlugInID;
}

// ============================================================================
// CCDPVCAMSensorInfo::get_interface_name
// ============================================================================
std::string CCDPVCAMSensorInfo::get_interface_name() const
{
    return ISensorInterfaceName;
}

// ============================================================================
// CCDPVCAMSensorInfo::get_version_number
// ============================================================================
std::string CCDPVCAMSensorInfo::get_version_number() const
{
    return VersionNumber;
}

// ============================================================================
// CCDPVCAMSensorInfo::supported_classes
// ============================================================================
void CCDPVCAMSensorInfo::supported_classes ( std::vector<std::string>& list ) const
{
    list.push_back(CLASS_NAME);
}



// ============================================================================
// CCDPVCAMSensor::CCDPVCAMSensor
// ============================================================================
CCDPVCAMSensor::CCDPVCAMSensor()
{

}

// ============================================================================
// CCDPVCAMSensor::~CCDPVCAMSensor
// ============================================================================
CCDPVCAMSensor::~CCDPVCAMSensor()
{

}

// ============================================================================
// CCDPVCAMSensor::init
// ============================================================================
void CCDPVCAMSensor::init( std::string device_name, bool sync )
{

    try
    {
        proxy_ = DevProxies::instance().proxy(device_name);

    }
    catch (Tango::DevFailed& df)
    {
        throw yat4tango::TangoYATException(df);
    }

}

// ============================================================================
// CCDPVCAMSensor::before_integration
// ============================================================================
void CCDPVCAMSensor::before_integration( void )
{
    proxy_->command_inout( "StartStandardAcq" );
}

}
