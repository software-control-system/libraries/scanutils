#include <scansl/ScanSL.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/Factory.h>
#include <boost/filesystem.hpp>

namespace ScanUtils
{
namespace fs = boost::filesystem;

#ifdef YAT_WIN32
const std::string SHARED_LIB_EXTENSION( ".dll" );
#else
const std::string SHARED_LIB_EXTENSION( ".so" );
#endif

Factory::Factory( const std::string& interface_name )
    : interface_name_(interface_name)
{

}

Factory::~Factory()
{

}

void Factory::init( std::string plugin_path_str )
{

    //- load the path
    fs::path plugin_path( plugin_path_str );

    SCAN_DEBUG << "plugin path string: " << plugin_path_str << ENDLOG;

    //- ensure this is a directory
    if ( !fs::exists(plugin_path) || !fs::is_directory( plugin_path ) )
    {
        SCAN_ERROR << "The specified plugin path (" << plugin_path << ") does not exist or is not a directory" << ENDLOG;
        THROW_YAT_ERROR("PLUGIN_ERROR",
                        "The specified plugin path does not exist or is not a directory",
                        "GenericFactory::init");
    }

    fs::directory_iterator di(plugin_path);
    fs::directory_iterator end;
    for (; di != end; ++di)
    {
        SCAN_DEBUG << "file found: " << di->path() << ENDLOG;
        if ( fs::is_regular( di->status() ) )
        {
            //- we found a file
            if ( fs::extension(di->path()) == SHARED_LIB_EXTENSION )
            {
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
                SCAN_DEBUG << "file load: " << di->path().string() << ENDLOG;
#else
                SCAN_DEBUG << "file load: " << di->path().native_file_string() << ENDLOG;
#endif
                //- we found a shared lib
                Entry plugin_objects;

                try
                {
#if defined(DEPRECATED_BOOST_METHODS_WORKAROUND)
                    std::pair<yat::IPlugInInfo*, yat::IPlugInFactory*> p = manager_.load( di->path().string() );
#else
                    std::pair<yat::IPlugInInfo*, yat::IPlugInFactory*> p = manager_.load( di->path().native_file_string() );
#endif

                    plugin_objects.info    = p.first;
                    plugin_objects.factory = p.second;
                }
                catch( yat::Exception& err)
                {
                    SCAN_ERROR << "library is not a yat plugin:" << err.to_string() << ENDLOG;
                    continue;
                }

                //- we found a yat plugin

                if ( plugin_objects.info->get_interface_name() == interface_name_ )
                {
                    //- we found a yat plugin with the right interface
                    IScanPlugInInfo* scan_plugin_info = 0;
                    try
                    {
                        scan_plugin_info = static_cast<IScanPlugInInfo*>(plugin_objects.info);
                        if (scan_plugin_info == NULL)
                            throw std::bad_cast();
                    }
                    catch( std::bad_cast& )
                    {
                        //- this is a ScanPlugin, but its info class is not a IScanPlugInInfo
                        //- ==> invalid plugin
                        SCAN_ERROR << "Invalid ScanSL plugin : " << di->path() << ENDLOG;
                        continue;
                    }

                    yat::OSStream oss;
                    oss << "Loading plugin " << plugin_objects.info->get_plugin_id() << " supporting : ";

                    std::vector<std::string> classes;
                    scan_plugin_info->supported_classes( classes );

                    for( std::size_t i = 0; i < classes.size(); ++i )
                    {
                        oss << classes[i] << " ";
                        factory_rep_.insert( std::make_pair( classes[i], plugin_objects ) );
                    }

                    SCAN_DEBUG << oss.str() << ENDLOG;
                }
            }
        }
    }
}

}

