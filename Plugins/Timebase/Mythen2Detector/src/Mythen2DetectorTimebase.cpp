/*!
 * \file
 * \brief    Definition of Mythen2DetectorTimebase class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeTimebase.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <yat/plugin/PlugInSymbols.h>
#include <yat4tango/ExceptionHelper.h>

namespace ScanUtils
{

class Mythen2DetectorTimebaseInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "Mythen2DetectorTimebase";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledTimebaseInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("Mythen2Detector");
    }
};

class Mythen2DetectorTimebase : public WaitingStateChangeTimebase
{

public:
    Mythen2DetectorTimebase()
    {
        config_.counting_state        = Tango::RUNNING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms     = 20;
        config_.abort_cmd_name        = "Stop";
        config_.timeout_ms            = 3000;
        config_.integration_time_attr = "exposureTime";
        config_.integration_time_gain = 1E3;
        config_.start_cmd_name        = "Snap";
    }

    void check_initial_condition( )
    {
		WaitingStateChangeTimebase::check_file_generation_condition("fileGeneration");
    }

    void before_run()
    {
        SCAN_DEBUG << "Mythen2DetectorTimebase::before_run" << ENDLOG;
        //- Set the nbFrame to 1
        Tango::DeviceAttribute nbframes_attr("nbFrames", 1L);
        proxy_->write_attribute( nbframes_attr );
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::Mythen2DetectorTimebase,
                          ScanUtils::Mythen2DetectorTimebaseInfo);
