#include <limits>
#include <scansl/Util.h>
#include <scansl/DevProxies.h>
#include <scansl/logging/ScanLogAdapter.h>
#include <scansl/actors/DataCollector.h>
#include <yat/utils/XString.h>
#include <yat4tango/ExceptionHelper.h>


namespace ScanUtils
{

#ifdef SCANSL_ENABLE_NEXUS
#endif

const size_t CONTINUOUS_CAPACITY_GRANULARITY = 20;

#define DOUBLE_NAN std::numeric_limits<double>::quiet_NaN()
#define FLOAT_NAN std::numeric_limits<float>::quiet_NaN()

enum
{
    MSG_ID_INIT_RUN = yat::FIRST_USER_MSG,
    MSG_ID_INIT_SCAN,
    MSG_ID_SET_PT_NUMBER,
    MSG_ID_BEGIN_OF_DIM,
    MSG_ID_SAVE_PRIOR_ACT_DATA,
    MSG_ID_SAVE_ACT_DATA,
    MSG_ID_SAVE_SENSOR_DATA,
    MSG_ID_END_OF_STEP,
    MSG_ID_END_OF_DIM,
    MSG_ID_END_OF_SCAN,
    MSG_ID_END_OF_RUN,
    MSG_ID_APPEND_LOG
};

class FileCloser
{
public:
    void operator ()( std::ofstream* s )
    {
        s->close();
        delete s;
    };
};

template <typename T>
struct Cleaner
{
    void visit( boost::any& buffer_any, int data_type )
    {
        if (data_type == TangoTraits<T>::type_id)
        {
            typename ScanArray<T>::ImgP& buffer = boost::any_cast<typename ScanArray<T>::ImgP&>( buffer_any );
            if (buffer)
                std::fill( buffer->data(), buffer->data() + buffer->num_elements(), std::numeric_limits<T>::quiet_NaN());
        }
    }
};

template <typename T>
struct Resizer
{
    void visit( boost::shared_ptr<DataSetter> data_setter, int data_type, size_t new_width )
    {
        if (data_type == TangoTraits<T>::type_id)
        {
            boost::any buffer_any = data_setter->get();
            typename ScanArray<T>::ImgP& buffer = boost::any_cast<typename ScanArray<T>::ImgP&>( buffer_any );
            typename ScanArray<T>::ImgP  new_buffer( new typename ScanArray<T>::Img( boost::extents[buffer->shape()[0]][new_width] ) );

            size_t old_width = buffer->shape()[1];
            size_t nb_to_copy = old_width < new_width ? old_width : new_width;
            (*new_buffer)[boost::indices[range()][range().finish(nb_to_copy)]] \
                    = (*buffer)[boost::indices[range()][range().finish(nb_to_copy)]];
            data_setter->set( boost::any(new_buffer) );
        }
    }
};

template <typename T>
struct ValueWriter
{
    typedef typename ScanArray<T>::Img  image_t;
    typedef typename ScanArray<T>::ImgP imagep_t;
    typedef typename image_t::index     index;


    void visit( boost::shared_ptr<DataSetter> data_setter,
                int data_type,
                boost::any& value,
                const std::vector<index>& where )
    {
        if (data_type == TangoTraits<T>::type_id)
        {
            boost::any buffer_any = data_setter->get();
            imagep_t& bufferp = boost::any_cast<imagep_t&>( buffer_any );
            (*bufferp)( where ) = boost::any_cast<T>( value );
        }
    }
};

// ============================================================================
// DataCollector::DataCollector
// ============================================================================
DataCollector::DataCollector( const ScanConfig& config, ContinuousTimebaseManagerPtr cont_tb_manager  )
    : yat::Task( yat::Task::Config( false,
                                    0,
                                    false,
                                    0,
                                    false,
                                    10,
                                    1000,
                                    true,
                                    0 ) ),
      config_(config),
      msg_counter_(0),
      scan_index_(0),
      dim1_pt_(0),
      dim2_pt_(0),
      continuous_(false),
      capacity_(0),
      has_error_( false )
{

    if ( config_.on_the_fly )
        capacity_ = CONTINUOUS_CAPACITY_GRANULARITY;
    else if ( config_.hw_continuous )
        capacity_ = config_.hw_continuous_nbpt;
    else
        capacity_ = config.integration_times.shape()[0];


    buffers_.actuators_prior_pos.reset( new std::vector<double>(config.actuators.size() + config.actuators2.size()) );

    const double nan_value = std::numeric_limits<double>::signaling_NaN();
    std::fill( buffers_.actuators_prior_pos->begin(), buffers_.actuators_prior_pos->end(), nan_value );

    std::vector< std::string > actuators_1d_names;
    for( std::size_t i = 0; i < config_.actuators.size(); ++i )
    {
        const std::string actuator = config_.actuators[i];
        std::string complete_name;
        if ( config_.hw_continuous )
        {
            // init with actuator value on timebase
            complete_name = Util::instance().complete_attr_name(actuator);
            // transform to get the 'real' actuator
            //## mettre cette ligne en commentaire...
            complete_name = boost::to_lower_copy( cont_tb_manager->get_actuator_value_attr( complete_name ));
        }
        else
        {
            // keep the name intact (keep alias)
            complete_name = actuator;
        }
        actuators_1d_names.push_back(complete_name);
    }

    this->register_data( actuators_1d_names, ACTUATOR,  1, "actuator_1_", actuators_attr_list_, actuator_data_ );
    this->register_data( config_.actuators2, ACTUATOR,  2, "actuator_2_", actuators2_attr_list_, actuator_data_ );
    this->register_data( config_.sensors,    SENSOR,    1, "data_",       sensors_attr_list_, sensor_data_ );

    this->register_timestamps_attr( );
    this->register_relative_timestamps( );

#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        //- init DataRecorder
        data_recorder_.reset( new DataRecorder(config) );
    }
#endif

    SCAN_DEBUG << "sensors count: " << sensor_data_.size() << ENDLOG;
    this->go();
}

// ============================================================================
// DataCollector::~DataCollector
// ============================================================================
DataCollector::~DataCollector()
{
}

// ============================================================================
// DataCollector::register_data
// ============================================================================
void DataCollector::register_data( const std::vector< std::string >& attr_names,
                                   DataType type,
                                   int scan_dim,
                                   std::string data_attr_basename,
                                   std::vector< boost::shared_ptr<Tango::Attr> >& attr_list,
                                   std::map<std::string, DataInfo>& data_)
{
    int scan_nb_dim = !config_.actuators2.empty() ? 2 : 1;
    for( std::size_t i = 0; i < attr_names.size(); ++i )
    {
        const std::string attr_name =  attr_names[i];
        SCAN_DEBUG << "Registering data " << attr_name << ENDLOG;
        DataInfo data_info;
        data_info.index = i;
        data_info.type = type;
        data_info.scan_dim = scan_dim;

        Tango::UserDefaultAttrProp attr_prop;
        std::pair<std::string, std::string> attr_name_resolved = Util::instance().resolve_attr_name(attr_name);
        data_info.attr_name = Util::instance().make_attr_name(attr_name_resolved.first, attr_name_resolved.second);
        data_info.attr_info = DevProxies::instance().proxy( attr_name_resolved.first )->attribute_query(attr_name_resolved.second);
        attr_prop.set_label( attr_name.c_str() );
        attr_prop.set_unit( data_info.attr_info.unit.c_str() );
        attr_prop.set_standard_unit( data_info.attr_info.standard_unit.c_str() );
        attr_prop.set_display_unit( data_info.attr_info.display_unit.c_str() );
        attr_prop.set_format( data_info.attr_info.format.c_str() );
        data_info.src_dims = static_cast<int>(data_info.attr_info.data_format);

        if ( config_.on_the_fly && data_info.attr_info.data_format != Tango::SCALAR )
        {
            THROW_YAT_ERROR( "CONFIG_ERROR",
                             "Only scalars are supported for on-the-fly scans",
                             "DataCollector::register_data" );
        }


        if (!config_.hw_continuous)
            data_info.storage_dims = data_info.src_dims + scan_nb_dim - (scan_dim - 1);
        else
            data_info.storage_dims = data_info.src_dims;

        // JIRA EXPDATA-114: Ajouter l'alias d'attribut dans les informations
        if( attr_name != data_info.attr_name )
            data_info.attr_alias = attr_name;

        std::ostringstream dataset_name;
        dataset_name << std::setfill('0');
        dataset_name << data_attr_basename;
        if (type == SENSOR)
            dataset_name << std::setw( 2 );
        dataset_name << data_info.index + 1;

        data_info.dataset_name = dataset_name.str();

        if (data_info.attr_info.data_format == Tango::SCALAR)
        {
            data_info.display_dims = (scan_nb_dim - scan_dim + 1);

#       define HANDLE_TYPE( DataType, CType ) \
    case DataType: \
            { \
    boost::shared_ptr< Tango::Attr > tango_attr; \
    boost::shared_ptr< DataSetter  > dset; \
    ScanArray<CType>::ImgP buffer; \
    if ( scan_nb_dim == 1 ) \
            { \
    boost::shared_ptr< SpectrumAttr<CType> > spectrum_attr(new SpectrumAttr<CType>(data_info.dataset_name, Tango::OPERATOR, Tango::READ, attr_prop )); \
    tango_attr = spectrum_attr; \
    dset = boost::static_pointer_cast<DataSetter>(spectrum_attr); \
    buffer.reset( new ScanArray<CType>::Img(boost::extents[1][capacity_]) ); \
        } \
    else if ( scan_nb_dim == 2 && scan_dim == 2 ) \
            { \
    boost::shared_ptr< SpectrumAttr<CType> > spectrum_attr(new SpectrumAttr<CType>(data_info.dataset_name, Tango::OPERATOR, Tango::READ, attr_prop )); \
    tango_attr = spectrum_attr; \
    dset = boost::static_pointer_cast<DataSetter>(spectrum_attr); \
    buffer.reset( new ScanArray<CType>::Img(boost::extents[1][config_.trajectories2.shape()[1]]) ); \
        } \
    else \
            { \
    boost::shared_ptr< ImageAttr<CType> > im_attr(new ImageAttr<CType>(data_info.dataset_name, Tango::OPERATOR, Tango::READ, attr_prop )); \
    tango_attr = im_attr; \
    dset = boost::static_pointer_cast<DataSetter>(im_attr); \
    buffer.reset( new ScanArray<CType>::Img(boost::extents[config_.trajectories2.shape()[1]][capacity_]) ); \
        } \
    std::fill( buffer->data(), buffer->data() + buffer->num_elements(), std::numeric_limits<CType>::quiet_NaN() ); \
    attr_list.push_back( tango_attr ); \
    data_info.datasetter = dset; \
    data_info.datasetter->set( buffer ); \
        } \
    break;

            switch (data_info.attr_info.data_type)
            {
            HANDLE_TYPE( Tango::DEV_SHORT,   Tango::DevShort )
                    HANDLE_TYPE( Tango::DEV_LONG,    Tango::DevLong )
                    HANDLE_TYPE( Tango::DEV_DOUBLE,  Tango::DevDouble )
                    HANDLE_TYPE( Tango::DEV_FLOAT,   Tango::DevFloat )
                    HANDLE_TYPE( Tango::DEV_BOOLEAN, Tango::DevBoolean )
                    HANDLE_TYPE( Tango::DEV_USHORT,  Tango::DevUShort )
                    HANDLE_TYPE( Tango::DEV_ULONG,   Tango::DevULong )
                    HANDLE_TYPE( Tango::DEV_UCHAR,   Tango::DevUChar )
                    HANDLE_TYPE( Tango::DEV_LONG64,  Tango::DevLong64 )
                    HANDLE_TYPE( Tango::DEV_ULONG64, Tango::DevULong64 )
            }
#       undef HANDLE_TYPE
        }
        else
        {
            data_info.display_dims = data_info.src_dims;

#       define HANDLE_TYPE( DataType, CType ) \
    case DataType: \
            { \
    boost::shared_ptr< Tango::Attr > tango_attr; \
    boost::shared_ptr< DataSetter > dset; \
    if ( data_info.src_dims == 1 ) \
            { \
    boost::shared_ptr< SpectrumAttr<CType> > spectrum_attr( new SpectrumAttr<CType>(data_info.dataset_name, Tango::OPERATOR, Tango::READ, attr_prop ) ); \
    tango_attr = spectrum_attr; \
    dset = boost::static_pointer_cast<DataSetter>(spectrum_attr); \
        } \
    else \
            { \
    boost::shared_ptr< ImageAttr<CType> > spectrum_attr( new ImageAttr<CType>(data_info.dataset_name, Tango::OPERATOR, Tango::READ, attr_prop ) ); \
    tango_attr = spectrum_attr; \
    dset = boost::static_pointer_cast<DataSetter>(spectrum_attr); \
        } \
    attr_list.push_back( tango_attr ); \
    data_info.datasetter = dset; \
        } \
    break;

            switch (data_info.attr_info.data_type)
            {
            HANDLE_TYPE( Tango::DEV_SHORT,   Tango::DevShort )
                    HANDLE_TYPE( Tango::DEV_LONG,    Tango::DevLong )
                    HANDLE_TYPE( Tango::DEV_DOUBLE,  Tango::DevDouble )
                    HANDLE_TYPE( Tango::DEV_FLOAT,   Tango::DevFloat )
                    HANDLE_TYPE( Tango::DEV_BOOLEAN, Tango::DevBoolean )
                    HANDLE_TYPE( Tango::DEV_USHORT,  Tango::DevUShort )
                    HANDLE_TYPE( Tango::DEV_ULONG,   Tango::DevULong )
                    HANDLE_TYPE( Tango::DEV_UCHAR,   Tango::DevUChar )
                    HANDLE_TYPE( Tango::DEV_LONG64,  Tango::DevLong64 )
                    HANDLE_TYPE( Tango::DEV_ULONG64, Tango::DevULong64 )
            }
#       undef HANDLE_TYPE
        }
        data_[ data_info.attr_name ] = data_info;
    }
}


// ============================================================================
// DataCollector::register_timestamps_attr
// ============================================================================
void DataCollector::register_timestamps_attr( void )
{
    int scan_nb_dim = !config_.actuators2.empty() ? 2 : 1;

    boost::shared_ptr< Tango::Attr > actuators_timestamp_attr, sensors_timestamp_attr;
    boost::shared_ptr< DataSetter  > actuators_dset, sensors_dset;
    ScanArray<double>::ImgP actuators_buffer, sensors_buffer;

    Tango::UserDefaultAttrProp actuators_attr_prop, sensors_attr_prop;
    actuators_attr_prop.set_label( "actuator timestamps" );
    actuators_attr_prop.set_unit( "s" );
    actuators_attr_prop.set_format( "%10.3f" );
    sensors_attr_prop.set_label( "sensor timestamps" );
    sensors_attr_prop.set_unit( "s" );
    sensors_attr_prop.set_format( "%10.3f" );

    if ( scan_nb_dim == 1 )
    {

        boost::shared_ptr< SpectrumAttr<double> > actuators_timestamp_spectrum_attr(new SpectrumAttr<double>("actuatorsTimestamps", Tango::OPERATOR, Tango::READ, actuators_attr_prop ));
        actuators_timestamp_attr = actuators_timestamp_spectrum_attr;
        actuators_dset = boost::static_pointer_cast<DataSetter>(actuators_timestamp_spectrum_attr);
        actuators_buffer.reset( new ScanArray<double>::Img(boost::extents[1][capacity_]) );

        boost::shared_ptr< SpectrumAttr<double> > sensors_timestamp_spectrum_attr(new SpectrumAttr<double>("sensorsTimestamps", Tango::OPERATOR, Tango::READ, sensors_attr_prop ));
        sensors_timestamp_attr = sensors_timestamp_spectrum_attr;
        sensors_dset = boost::static_pointer_cast<DataSetter>(sensors_timestamp_spectrum_attr);
        sensors_buffer.reset( new ScanArray<double>::Img(boost::extents[1][capacity_]) );
    }
    else
    {
        boost::shared_ptr< ImageAttr<double> > actuators_timestamp_img_attr(new ImageAttr<double>("actuatorsTimestamps", Tango::OPERATOR, Tango::READ, actuators_attr_prop ));
        actuators_timestamp_attr = actuators_timestamp_img_attr;
        actuators_dset = boost::static_pointer_cast<DataSetter>(actuators_timestamp_img_attr);
        actuators_buffer.reset( new ScanArray<double>::Img(boost::extents[config_.trajectories2.shape()[1]][capacity_]) );

        boost::shared_ptr< ImageAttr<double> > sensors_timestamp_img_attr(new ImageAttr<double>("sensorsTimestamps", Tango::OPERATOR, Tango::READ, sensors_attr_prop ));
        sensors_timestamp_attr = sensors_timestamp_img_attr;
        sensors_dset = boost::static_pointer_cast<DataSetter>(sensors_timestamp_img_attr);
        sensors_buffer.reset( new ScanArray<double>::Img(boost::extents[config_.trajectories2.shape()[1]][capacity_]) );
    }

    DataInfo actuators_info;
    actuators_info.type = TIMESTAMP;
    actuators_info.scan_dim = 1;
    actuators_info.index = 1;
    actuators_info.src_dims = 0;
    actuators_info.display_dims = scan_nb_dim;
    actuators_info.storage_dims = actuators_info.display_dims;
    actuators_info.attr_name = "actuatorsTimestamps";
    actuators_info.attr_info.name = "actuatorTimestamps";
    actuators_info.attr_info.data_type = Tango::DEV_DOUBLE;
    actuators_info.attr_info.data_format = Tango::SCALAR;
    actuators_info.attr_info.label = "actuator timestamps";
    actuators_info.attr_info.unit = "s";
    actuators_info.attr_info.format = "%10.3f";
    actuators_info.attr_info.description = "actuator timestamps";

    std::fill( actuators_buffer->data(), actuators_buffer->data() + actuators_buffer->num_elements(), double(0) );
    timestamps_attr_list_.push_back( actuators_timestamp_attr );
    actuators_info.datasetter = actuators_dset;
    actuators_info.datasetter->set( actuators_buffer );
    actuators_info.dataset_name = "actuators_timestamps";
    actuator_data_[ boost::to_lower_copy(actuators_info.attr_name) ] = actuators_info;


    DataInfo sensors_info;
    sensors_info.type = TIMESTAMP;
    sensors_info.scan_dim = 1;
    sensors_info.index = 1;
    sensors_info.src_dims = 0;
    sensors_info.display_dims = scan_nb_dim;
    sensors_info.storage_dims = actuators_info.display_dims;
    sensors_info.attr_name = "sensorsTimestamps";
    sensors_info.attr_info.name = "sensorTimestamps";
    sensors_info.attr_info.data_type = Tango::DEV_DOUBLE;
    sensors_info.attr_info.data_format = Tango::SCALAR;
    sensors_info.attr_info.label = "sensor timestamps";
    sensors_info.attr_info.unit = "s";
    sensors_info.attr_info.format = "%10.3f";
    sensors_info.attr_info.description = "sensor timestamps";

    std::fill( sensors_buffer->data(), sensors_buffer->data() + sensors_buffer->num_elements(), double(0) );
    timestamps_attr_list_.push_back( sensors_timestamp_attr );
    sensors_info.datasetter = sensors_dset;
    sensors_info.datasetter->set( sensors_buffer );
    sensors_info.dataset_name = "sensors_timestamps";
    actuator_data_[ boost::to_lower_copy(sensors_info.attr_name) ] = sensors_info;
}

// ============================================================================
// DataCollector::register_relative_timestamps
// ============================================================================
void DataCollector::register_relative_timestamps( void )
{
    int scan_nb_dim = !config_.actuators2.empty() ? 2 : 1;

    Tango::UserDefaultAttrProp sensors_attr_prop;
    boost::shared_ptr< DataSetter  > sensors_dset;
    ScanArray<double>::ImgP sensors_buffer;

    if ( scan_nb_dim == 1 )
    {
        boost::shared_ptr< SpectrumAttr<double> > sensors_timestamp_spectrum_attr(new SpectrumAttr<double>("sensorsRelTimestamps", Tango::OPERATOR, Tango::READ, sensors_attr_prop ));
        sensors_dset = boost::static_pointer_cast<DataSetter>(sensors_timestamp_spectrum_attr);
        sensors_buffer.reset( new ScanArray<double>::Img(boost::extents[1][capacity_]) );
    }
    else
    {
        boost::shared_ptr< ImageAttr<double> > sensors_timestamp_img_attr(new ImageAttr<double>("sensorsTimestamps", Tango::OPERATOR, Tango::READ, sensors_attr_prop ));
        sensors_dset = boost::static_pointer_cast<DataSetter>(sensors_timestamp_img_attr);
        sensors_buffer.reset( new ScanArray<double>::Img(boost::extents[config_.trajectories2.shape()[1]][capacity_]) );
    }

    DataInfo sensors_info;
    sensors_info.type = TIMESTAMP;
    sensors_info.attr_info.data_type = Tango::DEV_DOUBLE;
    sensors_info.attr_info.data_format = Tango::SCALAR;
    sensors_info.scan_dim = 1;
    sensors_info.index = 1;
    sensors_info.src_dims = 0;
    sensors_info.display_dims = scan_nb_dim;
    sensors_info.storage_dims = scan_nb_dim;
    sensors_info.attr_name = "sensorsRelTimestamps";
    sensors_info.attr_info.name = "sensorRelTimestamps";
    sensors_info.attr_info.label = "sensor relative timestamps";
    sensors_info.attr_info.unit = "s";
    sensors_info.attr_info.format = "%10.6f";
    sensors_info.attr_info.description = "sensor relative timestamps";

    std::fill( sensors_buffer->data(), sensors_buffer->data() + sensors_buffer->num_elements(), double(0) );
    sensors_info.datasetter = sensors_dset;
    sensors_info.datasetter->set( sensors_buffer );
    sensors_info.dataset_name = "sensors_rel_timestamps";
    actuator_data_[ "sensorsreltimestamps" ] = sensors_info;
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > DataCollector::get_actuators_attributes()
{
    return actuators_attr_list_;
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > DataCollector::get_actuators2_attributes()
{
    return actuators2_attr_list_;
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > DataCollector::get_sensors_attributes()
{
    return sensors_attr_list_;
}

//! get attributes list
std::vector< boost::shared_ptr<Tango::Attr> > DataCollector::get_timestamps_attributes()
{
    return timestamps_attr_list_;
}



// ============================================================================
// DataCollector::init_run
// ============================================================================
void DataCollector::init_run( void )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_INIT_RUN, 30000, false );
}

// ============================================================================
// DataCollector::init_scan
// ============================================================================
void DataCollector::init_scan( void )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_INIT_SCAN, 30000, false );
}

// ============================================================================
// DataCollector::set_point_nb
// ============================================================================
void DataCollector:: set_point_nb( size_t dim1_pt, size_t dim2_pt )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SET_PT_NUMBER, 3000, false, std::make_pair(dim1_pt, dim2_pt) );
}

// ============================================================================
// DataCollector::begin_of_dim
// ============================================================================
void DataCollector::begin_of_dim( void )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_BEGIN_OF_DIM, 3000, false );
}

// ============================================================================
// DataCollector::save_prior_actuator_data
// ============================================================================
void DataCollector::save_prior_actuator_data( std::vector<double>& positions )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_PRIOR_ACT_DATA, 3000, false, positions );
}

// ============================================================================
// DataCollector::save_actuator_data
// ============================================================================
void DataCollector::save_actuator_data( AttrValue& av )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_ACT_DATA, 3000, false, av );
}

// ============================================================================
// DataCollector::save_sensor_data
// ============================================================================
void DataCollector::save_sensor_data( AttrValue& av )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_SENSOR_DATA, 3000, false, av );
}

// ============================================================================
// DataCollector::timestamp_actuators
// ============================================================================
void DataCollector::timestamp_actuators( void )
{
    // put time since epoch for timestamp
    tm::time_duration since_epoch = tm::microsec_clock::universal_time() - tm::ptime(dt::date(1970,1,1));
    double timestamp = since_epoch.total_seconds() \
            + 1.0E-6 * (since_epoch.total_microseconds() - 1.0E6 * since_epoch.total_seconds());
    AttrValue act_time( "actuatorstimestamps", timestamp );
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_ACT_DATA, 3000, false, act_time );
}

// ============================================================================
// DataCollector::timestamp_sensors
// ============================================================================
void DataCollector::timestamp_sensors( void )
{
    tm::time_duration since_epoch = tm::microsec_clock::universal_time() - tm::ptime(dt::date(1970,1,1));
    double timestamp = since_epoch.total_seconds() \
            + 1.0E-6 * (since_epoch.total_microseconds() - 1.0E6 * since_epoch.total_seconds());
    AttrValue sens_time( "sensorstimestamps", timestamp );

    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_ACT_DATA, 3000, false, sens_time );

    // Relative timestamp
    tm::time_duration since_start = tm::microsec_clock::universal_time() - this->init_scan_timestamp_;
    double reltimestamp = since_start.total_seconds() \
            + 1.0E-6 * (since_start.total_microseconds() - 1.0E6 * since_start.total_seconds());
    AttrValue sens_rel_time( "sensorsreltimestamps", reltimestamp );

    this->increment_msgcntr();
    post_msg( *this, MSG_ID_SAVE_ACT_DATA, 3000, false, sens_rel_time );
}

// ============================================================================
// DataCollector::end_of_step
// ============================================================================
void DataCollector::end_of_step( void )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_END_OF_STEP, 3000, false );
}

// ============================================================================
// DataCollector::end_of_dim
// ============================================================================
void DataCollector::end_of_dim( void )
{
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_END_OF_DIM, 3000, false );
}

// ============================================================================
// DataCollector::end_of_scan
// ============================================================================
void DataCollector::end_of_scan( bool runsuccessfull )
{
    //- wait 30 sec on this one to be sure that data are collected & saved
    //- before going on with the end of the scan (after run action for example)
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_END_OF_SCAN, 30000, false, runsuccessfull );
}


// ============================================================================
// DataCollector::end_of_run
// ============================================================================
void DataCollector::end_of_run( void )
{
    //- wait 30 sec on this one to be sure that session is closed successfully
    this->increment_msgcntr();
    post_msg( *this, MSG_ID_END_OF_RUN, 30000, false );
}

// ============================================================================
// DataCollector::handle_message
// ============================================================================
void DataCollector::handle_message(yat::Message & msg)
throw(yat::Exception)
{
    try
    {
        MsgCntrDecrement msg_decrement_guard( this, msg.type() );

        if ( has_error_ && msg.type() != MSG_ID_END_OF_SCAN && msg.type() != MSG_ID_END_OF_RUN )
            return;

        switch( msg.type() )
        {
        case MSG_ID_INIT_RUN:
            //SCAN_DEBUG << "DataCollector::MSG_ID_INIT_RUN" << ENDLOG;
            this->on_init_run();
            break;
        case MSG_ID_INIT_SCAN:
            //SCAN_DEBUG << "DataCollector::MSG_ID_INIT_SCAN" << ENDLOG;
            this->on_init_scan();
            break;
        case MSG_ID_SET_PT_NUMBER:
            //SCAN_DEBUG << "DataCollector::MSG_ID_SET_PT_NUMBER" << ENDLOG;
            this->on_set_point_nb(msg);
            break;
        case MSG_ID_BEGIN_OF_DIM:
            //SCAN_DEBUG << "DataCollector::MSG_ID_BEGIN_OF_DIM" << ENDLOG;
            this->on_begin_of_dim();
            break;
        case MSG_ID_SAVE_PRIOR_ACT_DATA:
            //SCAN_DEBUG << "DataCollector::MSG_ID_SAVE_PRIOR_ACT_DATA" << ENDLOG;
            this->on_save_prior_actuator_data(msg);
            break;
        case MSG_ID_SAVE_ACT_DATA:
            //SCAN_DEBUG << "DataCollector::MSG_ID_SAVE_ACT_DATA" << ENDLOG;
            this->on_save_actuator_data(msg);
            break;
        case MSG_ID_SAVE_SENSOR_DATA:
            //SCAN_DEBUG << "DataCollector::MSG_ID_SAVE_SENSOR_DATA" << ENDLOG;
            this->on_save_sensor_data(msg);
            break;
        case MSG_ID_END_OF_STEP:
            //SCAN_DEBUG << "DataCollector::MSG_ID_END_OF_STEP" << ENDLOG;
            this->on_end_of_step();
            break;
        case MSG_ID_END_OF_DIM:
            //SCAN_DEBUG << "DataCollector::MSG_ID_END_OF_DIM" << ENDLOG;
            this->on_end_of_dim();
            break;
        case MSG_ID_END_OF_SCAN:
            //SCAN_DEBUG << "DataCollector::MSG_ID_INIT_RUN" << ENDLOG;
            this->on_end_of_scan();
            break;
        case MSG_ID_END_OF_RUN:
            //SCAN_DEBUG << "DataCollector::MSG_ID_END_OF_RUN" << ENDLOG;
            this->on_end_of_run();
            break;
        default: break;
        }
    }
    catch( Tango::DevFailed& df )
    {
        this->set_error( yat4tango::TangoYATException(df) );
        // throw ex; // in fact throwing the exception is useless since all msg are 'posted' and not 'waited'
    }
    catch( yat::Exception& ex )
    {
        this->set_error( ex );
        // throw; // in fact throwing the exception is useless since all msg are 'posted' and not 'waited'
    }
    catch( std::exception& e )
    {
        yat::Exception ex( "DATA_RECORDING_ERROR", e.what(), "DataCollector::handle_message" );
        this->set_error( ex );
        // throw; // in fact throwing the exception is useless since all msg are 'posted' and not 'waited'
    }
    catch( ... )
    {
        yat::Exception ex( "DATA_RECORDING_ERROR", "Unknown data recording error", "DataCollector::handle_message" );
        this->set_error( ex );
        // throw; // in fact throwing the exception is useless since all msg are 'posted' and not 'waited'
    }
}

// ============================================================================
// DataCollector::on_init_run
// ============================================================================
void DataCollector::on_init_run( void )
{
#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        data_recorder_->init_run( this );
    }
#endif
}

// ============================================================================
// DataCollector::init_scan
// ============================================================================
void DataCollector::on_init_scan( void )
{
    scan_index_ ++;
    init_scan_timestamp_ = tm::microsec_clock::universal_time();
    this->clear_buffers();
#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        data_recorder_->init_scan(scan_index_);
    }
#endif
}


// ============================================================================
// DataCollector::clear_buffers
// ============================================================================
void DataCollector::clear_buffers( void )
{
    for( std::map<std::string, DataInfo>::iterator it = actuator_data_.begin(); it != actuator_data_.end(); ++it )
    {
        //# BEGIN PROBLEM-1454
        it->second.reset();
        //# END PROBLEM-1454
        if (it->second.src_dims == 0)
        {
            boost::any buffer_any = it->second.datasetter->get();
            Tango::AttributeInfoEx& info = it->second.attr_info;

            Cleaner< Tango::DevShort   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevLong    >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevDouble  >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevFloat   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevBoolean >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevUShort  >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevULong   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevUChar   >().visit( buffer_any, info.data_type );
            //Cleaner< Tango::DevLong64  >().visit( buffer_any, info.data_type );
            //Cleaner< Tango::DevULong64 >().visit( buffer_any, info.data_type );
        }
    }
    for( std::map<std::string, DataInfo>::iterator it = sensor_data_.begin(); it != sensor_data_.end(); ++it )
    {
        //# BEGIN PROBLEM-1454
        it->second.reset();
        //# END PROBLEM-1454
        if (it->second.src_dims == 0)
        {
            boost::any buffer_any = it->second.datasetter->get();
            Tango::AttributeInfoEx& info = it->second.attr_info;

            Cleaner< Tango::DevShort   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevLong    >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevDouble  >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevFloat   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevBoolean >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevUShort  >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevULong   >().visit( buffer_any, info.data_type );
            Cleaner< Tango::DevUChar   >().visit( buffer_any, info.data_type );
            //Cleaner< Tango::DevLong64  >().visit( buffer_any, info.data_type );
            //Cleaner< Tango::DevULong64 >().visit( buffer_any, info.data_type );
        }
    }
}

// ============================================================================
// DataCollector::on_begin_of_dim
// ============================================================================
void DataCollector::on_begin_of_dim( void )
{
    if (config_.hw_continuous)
    {
        tm::time_duration from_start = tm::microsec_clock::universal_time() - this->init_scan_timestamp_;
        double first_timestamp = from_start.total_seconds() + 1E-3 * (from_start.total_milliseconds() - 1000 * from_start.total_seconds());
        double integration_time = config_.integration_times[0];

        boost::any sensors_timestamps_any = actuator_data_["sensorstimestamps"].datasetter->get();
        ScanArray<double>::ImgP& sensors_timestamps   = boost::any_cast<ScanArray<double>::ImgP&>(sensors_timestamps_any);

        boost::any actuators_timestamps_any = actuator_data_["actuatorstimestamps"].datasetter->get();
        ScanArray<double>::ImgP& actuators_timestamps   = boost::any_cast<ScanArray<double>::ImgP&>(actuators_timestamps_any);

        size_t width  = sensors_timestamps->shape()[1];
        for (size_t i = 0; i < width; i++)
        {
            (*sensors_timestamps)  [dim2_pt_][i] = first_timestamp + i * integration_time;
            (*actuators_timestamps)[dim2_pt_][i] = first_timestamp + i * integration_time;
        }
    }
}

// ============================================================================
// DataCollector::on_set_point_nb
// ============================================================================
void DataCollector::on_set_point_nb( yat::Message& msg )
{
    std::pair< size_t, size_t >* data = 0;
    msg.detach_data( data );
    size_t dim1_pt = data->first;
    size_t dim2_pt = data->second;
    delete data;

    if (config_.on_the_fly && dim1_pt >= capacity_)
    {
        //- must grow
        const size_t new_capacity = capacity_ + CONTINUOUS_CAPACITY_GRANULARITY;

        for( std::map<std::string, DataInfo>::iterator it = actuator_data_.begin(); it != actuator_data_.end(); ++it )
        {
            boost::any buffer_any = it->second.datasetter->get();
            Tango::AttributeInfoEx& info = it->second.attr_info;

            if ((*it).second.scan_dim == 1 && info.data_format == Tango::SCALAR)
            {
                Resizer< Tango::DevShort   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevLong    >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevDouble  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevFloat   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevBoolean >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevUShort  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevULong   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevUChar   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevLong64  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevULong64 >().visit( (*it).second.datasetter, info.data_type, new_capacity );
            }
        }
        for( std::map<std::string, DataInfo>::iterator it = sensor_data_.begin(); it != sensor_data_.end(); ++it )
        {
            boost::any buffer_any = it->second.datasetter->get();
            Tango::AttributeInfoEx& info = it->second.attr_info;

            if( it->second.scan_dim == 1 && info.data_format == Tango::SCALAR )
            {
                Resizer< Tango::DevShort   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevLong    >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevDouble  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevFloat   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevBoolean >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevUShort  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevULong   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevUChar   >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevLong64  >().visit( (*it).second.datasetter, info.data_type, new_capacity );
                Resizer< Tango::DevULong64 >().visit( (*it).second.datasetter, info.data_type, new_capacity );
            }
        }
        capacity_ = new_capacity;
    }
    dim1_pt_ = dim1_pt;
    dim2_pt_ = dim2_pt;
}

// ============================================================================
// DataCollector::on_save_prior_actuator_data
// ============================================================================
void DataCollector::on_save_prior_actuator_data( yat::Message& msg )
{
    std::vector<double>* data = 0;
    msg.detach_data( data );
    boost::scoped_ptr< std::vector<double> > guard( data );
    *(buffers_.actuators_prior_pos) = *data;
}

// ============================================================================
// DataCollector::on_save_actuator_data
// ============================================================================
void DataCollector::on_save_actuator_data( yat::Message& msg )
{
    AttrValue* data = 0;
    msg.detach_data( data );
    boost::scoped_ptr<AttrValue> guard(data);

    std::map<std::string, DataInfo>::iterator it;
    it = actuator_data_.find( boost::to_lower_copy(data->name) );
    if ( it == actuator_data_.end() )
    {
        yat::OSStream oss;
        oss << " The actuator " << data->name << " is not registered";
        THROW_YAT_ERROR("ACTUATOR_ERROR", oss.str(), "DataCollector::on_save_actuator_data");
    }
    DataInfo& di = (*it).second;
    std::vector< boost::multi_array_types::index > where(2);
    if (di.scan_dim == 1)
    {
        where[0] = dim2_pt_;
        where[1] = ( !config_.zig_zag || dim2_pt_ % 2 == 0 )
                ? dim1_pt_
                : capacity_ - dim1_pt_ - 1;
    }
    else
    {
        where[0] = 0;
        where[1] = dim2_pt_;
    }

    ValueWriter< Tango::DevShort   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevLong    >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevDouble  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevFloat   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevBoolean >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevUShort  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevULong   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    ValueWriter< Tango::DevUChar   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    //ValueWriter< Tango::DevLong64  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
    //ValueWriter< Tango::DevULong64 >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
}

// ============================================================================
// DataCollector::on_save_sensor_data
// ============================================================================
void DataCollector::on_save_sensor_data( yat::Message& msg )
{
    AttrValue* data = 0;
    msg.detach_data( data );
    boost::scoped_ptr<AttrValue> guard(data);

    std::map<std::string, DataInfo>::iterator it;
    it = sensor_data_.find( data->name );
    if ( it == sensor_data_.end() )
        it = actuator_data_.find( data->name );

    if ( it != sensor_data_.end() && it != actuator_data_.end() )
    {
        DataInfo& di = (*it).second;

        //# BEGIN PROBLEM-1454
        di.no_data = data->no_value;
        if( !di.no_data )
        //# END PROBLEM-1454
        {
            if ( di.src_dims == 0 )
            {
                // here we will write the scalar value inside the buffer (scan1d) or image (scan2d)

                std::vector< boost::multi_array_types::index > where(2);
                if (di.scan_dim == 1)
                {
                    where[0] = dim2_pt_;
                    where[1] = ( !config_.zig_zag || dim2_pt_ % 2 == 0 )
                            ? dim1_pt_
                            : capacity_ - dim1_pt_ - 1;
                }
                else
                {
                    where[0] = 0;
                    where[1] = dim2_pt_;
                }

                SCAN_DEBUG << "[ " << dim1_pt_ + 1 << " , " << dim2_pt_ + 1 << " ] Saving value of " << data->name << ENDLOG;

                ValueWriter< Tango::DevShort   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevLong    >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevDouble  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevFloat   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevBoolean >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevUShort  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevULong   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevUChar   >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevLong64  >().visit( di.datasetter, di.attr_info.data_type, data->value, where );
                ValueWriter< Tango::DevULong64 >().visit( di.datasetter, di.attr_info.data_type, data->value, where );

            }
            else
            {
                SCAN_DEBUG << "[ " << dim1_pt_ + 1 << " , " << dim2_pt_ + 1 << " ] Saving value of " << data->name << ENDLOG;

                // we only put the last step data (spectrum or image)
                di.datasetter->set( data->value );

                if (di.src_dims == 1)
                {
                    std::vector<size_t> shape(2);
                    if      (GetShape<Tango::DevShort  >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevLong   >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevDouble >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevFloat  >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevBoolean>()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevUShort >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevULong  >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevUChar  >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevLong64 >()( di.attr_info.data_type, data->value, shape )) {}
                    else if (GetShape<Tango::DevULong64>()( di.attr_info.data_type, data->value, shape )) {}

                    di.datasetter->set_width( shape[1] );
                }
            }
        }
    }
}

// ============================================================================
// DataCollector::on_end_of_step
// ============================================================================
void DataCollector::on_end_of_step( void )
{
    //- make the data available in the buffers
    buffers_.pt_nb  = dim1_pt_ + 1;
    buffers_.pt_nb2 = dim2_pt_ + 1;

    for( std::map<std::string, DataInfo>::iterator it = actuator_data_.begin(); it != actuator_data_.end(); ++it )
    {
        DataInfo& di = (*it).second;
        if (di.display_dims == 1 && di.src_dims == 0)
        {
            if (di.scan_dim == 1)
                di.datasetter->set_width(buffers_.pt_nb);
            else
                di.datasetter->set_width(buffers_.pt_nb2);
        }
    }
    for( std::map<std::string, DataInfo>::iterator it = sensor_data_.begin(); it != sensor_data_.end(); ++it )
    {
        DataInfo& di = (*it).second;
        if (di.display_dims == 1 && di.src_dims == 0)
        {
            if (di.scan_dim == 1)
                di.datasetter->set_width(buffers_.pt_nb);
            else
                di.datasetter->set_width(buffers_.pt_nb2);
        }
    }
#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        data_recorder_->save_scan();
    }
#endif
}

// ============================================================================
// DataCollector::on_end_of_dim
// ============================================================================
void DataCollector::on_end_of_dim( void )
{
    // (*buffers_.line_pt_nb)[dim2_pt_] = dim1_pt_ + 1;
}

// ============================================================================
// DataCollector::on_end_of_scan
// ============================================================================
void DataCollector::on_end_of_scan( void )
{
    // resize dataset for onthe fly scans before saving them
    // we know we are only dealing with scalar dataset here (spectrum/images are not supported and register_data throws)
    if (config_.on_the_fly && capacity_ > dim1_pt_)
    {
        const size_t final_capacity = dim1_pt_ + 1;

        for( std::map<std::string, DataInfo>::iterator it = actuator_data_.begin(); it != actuator_data_.end(); ++it )
        {
            boost::any buffer_any = (*it).second.datasetter->get();
            Tango::AttributeInfoEx& info = (*it).second.attr_info;

            if( (*it).second.scan_dim == 1 && info.data_format == Tango::SCALAR )
            {
                Resizer< Tango::DevShort   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevLong    >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevDouble  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevFloat   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevBoolean >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevUShort  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevULong   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevUChar   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevLong64  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevULong64 >().visit( (*it).second.datasetter, info.data_type, final_capacity );
            }
        }
        for( std::map<std::string, DataInfo>::iterator it = sensor_data_.begin(); it != sensor_data_.end(); ++it )
        {
            boost::any buffer_any = (*it).second.datasetter->get();
            Tango::AttributeInfoEx& info = (*it).second.attr_info;

            if ((*it).second.scan_dim == 1 && info.data_format == Tango::SCALAR)
            {
                Resizer< Tango::DevShort   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevLong    >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevDouble  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevFloat   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevBoolean >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevUShort  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevULong   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevUChar   >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevLong64  >().visit( (*it).second.datasetter, info.data_type, final_capacity );
                Resizer< Tango::DevULong64 >().visit( (*it).second.datasetter, info.data_type, final_capacity );
            }
        }
        capacity_ = final_capacity;
    }
#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        data_recorder_->end_scan();
    }
#endif
}

// ============================================================================
// DataCollector::on_end_of_run
// ============================================================================
void DataCollector::on_end_of_run( void )
{
#ifdef SCANSL_ENABLE_NEXUS
    if( config_.record_data )
    {
        data_recorder_->end_run();
    }
#endif
}


// ============================================================================
// DataCollector::check_for_error
// ============================================================================
void DataCollector::check_for_error( void )
throw(yat::Exception)
{
    yat::MutexLock guard(errors_mutex_);
    if (has_error_)
        throw last_error_;
}

// ============================================================================
// DataCollector::set_error
// ============================================================================
void DataCollector::set_error( const yat::Exception& ex )
{
    yat::MutexLock guard(errors_mutex_);
    last_error_ = ex;
    has_error_ = true;
}

// ============================================================================
// DataCollector::idle
// ============================================================================
bool DataCollector::idle()
{
    yat::MutexLock guard(msg_counter_mutex_);
    return (msg_counter_ == 0);
}

// ============================================================================
// DataCollector::increment_msgcntr
// ============================================================================
void DataCollector::increment_msgcntr( void )
{
    yat::MutexLock guard(msg_counter_mutex_);
    msg_counter_++;
}

// ============================================================================
// DataCollector::decrement_msgcntr
// ============================================================================
void DataCollector::decrement_msgcntr( void )
{
    yat::MutexLock guard(msg_counter_mutex_);
    msg_counter_--;
}


// ============================================================================
// DataCollector::get_buffers
// ============================================================================
ScanBuffers DataCollector::get_buffers( void )
{
    yat::MutexLock guard(buffers_mutex_);
    return buffers_;
}

// ============================================================================
// DataCollector::set_scan_data
// ============================================================================
void DataCollector::set_scan_data(ScanData* data_p)
{
    scan_data_p_ = data_p;
}


}

