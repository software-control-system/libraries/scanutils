/*!
 * \file
 * \brief    Declaration of SIS3820Sensor class
 * \author   Teresa Nunez - Hasylab/DESY
 */
#ifndef _SCANSERVER_SIS3820_SENSOR_H_
#define _SCANSERVER_SIS3820_SENSOR_H_

#include <scansl/interfaces/ISensor.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/DevProxies.h>
#include <scansl/plugin/Sensor.h>

namespace ScanUtils
{
//  namespace Sensor
//  {

class SIS3820SensorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const;

    virtual std::string get_interface_name(void) const;

    virtual std::string get_version_number(void) const;

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const;
};


class SIS3820Sensor : public Sensor
{
public: //! structors

    SIS3820Sensor();

    ~SIS3820Sensor();

public: //! ISensor implementation

    virtual void init( std::string device_name, bool sync );

    virtual void before_integration( void );

private: //! private implementation

    //! configuration
    ProxyP proxy_;
    bool must_arm_;
};
//  }
}

#endif
