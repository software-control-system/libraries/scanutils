/*!
 * \file
 * \brief    Definition of DiffractometerActuator class
 * \author   Julien Malik - Synchrotron SOLEIL
 */

#include <scansl/Util.h>
#include <scansl/interfaces/IPolledActuator.h>
#include <scansl/interfaces/IScanPlugInInfo.h>
#include <scansl/plugin/WaitingStateChangeActuator.h>
#include <yat/plugin/PlugInSymbols.h>

namespace ScanUtils
{
class DiffractometerActuatorInfo : public IScanPlugInInfo
{
public: //! IPlugInInfo implementation

    virtual std::string get_plugin_id(void) const
    {
        return "DiffractometerActuator";
    }

    virtual std::string get_interface_name(void) const
    {
        return IPolledActuatorInterfaceName;
    }

    virtual std::string get_version_number(void) const
    {
        return "1.0.0";
    }

public: //! IScanPlugInInfo implementation

    virtual void supported_classes ( std::vector<std::string>& list ) const
    {
        list.push_back("DiffractometerDevice");
    }
};

class DiffractometerActuator : public WaitingStateChangeActuator
{
public:
    DiffractometerActuator()
    {
        config_.move_state        = Tango::MOVING;
        config_.error_states.push_back(Tango::FAULT);
        config_.error_states.push_back(Tango::UNKNOWN);
        config_.polling_period_ms = 50;
        config_.abort_cmd_name    = "Abort";
    }
};
}

EXPORT_SINGLECLASS_PLUGIN(ScanUtils::DiffractometerActuator, ScanUtils::DiffractometerActuatorInfo);
