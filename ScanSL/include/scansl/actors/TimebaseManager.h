#ifndef _SCANSERVER_TIMEBASE_MANAGER_H
#define _SCANSERVER_TIMEBASE_MANAGER_H

#include <scansl/ScanConfig.h>
#include <scansl/Factory.h>
#include <scansl/AttrValue.h>
#include <scansl/interfaces/IPolledTimebase.h>
#include <scansl/actors/ITimebase.h>
#include <scansl/actors/SoftTimebase.h>
#include <boost/noncopyable.hpp>

namespace ScanUtils
{
class SCANSL_DECL TFactory : public Factory
{
public:
    TFactory();

    ~TFactory();

    ITimebasePtr create_hard_timebase( std::string name ) const;

    ITimebasePtr create_soft_timebase( void ) const;
};

class SCANSL_DECL TimebaseManager : private boost::noncopyable
{
public: //! structors

    TimebaseManager( const ScanConfig& config );

    ~TimebaseManager( );

public: //! modifers

    //! register
    void register_hard_timebases ( const std::vector<std::string>& device_names );

    //! register
    void register_soft_timebase ( void );

    //! generic register (initialize the timebase & call before_run)
    void register_timebase(const std::string& name, ITimebasePtr timebase);

    //! start all the timebases
    void start( double integration_time )
    throw ( yat::Exception );

    //! abort all timebases
    void abort( void )
    throw ( yat::Exception );

    //! true if timebase manager is in idle state
    bool finished_counting( void );

    //! throws if any error occurred, does nothing otherwise
    void error_check( void );

    //! call after_run on all timebases registered
    void after_run( void );

    //! reset the time base done flag collection
    void reset_done_flags() { done_timebases_.clear(); }

private:
    const ScanConfig& config_;

    //! the factory used to create the ITimebase implementation
    TFactory factory_;

    //! the timebase objects list (hardware + software)
//    std::map<std::string, ITimebasePtr> timebases_;
    std::vector<ITimebasePtr> timebases_;
    std::vector<std::string> timebases_names_;
    std::vector<bool> is_timebases_ni6602_;

    std::set<std::string> done_timebases_;

};
typedef boost::shared_ptr<TimebaseManager> TimebaseManagerPtr;
}

#endif
