#ifndef _SCANSERVER_SENSOR_MANAGER_H
#define _SCANSERVER_SENSOR_MANAGER_H

#include <scansl/ScanSL.h>
#include <scansl/interfaces/ISensor.h>
#include <scansl/ScanConfig.h>
#include <scansl/Factory.h>
#include <scansl/AttrValue.h>
#include <scansl/actors/ContinuousTimebaseManager.h>
#include <map>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

namespace ScanUtils
{
class SCANSL_DECL SensorManager : public Tango::CallBack, private boost::noncopyable
{
public: //! structors

    SensorManager( const ScanConfig& config, ContinuousTimebaseManagerPtr cont_tb_manager  );

    virtual ~SensorManager( );

public: //! modifiers
    
    //! register_sensors
    void register_sensors ( const std::vector<std::string>& attr_names );

    //! get sensors full attributes names
    std::set<std::string> get_sensors_names();

    //! get number of sensors
    size_t get_sensors_nb ( void );

    //! before_integration
    void before_integration( double integration_time );

    //! after_integration
    void after_integration( void );

    //! after_run
    void after_run( void );

    //! abort
    void abort( void );
    
    //! read
    void read();

    //! wait end of reading
    AttrValues wait_end( size_t timeout_ms );

    //! get errors stack
    const std::vector<yat::Error>& get_errors() { return state_.errors_; }

    //! get all errors stack
    const std::vector<yat::Error>& get_all_errors() { return state_.all_errors_; }

    void clear_error_flag() { state_.error_flag_ = false; }

    // prevent the sensor manager to read again all device sensors that failed
    void disable_failed_sensors(int x_pos, int y_pos);

    // clear error stack
    void clear_errors();

public: //! Tango::CallBack implementation

    void attr_read( Tango::AttrReadEvent* attr_written_data );

private:
    bool read_request_completed( void );

    const Tango::AttributeInfoEx& get_attr_info( std::string device_name, std::string attr_name ) const;

    //! read
    void read_by_attr();

    //! called by attr_read() if config_.read_attribute_async is true
    void single_attr_read( Tango::AttrReadEvent* attr_read_data );

private: //! impl

    const ScanConfig& config_;

    //! the factory which creates the SensorPtr
    GenericFactory<ISensor,   GenericSensor> factory_;

    //! a sensor subgroup associates a SensorPtr managing a single device,
    //! the list of concerned attributes for this device, and a Proxy to the device
    struct SubGroup
    {
        std::string              device_name;
        ISensorPtr               sensor_object;
        std::vector<std::string> attributes;
        std::vector<Tango::AttributeInfoEx> attributes_info;
        ProxyP                   proxy;
        bool                     enable; // default is true, but switch to false in case of read error
        bool                     data_read;
        std::map<std::string, bool> attributes_read;
        std::map<std::string, bool> attributes_enable;
    };

    //! the list of subgroup
    std::vector<SubGroup> sensors_group_;
    
    // set the data_read flag for the device
    void mark_as_read(const std::string& device_name);
    void mark_attr_as_read(const std::string& device_name, const std::string& attr_name);

    struct State
    {
        State() : pending_request_(false) {};

        //! a mutex to protect 'pending_sensors_request_'
        yat::Mutex mutex_;

        //! currently waiting to get the response of read_attributes_asynch
        bool pending_request_;

        //! the set of pending sensors to be read
        std::set<std::string> sensors_to_read_;
        AttrValues values_;

        std::vector<yat::Error> errors_;

        std::vector<yat::Error> all_errors_;

        bool error_flag_;

    } state_;



};
typedef boost::shared_ptr<SensorManager> SensorManagerPtr;
}

#endif
