#ifndef _SCANSERVER_STATE_H
#define _SCANSERVER_STATE_H

#include <scansl/ScanSL.h>
#include <scansl/Singleton.h>
#include <yat4tango/CommonHeader.h>

namespace ScanUtils
{

class SCANSL_DECL ScanState
{
protected:
    ScanState();
    virtual ~ScanState();

public:

    virtual Tango::DevState get_tango_state( void ) = 0;
    
    virtual std::string get_tango_status( void ) = 0;

    virtual ScanState& start( void );

    virtual ScanState& pause( bool on_context_error );

    virtual ScanState& resume( void );

    virtual ScanState& abort_on_successfullscan( void );

    virtual ScanState& abort_by_user( void );

    virtual ScanState& abort_on_error( yat::Exception& ex );

    virtual ScanState& end_of_point( bool manual );

    virtual ScanState& end_of_scan( void );
    
    virtual ScanState& end_of_run( void );

    virtual bool is_next_action_allowed( void );

};


class State_RUNNING : public ScanState, public Singleton<State_RUNNING>
{
public:
    virtual Tango::DevState get_tango_state( void );
    
    virtual std::string get_tango_status( void );

    virtual ScanState& pause( bool on_context_error );

    virtual ScanState& abort_on_successfullscan( void );

    virtual ScanState& abort_by_user( void );

    virtual ScanState& abort_on_error( yat::Exception& ex );

    virtual ScanState& end_of_point( bool manual );
    
    virtual ScanState& end_of_run( void );

    virtual bool is_next_action_allowed( void );
};




// base class for all the STANDBY states
class State_STANDBY : public ScanState
{
public:
    virtual Tango::DevState get_tango_state( void );
    
    virtual ScanState& resume( void );
    
    virtual ScanState& abort_on_successfullscan( void );

    virtual ScanState& abort_by_user( void );

    virtual ScanState& abort_on_error( yat::Exception& ex );
};


class State_PAUSED_UNDEFINITELY : public State_STANDBY, public Singleton<State_PAUSED_UNDEFINITELY>
{
    virtual std::string get_tango_status( void );
};

class State_PAUSED_ON_CONTEXT : public State_STANDBY, public Singleton<State_PAUSED_ON_CONTEXT>
{
    virtual std::string get_tango_status( void );
};




// base class for states where it is allowed to start a new scan
// start() returns State_RUNNING
class State_READYFORSCAN : public ScanState
{
public:
    virtual ScanState& start( void );
};

// either no scan have been started (after device init), either last scan was successfull
class State_ON : public State_READYFORSCAN, public Singleton<State_ON>
{
public:
    virtual Tango::DevState get_tango_state( void );
    
    virtual std::string get_tango_status( void );
};

// last scan has been aborted by user
class State_ABORTED_BY_USER : public State_READYFORSCAN, public Singleton<State_ABORTED_BY_USER>
{
public:
    virtual Tango::DevState get_tango_state( void );
    
    virtual std::string get_tango_status( void );
};

// a fatal error occurred during the last scan. it stopped automatically
class State_ABORTED_ON_ERROR : public State_READYFORSCAN, public Singleton<State_ABORTED_ON_ERROR>
{
public:
    virtual Tango::DevState get_tango_state( void );
    
    virtual std::string get_tango_status( void );


public: // specific methods of this class
    void set_error ( yat::Exception& ex );

private:
    yat::Exception last_error_;
};


}

#endif
